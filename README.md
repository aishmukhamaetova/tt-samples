# Development

```
npx bolt

#separate console
npx bolt watch

#separate console
npx bolt dev
```

# Staging

be sure you are there www-data@ip-10-216-9-208

```
npx bolt stage:stop
git pull
npx bolt
npx preconstruct build
npx bolt stage

#admin on port 5000
#app on port 4000
#backend on port 3000
```

# Install Bolt Manager

```
yarn global add bolt
bolt installs
# read about bolt https://github.com/boltpkg/bolt
```

## Bolt Add new packages

```
bolt w @tt/app add react-router-dom
```

## Start DB

```
bolt w @tt/backend start
https://github.com/Arattian/DynamoDb-GUI-Client GUI
#it will up on localhost:8000
npm run electron:serve

read
cat .dbconnect

#test staging DB
npx pm2 start ecosystem.config.js --only tt/backend --env staging

```

# Server Logs

```
npx pm2 logs 0

```

# Debug Bolt

```
npx bolt w @tt/admin start
```

# View latest commit

```
git for-each-ref --sort=committerdate refs/heads/ --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(color:red)%(objectname:short)%(color:reset) - %(contents:subject) - %(authorname) (%(color:green)%(committerdate:relative)%(color:reset))'
```

# Development

```
npx bolt
npx bolt watch
npx bolt dev
```

# Staging

be sure you are there www-data@ip-10-216-9-208

```
npx yarn stage:stop
git pull
npx bolt
npx preconstruct build
npx bolt stage

#admin on port 5000
#app on port 4000
#backend on port 3000
```

# Prod Up Mobile APP

```
npx bolt prodmobile:stop
npx bolt
npx preconstruct build
npx bolt build:app
npx pm2 start pm2-mobile.config.js --env production


```

# Prod Up Admin APP

```
npx bolt prodadmin:stop
npx bolt
npx preconstruct build
npx bolt build:admin
npx pm2 start pm2-admin.config.js --env production

```

# Install Bolt Manager

```
yarn global add bolt
bolt installs
# read about bolt https://github.com/boltpkg/bolt
```

## Bolt Add new packages

```
bolt w @tt/app add react-router-dom
```

## Start DB

```
bolt w @tt/backend start
https://github.com/Arattian/DynamoDb-GUI-Client GUI
#it will up on localhost:8000
npm run electron:serve

read
cat .dbconnect

```

# Server Logs

```
npx pm2 logs 0

```

# Debug Bolt

```
npx bolt w @tt/admin start
```

# View latest commit

```
git for-each-ref --sort=committerdate refs/heads/ --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(color:red)%(objectname:short)%(color:reset) - %(contents:subject) - %(authorname) (%(color:green)%(committerdate:relative)%(color:reset))'
```

# AWS CLI install

```
https://docs.aws.amazon.com/cli/latest/userguide/install-macos.html#awscli-install-osx-path
```

# Staging APN for VPC

https://docs.aws.amazon.com/AmazonS3/latest/dev/creating-access-points.html#access-points-policies

```
#set
aws s3control create-access-point --name staging-vpc-ap --account-id 222069511913 --bucket team-tasker--activity-videos2 --vpc-configuration VpcId=vpc-02afbb59ad1bc3402

#get
aws s3control get-access-point --name staging-vpc-ap --account-id 222069511913

VPC endpoints allow traffic to flow from your VPC to Amazon S3
https://awspolicygen.s3.us-east-1.amazonaws.com/policygen.html generaotor enpoint policy

Managin panel for VPC enpoints policy:
https://ap-southeast-2.console.aws.amazon.com/vpc/home?region=ap-southeast-2#CreateVpcEndpoint:

Edti VPC policy
https://ap-southeast-2.console.aws.amazon.com/vpc/home?region=ap-southeast-2#Endpoints:sort=vpcEndpointId

```
