FROM node:alpine

WORKDIR /usr/src/app

COPY . .

RUN yarn global add bolt
RUN bolt
RUN cp ./backend/env ./backend/.env

EXPOSE 3000
EXPOSE 4000

ENTRYPOINT ["npx", "pm2", "--no-daemon", "start", "pm2-docker.config.js", "--env", "production"]