import commonjs from 'rollup-plugin-commonjs'
import resolve from 'rollup-plugin-node-resolve'
import {uglify} from 'rollup-plugin-uglify'
import json from 'rollup-plugin-json'

const config = {
  external: [
    `aws-sdk`,
    `bcryptjs`,
    `express`,
    `debug`,
    `depd`,
    `dynamoose`,
    `dynalite`,
    `react`,
    `react-dom`,
    `uuid`,
  ],

  // globals: {
  //   'aws-sdk': 'AWS',
  // },

  input: `src/server.js`,
  output: {
    file: `../build/index.js`,
    format: `cjs`,
    name: `application`,
  },
  plugins: [
    json(),
    commonjs({
      // include: 'node_modules/**',
      exclude: [
        'node_modules/@tt/backend/**',
        'node_modules/@tt/db/**',
        'node_modules/@tt/express/**',
      ],
      ignore: ['conditional-runtime-dependency'],
      ignoreGlobal: true,
      // namedExports: {
      //   bcryptjs: [`genSaltSync`, `hashSync`, `compareSync`],
      //   'node_modules/inferno-redux/index.js': ['Provider'],
      //   'node_modules/aws-sdk/global.js': ['util'],
      // },
    }),
    resolve({
      mainFields: ['module', 'main'],
      preferBuiltins: true,
    }),
  ],
}

if (process.env.NODE_ENV === `production`) {
  config.plugins.push(uglify())
}

export default config
