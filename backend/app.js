const {start} = require(`@tt/db`)

const createError = require('http-errors')
const cors = require(`cors`)
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const auth = require('./auth')
const session = require('express-session')
const genuuid = require('./genuuid')
// var indexRouter = require('./routes/index')
const userRouter = require('./routes/user')
const activityRouter = require(`./routes/activity`)
const applicationRouter = require(`./routes/application`)
const audioRouter = require('./routes/audio')
const hiringManager = require(`./routes/hiring-manger`)
const videoRouter = require('./routes/video')

const FileStore = require('session-file-store')(session)

const PUBLIC_DIR = path.join(__dirname, `public`)
const INDEX_HTML = path.join(PUBLIC_DIR, `index.html`)

const fileStoreOptions = {}

async function run() {
  await start()

  var app = express()

  app.use(
    cors({
      credentials: true
    })
  )
  app.use(express.json())

  // rawBody experiment
  // app.use(
  //   express.json({
  //     verify: (req, res, buf, encoding) => {
  //       req.rawBody = buf.toString()
  //       console.log('rawBody:::', req.rawBody)
  //     }
  //   })
  // )

  app.use(express.urlencoded({extended: false}))
  app.use(cookieParser())

  // app.use(express.bodyParser())
  // app.use(express.static(PUBLIC_DIR))

  // https://github.com/expressjs/session
  // session middleware
  app.set('trust proxy', 1)
  app.use(
    session({
      // genid: function (req) {
      //   return genuuid() // use UUIDs for session IDs
      // },
      secret: 'application-session',
      resave: true,
      saveUninitialized: true,
      // cookie: {
      //   maxAge: 36000,
      //   httpOnly: false,
      //   secure: false // for normal http connection if https is there we have to set it to true
      // },
      unset: 'keep',
      // secure: true,
      store: new FileStore(fileStoreOptions)
      // store: new DynamoDBStore(options) add https://www.npmjs.com/package/connect-dynamodb
    })
  )

  // passport-saml configuratoin
  app.use(auth.initialize())
  app.use(auth.session())

  app.get(
    '/saml/login',
    auth.authenticate('saml', {failureRedirect: '/', failureFlash: true}),
    function (req, res) {
      res.redirect('/')
    }
  )

  app.post(
    '/saml/login',
    auth.authenticate('saml', {failureRedirect: '/', failureFlash: true}),
    function (req, res) {
      const {session, user} = res.req
      console.log('post /saml/login')
      req.session.save(function (err) {
        console.log(`session saved`)
        res.redirect(`/`)
      })
    }
  )

  // app.post('/auth/saml/logout/callback', auth.logoutSaml)

  app.get('/saml/logout', auth.logoutSaml)

  app.use(`/api/v1/activity`, activityRouter)
  app.use(`/api/v1/application`, applicationRouter)
  app.use(`/api/v1/hiring-manager`, hiringManager)
  app.use('/api/v1/user', userRouter)
  app.use(`/api/v1/audio`, audioRouter)
  app.use(`/api/v1/video`, videoRouter)

  // app.get('*', (req, res) => {
  //   res.sendFile(INDEX_HTML)
  // })

  // catch 404 and forward to error handler
  app.use(function (req, res, next) {
    next(createError(404))
  })

  // error handler
  app.use(function (err, req, res) {
    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    // render the error page
    res.status(err.status || 500)
    res.json('error')
  })

  return app
}

module.exports = run
