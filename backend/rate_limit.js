const rateLimit = require('express-rate-limit')

// 10 requests in 5 minutes per IP
const unauthorizedRateLimit = rateLimit({
  windowMs: 5 * 60 * 1000,
  max: 100
})
const authorizedRateLimit = rateLimit({
  windowMs: 5 * 60 * 1000,
  max: 500
})

exports.unauthorizedRateLimit = unauthorizedRateLimit
exports.authorizedRateLimit = authorizedRateLimit
