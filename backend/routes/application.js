var express = require('express')
var router = express.Router()
var rateLimits = require('../rate_limit')
var auth = require('../auth')

const {application, userCheckExpire} = require(`@tt/express`)
const {authorizedRateLimit, unauthorizedRateLimit} = rateLimits

//mobile routes
router.post(`/`, userCheckExpire, application.create) //application submit with audio/video
router.post(
  `/queryOwn`,
  userCheckExpire,
  authorizedRateLimit,
  application.queryOwn
)
router.post(
  `/withdraw`,
  userCheckExpire,
  authorizedRateLimit,
  application.update
)

//admin routes
router.post(`/update-admin`, auth.protected, application.update)

module.exports = router
