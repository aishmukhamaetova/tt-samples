var express = require('express')
var router = express.Router()
var auth = require('../auth')

const {hiringManager} = require(`@tt/express`)

//admin routes
router.post(`/seed`, auth.protected, hiringManager.seed)
router.post(`/query`, auth.protected, hiringManager.query)

module.exports = router
