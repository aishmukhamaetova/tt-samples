var express = require('express')
var router = express.Router()

const {audio} = require(`@tt/express`)
const {userCheckExpire} = require(`@tt/express`)

//mobile routes
router.post(`/link`, userCheckExpire, audio.link)

module.exports = router
