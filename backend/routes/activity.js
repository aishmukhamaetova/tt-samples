var express = require('express')
var router = express.Router()
var auth = require('../auth')
var rateLimits = require('../rate_limit')

const {activity} = require(`@tt/express`)
const {userCheckExpire} = require(`@tt/express`)
const {authorizedRateLimit, unauthorizedRateLimit} = rateLimits

//mobile routes
router.get(`/query`, unauthorizedRateLimit, activity.query) //public
router.post(
  `/query-favourites`,
  userCheckExpire,
  authorizedRateLimit,
  activity.queryFav
)
router.post(
  `/one-detail`,
  userCheckExpire,
  authorizedRateLimit,
  activity.findOne
) //userCheckExpire add

//admin routes
router.post(`/query-admin`, auth.protected, activity.queryAdmin)
router.post(`/update`, auth.protected, activity.update)
router.post(`/create`, auth.protected, activity.create)
router.post(`/seed`, auth.protected, activity.seed)
router.post(`/query-video-link`, auth.protected, activity.videoLink)

module.exports = router
