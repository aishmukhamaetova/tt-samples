var express = require('express')
var router = express.Router()

const {video} = require(`@tt/express`)
const {userCheckExpire} = require(`@tt/express`)

//mobile routes
router.post(`/link`, userCheckExpire, video.link)

module.exports = router
