var express = require('express')
var rateLimits = require('../rate_limit')
var router = express.Router()
var auth = require('../auth')

const {userSignup} = require(`@tt/express`)
const {userSignupVerify} = require(`@tt/express`)
const {userSignupResend} = require(`@tt/express`)
const {userLogin} = require(`@tt/express`)
const {userRemove} = require(`@tt/express`)
const {userLogout} = require(`@tt/express`)
const {userUpdate} = require(`@tt/express`)
const {userUpdateNote} = require(`@tt/express`)
const {userUpdateByCode} = require(`@tt/express`)
const {userCheckPasswordCode} = require(`@tt/express`)
const {userForgot} = require(`@tt/express`)
const {userSendFeedback} = require(`@tt/express`)
const {userQueryAdmin} = require(`@tt/express`)
const {userQueryOkta} = require(`@tt/express`)
const {userSession} = require(`@tt/express`)
const {userCheckExpire} = require(`@tt/express`)

const {authorizedRateLimit, unauthorizedRateLimit} = rateLimits

//mobile routes
router.post(`/login`, unauthorizedRateLimit, userLogin)
router.post(`/remove`, userCheckExpire, authorizedRateLimit, userRemove)
router.post(`/forgot`, unauthorizedRateLimit, userForgot)
router.post(
  `/sendfeedback`,
  userCheckExpire,
  authorizedRateLimit,
  userSendFeedback
)
router.post(`/signup`, unauthorizedRateLimit, userSignup)
router.get(`/signup/verify/:code`, unauthorizedRateLimit, userSignupVerify)
router.post(`/signup/resend`, unauthorizedRateLimit, userSignupResend)
router.post(`/logout`, authorizedRateLimit, userLogout) // userCheckExpire
router.post(`/update`, userCheckExpire, authorizedRateLimit, userUpdate)
router.post(`/session`, userCheckExpire, authorizedRateLimit, userSession)
router.post(`/updateByCode`, unauthorizedRateLimit, userUpdateByCode)
router.post(`/checkPasswordCode`, unauthorizedRateLimit, userCheckPasswordCode)

//admin routes
router.post(`/query-admin`, auth.protected, userQueryAdmin)
router.post(`/updatenote`, auth.protected, userUpdateNote)
router.post(`/okta`, auth.protected, userQueryOkta)

module.exports = router
