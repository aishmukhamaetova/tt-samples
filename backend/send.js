require('dotenv').config()

const AWS = require('aws-sdk')

const ses = new AWS.SES({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
  region: process.env.AWS_REGION,
  apiVersion: '2010-12-01',
})

const params = {
  Destination: {
    ToAddresses: [`miukki@gmail.com`], // Email address/addresses that you want to send your email
  },
  ConfigurationSetName: `team-tasker-set`,
  Message: {
    Body: {
      Html: {
        Charset: 'UTF-8',
        Data:
          "<html><body><h1>Hello  Charith</h1><p style='color:red'>Sample description</p> <p>Time 1517831318946</p></body></html>",
      },
      Text: {
        Charset: 'UTF-8',
        Data: 'Hello Charith Sample description time 1517831318946',
      },
    },
    Subject: {
      Charset: 'UTF-8',
      Data: 'Test email',
    },
  },
  Source: process.env.EMAILFROM || `no-reply.teamtasker@ozminerals.com`,
}

const sendEmail = ses.sendEmail(params).promise()

sendEmail
  .then(data => {
    console.log('email submitted to SES', data)
  })
  .catch(error => {
    console.log(error)
  })
