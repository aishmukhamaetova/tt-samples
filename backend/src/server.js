require('dotenv').config()

var run = require('../app')
var debug = require('debug')('@tt:backend')
var http = require('http')

const PORT = process.env.PORT || 3000

main()

var server

async function main() {
  console.log(`Team Tasker (Backend) v.1.0.0`)

  const app = await run()

  app.set('port', PORT)

  server = http.createServer(app)

  server.listen(PORT)
  server.on('error', onError)
  server.on('listening', onListening)
}

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error
  }

  var bind = typeof port === 'string' ? 'Pipe ' + PORT : 'Port ' + PORT

  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges')
      process.exit(1)
      break
    case 'EADDRINUSE':
      console.error(bind + ' is already in use')
      process.exit(1)
      break
    default:
      throw error
  }
}

function onListening() {
  var addr = server.address()
  var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port
  debug('Listening on ' + bind)
}
