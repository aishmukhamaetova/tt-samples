//https://github.com/mailtoharshit/OKTA-SAML-Node/blob/master/auth.js source

require('dotenv').config()
var passport = require('passport')
var SamlStrategy = require('passport-saml').Strategy
var config = require('./config.json')[process.env.SAML_ENV || 'staging']
var fs = require('fs')

console.log('SAML_ENV', process.env.SAML_ENV)
//users array to hold
let users = []
function findByEmail(emailaddress, fn) {
  for (var i = 0, len = users.length; i < len; i++) {
    var user = users[i]
    if (user.emailaddress === emailaddress) {
      return fn(null, user)
    }
  }
  return fn(null, null)
}

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.

passport.serializeUser(function (user, done) {
  console.log('passport.serializeUser calls')
  done(null, user.emailaddress)
})

passport.deserializeUser(function (id, done) {
  console.log('passport.deserializeUser', id)
  findByEmail(id, function (err, user) {
    console.log('passport.deserializeUser findByEmail', id, user)
    done(err, user)
  })
})

const Strategy = new SamlStrategy(
  {
    issuer: config.auth.issuer,
    path: '/saml/login',
    callbackUrl: '/saml/callback',
    logoutUrl: config.auth.logoutUrl,
    entryPoint: config.auth.entryPoint,
    cert: config.auth.cert
  },
  function (profile, done) {
    console.log('Succesfully Profile' + JSON.stringify(profile))
    const {nameID, nameIDFormat} = profile
    if (!profile.emailaddress) {
      return done(new Error('No emailaddress found'), null)
    }
    process.nextTick(function () {
      console.log('process.nextTick' + JSON.stringify(profile))
      findByEmail(profile.emailaddress, function (err, user) {
        if (err) {
          return done(err)
        }
        if (!user) {
          users.push(profile)
          return done(null, profile)
        }
        console.log('Ending Method for profiling')
        return done(null, user)
      })
    })
  }
)
passport.use(Strategy)

passport.protected = function (req, res, next) {
  const {sessionStore = {}, sessionID} = res.req
  const {sessions = {}} = sessionStore

  //for Memory storage
  // const passport_user = Object.keys(sessions)
  //   .map((key) => sessions[key])
  //   .find((i) => {
  //     console.log('i>>>>>', i)
  //     const {passport = {}} = JSON.parse(i)
  //     return passport.user ? true : false
  //   })

  //we use FileStorage
  // const getSessionFromFileStorage = (sessionID) => {
  //   if (!sessionID) {
  //     return
  //   }
  //   const output = JSON.parse(
  //     fs.readFileSync('./sessions/' + sessionID + '.json', 'utf8') || ''
  //   )
  //   return Object.prototype.toString.call(output) === '[object Object]'
  //     ? output.passport
  //     : {}
  // }

  //for File Storage is ok to use req.session && req.session.passport
  const passport = req.session && req.session.passport
  const isAuthenticated = passport && passport.user ? true : false

  console.log(
    'OKTA:::',
    'isAuthenticated',
    isAuthenticated,
    'req.isAuthenticated()',
    req.isAuthenticated()
  )

  if (process.env.SAML_ENV === 'dev') {
    return next()
  }

  if (req.isAuthenticated()) {
    return next()
  }

  return res.json({
    errors: [
      {
        message: 'Okta: not authorized to access this resource/api'
      }
    ]
  })

  // res.redirect('/saml/login')
}

passport.logoutSaml = function (req, res) {
  //Here add the nameID and nameIDFormat to the user if you stored it someplace.
  const [user = {}] = users
  req.user = user
  Strategy.logout(req, function (err, request) {
    if (!err) {
      req.session.destroy(function (err) {
        console.log('cannot access session here')

        console.log('Strategy.logout() success request:', request)
        req.logout()
        res.redirect('https://ozminerals.okta.com/login/signout') //res.redirect(request)
      })
    }
  })
}

exports = module.exports = passport
