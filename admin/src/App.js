import React from 'react'
import {
  BrowserRouter,
  Redirect,
  Switch,
  Route,
  useHistory,
  useLocation
} from 'react-router-dom'

import {PageAdminActivityCreate} from '@tt/page--admin--activity-create'
// import {PageAdminHome} from '@tt/pages-admin'
import {PageAdminActivityDetail} from '@tt/page--admin--activity-detail'
import {PageAdminApplicantCommunity} from '@tt/page--admin--applicant-community'
import {PageAdminActivities} from '@tt/page--admin--activities'
import {PageAdminCommunity} from '@tt/page--admin--community'

import {createGlobalStyle} from 'styled-components'
// import {CssBaseline} from '@material-ui/core'
import {createMuiTheme} from '@material-ui/core'
import {MuiThemeProvider} from '@material-ui/core'

import {config} from '@tt/domains'

const GlobalStyle = createGlobalStyle`
  html, body {
    font-family: 'Frutiger', 'Roboto', sans-serif;
    height: 100%;
    margin: 0;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-decoration: none;
    font-style: normal;
  }
`

const Theme = createMuiTheme({
  typography: {
    useNextVariants: true,
    fontFamily: ['Frutiger', 'Roboto', 'sans-serif'].join(',')
  },
  contrastThreshold: 3,
  tonalOffset: 0.2,
  palette: {
    primary: {500: '#FEB82B'},
    error: {500: '#ff0000'},
    contrastThreshold: 3,
    tonalOffset: 0.2
  },
  fontWeightMedium: 500,
  status: {
    danger: '#ff0000'
  }
})

const isAuthenticated = () => {
  const okta = sessionStorage.getItem('okta') || ''
  return config.SAML_ENV !== `dev` && !okta ? false : true
}

const PageAdminEntry = () => {
  return <Redirect to="/activities" />
}

const PageAdminProtected = ({Component, isEdit, isCreate, isView}) => {
  // if (!isAuthenticated()) {
  //   window.location.href = '/saml/login' // https://ozminerals.okta.com/
  //   return <></>
  // }

  return <Component isCreate={isCreate} isEdit={isEdit} isView={isView} />
}

export function App() {
  return (
    <MuiThemeProvider theme={Theme}>
      <GlobalStyle />

      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            <PageAdminEntry />
          </Route>
          <Route exact path="/community">
            <PageAdminProtected Component={PageAdminCommunity} />
          </Route>
          <Route path="/activities">
            <PageAdminProtected Component={PageAdminActivities} />
          </Route>
          <Route path="/activity/create">
            <PageAdminProtected
              Component={PageAdminActivityCreate}
              isCreate={true}
            />
          </Route>
          <Route path="/activity/detail/:id">
            <PageAdminProtected Component={PageAdminActivityDetail} />
          </Route>
          <Route path="/activity/edit/:id">
            <PageAdminProtected
              Component={PageAdminActivityCreate}
              isEdit={true}
            />
          </Route>
          <Route path="/activity/view/:id">
            <PageAdminProtected
              Component={PageAdminActivityCreate}
              isView={true}
            />
          </Route>
          <Route path="/applicant/community/:id">
            <PageAdminProtected Component={PageAdminApplicantCommunity} />
          </Route>
        </Switch>
      </BrowserRouter>
    </MuiThemeProvider>
  )
}
