import {configSet} from '@tt/domains'

configSet(`API`, process.env.REACT_APP_API)
configSet(`ACTIVITY_SIZE`, process.env.REACT_APP_ACTIVITY_SIZE ?? 10)
configSet(`SAML_ENV`, process.env.REACT_APP_SAML_ENV ?? `dev`)
