const sessions = {
  '915e2fad-a4ef-47e3-92c4-553e188bf4d8':
    '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"} }',
  '915e2fad-a4ef-47e3-92c4-543e188bf4d8':
    '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"passport":{"user":"Anja.Ishmukhametova@ozminerals.com"}}'
}

console.log(
  Object.keys(sessions)
    .map((key) => sessions[key])
    .find((i) => {
      const {passport = {}} = JSON.parse(i)
      return passport.user
    })
)
