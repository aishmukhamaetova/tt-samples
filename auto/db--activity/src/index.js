import {create} from './create'
import {makeModel} from './make-model'
import {remove} from './remove'
import {query} from './query'
import {update} from './update'

export const Activity = {
  create,
  makeModel,
  remove,
  query,
  update,
}