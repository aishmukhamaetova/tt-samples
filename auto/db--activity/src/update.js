import createDebugger from 'debug'

import {db} from '@tt/db'

const who = [`@tt`, `db--activity`, `update`]

const log = createDebugger(who.join(`:`))

export async function update(payload) {
  try {
    log(payload)

    const doc = await db.Activity.update(payload.id, payload)

    return {
      result: [doc],
      errors: [],
    }
  } catch (err) {
    log(`ERROR`, err)

    return {
      result: null,
      errors: [
        {
          message: err,
        },
      ],
    }
  }
}
