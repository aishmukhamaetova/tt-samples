import createDebugger from 'debug'

import {db} from '@tt/db'

const who = [`@tt`, `db--activity`, `create`]

const log = createDebugger(who.join(`:`))

export async function create(payload) {
  try {
    log(payload)

    const doc = new db.Activity(payload)
    await doc.save({overwrite: false})

    return {
      result: [doc],
      errors: []
    }
  } catch (err) {
    log(`ERROR`, err)

    return {
      result: null,
      errors: [
        {
          message: err
        }
      ]
    }
  }
}
