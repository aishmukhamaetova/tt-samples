import createDebugger from 'debug'

import {db} from '@tt/db'

const who = [`@tt`, `db--activity`, `query`]

const log = createDebugger(who.join(`:`))

export async function query(payload) {
  try {
    log(payload)

    const result = await db.Activity.scan()
      .all()
      .exec()

    return {
      result,
      errors: [],
    }
  } catch (err) {
    log(`ERROR`, err)

    return {
      result: null,
      errors: [
        {
          message: err
        }
      ]
    }
  }
}
