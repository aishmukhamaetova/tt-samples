import dynamoose from 'dynamoose'
import uuid from 'uuid/v4'

export function makeModel() {
  const ActivitySchema = new dynamoose.Schema(
    {
      id: {
        type: String,
        index: {
          global: true,
          project: true,
        },
        required: true,
        default: uuid,
      },
      title: {
        type: String,
      },
      status: {
        type: String,
      },
      hiringManagerId: {
        type: String,
      },
      tags: {
        type: [String],
      },
      iconId: {
        type: String,
      },
      location: {
        type: [String],
      },
      duration: {
        type: String,
      },
      durationType: {
        type: String,
      },
      summary: {
        type: String,
      },
      dateStart: {
        type: Date,
      },
      dateEnd: {
        type: Date,
      },
      description: {
        type: String,
      },
      included: {
        type: [String],
      },
      aboutTeam: {
        type: String,
      },
      dateClose: {
        type: Date,
      },
      videoLink: {
        type: String,
      },
      notes: {
        type: String,
      },
    },
    {
      timestamps: true,
    },
  )

  const Activity = dynamoose.model(`Activity`, ActivitySchema)

  return {Activity, ActivitySchema}
}
