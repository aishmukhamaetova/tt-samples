import React from 'react'
import styled from 'styled-components'
import {HeaderLogo} from '@tt/component--admin--header-logo'
import {BlockAdminHeaderUser} from '@tt/block--admin--header--user'
import {BlockAdminHeaderLogout} from '@tt/block--admin--header--logout'
import {fromOkta} from '@tt/domains'
import {useStore} from 'effector-react'

const A = {}

const HeaderContainer = ({passport = {}}) => (
  <A.Section>
    <A.AreaLogo>
      <HeaderLogo />
    </A.AreaLogo>
    <A.AreaTitle>Team Tasker Admin Portal</A.AreaTitle>
    <A.AreaUser>
      <BlockAdminHeaderUser {...passport} />
    </A.AreaUser>
    <A.AreaLogout>
      <BlockAdminHeaderLogout />
    </A.AreaLogout>
  </A.Section>
)
export function SectionAdminHeader() {
  React.useEffect(function () {
    fromOkta.on.clear()
    fromOkta.run.auth()
  }, [])

  const passport = useStore(fromOkta.$.passport)

  return <HeaderContainer passport={passport} />
}

A.Section = styled.div`
  display: grid;
  grid-template-areas: '. logo title user logout .';
  grid-template-rows: 70px;
  grid-template-columns: 3% 107px 1fr 120px 80px 3%;
  background-color: #feb82b;
`

A.AreaLogo = styled.div`
  grid-area: logo;
  display: flex;
  align-items: center;
  justify-content: center;
`
A.AreaTitle = styled.div`
  grid-area: title;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 24px;
  font-weight: bold;
  letter-spacing: -0.48px;
`
A.AreaUser = styled.div`
  grid-area: user;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`
A.AreaLogout = styled.div`
  grid-area: logout;
  display: flex;
  align-items: center;
`
