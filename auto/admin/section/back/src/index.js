import React from 'react'
import styled from 'styled-components'
import {BlockAdminBack} from '@tt/block--admin--back'
import {useHistory} from 'react-router-dom'

const A = {}

export function SectionAdminBack() {
  const history = useHistory()
  const onBackClick = () => {
    history.goBack()
  }

  return (
    <A.Section>
      <A.AreaBackButton>
        <BlockAdminBack onClick={onBackClick} />
      </A.AreaBackButton>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas: '. back-button .';
  grid-template-rows: 70px;
  grid-template-columns: 3% 70px 1fr;
`

A.AreaBackButton = styled.div`
  grid-area: back-button;
  display: flex;
  align-items: center;
`
