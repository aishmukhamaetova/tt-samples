import React from 'react'
import styled from 'styled-components'
import {IconFilter} from '@tt/ui-admin'
import {fromActivity} from '@tt/domains'

const A = {}

export function BlockAdminActivitiesTableHeader(props) {
  return (
    <A.Block>
      <A.AreaTitle>
        <div onClick={() => fromActivity.on.switchSort({type: 'title'})}>
          Title
          <IconFilter />
        </div>
      </A.AreaTitle>
      <A.AreaOpen>
        <div onClick={() => fromActivity.on.switchSort({type: `createdAt`})}>
          Open Date
          <IconFilter />
        </div>
      </A.AreaOpen>
      <A.AreaClose>
        <div onClick={() => fromActivity.on.switchSort({type: `dateClose`})}>
          Close Date
          <IconFilter />
        </div>
      </A.AreaClose>
      <A.AreaManager>
        <div
          onClick={() => fromActivity.on.switchSort({type: `hiringManagerId`})}
        >
          Hiring Manager
          <IconFilter />
        </div>
      </A.AreaManager>
      <A.AreaStatus>
        <div onClick={() => fromActivity.on.switchSort({type: `status`})}>
          Activity Status
          <IconFilter />
        </div>
      </A.AreaStatus>
      <A.AreaQnty>
        <div
          onClick={() =>
            fromActivity.on.switchSort({type: `applicationsCount`})
          }
        >
          No. Applications
          <IconFilter />
        </div>
      </A.AreaQnty>
      <A.AreaExport>Export</A.AreaExport>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'title open close manager status qnty export';
  grid-template-columns: 2fr 15% 15% 15% 15% 15% 1fr;
  grid-template-rows: 50px;
`

A.AreaTitle = styled.div`
  grid-area: title;
  font-weight: bold;
  font-size: 1.1em;
  display: flex;
  align-items: center;
  cursor: pointer;
`
A.AreaOpen = styled.div`
  grid-area: open;
  font-weight: bold;
  font-size: 1.1em;
  display: flex;
  align-items: center;
  cursor: pointer;
`
A.AreaClose = styled.div`
  grid-area: close;
  font-weight: bold;
  font-size: 1.1em;
  display: flex;
  align-items: center;
  cursor: pointer;
`
A.AreaManager = styled.div`
  grid-area: manager;
  font-weight: bold;
  font-size: 1.1em;
  display: flex;
  align-items: center;
  cursor: pointer;
`
A.AreaStatus = styled.div`
  grid-area: status;
  font-weight: bold;
  font-size: 1.1em;
  display: flex;
  align-items: center;
  cursor: pointer;
`
A.AreaQnty = styled.div`
  grid-area: qnty;
  font-weight: bold;
  font-size: 1.1em;
  display: flex;
  align-items: center;
  cursor: pointer;
`
A.AreaExport = styled.div`
  grid-area: export;
  font-weight: bold;
  font-size: 1.1em;
  display: flex;
  align-items: center;
`
