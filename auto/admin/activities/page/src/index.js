import React from 'react'
import styled from 'styled-components'
import {SectionAdminHeader} from '@tt/section--admin--header'
import {SectionAdminActivitiesTabs} from '@tt/section--admin--activities--tabs'
import {SectionAdminActivitiesAdd} from '@tt/section--admin--activities--add'
import {SectionAdminActivitiesTable} from '@tt/section--admin--activities--table'
import {useHistory, useParams, useLocation} from 'react-router-dom'

const A = {}

export function PageAdminActivities() {
  return (
    <A.Page>
      <A.AreaHeader>
        <SectionAdminHeader />
      </A.AreaHeader>
      <A.AreaTabs>
        <SectionAdminActivitiesTabs indexActive={0} />
      </A.AreaTabs>
      <A.AreaAdd>
        <SectionAdminActivitiesAdd />
      </A.AreaAdd>
      <A.AreaTable>
        <SectionAdminActivitiesTable />
      </A.AreaTable>
    </A.Page>
  )
}

A.Page = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'tabs'
    'add'
    'table';
`

A.AreaHeader = styled.div`
  grid-area: header;
`
A.AreaTabs = styled.div`
  grid-area: tabs;
  padding-top: 20px;
`
A.AreaAdd = styled.div`
  grid-area: add;
  padding-top: 25px;
`
A.AreaTable = styled.div`
  grid-area: table;
`
