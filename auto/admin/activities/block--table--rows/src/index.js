import {useList} from 'effector-react'
import React from 'react'
import styled from 'styled-components'
import {BlockAdminActivitiesTableOneRow} from '@tt/block--admin--activities--table--one-row'
import {fromActivity} from '@tt/domains'
import {fromUser} from '@tt/domains'
import {useHistory} from 'react-router-dom'

const A = {}

export function BlockAdminActivitiesTableRows() {
  const history = useHistory()
  const onClick = id => history.push(`/activity/view/${id}`)

  React.useEffect(() => {
    fromActivity.run.queryAdmin()
    fromUser.run.queryAdmin()
  })

  const list = useList(fromActivity.$.list, doc => (
    <A.AreaRow>
      <BlockAdminActivitiesTableOneRow
        doc={doc}
        onClick={doc => onClick(doc.id)}
      />
    </A.AreaRow>
  ))

  return <A.Block>{list}</A.Block>
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'row';
  grid-auto-rows: auto;
`

A.AreaRow = styled.div``
