import React from 'react'
import styled from 'styled-components'
import {useHistory} from 'react-router-dom'

import {fromActivity} from '@tt/domains'
import {fromUser} from '@tt/domains'
import {useStore} from 'effector-react'

const A = {}

export function BlockAdminActivitiesTabsTabs({indexActive}) {
  const history = useHistory()
  const onClickFn = path => history.push(path)

  const countActivities = useStore(fromActivity.$.listCount)
  const userListCount = useStore(fromUser.$.count)

  return (
    <A.Block>
      <A.AreaA
        isActive={indexActive === 0}
        onClick={indexActive =>
          !(indexActive === 0) && onClickFn('/activities')
        }
      >
        Activities ({countActivities})
      </A.AreaA>
      <A.AreaB
        isActive={indexActive === 1}
        onClick={indexActive => !(indexActive === 1) && onClickFn('/community')}
      >
        Community ({userListCount})
      </A.AreaB>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'a . b .';
  grid-template-columns: auto 2% auto 1fr;
  grid-template-rows: 50px;
`

const TabMain = styled.div`
  font-weight: bold;
  font-size: 21px;
  letter-spacing: -0.42px;
  display: flex;
  align-items: center;
  border-bottom: ${props => (props.isActive ? `5px solid #feb82b` : `none`)}
  white-space: nowrap;
  cursor: ${props => (props.isActive ? `default` : `pointer`)};
`

A.AreaA = styled(TabMain)`
  grid-area: a;
`
A.AreaB = styled(TabMain)`
  grid-area: b;
`
