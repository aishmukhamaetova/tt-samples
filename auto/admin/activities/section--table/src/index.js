import React from 'react'
import styled from 'styled-components'
import {BlockAdminActivitiesTableHeader} from '@tt/block--admin--activities--table--header'
import {BlockAdminActivitiesTableRows} from '@tt/block--admin--activities--table--rows'

const A = {}

export function SectionAdminActivitiesTable() {
  return (
    <A.Section>
      <A.AreaHeader>
        <BlockAdminActivitiesTableHeader />
      </A.AreaHeader>
      <A.AreaRows>
        <BlockAdminActivitiesTableRows />
      </A.AreaRows>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas:
    '. header .'
      '. rows .'
  ;
  margin-top: 20px;
  grid-template-columns: 3% 1fr 3%;
`

A.AreaHeader = styled.div`
  grid-area: header;
  border-bottom: 1px solid #d8d8d8;
`
A.AreaRows = styled.div`
  grid-area: rows;
`
