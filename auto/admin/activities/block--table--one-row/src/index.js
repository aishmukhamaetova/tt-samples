import React from 'react'
import styled from 'styled-components'
import {useHistory} from 'react-router-dom'
import {BlockAdminActivityDetailShortlistButtonCsv} from '@tt/block--admin--activity-detail--shortlist--button-csv'

import {fromActivity} from '@tt/domains'
import {useStore} from 'effector-react'
import {formatDate} from '@tt/date-format'

const A = {}

export function BlockAdminActivitiesTableOneRow({doc}) {
  const history = useHistory()
  const list = useStore(fromActivity.$.list)
  const short =
    (!/\s/g.test(doc?.title) &&
      doc?.title?.length > 20 &&
      doc?.title?.substr(0, 20)) ||
    doc?.title

  const toUppercase = username =>
    username?.charAt(0)?.toUpperCase() + username?.substring(1)

  const dataListPrepare = activityDoc => {
    let applicationList = []

    const {
      tags = [],
      location = [],
      comment,
      id,
      hiringManagerId,
      title,
    } = activityDoc

    applicationList.push(
      activityDoc?.applicationList?.map(application => {
        const type = application?.audioLink
          ? `Audio`
          : application?.videoLink
          ? `Video`
          : `Writting`

        const {
          id,
          applicationList,
          favouriteActivities,
          selected,
          userId,
          applicationsCount,
          updatedAt,
          included,
          iconId,
          user,
          hiringManagerId,
          title,
          skills,
          createdAt,
          activityId,
          audioLink,
          videoLink,
          text,
          status,
          ...props
        } = {
          ...application,
          ActivityTitle: activityDoc?.title,
          HiringManager: toUppercase(activityDoc?.hiringManagerId),
          ApplicantName: application?.user?.name,
          Skills: application?.user?.skills,
          Phone: application?.user?.phone,
          Email: application?.user?.email,
          [`Application`]: `${type}: ${application?.audioLink ||
            application?.videoLink ||
            application?.text}`,
          InternalFeedback: comment,
        }
        return {...props}
      }),
    )
    applicationList = applicationList.filter(i => i.length)
    const [dataList = []] = applicationList
    return dataList
  }

  return (
    <A.Block>
      <A.AreaTitle onClick={() => history.push(`/activity/detail/${doc.id}`)}>
        {short || ``}
      </A.AreaTitle>
      <A.AreaOpen>{formatDate(doc.createdAt, 'HH:mm')}</A.AreaOpen>
      <A.AreaClose>{formatDate(doc.dateClose)}</A.AreaClose>
      <A.AreaManager>{doc.hiringManagerId}</A.AreaManager>
      <A.AreaStatus>{doc.status}</A.AreaStatus>
      <A.AreaQnty>
        {doc.applicationsCount + 0 > 0 ? (
          <A.Link onClick={() => history.push(`/activity/detail/${doc.id}`)}>
            {doc.applicationsCount}
          </A.Link>
        ) : (
          doc.applicationsCount
        )}
      </A.AreaQnty>
      <A.AreaExport>
        <BlockAdminActivityDetailShortlistButtonCsv
          dataList={dataListPrepare(doc)}
        />
      </A.AreaExport>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'title open close manager status qnty export';
  grid-template-columns: 2fr 15% 15% 15% 15% 15% 1fr;
  grid-template-rows: 50px;
`

A.AreaTitle = styled.div`
  grid-area: title;
  font-size: 0.9em;
  display: flex;
  align-items: center;

  color: #549add;
  text-decoration: underline;
  cursor: pointer;

  &:hover {
    text-decoration: none;
  }
`

A.Link = styled.span`
  color: #549add;
  text-decoration: underline;
  cursor: pointer;

  &:hover {
    text-decoration: none;
  }
`
A.AreaOpen = styled.div`
  grid-area: open;
  font-size: 0.9em;
  display: flex;
  align-items: center;
  text-align: left;
`
A.AreaClose = styled.div`
  grid-area: close;
  font-size: 0.9em;
  display: flex;
  align-items: center;
  text-align: left;
`
A.AreaManager = styled.div`
  grid-area: manager;
  font-size: 0.9em;
  display: flex;
  align-items: center;

  text-transform: capitalize;
`
A.AreaStatus = styled.div`
  grid-area: status;
  font-size: 0.9em;
  display: flex;
  align-items: center;
`
A.AreaQnty = styled.div`
  grid-area: qnty;
  font-size: 0.9em;
  display: flex;
  align-items: center;
`
A.AreaExport = styled.div`
  grid-area: export;
  font-size: 0.9em;
  display: flex;
  align-items: center;
`
