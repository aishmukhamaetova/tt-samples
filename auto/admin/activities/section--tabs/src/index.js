import React from 'react'
import styled from 'styled-components'
import {BlockAdminActivitiesTabsTabs} from '@tt/block--admin--activities--tabs--tabs'

const A = {}

export function SectionAdminActivitiesTabs({indexActive}) {
  return (
    <A.Section>
      <A.AreaTabs>
        <BlockAdminActivitiesTabsTabs indexActive={indexActive} />
      </A.AreaTabs>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas: '. tabs .';
  grid-template-columns: 3% 1fr 3%;
`

A.AreaTabs = styled.div`
  grid-area: tabs;
  border-bottom: 1px solid #d8d8d8;
`
