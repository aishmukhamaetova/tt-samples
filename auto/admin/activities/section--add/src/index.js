import React from 'react'
import styled from 'styled-components'
import {ButtonAddActivity} from '@tt/component--admin--button-add-activity'
import {useHistory} from 'react-router-dom'
const A = {}

export function SectionAdminActivitiesAdd() {
  const history = useHistory()
  const onClick = () => history.push(`/activity/create`)
  return (
    <A.Section>
      <A.AreaButton>
        <ButtonAddActivity onClick={onClick} />
      </A.AreaButton>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas: '. button .';
  grid-template-columns: 3% 1fr 3%;
`

A.AreaButton = styled.div`
  grid-area: button;
  display: flex;
  justify-content: flex-end;
`
