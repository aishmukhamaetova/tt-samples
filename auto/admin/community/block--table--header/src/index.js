import React from 'react'
import styled from 'styled-components'
import {IconFilter} from '@tt/ui-admin'
import {fromUser} from '@tt/domains'

const A = {}

export function BlockAdminCommunityTableHeader(props) {
  return (
    <A.Block>
      <A.AreaName>
        <div onClick={fromUser.on.switchSort}>
          Name
          <IconFilter />
        </div>
      </A.AreaName>
      <A.AreaEmail>Email</A.AreaEmail>
      <A.AreaPhone>Phone</A.AreaPhone>
      <A.AreaSkills>Skills</A.AreaSkills>
      <A.AreaApplicationsCount>No. Applications</A.AreaApplicationsCount>
      <A.AreaApplicationsSuccess>Application Success</A.AreaApplicationsSuccess>
      <A.AreaComment>Comment</A.AreaComment>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'name email phone skills applications-count applications-success comment';
  grid-template-columns: 15% 15% 15% 20% 1fr 1fr 5%;
  grid-template-rows: 50px;
`

A.AreaName = styled.div`
  grid-area: name;
  font-weight: bold;
  display: flex;
  align-items: center;
  cursor: pointer;
`
A.AreaEmail = styled.div`
  grid-area: email;
  font-weight: bold;

  display: flex;
  align-items: center;
`
A.AreaPhone = styled.div`
  grid-area: phone;
  font-weight: bold;

  display: flex;
  align-items: center;
`
A.AreaSkills = styled.div`
  grid-area: skills;
  font-weight: bold;

  display: flex;
  align-items: center;
`
A.AreaApplicationsCount = styled.div`
  grid-area: applications-count;
  font-weight: bold;

  display: flex;
  align-items: center;
`
A.AreaApplicationsSuccess = styled.div`
  grid-area: applications-success;
  font-weight: bold;

  display: flex;
  align-items: center;
`
A.AreaComment = styled.div`
  grid-area: comment;
  font-weight: bold;

  display: flex;
  align-items: center;
`
