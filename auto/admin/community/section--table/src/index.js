import React from 'react'
import styled from 'styled-components'
import {BlockAdminCommunityTableHeader} from '@tt/block--admin--community--table--header'
import {BlockAdminCommunityTableRows} from '@tt/block--admin--community--table--rows'

const A = {}

export function SectionAdminCommunityTable(props) {
  return (
    <A.Section>
      <A.AreaHeader>
        <BlockAdminCommunityTableHeader />
      </A.AreaHeader>
      <A.AreaRows>
        <BlockAdminCommunityTableRows
          list={props.list}
          onApplicationsClick={props.onApplicationsClick}
        />
      </A.AreaRows>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas:
    '. header .'
    '. rows .';
  margin-top: 20px;
  grid-template-columns: 3% 1fr 3%;
`

A.AreaHeader = styled.div`
  grid-area: header;
  border-bottom: 1px solid #d8d8d8;
`
A.AreaRows = styled.div`
  grid-area: rows;
`
