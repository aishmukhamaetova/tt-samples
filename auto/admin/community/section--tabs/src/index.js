import React from 'react'
import styled from 'styled-components'
import {BlockAdminCommunityTabsTabs} from '@tt/block--admin--community--tabs--tabs'

const A = {}

export function SectionAdminCommunityTabs() {
  return (
    <A.Section>
      <A.AreaTabs>
        <BlockAdminCommunityTabsTabs />
      </A.AreaTabs>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas:
    '. tabs .'
  ;
  grid-template-columns: 3% 1fr 3%;
`

A.AreaTabs = styled.div`
  grid-area: tabs;
  border-bottom: 1px solid #d8d8d8;
`
