import React from 'react'
import styled from 'styled-components'
import {IconComment} from '@tt/ui-admin'
import Typography from '@material-ui/core/Typography'
import Tooltip from '@material-ui/core/Tooltip'
import {withStyles} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'

const HtmlTooltip = withStyles(theme => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 320,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
  },
}))(Tooltip)

const A = {}

export function BlockAdminCommunityTableOneRow(props) {
  const {doc} = props
  let skills = doc?.skills || ''
  return (
    <A.Block>
      <A.AreaName onClick={() => props.onApplicationsClick?.({id: doc.id})}>
        {doc?.name || `Empty name`}
      </A.AreaName>
      <A.AreaEmail>{doc?.email}</A.AreaEmail>
      <A.AreaPhone>{doc?.phone}</A.AreaPhone>
      <A.AreaSkills>{skills.split(',').join(', ')}</A.AreaSkills>
      <A.AreaApplicationsCount
        applicationList={doc?.applicationList?.length}
        onClick={() =>
          doc?.applicationList?.length !== 0 &&
          props.onApplicationsClick?.({id: doc.id})
        }
      >
        {doc?.applicationList?.length}
      </A.AreaApplicationsCount>
      <A.AreaApplicationsSuccess
        applicationsSuccess={doc?.applicationsSuccess}
        onClick={() =>
          doc?.applicationsSuccess !== 0 &&
          props.onApplicationsClick?.({id: doc.id})
        }
      >
        {doc?.applicationsSuccess}
      </A.AreaApplicationsSuccess>
      <A.AreaComment>
        <HtmlTooltip
          placement="left-end"
          title={<Typography color="inherit">{doc.note}</Typography>}
        >
          <Button>
            <IconComment />
          </Button>
        </HtmlTooltip>
      </A.AreaComment>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'name email phone skills applications-count applications-success comment';
  grid-template-columns: 15% 15% 15% 20% 1fr 1fr 5%;
  grid-template-rows: 50px;
`

A.AreaName = styled.div`
  grid-area: name;
  display: flex;
  font-size: 0.9em;
  align-items: center;
  color: #549add;
  text-decoration: underline;
  cursor: pointer;
  &:hover {
    text-decoration: none;
  }
`
A.AreaEmail = styled.div`
  grid-area: email;
  font-size: 0.9em;
  display: flex;
  align-items: center;
`
A.AreaPhone = styled.div`
  grid-area: phone;
  font-size: 0.9em;
  display: flex;
  align-items: center;
`
A.AreaSkills = styled.div`
  grid-area: skills;
  font-size: 0.9em;
  display: flex;
  align-items: center;
`
A.AreaApplicationsCount = styled.a`
  grid-area: applications-count;
  font-size: 0.9em;
  display: flex;
  align-items: center;
  color: ${props => (props.applicationList != 0 ? '#549add' : '#000')};
  text-decoration: ${props =>
    props.applicationList != 0 ? 'underline' : 'none'};
  cursor: ${props => (props.applicationList != 0 ? 'pointer' : 'default')};
  &:hover {
    text-decoration: none;
  }
`
A.AreaApplicationsSuccess = styled.a`
  grid-area: applications-success;
  font-size: 0.9em;
  display: flex;
  align-items: center;
  color: ${props => (props.applicationsSuccess != 0 ? '#549add' : '#000')};
  text-decoration: ${props =>
    props.applicationsSuccess != 0 ? 'underline' : 'none'};
  &:hover {
    text-decoration: none;
  }
  cursor: ${props => (props.applicationsSuccess != 0 ? 'pointer' : 'default')};
`
A.AreaComment = styled.div`
  grid-area: comment;
  font-size: 0.9em;
  display: flex;
  align-items: center;
  justify-content: center;

  cursor: pointer;
`
