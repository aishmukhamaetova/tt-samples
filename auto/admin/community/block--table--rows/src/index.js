import React from 'react'
import styled from 'styled-components'
import {BlockAdminCommunityTableOneRow} from '@tt/block--admin--community--table--one-row'

const A = {}

export function BlockAdminCommunityTableRows(props) {
  return (
    <A.Block>
      {props.list.map(doc => (
        <A.AreaRow key={doc.id}>
          <BlockAdminCommunityTableOneRow
            doc={doc}
            onApplicationsClick={props.onApplicationsClick}
          />
        </A.AreaRow>
      ))}
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'row';
  grid-auto-rows: auto;
`

A.AreaRow = styled.div``
