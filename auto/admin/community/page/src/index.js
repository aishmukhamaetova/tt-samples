import React from 'react'
import styled from 'styled-components'
import {SectionAdminHeader} from '@tt/section--admin--header'
import {SectionAdminActivitiesTabs} from '@tt/section--admin--activities--tabs'
import {SectionAdminCommunityTable} from '@tt/section--admin--community--table'
import {useStore} from 'effector-react'
import {fromActivity} from '@tt/domains'
import {fromUser} from '@tt/domains'
import {useHistory} from 'react-router-dom'

const A = {}

export function PageAdminCommunity() {
  const history = useHistory()

  React.useEffect(() => {
    fromActivity.run.queryAdmin()
    fromUser.run.queryAdmin()
  }, [])

  const list = useStore(fromUser.$.list)
  const onApplicationsClick = ({id}) =>
    history.push(`/applicant/community/${id}`)

  return (
    <A.Page>
      <A.AreaHeader>
        <SectionAdminHeader />
      </A.AreaHeader>
      <A.AreaTabs>
        <SectionAdminActivitiesTabs indexActive={1} />
      </A.AreaTabs>
      <A.AreaTable>
        <SectionAdminCommunityTable
          list={list}
          onApplicationsClick={onApplicationsClick}
        />
      </A.AreaTable>
    </A.Page>
  )
}

A.Page = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'tabs'
    'table';
`

A.AreaHeader = styled.div`
  grid-area: header;
`
A.AreaTabs = styled.div`
  grid-area: tabs;
  padding-top: 20px;
`
A.AreaTable = styled.div`
  grid-area: table;
`
