import React from 'react'
import styled from 'styled-components'

const A = {}

export function BlockAdminCommunityTabsTabs(props) {
  return (
    <A.Block>
      <A.AreaA>
        Activities (3)
      </A.AreaA>
      <A.AreaB>
        Community (7)
      </A.AreaB>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas:
    'a b .'
  ;
  grid-template-columns: 200px 200px 1fr;
  grid-template-rows: 50px;
`

A.AreaA = styled.div`
  grid-area: a;
  font-weight: bold;
  font-size: 21px;
  letter-spacing: -0.42px;;
  display: flex;
  align-items: center;
  border-bottom: 5px solid #feb82b;
  white-space: nowrap;
`
A.AreaB = styled.div`
  grid-area: b;
  font-weight: bold;
  font-size: 21px;
  letter-spacing: -0.42px;;
  display: flex;
  align-items: center;
  white-space: nowrap;
  cursor: pointer;
`
