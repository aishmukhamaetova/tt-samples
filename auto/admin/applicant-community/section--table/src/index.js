import React from 'react'
import styled from 'styled-components'
import {BlockAdminApplicantCommunityTableHeader} from '@tt/block--admin--applicant-community--table-header'
import {BlockAdminApplicantCommunityTableRow} from '@tt/block--admin--applicant-community--table-row'

const A = {}

export function SectionAdminApplicantCommunityTable(props) {
  const [sortType, setSortType] = React.useState(null)

  return (
    <A.Section>
      <A.AreaHeader>
        <BlockAdminApplicantCommunityTableHeader
          onSwitchSort={() => setSortType((v) => !v)}
        />
      </A.AreaHeader>
      <A.AreaTable>
        <BlockAdminApplicantCommunityTableRow
          list={props.list}
          onActivityClick={props.onActivityClick}
          sortType={sortType}
          onChangeStatus={props.onChangeStatus}
        />
      </A.AreaTable>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas:
    '. header .'
    '. table .';
  /* padding-left: 52px; */
  /* padding-right: 52px; */
  padding-top: 10px;

  grid-template-columns: 3% 1fr 3%;
`

A.AreaHeader = styled.div`
  grid-area: header;
  display: flex;
  justify-content: center;
`
A.AreaTable = styled.div`
  grid-area: table;
  display: flex;
  justify-content: center;
`
