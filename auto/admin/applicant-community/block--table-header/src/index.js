import React from 'react'
import styled from 'styled-components'
import {IconFilter} from '@tt/ui-admin'

const A = {}

export function BlockAdminApplicantCommunityTableHeader(props) {
  return (
    <A.Block>
      <A.AreaActivity>
        <div onClick={props.onSwitchSort}>
          Activity
          <IconFilter />
        </div>
      </A.AreaActivity>
      <A.AreaManager>Hiring Manager</A.AreaManager>
      <A.AreaOpenDate>Start Date</A.AreaOpenDate>
      <A.AreaCloseDate>Close Date</A.AreaCloseDate>
      <A.AreaApplied>Applied on</A.AreaApplied>
      <A.AreaVideo>Video</A.AreaVideo>
      <A.AreaAudio>Audio</A.AreaAudio>
      <A.AreaText>Text</A.AreaText>
      <A.AreaActStatus>Activity status</A.AreaActStatus>
      <A.AreaStatus>Application status</A.AreaStatus>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'activity manager open-date close-date applied video audio text actstatus status';
  grid-template-columns: 2fr 10% 10% 10% 10% 10% 10% 10% 1fr 1fr;
  font-weight: bold;
  padding-top: 10px;
  padding-bottom: 5px;
  border-bottom: 2px solid #ebebeb;
  flex: 1;
`

A.AreaActivity = styled.div`
  grid-area: activity;
  display: flex;
  align-items: center;
  cursor: pointer;
`
A.AreaManager = styled.div`
  grid-area: manager;
  display: flex;
  align-items: center;
`
A.AreaOpenDate = styled.div`
  grid-area: open-date;
  display: flex;
  align-items: center;
`
A.AreaCloseDate = styled.div`
  grid-area: close-date;
  display: flex;
  align-items: center;
`
A.AreaApplied = styled.div`
  grid-area: applied;
  display: flex;
  align-items: center;
`
A.AreaVideo = styled.div`
  grid-area: video;
  display: flex;
  align-items: center;
  a {
    color: black;
    &:hover {
      text-decoration: none;
    }
  }
`
A.AreaAudio = styled.div`
  grid-area: audio;
  display: flex;
  align-items: center;
  a {
    color: black;
    &:hover {
      text-decoration: none;
    }
  }
`
A.AreaText = styled.div`
  grid-area: text;
  display: flex;
  align-items: center;
  justify-content: center;
`
A.AreaStatus = styled.div`
  grid-area: status;
  display: flex;
  align-items: center;
`

A.AreaActStatus = styled.div`
  grid-area: actstatus;
  display: flex;
  align-items: center;
`
