import React from 'react'
import styled from 'styled-components'
import {Application} from '@tt/domains'
import {Select} from '@tt/component--admin--select'

import {IconComment} from '@tt/ui-admin'
import Typography from '@material-ui/core/Typography'
import Tooltip from '@material-ui/core/Tooltip'
import {withStyles} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import {formatDate} from '@tt/date-format'

const HtmlTooltip = withStyles(theme => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 220,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
  },
}))(Tooltip)

const A = {}

function formatLink(v) {
  try {
    return v.split(`/`).slice(-1)[0]
  } catch (err) {
    return ``
  }
}

const optionsDefault = [
  {id: Application.statusMap.RECEIVED, name: `Received`},
  {id: Application.statusMap.REVIEW, name: `Review`},
  {id: Application.statusMap.SHORTLIST, name: `Shortlist`},
  {id: Application.statusMap.SUCCESSFUL, name: `Successful`},
  {id: Application.statusMap.UNSUCCESSFUL, name: `Unsuccessful`},
  {id: Application.statusMap.WITHDRAWN, name: `Withdrawn`},
]

function getSelectProps(status = `received`, activityStatus) {
  if (status === Application.statusMap.WITHDRAWN) {
    return {
      value: Application.statusMap.WITHDRAWN,
      options: [
        ...optionsDefault,
        {id: Application.statusMap.WITHDRAWN, name: `Withdrawn`},
      ],
    }
  }
  if (activityStatus === `filled` || activityStatus === `cancelled`) {
    if (status === Application.statusMap.SUCCESSFUL) {
      return {
        value: Application.statusMap.SUCCESSFUL,
        options: [{id: Application.statusMap.SUCCESSFUL, name: `Successful`}],
      }
    } else {
      return {
        value: Application.statusMap.UNSUCCESSFUL,
        options: [
          {id: Application.statusMap.UNSUCCESSFUL, name: `Unsuccessful`},
        ],
      }
    }
  }

  return {
    value: status,
    options: optionsDefault,
  }
}

const toUppercase = username =>
  username.charAt(0).toUpperCase() + username.substring(1)

export function Row(props) {
  const {doc} = props

  const isDisabled =
    doc.status === Application.statusMap.WITHDRAWN ||
    doc?.activity?.status === `filled` ||
    doc?.activity?.status === `cancelled`

  const {value, options} = getSelectProps(doc.status, doc?.activity?.status)

  return (
    <A.Block>
      <A.AreaActivity
        onClick={() => props?.onActivityClick({id: doc?.activity?.id})}
      >
        {doc?.activity?.title}
      </A.AreaActivity>
      <A.AreaManager>{doc?.activity?.hiringManagerId}</A.AreaManager>
      <A.AreaOpenDate>{formatDate(doc?.activity?.dateStart)}</A.AreaOpenDate>
      <A.AreaCloseDate>{formatDate(doc?.activity?.dateClose)}</A.AreaCloseDate>
      <A.AreaApplied>{formatDate(doc?.activity?.createdAt)}</A.AreaApplied>
      <A.AreaVideo>{formatLink(doc?.videoLink)}</A.AreaVideo>
      <A.AreaAudio>{formatLink(doc?.audioLink)}</A.AreaAudio>
      <A.AreaText>
        {doc?.text ? (
          <HtmlTooltip
            placement="left-end"
            title={<Typography color="inherit">{doc?.text}</Typography>}
          >
            <Button>
              <IconComment />
            </Button>
          </HtmlTooltip>
        ) : null}
      </A.AreaText>
      <A.AreaStatus>
        <Select
          options={options}
          valueById={value}
          isDisabled={isDisabled}
          onChange={({id: status}) =>
            props.onChangeStatus({id: doc.id, status})
          }
        />
      </A.AreaStatus>
      <A.AreaActivityStatus>
        {toUppercase(doc?.activity?.status)}
      </A.AreaActivityStatus>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'activity manager open-date close-date applied video audio text actstatus status';
  font-size: 14px;
  grid-template-columns: 2fr 10% 10% 10% 10% 10% 10% 10% 1fr 1fr;
  padding-top: 10px;
  padding-bottom: 5px;
`

A.AreaActivity = styled.div`
  grid-area: activity;
  display: flex;
  align-items: center;

  color: #549add;
  text-decoration: underline;
  cursor: pointer;

  border: 1px solid transparent;
  &:hover {
    text-decoration: none;
  }
`
A.AreaManager = styled.div`
  grid-area: manager;
  display: flex;
  align-items: center;
  text-transform: capitalize;
`
A.AreaOpenDate = styled.div`
  grid-area: open-date;
  display: flex;
  align-items: center;
`
A.AreaCloseDate = styled.div`
  grid-area: close-date;
  display: flex;
  align-items: center;
`
A.AreaApplied = styled.div`
  grid-area: applied;
  display: flex;
  align-items: center;
`
A.AreaVideo = styled.div`
  grid-area: video;
  display: flex;
  align-items: center;
  a {
    color: black;
    &:hover {
      text-decoration: none;
    }
  }
`
A.AreaAudio = styled.div`
  grid-area: audio;
  display: flex;
  align-items: center;
  a {
    color: black;
    &:hover {
      text-decoration: none;
    }
  }
`
A.AreaText = styled.div`
  grid-area: text;
  display: flex;
  align-items: center;
  justify-content: center;
`
A.AreaStatus = styled.div`
  grid-area: status;
  display: flex;
  align-items: center;
`
A.AreaActivityStatus = styled.div`
  grid-area: actstatus;
  display: flex;
  align-items: center;
`
