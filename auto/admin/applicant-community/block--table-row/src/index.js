import React from 'react'
import styled from 'styled-components'
import {Row} from './row'

const A = {}

function sortByName(sortType, list) {
  if (sortType === null) {
    return [...list].sort(
      (a, b) => new Date(b.createdAt) - new Date(a.createdAt)
    )
  }

  try {
    return sortType
      ? [...list].sort((a, b) =>
          a.activity.title.localeCompare(b.activity.title)
        )
      : [...list].sort((a, b) =>
          b.activity.title.localeCompare(a.activity.title)
        )
  } catch (err) {
    return list
  }
}

export function BlockAdminApplicantCommunityTableRow(props) {
  const list = sortByName(props.sortType, props.list || [])

  return (
    <A.Block>
      {list.map((doc) => (
        <A.AreaRow key={doc.id}>
          <Row
            doc={doc}
            onActivityClick={props.onActivityClick}
            onChangeStatus={props.onChangeStatus}
          />
        </A.AreaRow>
      ))}
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'row';
  grid-auto-rows: auto;

  flex: 1;
`

A.AreaRow = styled.div``
