import React from 'react'
import styled from 'styled-components'
import {SectionAdminHeader} from '@tt/section--admin--header'
import {SectionAdminBack} from '@tt/section--admin--back'
import {SectionAdminApplicantCommunityInfo} from '@tt/section--admin--applicant-community--info'
import {SectionAdminApplicantCommunityTable} from '@tt/section--admin--applicant-community--table'
import {useStore} from 'effector-react'
import {fromUser} from '@tt/domains'
import {useHistory} from 'react-router-dom'
import {useParams} from 'react-router-dom'
import {SectionAdminActivityTitle} from '@tt/section--admin--activity-detail--title'
import {SectionAdminActivityNote} from '@tt/section--admin--activity--note'
import {SectionAdminApplicantNoteSave} from '@tt/section--admin--applicant-note--save'

const A = {}

export function PageAdminApplicantCommunity() {
  const history = useHistory()
  const {id} = useParams()

  React.useEffect(() => {
    fromUser.run.queryAdmin()
  }, [])

  const list = useStore(fromUser.$.list)

  const doc = list.find((v) => v.id === id)
  const applicationList = doc?.applicationList || []

  const onActivityClick = ({id}) => history.push(`/activity/detail/${id}`)

  return (
    <A.Page>
      <A.AreaHeader>
        <SectionAdminHeader />
      </A.AreaHeader>
      <A.AreaBack>
        <SectionAdminBack />
      </A.AreaBack>
      <A.AreaTitle>
        <SectionAdminActivityTitle
          doc={doc}
          username={doc?.name || `User name`}
        />
      </A.AreaTitle>
      <A.AreaInfo>
        <SectionAdminApplicantCommunityInfo doc={doc} />
      </A.AreaInfo>
      <A.AreaTable>
        <SectionAdminApplicantCommunityTable
          list={applicationList}
          onActivityClick={onActivityClick}
          onChangeStatus={(payload) => fromUser.run.updateApplication(payload)}
        />
      </A.AreaTable>
      <A.AreaInternal>
        <SectionAdminActivityNote doc={doc} isCommunity={true} />
      </A.AreaInternal>
      <A.AreaSave>
        <SectionAdminApplicantNoteSave doc={doc} />
      </A.AreaSave>{' '}
    </A.Page>
  )
}

A.Page = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'back'
    'title'
    'info'
    'table'
    'internal'
    'save';
  min-height: 100vh;
  grid-template-rows: auto auto auto auto 1fr 250px;
`

A.AreaSave = styled.div`
  grid-area: save;
`

A.AreaHeader = styled.div`
  grid-area: header;
`
A.AreaBack = styled.div`
  grid-area: back;
`
A.AreaTitle = styled.div`
  grid-area: title;
`
A.AreaInfo = styled.div`
  grid-area: info;
`
A.AreaTable = styled.div`
  grid-area: table;
`
A.AreaInternal = styled.div`
  grid-area: internal;
  background: #e8e8e8;
`
