import React from 'react'
import styled from 'styled-components'
import {ButtonSave} from '@tt/component--admin--button-save'
import {
  validationerrors,
  fields,
  queryAdminOne,
} from '@tt/domain--admin--applicant--form'

const A = {}

export function SectionAdminApplicantNoteSave({doc}) {
  const [error] = validationerrors.use().filter(object => {
    const [key] = Object.keys(object)
    return key == 'adminNote'
  })
  const adminNote = fields.adminNote.use()
  return (
    <A.Section>
      <A.AreaSave>
        <ButtonSave
          disabled={error?.adminNote}
          onClick={async () => {
            try {
              const rSubmit = await queryAdminOne.run({
                id: doc?.id,
                adminNote: adminNote,
              })
              // if (rSubmit?.errors?.length === 0) {
              // }
            } catch (err) {
              console.log(`ERROR`, err)
            }
          }}
        />
      </A.AreaSave>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas: '. save .';
  grid-template-columns: 3% 1fr 3%;
  grid-template-rows: 80px;
`

A.AreaSave = styled.div`
  grid-area: save;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`
