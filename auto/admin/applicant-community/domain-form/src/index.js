import createDebugger from 'debug'
import {combineFields} from '@tt/effector'
import {makeSimple} from '@tt/effector'
import {makeCoeffect} from '@tt/effector'
import {config} from '@tt/domains'
import {createEvent} from 'effector'
import {createStore} from 'effector'

const who = [`@tt`, `domain--admin--applicant--form`]

const watcher = createDebugger(who.join(`:`))

export const typeMap = {
  STRING: `String`,
  FILE: `File`,
  OBJECT: `Object`,
}

const clear = createEvent()

export const on = {
  clear,
}

export const validationerrors = makeSimple(
  {
    name: `validationerrors`,
    type: typeMap.OBJECT,
    defaultValue: [],
    validate: [],
  },
  {clear, watcher},
)

export const fields = {
  adminNote: makeSimple(
    {
      name: `adminNote`,
      type: typeMap.STRING,
      defaultValue: ``,
      validate: [],
    },
    {clear, watcher},
  ),
}

const errorSetter = obj => {
  const [value] = Object.values(obj)
  validationerrors.on.set([].concat(validationerrors.$.store.getState(), [obj]))
}
const removeError = fieldName => {
  const errors = validationerrors.$.store.getState().filter(object => {
    const [key] = Object.keys(object)
    return fieldName !== key
  })
  validationerrors.on.set(errors)
}

fields.adminNote.$.store.watch(v => {
  if (!v) {
    errorSetter({adminNote: `Admin Note not defined`})
  } else {
    removeError(`adminNote`)
  }
})

export const $form = combineFields({fields})
export const $validationMap = combineFields({fields}, {storeName: `isValid`})

export const queryAdminOne = makeCoeffect({
  store: $form,
  async handler(payload, options) {
    const {params, state} = payload
    watcher(`state`, state, `payload`, payload)

    const {id, adminNote} = params

    const url = `${config.API}/api/v1/user/updatenote`
    const requestOptions = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    }
    const res = await fetch(url, requestOptions)
    const r = await res.json()
    const {user, errors} = r

    if (errors.length === 0) {
      fields.adminNote.on.set(user?.adminNote)

      return {user, errors}
    }
  },
})
