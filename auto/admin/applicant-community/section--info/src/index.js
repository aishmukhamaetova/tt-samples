import React from 'react'
import styled from 'styled-components'

const A = {}

export function SectionAdminApplicantCommunityInfo(props) {
  return (
    <A.Section>
      <A.AreaEmail>{props.doc?.email}</A.AreaEmail>
      <A.AreaPhone>{props.doc?.phone}</A.AreaPhone>
      <A.AreaTags>{props.doc?.skills}</A.AreaTags>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas:
    '. email .'
    '. phone .'
    '. tags .';
  padding-top: 10px;
  grid-template-columns: 3% 1fr 3%;
`

A.AreaEmail = styled.div`
  grid-area: email;
`
A.AreaPhone = styled.div`
  grid-area: phone;
`
A.AreaTags = styled.div`
  grid-area: tags;
`
