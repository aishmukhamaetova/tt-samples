import React from 'react'
import styled from 'styled-components'
import {Input} from '@tt/component--admin--input'
import {Select} from '@tt/component--admin--select--duration'
import {fields} from '@tt/domain--admin--activity-create--form'
import {Label} from '@tt/component--admin--label'
import {mediaGrid} from '@tt/ui-admin'

const A = {}

export function BlockAdminActivityCreateSummaryDuration({isView}) {
  const duration = fields.duration.use()
  const durationType = fields.durationType.use()
  return (
    <A.Block isView={isView}>
      <A.AreaLabel>Duration</A.AreaLabel>
      <A.AreaInput>
        {isView ? (
          duration
        ) : (
          <Input
            value={duration}
            onChange={({target: {value}}) => fields.duration.on.set(value)}
          />
        )}
      </A.AreaInput>
      <A.AreaSelect>
        {isView ? (
          durationType
        ) : (
          <Select
            valueById={durationType}
            onChange={({id}) => fields.durationType.on.set(id)}
          />
        )}
      </A.AreaSelect>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'label input select';
  grid-column-gap: 15px;
  grid-template-columns: ${props =>
    props.isView ? `20% 10px 200px` : `20% 120px 200px`};

  ${mediaGrid(`sm`)} {
    //xs phone
    grid-template-columns: ${props =>
      props.isView ? `20% 10px 120px` : `20% 80px 120px`};
  }

  grid-template-rows: 75px;
`

A.AreaLabel = styled(Label)`
  grid-area: label;
`
A.AreaInput = styled.div`
  grid-area: input;
  display: flex;
  align-items: center;
`
A.AreaSelect = styled.div`
  grid-area: select;
  display: flex;
  align-items: center;
`
