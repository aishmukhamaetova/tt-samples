import React from 'react'
import styled from 'styled-components'
import {Select} from '@tt/component--admin--select--hiring-manager'
import {fields} from '@tt/domain--admin--activity-create--form'
import {Label} from '@tt/component--admin--label'
import {mediaGrid} from '@tt/ui-admin'

const A = {}

export function BlockAdminActivityCreateInfoManager({isView}) {
  const hiringManagerId = fields.hiringManagerId.use()

  const toUppercase = username =>
    username.charAt(0).toUpperCase() + username.substring(1)

  return (
    <A.Block>
      <A.AreaLabel>Hiring Manager</A.AreaLabel>
      <A.AreaSelect>
        {isView ? (
          <Select
            isDisabled={isView}
            valueById={hiringManagerId}
            defaultValue={toUppercase(hiringManagerId)}
          />
        ) : (
          <Select
            valueById={hiringManagerId}
            onChange={({id}) => fields.hiringManagerId.on.set(id)}
          />
        )}
      </A.AreaSelect>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'label select';
  grid-column-gap: 15px;
  grid-template-rows: 45px;
  grid-template-columns: 20% 320px;
  ${mediaGrid(`sm`)} {
    //xs phone
    grid-template-columns: 20% 200px;
  }
`

A.AreaLabel = styled(Label)`
  grid-area: label;
`
A.AreaSelect = styled.div`
  grid-area: select;
  display: flex;
  align-items: center;
`
