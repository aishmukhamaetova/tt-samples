import React from 'react'
import styled from 'styled-components'
import {Label} from '@tt/component--admin--label'
import {ButtonOutline} from '@tt/ui-admin'
import {fields} from '@tt/domain--admin--activity-create--form'
import {Grid} from '@material-ui/core'

const A = {}

const list = [
  {id: `0`, name: `Prominent Hill`, as: `AreaButtonOne`},
  {id: `1`, name: `Flexible Location`, as: `AreaButtonTwo`},
  {id: `2`, name: `Adelaide Office`, as: `AreaButtonThree`},
]

export function BlockAdminActivityCreateSummaryLocation(props) {
  const {doc, isView} = props
  const [selectMap, setSelectMap] = React.useState({})
  function select({id}) {
    setSelectMap(state => {
      const r = {
        ...state,
        [id]: !state[id] ?? true,
      }

      fields.location.on.set(
        Object.keys(r)
          .filter(id => r[id])
          .map(id => list[id].name)
          .join(`,`),
      )

      return r
    })
  }

  // React.useEffect(() => {
  //   setSelectMap({})
  // }, [isView])

  React.useEffect(() => {
    if (!doc) return

    const locationList = doc.location || []

    for (let location of locationList) {
      const item = list.find(v => v.name === location)
      if (item) {
        select({id: item.id})
      }
    }
  }, [doc])

  function onClick({id}) {
    select({id})
  }

  return (
    <A.Block>
      <A.AreaLabel>Location</A.AreaLabel>
      <A.AreaText>
        Select one or more inclusions which is appropriate to this activity
      </A.AreaText>
      <A.AreaButton>
        <Grid container spacing={2}>
          {list.map(({id, name, as}) => {
            return isView ? (
              selectMap[id] && (
                <Grid item key={id}>
                  {isView ? (
                    <A.ButtonDefault>{name}</A.ButtonDefault>
                  ) : (
                    <A.ButtonLocation isselected={selectMap[id]}>
                      {name}
                    </A.ButtonLocation>
                  )}
                </Grid>
              )
            ) : (
              <Grid item key={id}>
                <A.ButtonLocation
                  isselected={selectMap[id]}
                  onClick={() => onClick({id})}
                >
                  {name}
                </A.ButtonLocation>
              </Grid>
            )
          })}
        </Grid>
      </A.AreaButton>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas:
    'label text'
    'label btn';
  grid-column-gap: 15px;
  grid-template-columns: 20% 1fr;
  grid-template-rows: 50px 50px;
`

A.AreaLabel = styled(Label)`
  grid-area: label;
`
A.AreaText = styled.div`
  grid-area: text;
  display: flex;
  align-items: center;
  font-size: 0.9em;
`
A.AreaButton = styled.div`
  grid-area: btn;
`

A.ButtonLocation = styled(ButtonOutline)`
  && {
    width: 100%;
    height: 40px;
    white-space: nowrap;
    font-size: 0.9em;
    background: ${props =>
      props.isselected ? 'rgba(254,184,43,0.5)' : '#fff'};
    width: 10vw;
  }
`

A.ButtonDefault = styled.div`
  && {
    background: rgba(254, 184, 43, 0.5);
    border: solid 1px rgba(0, 0, 0, 0.3);
    border-radius: 5px;
    box-shadow: 0 4px 5px -4px rgba(0, 0, 0, 0.6);
    height: 40px;
    line-height: 40px;
    text-align: center;
    width: 100%;
    white-space: nowrap;
    font-size: 0.9em;
    width: 10vw;
  }
`
