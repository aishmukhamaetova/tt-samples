import React from 'react'
import styled from 'styled-components'
import {Textarea} from '@tt/component--admin--textarea'
import {fields} from '@tt/domain--admin--activity-create--form'
import {Label} from '@tt/component--admin--label'
import {mediaGrid} from '@tt/ui-admin'

const A = {}

export function BlockAdminCreateInternalNotes({isView, doc}) {
  const notes = fields.notes.use()
  const community_note = doc?.activity?.comment || `Empty note`
  return (
    <A.Block>
      <A.AreaLabel alignItems={`flex-start`}>Activity notes</A.AreaLabel>
      <A.AreaInput paddingTop={`8px`}>
        {isView ? (
          notes
        ) : (
          <Textarea
            value={notes}
            onChange={({target: {value}}) => fields.notes.on.set(value)}
            placeholder={`Internal notes for Oz Minerals reference`}
          />
        )}
      </A.AreaInput>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'label input';
  grid-template-columns: 20% 420px;
  ${mediaGrid(`sm`)} {
    //xs phone
    grid-template-columns: 20% 200px;
  }

  grid-column-gap: 15px;
  grid-template-rows: 100px;
  align-items: start;
`

A.AreaLabel = styled(Label)`
  grid-area: label;
  align-items: start;
`
A.AreaInput = styled.div`
  grid-area: input;
  display: flex;
  padding-top: ${props => props.paddingTop || '8px'};
  align-items: center;
  width: 100%;
`
