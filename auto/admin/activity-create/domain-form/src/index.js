import createDebugger from 'debug'
import {combineFields} from '@tt/effector'
import {makeSimple} from '@tt/effector'
import {makeCoeffect} from '@tt/effector'
import {config} from '@tt/domains'
import {createEvent} from 'effector'
import {createStore} from 'effector'

const who = [`@tt`, `domain--admin--activity-create--form`]

const watcher = createDebugger(who.join(`:`))

// const matchLengthMoreZero = v => v.length > 0
// const matchTrue = v => v === true

export const typeMap = {
  STRING: `String`,
  FILE: `File`,
  OBJECT: `Object`
}

const clear = createEvent()

export const on = {
  clear
}

export const validationerrors = makeSimple(
  {
    name: `validationerrors`,
    type: typeMap.OBJECT,
    defaultValue: [],
    validate: []
  },
  {clear, watcher}
)

export const error = makeSimple(
  {
    name: `error`,
    type: typeMap.STRING,
    defaultValue: null,
    validate: []
  },
  {clear, watcher}
)

export const fields = {
  title: makeSimple(
    {
      name: `title`,
      type: typeMap.STRING,
      defaultValue: ``,
      validate: []
    },
    {clear, watcher}
  ),
  hiringManagerId: makeSimple(
    {
      name: `hiringManagerId`,
      type: typeMap.STRING,
      defaultValue: ``,
      validate: []
    },
    {clear, watcher}
  ),
  status: makeSimple(
    {
      name: `status`,
      type: typeMap.STRING,
      defaultValue: `open`,
      validate: []
    },
    {clear, watcher}
  ),
  iconId: makeSimple(
    {
      name: `iconId`,
      type: typeMap.STRING,
      defaultValue: `tt`,
      validate: []
    },
    {clear, watcher}
  ),
  tags: makeSimple(
    {
      name: `tags`,
      type: typeMap.STRING,
      defaultValue: ``,
      validate: []
    },
    {clear, watcher}
  ),
  duration: makeSimple(
    {
      name: `duration`,
      type: typeMap.STRING,
      defaultValue: ``,
      validate: []
    },
    {clear, watcher}
  ),
  durationType: makeSimple(
    {
      name: `durationType`,
      type: typeMap.STRING,
      defaultValue: ``,
      validate: []
    },
    {clear, watcher}
  ),
  summary: makeSimple(
    {
      name: `summary`,
      type: typeMap.STRING,
      defaultValue: ``,
      validate: []
    },
    {clear, watcher}
  ),
  description: makeSimple(
    {
      name: `description`,
      type: typeMap.STRING,
      defaultValue: ``,
      validate: []
    },
    {clear, watcher}
  ),
  aboutTeam: makeSimple(
    {
      name: `aboutTeam`,
      type: typeMap.STRING,
      defaultValue: ``,
      validate: []
    },
    {clear, watcher}
  ),
  notes: makeSimple(
    {
      name: `notes`,
      type: typeMap.STRING,
      defaultValue: ``,
      validate: []
    },
    {clear, watcher}
  ),
  location: makeSimple(
    {
      name: `location`,
      type: typeMap.STRING,
      defaultValue: ``,
      validate: []
    },
    {clear, watcher}
  ),
  included: makeSimple(
    {
      name: `included`,
      type: typeMap.STRING,
      defaultValue: ``,
      validate: []
    },
    {clear, watcher}
  ),
  dateStart: makeSimple(
    {
      name: `dateStart`,
      type: typeMap.STRING,
      defaultValue: new Date(),
      validate: []
    },
    {clear, watcher}
  ),
  dateEnd: makeSimple(
    {
      name: `dateEnd`,
      type: typeMap.STRING,
      defaultValue: new Date(),
      validate: []
    },
    {clear, watcher}
  ),
  dateClose: makeSimple(
    {
      name: `dateClose`,
      type: typeMap.STRING,
      defaultValue: new Date(),
      validate: []
    },
    {clear, watcher}
  ),
  file: makeSimple(
    {
      name: `file`,
      type: typeMap.FILE,
      defaultValue: null,
      validate: []
    },
    {clear, watcher}
  ),
  videoLink: makeSimple(
    {
      name: `videoLink`,
      type: typeMap.STRING,
      defaultValue: ``,
      validate: []
    },
    {clear, watcher}
  )
}

const errorSetter = (obj) => {
  const [value] = Object.values(obj)
  error.on.set(value)
  validationerrors.on.set([].concat(validationerrors.$.store.getState(), [obj]))
}
const removeError = (fieldName) => {
  const errors = validationerrors.$.store.getState().filter((object) => {
    const [key] = Object.keys(object)
    return fieldName !== key
  })
  validationerrors.on.set(errors)
}

// validationerrors.$.store.watch(v => {
//   watcher('watcher validationerrors', v)
// })

fields.title.$.store.watch((v) => {
  if (!v) {
    errorSetter({title: `Title not defined`})
  } else {
    removeError(`title`)
  }
})

fields.hiringManagerId.$.store.watch((v) => {
  if (!v) {
    errorSetter({hiringManagerId: `Hiring Manager not defined`})
  } else {
    removeError(`hiringManagerId`)
  }
})

fields.status.$.store.watch((v) => {
  if (!v) {
    errorSetter({status: `Activity status not defined`})
  } else {
    removeError(`status`)
  }
})

fields.iconId.$.store.watch((v) => {
  if (!v) {
    errorSetter({iconId: `Activity Icon not defined`})
  } else {
    removeError(`iconId`)
  }
})

fields.tags.$.store.watch((v) => {
  if (!v) {
    errorSetter({tags: `Tags not defined`})
  } else {
    removeError(`tags`)
  }
})

fields.duration.$.store.watch((v) => {
  if (!v) {
    errorSetter({duration: `Duration not defined`})
  } else {
    removeError(`duration`)
  }
})

fields.durationType.$.store.watch((v) => {
  if (!v) {
    errorSetter({durationType: `Duration Type not defined`})
  } else {
    removeError(`durationType`)
  }
})

fields.summary.$.store.watch((v) => {
  if (!v) {
    errorSetter({summary: `Summary not defined`})
  } else {
    removeError(`summary`)
  }
})

fields.description.$.store.watch((v) => {
  if (!v) {
    errorSetter({description: `Description not defined`})
  } else {
    removeError(`description`)
  }
})

fields.aboutTeam.$.store.watch((v) => {
  if (!v) {
    errorSetter({aboutTeam: `About the Team not defined`})
  } else {
    removeError(`aboutTeam`)
  }
})

fields.location.$.store.watch((v) => {
  if (!v) {
    errorSetter({location: `Location not defined`})
  } else {
    removeError(`location`)
  }
})

fields.included.$.store.watch((v) => {
  if (!v) {
    errorSetter({included: `What's included not defined`})
  } else {
    removeError(`included`)
  }
})

fields.dateStart.$.store.watch((v) => {
  if (!v) {
    errorSetter({dateStart: `Start Date not defined`})
  } else {
    removeError(`dateStart`)
  }
})

fields.dateEnd.$.store.watch((v) => {
  if (!v) {
    errorSetter({dateEnd: `End Date not defined`})
  } else {
    removeError(`dateEnd`)
  }
})

fields.dateClose.$.store.watch((v) => {
  if (!v) {
    errorSetter({dateClose: `Application Close Date not defined`})
  } else {
    removeError(`dateClose`)
  }
})

fields.file.$.store.watch((v) => {
  //   ${config.AWS_ACTIVITY_SIZE} undefined ::: debug

  if (v) {
    removeError(`videoLink`)
  }

  if (!v) {
    errorSetter({file: `File not defined`})
    return
  }

  const SIZE = config.ACTIVITY_SIZE ?? 10

  if (v && v.size > SIZE * 10 ** 6) {
    fields.file.on.set(null)
    removeError(`file`)
    errorSetter({file: `Video should be less than 10MB`})
    return
  }

  removeError(`file`)
})

fields.videoLink.$.store.watch((v) => {
  if (!v) {
    errorSetter({videoLink: `Video Link not defined`})
  } else {
    removeError(`videoLink`)
    removeError(`file`)
  }
})

export const $form = combineFields({fields})
export const $validationMap = combineFields({fields}, {storeName: `isValid`})

export const queryAdminOne = makeCoeffect({
  store: $form,
  async handler(payload, options) {
    const {params, state} = payload
    watcher(`state`, state, `payload`, payload)

    const {id} = params

    const url = `${config.API}/api/v1/activity/query-admin`
    const requestOptions = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin'
    }
    const res = await fetch(url, requestOptions)
    const r = await res.json()
    const {activityList, errors} = r
    if (errors.length === 0) {
      const doc = activityList.find((doc) => doc.id === id)

      fields.title.on.set(doc.title)
      fields.hiringManagerId.on.set(doc.hiringManagerId)
      fields.status.on.set(doc.status)
      fields.iconId.on.set(doc.iconId)
      fields.tags.on.set(doc.tags?.join(`, `) || ``)
      fields.duration.on.set(doc.duration)
      fields.durationType.on.set(doc.durationType)
      fields.summary.on.set(doc.summary)
      fields.description.on.set(doc.description)
      fields.aboutTeam.on.set(doc.aboutTeam)
      fields.aboutTeam.on.set(doc.aboutTeam)
      fields.notes.on.set(doc.comment)
      fields.location.on.set(doc.location?.join(`,`) || ``)
      fields.included.on.set(doc.included?.join(`,`) || ``)
      fields.dateStart.on.set(doc.dateStart)
      fields.dateEnd.on.set(doc.dateEnd)
      fields.dateClose.on.set(doc.dateClose)
      fields.videoLink.on.set(doc.videoLink)

      return doc
    }
  }
})
