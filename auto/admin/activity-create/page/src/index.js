import React from 'react'
import styled from 'styled-components'
import {SectionAdminHeader} from '@tt/section--admin--header'
import {SectionAdminBack} from '@tt/section--admin--back'
import {SectionAdminActivityTitle} from '@tt/section--admin--activity-detail--title'
import {SectionAdminActivityCreateError} from '@tt/section--admin--activity-create--error'
import {SectionAdminActivityCreateInfo} from '@tt/section--admin--activity-create--info'
import {SectionAdminActivityCreateSummary} from '@tt/section--admin--activity-create--summary'
import {SectionAdminActivityCreateDetails} from '@tt/section--admin--activity-create--details'
import {SectionAdminActivityNote} from '@tt/section--admin--activity--note'
import {SectionAdminActivityCreateSave} from '@tt/section--admin--activity-create--save'
import {queryAdminOne, on} from '@tt/domain--admin--activity-create--form'
import {useParams} from 'react-router-dom'
import {useHistory} from 'react-router-dom'

const A = {}

export function PageAdminActivityCreate(props) {
  const {id} = useParams()
  const history = useHistory()
  const [doc, setDoc] = React.useState(null)
  const {isView, isCreate, isEdit, isActivityStatusDisabled = true} = props
  React.useEffect(() => {
    async function fetch() {
      const doc = await queryAdminOne.run({id})
      setDoc(doc)
    }

    if (props.isEdit || isView) {
      fetch()
    }

    return () => on.clear()
  }, [props.isEdit])

  function onSaveSuccess({doc}) {
    // history.push(`/activity/view/${doc.id}`)
    history.push(`/activities`)
  }

  function onClickEdit() {
    if (isView) {
      history.push(`/activity/edit/${id}`)
    }
  }

  return (
    <A.Page>
      <A.AreaHeader>
        <SectionAdminHeader />
      </A.AreaHeader>
      <A.AreaBack>
        <SectionAdminBack />
      </A.AreaBack>
      <A.AreaTitle>
        <SectionAdminActivityTitle
          doc={doc}
          onClickEdit={onClickEdit}
          isView={isView}
          isCreate={isCreate}
          isEdit={isEdit}
        />
      </A.AreaTitle>
      {isView ? null : (
        <A.AreaError>
          <SectionAdminActivityCreateError doc={doc} isView={isView} />
        </A.AreaError>
      )}
      <A.AreaInfo>
        <SectionAdminActivityCreateInfo
          doc={doc}
          isView={isView}
          isActivityStatusDisabled={isActivityStatusDisabled}
        />
      </A.AreaInfo>
      <A.AreaSummary>
        <SectionAdminActivityCreateSummary doc={doc} isView={isView} />
      </A.AreaSummary>
      <A.AreaDetails>
        <SectionAdminActivityCreateDetails doc={doc} isView={isView} />
      </A.AreaDetails>
      <A.AreaInternal>
        <SectionAdminActivityNote doc={doc} isView={isView} />
      </A.AreaInternal>
      {isView ? null : (
        <A.AreaSave>
          <SectionAdminActivityCreateSave
            doc={doc}
            isView={isView}
            onSaveSuccess={onSaveSuccess}
          />
        </A.AreaSave>
      )}
    </A.Page>
  )
}

A.Page = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'back'
    'title'
    'error'
    'info'
    'summary'
    'details'
    'internal'
    'save';
`

A.AreaHeader = styled.div`
  grid-area: header;
`
A.AreaBack = styled.div`
  grid-area: back;
`
A.AreaTitle = styled.div`
  grid-area: title;
`
A.AreaError = styled.div`
  grid-area: error;
`
A.AreaInfo = styled.div`
  grid-area: info;
`
A.AreaSummary = styled.div`
  grid-area: summary;
`
A.AreaDetails = styled.div`
  grid-area: details;
`
A.AreaInternal = styled.div`
  grid-area: internal;
  background: #e8e8e8;
`
A.AreaSave = styled.div`
  grid-area: save;
`
