import React from 'react'
import styled from 'styled-components'
import {error, validationerrors} from '@tt/domain--admin--activity-create--form'

const A = {}

export function SectionAdminActivityCreateError() {
  const errors = validationerrors.use() || []
  const errorsText = errors.length
    ? errors
        .map(obj => {
          const [key] = Object.keys(obj)
          return obj[key]
        })
        .join(', ')
    : ''
  return (
    <A.Section>
      <A.AreaErrorText>
        {errors.length > 0 ? `* Please complete all fields.` : null}
      </A.AreaErrorText>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas: '. error-text .';
  grid-template-columns: 3% 1fr 3%;
  grid-template-rows: 40px;
`

A.AreaErrorText = styled.div`
  grid-area: error-text;
  font-size: 14px;
  color: red;
  display: flex;
  align-items: center;
`
