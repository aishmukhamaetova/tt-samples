import React from 'react'
import styled from 'styled-components'
import {BlockAdminActivityCreateSummaryTitle} from '@tt/block--admin--activity-create--summary-title'
import {BlockAdminActivityCreateSummaryTags} from '@tt/block--admin--activity-create--summary-tags'
import {BlockAdminActivityCreateSummaryIcon} from '@tt/block--admin--activity-create--summary-icon'
import {BlockAdminActivityCreateSummaryLocation} from '@tt/block--admin--activity-create--summary-location'
import {BlockAdminActivityCreateSummaryDuration} from '@tt/block--admin--activity-create--summary-duration'
import {BlockAdminActivityCreateTextAreaBlock} from '@tt/block--admin--activity-create--details-description'

const A = {}

export function SectionAdminActivityCreateSummary({doc, isView}) {
  return (
    <A.Section>
      <A.AreaTitle>
        <BlockAdminActivityCreateSummaryTitle doc={doc} isView={isView} />
      </A.AreaTitle>
      <A.AreaTags>
        <BlockAdminActivityCreateSummaryTags doc={doc} isView={isView} />
      </A.AreaTags>
      <A.AreaIcon>
        <BlockAdminActivityCreateSummaryIcon doc={doc} isView={isView} />
      </A.AreaIcon>
      <A.AreaLocation>
        <BlockAdminActivityCreateSummaryLocation doc={doc} isView={isView} />
      </A.AreaLocation>
      <A.AreaDuration>
        <BlockAdminActivityCreateSummaryDuration doc={doc} isView={isView} />
      </A.AreaDuration>
      <A.AreaSummary>
        <BlockAdminActivityCreateTextAreaBlock
          height={isView ? `auto` : `200px`}
          title={`Summary`}
          doc={doc}
          isView={isView}
          typeField={`summary`}
          placeholder={`A short but complete sentence tht takes up first, second and third line of this paragraph.`}
        />
      </A.AreaSummary>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas:
    '. title    .'
    '. tags     .'
    '. icon     .'
    '. location .'
    '. duration .'
    '. summary  .';
  grid-template-columns: 3% 1fr 3%;
  padding-top: 15px;
`

A.AreaTitle = styled.div`
  grid-area: title;
`
A.AreaTags = styled.div`
  grid-area: tags;
`
A.AreaIcon = styled.div`
  grid-area: icon;
`
A.AreaLocation = styled.div`
  grid-area: location;
`
A.AreaDuration = styled.div`
  grid-area: duration;
`
A.AreaSummary = styled.div`
  grid-area: summary;
`
