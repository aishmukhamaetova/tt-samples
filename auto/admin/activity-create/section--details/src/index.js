import React from 'react'
import styled from 'styled-components'
import {BlockAdminActivityCreateDetailsTitle} from '@tt/block--admin--activity-create--details-title'
import {BlockAdminActivityCreateDetailsStartDate} from '@tt/block--admin--activity-create--details-start-date'
import {BlockAdminActivityCreateDetailsEndDate} from '@tt/block--admin--activity-create--details-end-date'
import {BlockAdminActivityCreateTextAreaBlock} from '@tt/block--admin--activity-create--details-description'
import {BlockAdminActivityCreateDetailsIncluded} from '@tt/block--admin--activity-create--details-included'
import {BlockAdminActivityCreateDetailsCloseDate} from '@tt/block--admin--activity-create--details-close-date'
import {BlockAdminActivityCreateDetailsVideoFile} from '@tt/block--admin--activity-create--details-video-file'

const A = {}

export function SectionAdminActivityCreateDetails({doc, isView}) {
  return (
    <A.Section>
      <A.AreaTitle>
        <BlockAdminActivityCreateDetailsTitle doc={doc} isView={isView} />
      </A.AreaTitle>
      <A.AreaStartDate>
        <BlockAdminActivityCreateDetailsStartDate doc={doc} isView={isView} />
      </A.AreaStartDate>
      <A.AreaEndDate>
        <BlockAdminActivityCreateDetailsEndDate doc={doc} isView={isView} />
      </A.AreaEndDate>
      <A.AreaDescription>
        <BlockAdminActivityCreateTextAreaBlock
          height={isView ? `auto` : `200px`}
          title={`Description`}
          doc={doc}
          isView={isView}
          typeField={`description`}
          placeholder={`A full description of the activity. The language of this section is to be from the perspective of the applicant. \n\nSuggested items to inform the applicant include but not limited to:\nWhere -\nWhen can I start -\nDuration -\nSkills I need to bring -\nWhat do I need to achieve -\n`}
        />
      </A.AreaDescription>
      <A.AreaIncluded>
        <BlockAdminActivityCreateDetailsIncluded doc={doc} isView={isView} />
      </A.AreaIncluded>

      <A.AreaAbout>
        <BlockAdminActivityCreateTextAreaBlock
          height={isView ? `auto` : `200px`}
          title={`About the team`}
          doc={doc}
          isView={isView}
          typeField={`aboutTeam`}
          placeholder={`Give the applicant an insight as to which OZM resources they will be working with and what the team culture is like.`}
        />
      </A.AreaAbout>
      <A.AreaCloseDate>
        <BlockAdminActivityCreateDetailsCloseDate doc={doc} isView={isView} />
      </A.AreaCloseDate>
      <A.AreaVideoFile>
        <BlockAdminActivityCreateDetailsVideoFile doc={doc} isView={isView} />
      </A.AreaVideoFile>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas:
    '. title .'
    '. start-date .'
    '. end-date .'
    '. description .'
    '. included .'
    '. about .'
    '. close-date .'
    '. video-file .';
  grid-template-columns: 3% 1fr 3%;
  padding-top: 15px;
`

A.AreaTitle = styled.div`
  grid-area: title;
`
A.AreaStartDate = styled.div`
  grid-area: start-date;
`
A.AreaEndDate = styled.div`
  grid-area: end-date;
`
A.AreaDescription = styled.div`
  grid-area: description;
`
A.AreaIncluded = styled.div`
  grid-area: included;
`
A.AreaAbout = styled.div`
  grid-area: about;
`
A.AreaCloseDate = styled.div`
  grid-area: close-date;
`
A.AreaVideoFile = styled.div`
  grid-area: video-file;
`
