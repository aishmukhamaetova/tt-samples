import React from 'react'
import styled from 'styled-components'
import {Select} from '@tt/component--admin--select--status'
import {fields} from '@tt/domain--admin--activity-create--form'
import {Label} from '@tt/component--admin--label'

const A = {}

export function BlockAdminActivityCreateInfoStatus({
  isView,
  isActivityStatusDisabled,
}) {
  const status = fields.status.use()
  return (
    <A.Block>
      <A.AreaLabel>Activity&nbsp;status</A.AreaLabel>
      <A.AreaSelect>
        {isView ? (
          <Select
            isDisabled={isView}
            isActivityStatusDisabled={isActivityStatusDisabled}
            valueById={status}
          />
        ) : (
          <Select
            isActivityStatusDisabled={isActivityStatusDisabled}
            valueById={status}
            onChange={({id}) =>
              !isActivityStatusDisabled && fields.status.on.set(id)
            }
          />
        )}
      </A.AreaSelect>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: '. status select .';
  grid-template-rows: 45px;
  grid-template-columns: 3% 140px 100px 3%;
`

A.AreaLabel = styled(Label)`
  grid-area: status;
  align-items: center;
  white-space: nowrap;
`
A.AreaSelect = styled.div`
  grid-area: select;
  display: flex;
  align-items: center;
`
