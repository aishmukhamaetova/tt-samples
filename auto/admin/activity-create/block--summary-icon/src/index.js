import React from 'react'
import styled from 'styled-components'
import {Label} from '@tt/component--admin--label'

import {ImageIcon} from '@tt/component--admin--image--success-factors'
import {fields} from '@tt/domain--admin--activity-create--form'
import {mediaGrid} from '@tt/ui-admin'

const A = {}

const list = [
  {
    id: `tt`,
    name: `General Team Tasker`,
    imageLink: `/images/icon-tt.png`,
    as: `AreaIconTT`,
  },
  {
    id: `success`,
    name: `Success factors`,
    imageLink: `/images/icon-success-factors.png`,
    as: `AreaIconSuccess`,
  },
  {
    id: `bi`,
    name: `BI/Reporting`,
    imageLink: `/images/icon-bi.png`,
    as: `AreaIconBi`,
  },
  {
    id: `mechanic`,
    name: `Mechanic`,
    imageLink: `/images/icon-mechanic.png`,
    as: `AreaIconMechanic`,
  },
  {
    id: `agile`,
    name: `Agile`,
    imageLink: `/images/icon-agile.png`,
    as: `AreaIconAgile`,
  },
  {
    id: `pm`,
    name: `Project Management`,
    imageLink: `/images/icon-project-management.png`,
    as: `AreaIconPm`,
  },
  {
    id: `cm`,
    name: `Change Management`,
    imageLink: `/images/icon-change-management.png`,
    as: `AreaIconCm`,
  },
]

const Item = ({doc, isselected, onClick}) => {
  const Component = A[doc.as]

  return (
    <Component>
      <ImageIcon
        className={doc?.id}
        alt={doc.name}
        src={doc.imageLink}
        isselected={isselected}
        onClick={onClick}
      />
    </Component>
  )
}

export function BlockAdminActivityCreateSummaryIcon({doc, isView}) {
  const [selectedIndex, setSelectedIndex] = React.useState(0)

  React.useEffect(() => {
    if (doc && doc.iconId) {
      const index = list.findIndex(v => v.id === doc.iconId)
      if (index !== -1) {
        setSelectedIndex(index)
      }
    }
  }, [doc])

  function onClick({index}) {
    setSelectedIndex(index)

    fields.iconId.on.set(list[index].id)
  }

  return (
    <A.Block>
      <A.AreaLabel>Activity Icon</A.AreaLabel>
      <A.AreaText>Select one which is appropriate to this activity</A.AreaText>
      {list.map((doc, index) =>
        isView ? (
          index === selectedIndex && (
            <Item
              key={doc.id}
              doc={{
                ...doc,
                as: `AreaIconTT`,
              }}
              onClick={() => onClick({index})}
              isselected={index === selectedIndex}
            />
          )
        ) : (
          <Item
            key={doc.id}
            doc={doc}
            onClick={() => onClick({index})}
            isselected={index === selectedIndex}
          />
        ),
      )}
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas:
    'label text text text text text text text'
    'label icon-tt icon-success icon-bi icon-mechanic icon-agile icon-pm icon-cm';

  
  ${mediaGrid(`sm`)} {
    //xs phone
    grid-template-areas:
      'label text text text text text text text'
      'label icon-tt icon-success icon-bi icon-mechanic icon-agile icon-pm icon-cm'
    grid-template-columns: 20% 15% 15% 15% 15% 15% 15% 15%;
    grid-row-gap: 15px;
  }

  grid-column-gap: 15px;
  grid-template-columns: 20% 7.5% 7.5% 7.5% 7.5% 7.5% 7.5% 7.5%;
  grid-template-rows: 45px;
`

A.AreaLabel = styled(Label)`
  grid-area: label;
`
A.AreaText = styled.div`
  grid-area: text;
  display: flex;
  align-items: center;
  font-size: 0.9em;
`
A.AreaIconSuccess = styled.div`
  grid-area: icon-success;
`

A.AreaIconTT = styled.div`
  grid-area: icon-tt;
`

A.AreaIconBi = styled.div`
  grid-area: icon-bi;
`
A.AreaIconMechanic = styled.div`
  grid-area: icon-mechanic;
`
A.AreaIconAgile = styled.div`
  grid-area: icon-agile;
`
A.AreaIconPm = styled.div`
  grid-area: icon-pm;
`
A.AreaIconCm = styled.div`
  grid-area: icon-cm;
`
