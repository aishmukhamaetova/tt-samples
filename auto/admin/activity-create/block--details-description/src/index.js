import React from 'react'
import styled from 'styled-components'
import {Textarea} from '@tt/component--admin--textarea'
import {fields} from '@tt/domain--admin--activity-create--form'
import {Label} from '@tt/component--admin--label'
import {mediaGrid} from '@tt/ui-admin'

const A = {}

export function BlockAdminActivityCreateTextAreaBlock({
  isView,
  height,
  title,
  typeField,
  placeholder = '',
}) {
  const ctx = fields[typeField].use()

  return (
    <A.Block>
      <A.AreaLabel alignItems={`flex-start`}>{title || 'Title'}</A.AreaLabel>
      <A.AreaInput>
        {isView ? (
          ctx
        ) : (
          <Textarea
            placeholder={placeholder}
            height={height}
            value={ctx}
            onChange={({target: {value}}) => fields[typeField].on.set(value)}
          />
        )}
      </A.AreaInput>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'label input';
  grid-column-gap: 15px;
  grid-template-columns: 20% 420px;
  ${mediaGrid(`sm`)} {
    //xs phone
    grid-template-columns: 20% 300px;
  }
  grid-template-rows: ${props => props.height || `auto`};
  margin-bottom: 15px;
`

A.AreaLabel = styled(Label)`
  grid-area: label;
  align-items: flex-start;
`
A.AreaInput = styled.div`
  && {
    grid-area: input;
    display: flex;
    align-items: center;
    width: 100%;
  }
`
