import React from 'react'
import styled from 'styled-components'
import {
  fields,
  validationerrors
} from '@tt/domain--admin--activity-create--form'
import {Label} from '@tt/component--admin--label'
import {IconAdd} from '@tt/component--admin--icon-add'
import {IconFileVideo} from '@tt/component--admin--video-file'

const A = {}

const makeShort = (videoLink) =>
  videoLink?.length < 10
    ? videoLink
    : `${videoLink?.substr(0, 3)}...${videoLink?.substr(
        videoLink.length - 5,
        5
      )}`

function FieldFile({videoLink = '', originVideoLink, isView, error}) {
  const short = makeShort(videoLink)
  const value = fields.file.use()
  const {name} = value || {}
  return (
    <>
      <A.BlockLabel error={error} isView={isView}>
        <A.AreaIcon isView={isView}>
          {short || videoLink || name ? <A.IconFileVideo /> : <A.IconAdd />}
          {!isView ? (
            <input
              checked={value}
              type="file"
              onChange={({target: {files}}) => fields.file.on.set(files[0])}
            />
          ) : null}
        </A.AreaIcon>
        <A.AreaText error={error}>
          {originVideoLink ? (
            <a href={originVideoLink} target={`_blank`}>
              {short || (name && makeShort(name))}
            </a>
          ) : (
            <>{short || (name && makeShort(name)) || 'Upload Video'}</>
          )}
        </A.AreaText>
      </A.BlockLabel>
      <></>
    </>
  )
}

export function BlockAdminActivityCreateDetailsVideoFile({doc, isView}) {
  let {videoLink} = doc || {}
  const [error] = validationerrors.use().filter((object) => {
    const [key] = Object.keys(object)
    return key == 'file'
  })

  const takeName = (videoLink = '') =>
    Array.isArray(videoLink.match(/((\-)?\w+)+\.mp4/, ''))
      ? videoLink.match(/((\-)?\w+)+\.mp4/, '')[0]
      : ''

  return (
    <A.Block>
      <A.AreaLabel>Video file</A.AreaLabel>
      <A.AreaInput>
        <FieldFile
          error={error?.file ? true : false}
          videoLink={takeName(videoLink)}
          originVideoLink={videoLink}
          isView={isView}
        />
      </A.AreaInput>
      {error?.file ? <A.AreaErrorText>{error?.file}</A.AreaErrorText> : null}
    </A.Block>
  )
}

A.AreaErrorText = styled.div`
  font-size: 14px;
  color: red;
  grid-area: error;
  align-items: flex-start;
  padding-top: 5px;
`
A.Block = styled.div`
  display: grid;
  grid-template-areas:
    'label input'
    'label error';
  grid-column-gap: 15px;
  grid-template-columns:
    20% 1fr
    20% 1fr;
  margin-bottom: 35px;
  align-items: flex-start;
`

A.AreaLabel = styled(Label)`
  grid-area: label;
  align-items: flex-start;
`
A.AreaInput = styled.div`
  grid-area: input;
  display: flex;
  align-items: flex-start;
`
A.BlockLabel = styled.div`
  padding: 5px;
  border: 1px solid red;
  border: ${(props) =>
    props.error && !props.isView
      ? 'solid 1px rgba(255, 0, 0, 0.3)'
      : 'solid 1px rgba(0, 0, 0, 0.3)'};
  border-radius: 5px;
  box-shadow: ${(props) =>
    props.error && !props.isView
      ? '0 4px 5px -4px rgba(255, 0, 0, 0.6)'
      : '0 4px 5px -4px rgba(0, 0, 0, 0.6)'};

  text-align: flex-start;
  width: auto;
  height: 70px;
  display: grid;
  grid-template-areas:
    'icon'
    'text';
`

A.AreaIcon = styled.span`
  && {
    cursor: ${(props) => (props.isView ? 'default' : 'pointer')};
    text-align: center;
    grid-area: icon;
    position: relative;
    input {
      cursor: ${(props) => (props.isView ? 'default' : 'pointer')};
      position: absolute;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      opacity: 0;
      z-index: 2;
    }
    height: 40px;
  }
`
A.IconAdd = styled(IconAdd)`
  && {
    font-size: 1.3em;
    margin: 10px auto;
    position: relative;
    z-index: 1;
  }
`

A.IconFileVideo = styled(IconFileVideo)`
  && {
    font-size: 1.3em;
    margin: 10px auto;
    position: relative;
    z-index: 1;
  }
`
A.AreaText = styled.span`
  grid-area: text;
  height: 20px;
  text-align: center;
  font-size: 0.8em;
  a {
    color: ${(props) => (props.error ? 'red' : '#549add')};
    &:hover {
      text-decoration: none;
    }
  }
`
