import React from 'react'
import styled from 'styled-components'

const A = {}

export function BlockAdminActivityCreateInternalTitle({isCommunity}) {
  return (
    <A.Block>
      <A.AreaTitle>
        {isCommunity ? (
          <span>Community Note</span>
        ) : (
          <span>
            Internal only <span>(optional)</span>
          </span>
        )}
      </A.AreaTitle>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'title';
`

A.AreaTitle = styled.div`
  grid-area: title;
  color: #aaa;
  font-weight: bold;
  span {
    font-weight: normal;
  }
`
