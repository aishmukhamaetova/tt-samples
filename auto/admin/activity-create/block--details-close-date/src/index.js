import React from 'react'
import styled from 'styled-components'
import {Datepicker} from '@tt/component--admin--date'
import {fields} from '@tt/domain--admin--activity-create--form'
import {Label} from '@tt/component--admin--label'
import {formatDate} from '@tt/date-format'

const A = {}

export function BlockAdminActivityCreateDetailsCloseDate({isView}) {
  const dateClose = fields.dateClose.use()
  return (
    <A.Block>
      <A.AreaLabel>Application Close Date</A.AreaLabel>
      <A.AreaInput>
        {isView ? (
          formatDate(dateClose)
        ) : (
          <Datepicker
            format="dd/MM/yyyy"
            minDateMessage=""
            maxDateMessage=""
            value={dateClose}
            onChange={value => fields.dateClose.on.set(value)}
          />
        )}
      </A.AreaInput>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'label input';
  grid-column-gap: 15px;
  grid-template-columns: 20% 1fr;
  grid-template-rows: 85px;
  margin-top: -15px;
`

A.AreaLabel = styled(Label)`
  grid-area: label;
`
A.AreaInput = styled.div`
  grid-area: input;
  display: flex;
  align-items: center;
  width: 320px;
`
