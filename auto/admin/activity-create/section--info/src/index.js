import React from 'react'
import styled from 'styled-components'
import {BlockAdminActivityCreateInfoTitle} from '@tt/block--admin--activity-create--info-title'
import {BlockAdminActivityCreateInfoStatus} from '@tt/block--admin--activity-create--info-status'
import {BlockAdminActivityCreateInfoManager} from '@tt/block--admin--activity-create--info-manager'
import {mediaGrid} from '@tt/ui-admin'

const A = {}

export function SectionAdminActivityCreateInfo({
  isView,
  isActivityStatusDisabled,
}) {
  return (
    <A.Section>
      <A.AreaTitle>
        <BlockAdminActivityCreateInfoTitle isView={isView} />
      </A.AreaTitle>
      <A.AreaStatus>
        <BlockAdminActivityCreateInfoStatus
          isView={isView}
          isActivityStatusDisabled={isActivityStatusDisabled}
        />
      </A.AreaStatus>
      <A.AreaManager>
        <BlockAdminActivityCreateInfoManager isView={isView} />
      </A.AreaManager>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;

  grid-template-areas:
    '. title  status   .'
    '. manager manager .';
  grid-template-columns: 3% 47% 47% 3%;
  grid-row-gap: 15px;
  margin-top: 10px;
`

A.AreaTitle = styled.div`
  grid-area: title;
`
A.AreaStatus = styled.div`
  grid-area: status;
`
A.AreaManager = styled.div`
  grid-area: manager;
`
