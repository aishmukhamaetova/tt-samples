import React from 'react'
import styled from 'styled-components'

const A = {}

export function SectionAdminActivityCreateTitle({onClickEdit, isView}) {
  return (
    <A.Section>
      <A.AreaTitle>{isView ? 'View Activity' : 'New Activity'}</A.AreaTitle>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas: '. title .';
  grid-template-columns: 3% 1fr 3%;
  grid-template-rows: 50px;
`

A.AreaTitle = styled.div`
  grid-area: title;
  font-size: 21px;
  letter-spacing: -0.42px;
  font-weight: bold;
  border-bottom: 1px solid #d8d8d8;
  display: flex;
  align-items: center;
`
