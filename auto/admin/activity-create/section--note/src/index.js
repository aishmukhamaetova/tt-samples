import React from 'react'
import styled from 'styled-components'
import {BlockAdminActivityCreateInternalTitle} from '@tt/block--admin--activity-create--internal-title'
import {BlockAdminCreateInternalNotes} from '@tt/block--admin--activity-create--internal-notes'
import {BlockAdminCreateInternalNotesApplicant} from '@tt/block--admin--applicant-create--internal-notes-applicant'

const A = {}

export function SectionAdminActivityNote({isView, doc, isCommunity}) {
  return (
    <A.Section>
      <A.AreaTitle>
        <BlockAdminActivityCreateInternalTitle
          isCommunity={isCommunity}
          isView={isView}
        />
      </A.AreaTitle>
      <A.AreaNotes>
        {isCommunity ? (
          <BlockAdminCreateInternalNotesApplicant
            doc={doc}
            isCommunity={isCommunity}
            isView={isView}
          />
        ) : (
          <BlockAdminCreateInternalNotes doc={doc} isView={isView} />
        )}
      </A.AreaNotes>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas:
    '. title .'
    '. notes .';
  grid-template-rows: 70px 150px;
  grid-template-columns: 3% 1fr 3%;
  margin-top: 1em;
`

A.AreaTitle = styled.div`
  grid-area: title;
  display: flex;
  align-items: center;
`
A.AreaNotes = styled.div`
  grid-area: notes;
`
