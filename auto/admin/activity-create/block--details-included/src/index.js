import React from 'react'
import styled from 'styled-components'
import {fields} from '@tt/domain--admin--activity-create--form'
import {Label} from '@tt/component--admin--label'
import {ImageBlock} from '@tt/component--admin--image--block'
import {Grid} from '@material-ui/core'

const A = {}

const list = [
  {
    id: 0,
    name: `group0`,
    as: `AreaFirst`,
    block: [
      {
        id: `travel`,
        name: `Travel and Accommodation`,
        imageLink: `/images/opportunity-travel.jpg`,
        as: `AreaFirst`,
      },
      {
        id: `foor`,
        name: `Food`,
        imageLink: `/images/opportunity-food.png`,
        as: `AreaSecond`,
      },
      {
        id: `site`,
        name: `Site Experience`,
        imageLink: `/images/opportunity-site.png`,
        as: `AreaThird`,
      },
    ],
  },
  {
    id: 1,
    name: `group1`,
    as: `AreaSecond`,
    block: [
      {
        id: `home`,
        name: `Work from Home`,
        imageLink: `/images/opportunity-wfh.png`,
        as: `AreaFirst`,
      },
      {
        id: `flex`,
        name: `Flex Working Hours`,
        imageLink: `/images/opportunity-flex.png`,
        as: `AreaSecond`,
      },
      {
        id: `ncomm`,
        name: `No Commuting`,
        imageLink: `/images/opportunity-no-commuting.png`,
        as: `AreaThird`,
      },
    ],
  },
]

const Item = ({doc, isselected, onClick, isView}) => {
  return (
    <Grid item isView={isView}>
      <ImageBlock
        id={doc.id}
        block={doc.block}
        isselected={isselected}
        onClick={onClick}
        isView={isView}
      />{' '}
    </Grid>
  )
}
export function BlockAdminActivityCreateDetailsIncluded(props) {
  const {doc, isView} = props
  const [selectMap, setSelectMap] = React.useState({})

  function select({id}) {
    setSelectMap(state => {
      const r = {
        ...state,
        [id]: !state[id] ?? true,
      }

      fields.included.on.set(
        Object.keys(r)
          .filter(id => r[id])
          .map(id => list[id].name)
          .join(`,`),
      )

      return r
    })
  }

  React.useEffect(() => {
    setSelectMap({})
  }, [isView])

  React.useEffect(() => {
    if (!doc) return

    const includedList = doc.included || []

    for (let include of includedList) {
      const item = list.find(v => v.name === include)
      if (item) {
        select({id: item.id})
      }
    }
  }, [doc])

  function onClick({id}) {
    select({id})
  }

  return (
    <A.Block>
      <A.AreaLabel>What's included</A.AreaLabel>
      <A.AreaText>
        Select one or more inclusions which is appropriate to this activity
      </A.AreaText>
      <A.AreaImage>
        <Grid container spacing={2}>
          {list.map(doc =>
            isView ? (
              selectMap[doc.id] && (
                <Item
                  isView={isView}
                  key={doc.id}
                  doc={{
                    ...doc,
                  }}
                  isselected={selectMap[doc.id]}
                />
              )
            ) : (
              <Item
                key={doc.id}
                doc={doc}
                onClick={() => onClick({id: doc.id})}
                isselected={selectMap[doc.id]}
              />
            ),
          )}
        </Grid>
      </A.AreaImage>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas:
    'label text'
    'label image';
  grid-column-gap: 15px;
  grid-template-columns: 20% 1fr;
  margin-bottom: 25px;
  grid-template-rows: 50px;
`

A.AreaLabel = styled(Label)`
  grid-area: label;
`
A.AreaText = styled.div`
  grid-area: text;
  display: flex;
  align-items: center;
  font-size: 0.9em;
`
A.AreaImage = styled.div`
  grid-area: image;
`
