import React from 'react'
import styled from 'styled-components'
import {Textarea} from '@tt/component--admin--textarea'
import {fields} from '@tt/domain--admin--activity-create--form'
import {Label} from '@tt/component--admin--label'
import {mediaGrid} from '@tt/ui-admin'

const A = {}

export function BlockAdminActivityCreateSummarySummary({isView}) {
  const summary = fields.summary.use()
  return (
    <A.Block>
      <A.AreaLabel alignItems={`flex-start`}>Summary!</A.AreaLabel>
      <A.AreaInput>
        {isView ? (
          summary
        ) : (
          <Textarea
            value={summary}
            onChange={({target: {value}}) => fields.summary.on.set(value)}
          />
        )}
      </A.AreaInput>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'label input';
  grid-column-gap: 15px;
  grid-template-columns: 20% 320px;
  ${mediaGrid(`sm`)} {
    //xs phone
    grid-template-columns: 20% 200px;
  }
  grid-template-rows: 90px;
`

A.AreaLabel = styled(Label)`
  grid-area: label;
`
A.AreaInput = styled.div`
  grid-area: input;
  display: flex;
  align-items: center;
  width: 100%;
`
