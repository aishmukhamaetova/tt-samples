import React from 'react'
import styled from 'styled-components'
import {Input} from '@tt/component--admin--input'
import {Label} from '@tt/component--admin--label'
import {fields} from '@tt/domain--admin--activity-create--form'
import {mediaGrid} from '@tt/ui-admin'

const A = {}

export function BlockAdminActivityCreateSummaryTags({isView}) {
  const tags = fields.tags.use()
  return (
    <A.Block>
      <A.AreaLabel>Tags</A.AreaLabel>
      <A.AreaInput>
        {isView ? (
          tags
        ) : (
          <Input
            placeholder={`Add 3-5 tags which describes the activity`}
            value={tags}
            onChange={({target: {value}}) => fields.tags.on.set(value)}
          />
        )}
      </A.AreaInput>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'label input';
  grid-column-gap: 15px;
  grid-template-columns: 20% 320px;
  ${mediaGrid(`sm`)} {
    //xs phone
    grid-template-columns: 20% 200px;
  }
  grid-template-rows: 45px;
`

A.AreaLabel = styled(Label)`
  grid-area: label;
`
A.AreaInput = styled.div`
  grid-area: input;
  display: flex;
  align-items: center;
`
