import React from 'react'
import styled from 'styled-components'
import {Datepicker} from '@tt/component--admin--date'
import {fields} from '@tt/domain--admin--activity-create--form'
import {Label} from '@tt/component--admin--label'
import {mediaGrid} from '@tt/ui-admin'
import {formatDate} from '@tt/date-format'

const A = {}

export function BlockAdminActivityCreateDetailsStartDate({isView}) {
  const dateStart = fields.dateStart.use()
  return (
    <A.Block>
      <A.AreaLabel>Start Date</A.AreaLabel>
      <A.AreaInput>
        {isView ? (
          formatDate(dateStart)
        ) : (
          <Datepicker
            format="dd/MM/yyyy"
            minDateMessage=""
            maxDateMessage=""
            value={dateStart}
            onChange={value => {
              fields.dateEnd.on.set(value)
              fields.dateStart.on.set(value)
            }}
          />
        )}
      </A.AreaInput>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'label input';
  grid-column-gap: 15px;
  grid-template-columns: 20% 320px;
  ${mediaGrid(`sm`)} {
    //xs phone
    grid-template-columns: 20% 200px;
  }
`

A.AreaLabel = styled(Label)`
  grid-area: label;
`
A.AreaInput = styled.div`
  grid-area: input;
  display: flex;
  align-items: center;
  width: 100%;
`
