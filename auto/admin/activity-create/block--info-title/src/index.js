import React from 'react'
import styled from 'styled-components'
import {Input} from '@tt/component--admin--input'
import {fields} from '@tt/domain--admin--activity-create--form'
import {Label} from '@tt/component--admin--label'
import {mediaGrid} from '@tt/ui-admin'

const A = {}

export function BlockAdminActivityCreateInfoTitle({isView}) {
  const title = fields.title.use()
  const short =
    `${(title.length > 30 && title.substr(0, 30)) || title}${
      title.length > 30 ? `...` : ``
    }` || title

  return (
    <A.Block>
      <A.AreaLabel>Title</A.AreaLabel>
      <A.AreaInput>
        {isView ? (
          short
        ) : (
          <Input
            placeholder={`Activity Title`}
            value={title}
            onChange={({target: {value}}) => fields.title.on.set(value)}
          />
        )}
      </A.AreaInput>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'label input';
  grid-column-gap: 15px;
  grid-template-columns: 40% 320px;

  ${mediaGrid(`lg`)} {
    //sm tablet
    grid-template-columns: 40% 220px;
  }

  ${mediaGrid(`md`)} {
    //sm tablet
    grid-template-columns: 40% 150px;
  }

  ${mediaGrid(`sm`)} {
    //xs phone
    grid-template-columns: 40% 100px;
  }

  grid-template-rows: 45px;
`

A.AreaLabel = styled(Label)`
  grid-area: label;
`
A.AreaInput = styled.div`
  grid-area: input;
  display: flex;
  align-items: center;
`
