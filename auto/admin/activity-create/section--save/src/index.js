import React from 'react'
import styled from 'styled-components'
import {ButtonSave} from '@tt/component--admin--button-save'
import {runSubmit} from './domain-run-submit'
import {runQueryVideoLink} from './run-query-video-link'
import {
  fields,
  validationerrors,
} from '@tt/domain--admin--activity-create--form'

const A = {}

export function SectionAdminActivityCreateSave(props) {
  const {doc} = props
  const file = fields.file.use()
  const errors = validationerrors.use() || []
  return (
    <A.Section>
      <A.AreaSave>
        <ButtonSave
          disabled={errors.length > 0}
          onClick={async () => {
            if (errors.length > 0) {
              return
            }
            try {
              if (file) {
                const r = await runQueryVideoLink.run({
                  activityId: `12345`,
                  filename: `filename.mp4`,
                  type: `video/mp4`,
                })

                const [fromS3BucketVideoLink] = r.link.split(`?`)
                if (r.errors.length === 0) {
                  const rSubmit = await runSubmit.run({
                    id: doc?.id,
                    videoLink: fromS3BucketVideoLink,
                  })

                  if (rSubmit.errors.length === 0) {
                    props.onSaveSuccess?.({
                      doc: rSubmit.activityList?.[0] ?? rSubmit.activity,
                    })
                  }
                }
              } else {
                const rSubmit = await runSubmit.run({
                  id: doc?.id,
                })

                if (rSubmit.errors.length === 0) {
                  props.onSaveSuccess?.({
                    doc: rSubmit.activityList?.[0] ?? rSubmit.activity,
                  })
                }
              }
            } catch (err) {
              console.log(`ERROR`, err)
            }
          }}
        />
      </A.AreaSave>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas: '. save .';
  grid-template-columns: 3% 1fr 3%;
  grid-template-rows: 80px;
`

A.AreaSave = styled.div`
  grid-area: save;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`
