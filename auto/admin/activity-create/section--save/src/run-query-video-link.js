import createDebugger from 'debug'
import {createStore} from 'effector'
import {makeCoeffect} from '@tt/effector'
import {config} from '@tt/domains'
import {runUploadVideo} from './run-upload-video'

const who = [`@tt`, `page-admin-activity-create`, `run-query-video-link`]

const watcher = createDebugger(who.join(`:`))
const debug = watcher

const storeFake = createStore(null)

export const runQueryVideoLink = makeCoeffect(
  {
    store: storeFake,
    async handler({state, params}) {
      debug(`state`, state)
      debug(`params`, params)

      const {activityId, filename, type} = params

      const body = {
        activityId,
        filename,
        type,
      }

      const url = `${config.API}/api/v1/activity/query-video-link`
      const options = {
        method: `POST`,
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      }
      const res = await fetch(url, options)
      const r = await res.json()

      debug(`RESULT`, r)

      return r
    },
  },
  {watcher},
)

runQueryVideoLink.run.done.watch(({result, params}) => {
  debug(`runQueryVideoLink.done.watch`)

  if (result.errors.length === 0) {
    const {type} = params
    const {link} = result

    runUploadVideo.run({
      link,
      type,
    })
  }
})
