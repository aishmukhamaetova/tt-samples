import createDebugger from 'debug'
import {makeCoeffect} from '@tt/effector'
import {$form} from '@tt/domain--admin--activity-create--form'
import {config} from '@tt/domains'

const who = [`@tt`, `page-admin-activity-create`, `run-submit`]

const watcher = createDebugger(who.join(`:`))
const debug = watcher

export const runSubmit = makeCoeffect(
  {
    store: $form,
    async handler({state, params}) {
      debug(`state`, state)
      debug(`params`, params)

      const body = {
        tags: ['tag1', 'tag2', 'tag3'],
        iconId: 'icon1',
        location: ['location1', 'location2'],
        duration: 1,
        durationType: `weeks`,
        dateStart: '2020-01-15T00:00:00.000Z',
        dateEnd: '2020-01-20T00:00:00.000Z',
        included: ['group1', 'group2'],
        aboutTeam: 'About team',
        dateClose: '2020-01-14T00:00:00.000Z',
        videoLink: 'Some link to mp4',
        comment: 'Private notes about user',
        ...state,
        ...params,
      }

      body.comment = state.notes
      body.location =
        state.location.trim() === `` ? [] : state.location.split(`,`) || []
      body.tags =
        state.tags.trim() === ``
          ? []
          : state.tags.split(`,`).map(v => v.trim()) || []
      body.included =
        state.included.trim() === `` ? [] : state.included.split(`,`) || []

      const url = body.id
        ? `${config.API}/api/v1/activity/update`
        : `${config.API}/api/v1/activity/create`
      const options = {
        method: `POST`,
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      }
      const res = await fetch(url, options)
      const r = await res.json()

      debug(`RESULT???`, r)

      return r
    },
  },
  {watcher},
)
