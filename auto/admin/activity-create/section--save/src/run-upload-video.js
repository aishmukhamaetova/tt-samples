import createDebugger from 'debug'
import {makeCoeffect} from '@tt/effector'
import {$form} from '@tt/domain--admin--activity-create--form'

const who = [`@tt`, `page-admin-activity-create`, `run-upload-video`]

const watcher = createDebugger(who.join(`:`))
const debug = watcher

const ACL = `public-read`

function createCORSRequest(method, url) {
  var xhr = new XMLHttpRequest()

  if (xhr.withCredentials != null) {
    xhr.open(method, url, true)
  } else if (typeof XDomainRequest !== 'undefined') {
    xhr = new XDomainRequest() // eslint-disable-line
    xhr.open(method, url)
  } else {
    xhr = null
  }

  return xhr
}

const fetchRequest = ({file, link, type}) =>
  new Promise((resolve, reject) => {
    const xhr = createCORSRequest(`PUT`, link)

    xhr.onload = () => {
      if (xhr.status === 200) {
        resolve(xhr)
      } else {
        reject(xhr)
      }
    }

    xhr.setRequestHeader(`Content-Type`, type)
    xhr.setRequestHeader(`x-amz-acl`, ACL)

    xhr.send(file)
  })

export const runUploadVideo = makeCoeffect(
  {
    store: $form,
    async handler({state, params}) {
      const {file} = state
      const {link, type} = params

      const xhr = await fetchRequest({link, file, type})

      debug(`xhr`, xhr)

      return xhr
    },
  },
  {watcher},
)
