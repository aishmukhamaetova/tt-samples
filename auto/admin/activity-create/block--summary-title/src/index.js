import React from 'react'
import styled from 'styled-components'

const A = {}

export function BlockAdminActivityCreateSummaryTitle(props) {
  return (
    <A.Block>
      <A.AreaTitle>Activity Summary</A.AreaTitle>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'title';
  grid-template-rows: 50px;
  border-top: 1px solid #d8d8d8;
`

A.AreaTitle = styled.div`
  grid-area: title;
  color: #aaa;
  font-size: 18px;
  font-weight: bold;
  letter-spacing: -0.36px;
  display: flex;
  align-items: center;
`
