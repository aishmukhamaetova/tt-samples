import React from 'react'
import styled from 'styled-components'
import {BlockAdminActivityDetailButtonEdit} from '@tt/block--admin--activity-detail--button-edit'

const A = {}

export function SectionAdminActivityTitle(props) {
  const {
    doc,
    isView,
    isCreate,
    isEdit,
    isDetail,
    onClickEdit,
    onClickView,
    username,
  } = props

  const toUppercase = username =>
    username.charAt(0).toUpperCase() + username.substring(1)

  return (
    <A.Section>
      <A.AreaTitle>
        {isView
          ? `Activity Overview`
          : username
          ? toUppercase(username)
          : isCreate
          ? `New Activity`
          : isEdit
          ? `Edit Activity`
          : isDetail
          ? doc?.title || `Activity detail`
          : ''}
      </A.AreaTitle>
      <A.AreaButtonTools>
        {isView || isDetail ? (
          <BlockAdminActivityDetailButtonEdit
            onClickEdit={onClickEdit}
            onClickView={onClickView}
            isDetail={isDetail}
          />
        ) : null}
      </A.AreaButtonTools>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas: '. title button-tools .';
  grid-template-columns: 3% 1fr 1fr 3%;
  grid-template-rows: 50px;
  border-bottom: 1px solid #d8d8d8;
  margin-bottom: 15px;
`

A.AreaTitle = styled.div`
  grid-area: title;
  font-size: 21px;
  letter-spacing: -0.42px;
  font-weight: bold;
  display: flex;
  align-items: center;
`
A.AreaButtonTools = styled.div`
  grid-area: button-tools;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`
