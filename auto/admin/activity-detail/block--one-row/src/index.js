import React from 'react'
import styled from 'styled-components'
import {Checkbox} from '@tt/component--admin--checkbox'
import {Application} from '@tt/domains'
import {Select} from '@tt/component--admin--select'

import {IconComment} from '@tt/ui-admin'
import Typography from '@material-ui/core/Typography'
import Tooltip from '@material-ui/core/Tooltip'
import {withStyles} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import {formatDate} from '@tt/date-format'
import {fromActivity} from '@tt/domains'
import {useStore} from 'effector-react'

const A = {}

const HtmlTooltip = withStyles(theme => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 220,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
  },
}))(Tooltip)

function formatLink(v) {
  try {
    return v.split(`/`).slice(-1)[0]
  } catch (err) {
    return ``
  }
}

const optionsDefault = [
  {id: Application.statusMap.RECEIVED, name: `Received`},
  {id: Application.statusMap.REVIEW, name: `Review`},
  {id: Application.statusMap.SHORTLIST, name: `Shortlist`},
  {id: Application.statusMap.SUCCESSFUL, name: `Successful`},
  {id: Application.statusMap.UNSUCCESSFUL, name: `Unsuccessful`},
  {id: Application.statusMap.WITHDRAWN, name: `Withdrawn`},
]

function getSelectProps(status = `received`, activityStatus) {
  if (status === Application.statusMap.WITHDRAWN) {
    return {
      value: Application.statusMap.WITHDRAWN,
      options: [
        ...optionsDefault,
        {id: Application.statusMap.WITHDRAWN, name: `Withdrawn`},
      ],
    }
  }
  if (activityStatus === `filled` || activityStatus === `cancelled`) {
    if (status === Application.statusMap.SUCCESSFUL) {
      return {
        value: Application.statusMap.SUCCESSFUL,
        options: [{id: Application.statusMap.SUCCESSFUL, name: `Successful`}],
      }
    } else {
      return {
        value: Application.statusMap.UNSUCCESSFUL,
        options: [
          {id: Application.statusMap.UNSUCCESSFUL, name: `Unsuccessful`},
        ],
      }
    }
  }

  return {
    value: status,
    options: optionsDefault,
  }
}

export function BlockAdminActivityDetailOneRow(props) {
  const {activityDoc, doc} = props
  const isDisabled =
    doc.status === Application.statusMap.WITHDRAWN ||
    activityDoc.status === `filled` ||
    activityDoc.status === `cancelled`

  const {value, options} = getSelectProps(doc.status, activityDoc.status)
  const all = useStore(fromActivity.$.all)

  return (
    <A.Block>
      <A.AreaApplied>{formatDate(doc.createdAt)}</A.AreaApplied>
      <A.AreaName>{doc?.user?.name}</A.AreaName>
      <A.AreaEmail>{doc?.user?.email}</A.AreaEmail>
      <A.AreaPhone>{doc?.user?.phone}</A.AreaPhone>
      <A.AreaVideo>
        {doc.videoLink ? (
          <A.Link href={doc.videoLink}>{formatLink(doc.videoLink)}</A.Link>
        ) : null}
      </A.AreaVideo>
      <A.AreaAudio>
        {doc.audioLink ? (
          <A.Link href={doc.audioLink}>{formatLink(doc.audioLink)}</A.Link>
        ) : null}
      </A.AreaAudio>
      <A.AreaText>
        {doc?.text ? (
          <HtmlTooltip
            placement="left-end"
            title={<Typography color="inherit">{doc?.text}</Typography>}
          >
            <Button>
              <IconComment />
            </Button>
          </HtmlTooltip>
        ) : null}
      </A.AreaText>
      <A.AreaStatus>
        <Select
          options={options}
          valueById={value}
          isDisabled={isDisabled}
          onChange={({id: status}) =>
            props.onChangeStatus({id: doc.id, status})
          }
        />
      </A.AreaStatus>
      <A.AreaAll>
        <Checkbox
          checked={doc?.selected}
          handleChange={v => {
            fromActivity.on.onSelectOne({
              actId: activityDoc?.id,
              applId: doc?.id,
              v,
            })
          }}
        />
      </A.AreaAll>
    </A.Block>
  )
}

A.Link = styled.a`
  color: #549add;
  text-decoration: underline;
  cursor: pointer;

  &:hover {
    text-decoration: none;
  }
`
A.Block = styled.div`
  display: grid;
  grid-template-areas: 'applied name email phone video audio text status all';
  font-size: 14px;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 50px;
  padding-top: 10px;
  padding-bottom: 5px;
  width: 100%;
`

A.AreaApplied = styled.div`
  grid-area: applied;
  display: flex;
  align-items: center;
`
A.AreaName = styled.div`
  grid-area: name;
  display: flex;
  align-items: center;
`
A.AreaEmail = styled.div`
  grid-area: email;
  display: flex;
  align-items: center;
`
A.AreaPhone = styled.div`
  grid-area: phone;
  display: flex;
  align-items: center;
`
A.AreaVideo = styled.div`
  grid-area: video;
  display: flex;
  align-items: center;
`
A.AreaAudio = styled.div`
  grid-area: audio;
  display: flex;
  align-items: center;
`
A.AreaText = styled.div`
  grid-area: text;

  font-size: 0.9em;
  display: flex;
  align-items: center;
  justify-content: center;

  cursor: pointer;
`
A.AreaStatus = styled.div`
  grid-area: status;
  display: flex;
  align-items: center;
`
A.AreaAll = styled.div`
  grid-area: all;
  display: flex;
  align-items: center;
`
