import React from 'react'
import styled from 'styled-components'
import {BlockAdminActivityDetailInfoStatus} from '@tt/block--admin--activity-detail--info'
import {formatDate} from '@tt/date-format'

const A = {}

export function SectionAdminActivityDetailInfo(props) {
  const {activityDoc} = props
  return (
    <A.Section>
      <A.AreaTags>{activityDoc?.tags?.join(`,`)}</A.AreaTags>
      <A.AreaLocation>{activityDoc?.location?.join(`,`)}</A.AreaLocation>
      <A.AreaManager>{activityDoc?.hiringManagerId}</A.AreaManager>
      <A.AreaStatus>
        <BlockAdminActivityDetailInfoStatus
          activityDoc={activityDoc}
          onSelectChange={props.onSelectChange}
        />
      </A.AreaStatus>
      <A.AreaOpenDate>
        Open Date: {formatDate(activityDoc?.createdAt)}
      </A.AreaOpenDate>
      <A.AreaCloseDate>
        Close Date: {formatDate(activityDoc?.dateClose)}
      </A.AreaCloseDate>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas:
    '. tags     status open-date  .'
    '. location status close-date .'
    '. manager  status .          .';
  grid-template-columns: 3% 1fr 1fr 1fr 3%;
  padding-top: 10px;
`

A.AreaTags = styled.div`
  grid-area: tags;
`
A.AreaLocation = styled.div`
  grid-area: location;
`
A.AreaManager = styled.div`
  grid-area: manager;
`
A.AreaStatus = styled.div`
  grid-area: status;
  display: flex;
  justify-content: center;
`
A.AreaOpenDate = styled.div`
  grid-area: open-date;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`
A.AreaCloseDate = styled.div`
  grid-area: close-date;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`
