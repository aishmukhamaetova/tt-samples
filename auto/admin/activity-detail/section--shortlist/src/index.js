import React from 'react'
import styled from 'styled-components'
import {BlockAdminActivityDetailShortlistButtonCsv} from '@tt/block--admin--activity-detail--shortlist--button-csv'
import {ButtonShortlist} from '@tt/component--admin--button-shortlist'

const A = {}

export function SectionAdminActivityDetailShortlist({activityDoc = {}}) {
  // const list = useStore(fromActivity.$.list)

  const toUppercase = username =>
    username?.charAt(0)?.toUpperCase() + username?.substring(1)

  const dataListPrepare = activityDoc => {
    let applicationList = []

    const {
      tags = [],
      location = [],
      comment,
      id,
      hiringManagerId,
      title,
    } = activityDoc
    const activityId = id
    const selected =
      Array.isArray(activityDoc.applicationList) &&
      activityDoc.applicationList?.filter(appl => appl.selected)

    if (selected.length > 0) {
      applicationList.push(
        selected.map(application => {
          const type = application?.audioLink
            ? `Audio`
            : application?.videoLink
            ? `Video`
            : `Writting`

          const {
            id,
            applicationList,
            favouriteActivities,
            selected,
            userId,
            applicationsCount,
            updatedAt,
            included,
            iconId,
            user,
            hiringManagerId,
            title,
            skills,
            createdAt,
            activityId,
            audioLink,
            videoLink,
            text,
            status,
            ...props
          } = {
            ...application,
            ActivityTitle: activityDoc?.title,
            HiringManager: toUppercase(activityDoc?.hiringManagerId),
            ApplicantName: application?.user?.name,
            Skills: application?.user?.skills,
            Phone: application?.user?.phone,
            Email: application?.user?.email,
            [`Application`]: `${type}: ${application?.audioLink ||
              application?.videoLink ||
              application?.text}`,
            InternalFeedback: comment,
          }
          return {...props}
        }),
      )
      applicationList = applicationList.filter(i => i.length)
      const [dataList = []] = applicationList
      return dataList
    }
  }

  return (
    <A.Section>
      <A.AreaFlex></A.AreaFlex>
      <A.AreaCsv>
        <BlockAdminActivityDetailShortlistButtonCsv
          dataList={dataListPrepare(activityDoc)}
        />
      </A.AreaCsv>
      <A.AreaShortlist>{/* <ButtonShortlist /> */}</A.AreaShortlist>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas: '. flex csv .';
  grid-template-columns: 3% 1fr 50px 3%;
  padding-top: 25px;
`

A.AreaFlex = styled.div`
  grid-area: flex;
  border-bottom: 1px solid #d8d8d8;
`
A.AreaCsv = styled.div`
  grid-area: csv;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding-bottom: 5px;
  border-bottom: 1px solid #d8d8d8;
`
A.AreaShortlist = styled.div`
  grid-area: shortlist;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding-bottom: 5px;
  border-bottom: 1px solid #d8d8d8;
`
