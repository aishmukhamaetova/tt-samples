import React from 'react'
import styled from 'styled-components'
import {IconCsv} from '@tt/component--admin--icon-csv'
import {CSVLink, CSVDownload} from 'react-csv'
const A = {}

export function BlockAdminActivityDetailShortlistButtonCsv({dataList = []}) {
  return (
    <A.Block>
      <A.AreaIcon disabled={!(dataList.length > 0)}>
        {dataList.length ? (
          <CSVLink data={dataList}>
            <IconCsv disabled={false} />
          </CSVLink>
        ) : (
          <IconCsv disabled={true} />
        )}
      </A.AreaIcon>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'icon';
`

A.AreaIcon = styled.div`
  grid-area: icon;
  cursor: ${({disabled}) => (disabled ? `default` : `pointer`)};
`
