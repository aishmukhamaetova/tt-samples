import React from 'react'
import styled from 'styled-components'
import {IconEdit} from '@tt/component--admin--icon-edit'
import {IconView} from '@tt/component--admin--icon-view'

const A = {}

export function BlockAdminActivityDetailButtonEdit({
  onClickEdit,
  onClickView,
  isDetail,
}) {
  return (
    <A.Section>
      {isDetail ? (
        <A.AreaView onClick={onClickView}>
          <IconView />
        </A.AreaView>
      ) : null}
      <A.AreaEdit onClick={onClickEdit}>
        <IconEdit />
      </A.AreaEdit>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-column-gap: 15px;
  grid-template-areas: 'view edit';
  cursor: pointer;
  user-select: none;
`

A.AreaEdit = styled.div`
  grid-area: edit;
`
A.AreaView = styled.div`
  grid-area: view;
`
