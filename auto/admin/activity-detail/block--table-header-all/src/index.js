import React from 'react'
import styled from 'styled-components'
import {Checkbox} from '@tt/component--admin--checkbox'
import {fromActivity} from '@tt/domains'
import {useStore} from 'effector-react'

const A = {}

export function BlockAdminActivityDetailTableHeaderAll({onSelectAll, id}) {
  const all = useStore(fromActivity.$.all)
  const list = useStore(fromActivity.$.list)
  const activityDoc = list.find(doc => doc.id === id) || {}
  const {applicationList = []} = activityDoc

  const setAll = () => {
    const selected = applicationList?.filter(i => i.selected) || []
    return selected?.length < applicationList?.length ? false : all?.v
  }

  const allLocal = setAll()
  return (
    <A.Block>
      <A.AreaCheckbox
        onClick={() => fromActivity.on.onSelectAll({id, v: allLocal})}
      >
        <Checkbox checked={allLocal} />
      </A.AreaCheckbox>
      <A.AreaAll>All</A.AreaAll>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'checkbox all';
`

A.AreaCheckbox = styled.div`
  grid-area: checkbox;
  display: flex;
  align-items: center;
`
A.AreaAll = styled.div`
  grid-area: all;
  display: flex;
  align-items: center;
`
