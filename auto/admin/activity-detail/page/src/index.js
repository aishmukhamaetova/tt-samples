import React from 'react'
import styled from 'styled-components'
import {SectionAdminHeader} from '@tt/section--admin--header'
import {SectionAdminBack} from '@tt/section--admin--back'
import {SectionAdminActivityTitle} from '@tt/section--admin--activity-detail--title'
import {SectionAdminActivityDetailInfo} from '@tt/section--admin--activity-detail--info'
import {SectionAdminActivityDetailShortlist} from '@tt/section--admin--activity-detail--shortlist'
import {SectionAdminActivityDetailTable} from '@tt/section--admin--activity-detail--table'
import {useStore} from 'effector-react'
import {fromActivity} from '@tt/domains'
import {useParams} from 'react-router-dom'
import {useHistory} from 'react-router-dom'

const A = {}

export function PageAdminActivityDetail() {
  const history = useHistory()
  const {id} = useParams()
  //look-here
  const list = useStore(fromActivity.$.list)
  const activityDoc = list.find(doc => doc.id === id)

  React.useEffect(() => {
    fromActivity.run.queryAdmin()
  }, [])

  function onClickEdit() {
    history.push(`/activity/edit/${id}`)
  }
  function onClickView() {
    history.push(`/activity/view/${id}`)
  }

  return (
    <A.Page>
      <A.AreaHeader>
        <SectionAdminHeader />
      </A.AreaHeader>
      <A.AreaBack>
        <SectionAdminBack />
      </A.AreaBack>
      <A.AreaTitle>
        <SectionAdminActivityTitle
          doc={activityDoc}
          onClickEdit={onClickEdit}
          onClickView={onClickView}
          isDetail={true}
        />
      </A.AreaTitle>
      <A.AreaInfo>
        <SectionAdminActivityDetailInfo
          activityDoc={activityDoc}
          onSelectChange={({id: status}) =>
            fromActivity.run.update({id, status})
          }
        />
      </A.AreaInfo>
      <A.AreaShortlist>
        <SectionAdminActivityDetailShortlist activityDoc={activityDoc} />
      </A.AreaShortlist>
      <A.AreaTable>
        <SectionAdminActivityDetailTable
          id={id}
          activityDoc={activityDoc}
          onChangeStatus={payload =>
            fromActivity.run.updateApplication(payload)
          }
        />
      </A.AreaTable>
    </A.Page>
  )
}

A.Page = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'back'
    'title'
    'info'
    'shortlist'
    'table';
`

A.AreaHeader = styled.div`
  grid-area: header;
`
A.AreaBack = styled.div`
  grid-area: back;
`
A.AreaTitle = styled.div`
  grid-area: title;
`
A.AreaInfo = styled.div`
  grid-area: info;
`
A.AreaShortlist = styled.div`
  grid-area: shortlist;
`
A.AreaTable = styled.div`
  grid-area: table;
`
