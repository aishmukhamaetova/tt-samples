import React from 'react'
import styled from 'styled-components'
import {Checkbox} from '@tt/component--admin--checkbox'

const A = {}

export function BlockAdminActivityDetailTableRow(props) {
  return (
    <A.Block>
      <A.AreaApplied>02/11/2019</A.AreaApplied>
      <A.AreaName>Jane Alpine</A.AreaName>
      <A.AreaEmail>jenethelady@janemechanic...</A.AreaEmail>
      <A.AreaPhone>+64 555 666 777</A.AreaPhone>
      <A.AreaVideo>IMG_7018.MOV</A.AreaVideo>
      <A.AreaAudio>Oz Mechanic.m4a</A.AreaAudio>
      <A.AreaText>Text Upload</A.AreaText>
      <A.AreaStatus>select field</A.AreaStatus>
      <A.AreaAll>
        <Checkbox />
      </A.AreaAll>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'applied name email phone video audio text status all';
  font-size: 14px;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 50px;
  padding-top: 10px;
  padding-bottom: 5px;
  width: 100%;
`

A.AreaApplied = styled.div`
  grid-area: applied;
  display: flex;
  align-items: center;
`
A.AreaName = styled.div`
  grid-area: name;
  display: flex;
  align-items: center;
`
A.AreaEmail = styled.div`
  grid-area: email;
  display: flex;
  align-items: center;
`
A.AreaPhone = styled.div`
  grid-area: phone;
  display: flex;
  align-items: center;
`
A.AreaVideo = styled.div`
  grid-area: video;
  display: flex;
  align-items: center;
  a {
    color: black;
    &:hover {
      text-decoration: none;
    }
  }
`
A.AreaAudio = styled.div`
  grid-area: audio;
  display: flex;
  align-items: center;
  a {
    color: black;
    &:hover {
      text-decoration: none;
    }
  }
`
A.AreaText = styled.div`
  grid-area: text;
  display: flex;
  align-items: center;
`
A.AreaStatus = styled.div`
  grid-area: status;
  display: flex;
  align-items: center;
`
A.AreaAll = styled.div`
  grid-area: all;
  display: flex;
  align-items: center;
`
