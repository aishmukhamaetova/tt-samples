import React from 'react'
import styled from 'styled-components'
import {BlockAdminActivityDetailTableHeader} from '@tt/block--admin--activity-detail--table-header'
import {BlockAdminActivityDetailTableRows} from '@tt/block--admin--activity-detail--table--rows'

const A = {}

export function SectionAdminActivityDetailTable(props) {
  const {activityDoc, id} = props

  const [sortType, setSortType] = React.useState(null)

  return (
    <A.Section>
      <A.AreaHeader>
        <BlockAdminActivityDetailTableHeader
          id={id}
          onSwitchSort={() => setSortType(v => !v)}
        />
      </A.AreaHeader>
      <A.AreaTable>
        <BlockAdminActivityDetailTableRows
          activityDoc={activityDoc}
          sortType={sortType}
          onChangeStatus={props.onChangeStatus}
        />
      </A.AreaTable>
    </A.Section>
  )
}

A.Section = styled.div`
  display: grid;
  grid-template-areas:
    '. header .'
    '. table  .';
  grid-template-columns: 3% 1fr 3%;
  padding-top: 10px;
`

A.AreaHeader = styled.div`
  grid-area: header;
  display: flex;
  justify-content: center;
`
A.AreaTable = styled.div`
  grid-area: table;
  display: flex;
  justify-content: center;
`
