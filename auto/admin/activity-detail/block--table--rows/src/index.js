import React from 'react'
import styled from 'styled-components'
import {BlockAdminActivityDetailOneRow} from '@tt/block--admin--activity-detail--one-row'

const A = {}

function sortByName(sortType, list) {
  if (sortType === null) {
    return [...list].sort(
      (a, b) => new Date(b.createdAt) - new Date(a.createdAt)
    )
  }

  try {
    return sortType
      ? [...list].sort((a, b) => a.user.name.localeCompare(b.user.name))
      : [...list].sort((a, b) => b.user.name.localeCompare(a.user.name))
  } catch (err) {
    return list
  }
}

export function BlockAdminActivityDetailTableRows(props) {
  const {activityDoc} = props

  const list = sortByName(props.sortType, activityDoc?.applicationList || [])

  return (
    <A.Block>
      {list.map((doc) => (
        <A.AreaRow key={doc.id}>
          <BlockAdminActivityDetailOneRow
            activityDoc={activityDoc}
            doc={doc}
            onChangeStatus={props.onChangeStatus}
          />
        </A.AreaRow>
      ))}
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'row';
  grid-auto-rows: auto;

  width: 100%;
`

A.AreaRow = styled.div``
