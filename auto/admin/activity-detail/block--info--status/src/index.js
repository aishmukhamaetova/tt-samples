import React from 'react'
import styled from 'styled-components'
import {Select} from '@tt/component--admin--select'

const A = {}

export function BlockAdminActivityDetailInfoStatus(props) {
  const {activityDoc} = props

  return (
    <A.Block>
      <A.AreaStatus>Status:</A.AreaStatus>
      <A.AreaSelect>
        <Select
          valueById={activityDoc?.status}
          onChange={props.onSelectChange}
        />
      </A.AreaSelect>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'status select';
  grid-template-rows: 45px;
  grid-template-columns: 100px 200px;
`

A.AreaStatus = styled.div`
  grid-area: status;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding-right: 25px;
`
A.AreaSelect = styled.div`
  grid-area: select;
  display: flex;
  align-items: center;
`
