import React from 'react'
import styled from 'styled-components'
import {BlockAdminActivityDetailTableHeaderAll} from '@tt/block--admin--activity-detail--table-header-all'
import {IconFilter} from '@tt/ui-admin'

const A = {}

export function BlockAdminActivityDetailTableHeader(props) {
  return (
    <A.Block>
      <A.AreaApplied>Applied on</A.AreaApplied>
      <A.AreaName>
        <div onClick={props.onSwitchSort}>
          Name
          <IconFilter />
        </div>
      </A.AreaName>
      <A.AreaEmail>Email</A.AreaEmail>
      <A.AreaPhone>Phone</A.AreaPhone>
      <A.AreaVideo>Video</A.AreaVideo>
      <A.AreaAudio>Audio</A.AreaAudio>
      <A.AreaText>Text</A.AreaText>
      <A.AreaStatus>Application status</A.AreaStatus>
      <A.AreaAll>
        <BlockAdminActivityDetailTableHeaderAll id={props?.id} />
      </A.AreaAll>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'applied name email phone video audio text status all';
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 50px;
  font-weight: bold;
  padding-top: 10px;
  padding-bottom: 5px;
  border-bottom: 1px solid #d8d8d8;
  width: 100%;
`

A.AreaApplied = styled.div`
  grid-area: applied;
  display: flex;
  align-items: center;
`
A.AreaName = styled.div`
  grid-area: name;
  display: flex;
  align-items: center;
  cursor: pointer;
`
A.AreaEmail = styled.div`
  grid-area: email;
  display: flex;
  align-items: center;
`
A.AreaPhone = styled.div`
  grid-area: phone;
  display: flex;
  align-items: center;
`
A.AreaVideo = styled.div`
  grid-area: video;
  display: flex;
  align-items: center;
  a {
    color: black;
    &:hover {
      text-decoration: none;
    }
  }
`
A.AreaAudio = styled.div`
  grid-area: audio;
  display: flex;
  align-items: center;
  a {
    color: black;
    &:hover {
      text-decoration: none;
    }
  }
`
A.AreaText = styled.div`
  grid-area: text;
  display: flex;
  align-items: center;
  justify-content: center;
`
A.AreaStatus = styled.div`
  grid-area: status;
  display: flex;
  align-items: center;
`
A.AreaAll = styled.div`
  grid-area: all;
`
