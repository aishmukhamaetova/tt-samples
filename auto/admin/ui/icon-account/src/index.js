import React from 'react'
import styled from 'styled-components'
import {FaRegUserCircle} from 'react-icons/fa'

const A = {}

export function IconAccount({className}) {
  return (
    <A.Icon className={className} />
  )
}

A.Icon = styled(FaRegUserCircle)`
`