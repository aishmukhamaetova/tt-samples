import React from 'react'
import {default as __TextField} from '@material-ui/core/TextField';
import styled from 'styled-components'

const A = {}

export function Input(props) {
  return (
    <A.TextField
      variant="outlined"
      size="small"
      fullWidth
      {...props}
    />
  )
}

A.TextField = styled(__TextField)`
`