import React from 'react'
import styled from 'styled-components'
import {default as __Select} from 'react-select'

const A = {}

const options = [
  {id: `weeks`, name: `Weeks`},
  {id: `months`, name: `Months`},
]

const styles = {
  container: base => ({
    ...base,
    flex: 1,
  }),
}

export function Select({valueById, ...props}) {
  const value = valueById
    ? options.find(({id}) => id === valueById)
    : props.value

  return (
    <A.Select
      options={options}
      isClearable={false}
      isSearchable={true}
      styles={styles}
      getOptionLabel={({name}) => name}
      getOptionValue={({id}) => id}
      {...props}
      value={value}
      placeholder={`Select`}
      theme={theme => ({
        ...theme,
        colors: {
          ...theme.colors,
          primary25: 'rgba(254,184,43,0.5)',
          primary: 'rgba(254,184,43,1)',
        },
      })}
    />
  )
}

A.Select = styled(__Select)`
  && {
    .css-1wa3eu0-placeholder {
      color: rgba(0, 0, 0, 0.38);
    }
  }
`
