import React from 'react'
import styled from 'styled-components'

const A = {}

export function Label({children, alignItems}) {
  return <A.Label alignItems={alignItems}>{children}</A.Label>
}

A.Label = styled.div`
  font-weight: bold;
  display: flex;
  align-items: ${props => (props.alignItems ? props.alignItems : 'center')};
  padding-top: ${props => (props.alignItems === 'flex-start' ? `5px` : `0`)};
  font-size: 1em;
`
