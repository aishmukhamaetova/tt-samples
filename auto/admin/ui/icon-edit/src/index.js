import React from 'react'
import styled from 'styled-components'
import {FaPencilAlt} from 'react-icons/fa'

const A = {}

export function IconEdit({className}) {
  return (
    <A.Icon className={className} />
  )
}

A.Icon = styled(FaPencilAlt)`
`