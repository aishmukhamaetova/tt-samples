import React from 'react'
import styled from 'styled-components'

const A = {}

export function ImageIcon({
  className,
  isselected,
  alt = '',
  src,
  title,
  onClick,
}) {
  return (
    <A.Select isselected={isselected} onClick={onClick}>
      <A.Image className={className} atl={alt} src={src} />
      <A.Label>{alt}</A.Label>
    </A.Select>
  )
}

A.Select = styled.div`
  cursor: pointer;
  background-color: ${props =>
    props.isselected ? 'rgba(254, 184, 43, 0.5)' : 'none'};
  border: solid 1px
    ${props => (props.isselected ? 'rgba(0, 0, 0, 0.3)' : 'transparent')};
  border-radius: 5px;
  box-shadow: ${props =>
    props.isselected ? '0 4px 5px -4px rgba(0, 0, 0, 0.6)' : 'none'};
  padding: 5px;
  text-align: center;
  width: 70px;
`

A.Image = styled.img`
  width: 60px;
  height: 60px;
  &.tt {
    width: 53px;
    height: 53px;
    border-radius: 5px;
    box-shadow: 0px 1px 2px 1px rgba(0, 0, 0, 0.1);
    border: 1px solid rgba(0, 0, 0, 0.2);
  }
`
A.Label = styled.div`
  font-size: 0.8em;
  height: 40px;
`
