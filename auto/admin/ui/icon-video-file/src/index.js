import React from 'react'
import styled from 'styled-components'
import {FaRegFileVideo} from 'react-icons/fa'

const A = {}

export function IconFileVideo({className}) {
  return <A.Icon className={className} />
}

A.Icon = styled(FaRegFileVideo)`
  color: rgba(0, 0, 0, 0.7);
`
