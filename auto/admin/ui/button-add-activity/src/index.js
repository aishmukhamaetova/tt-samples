import React from 'react'
import Button from '@material-ui/core/Button'
import styled from 'styled-components'
import {IconAdd} from '@tt/component--admin--icon-add'

const A = {}

export function ButtonAddActivity(props) {
  return (
    <A.Button size="small" variant="outlined" {...props}>
      <A.IconAdd />
      ADD ACTIVITY
    </A.Button>
  )
}

A.IconAdd = styled(IconAdd)`
  && {
    margin-right: 5px;
  }
`
A.Button = styled(Button)`
  && {
    padding: 4px 15px;
    font-size: 1em;
    color: rgba(0, 0, 0, 0.8);
    font-weight: 300;
    letter-spacing: -0.42px;
  }
`
A.Block = styled.div`
  display: grid;
  grid-template-areas: 'icon name';
  cursor: pointer;
`

A.AreaIcon = styled.div`
  grid-area: icon;
  display: flex;
  align-items: center;
  justify-content: center;
`
