import React from 'react'
import styled from 'styled-components'
import {useHistory} from 'react-router-dom'

const A = {}

export function HeaderLogo({className}) {
  const history = useHistory()

  const onClick = () => history.push(`/`)

  return (
    <A.Image
      onClick={onClick}
      className={className}
      atl="Header logo"
      src="/images/header-logo.svg"
    />
  )
}

A.Image = styled.img`
  cursor: pointer;
`
