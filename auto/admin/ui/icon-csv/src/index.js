import React from 'react'
import styled from 'styled-components'
import {FaFileCsv} from 'react-icons/fa'

const A = {}

export function IconCsv({className, disabled}) {
  return <A.Icon disabled={disabled} className={className} />
}

A.Icon = styled(FaFileCsv)`
  font-size: 1.5em;
  color: ${({disabled}) =>
    disabled ? `rgba(0, 0, 0, 0.2)` : `rgba(0, 0, 0, 0.5)`};
  &:hover {
    color: ${({disabled}) => (disabled ? `rgba(0, 0, 0, 0.2)` : `#feb82b`)};
  }
`
