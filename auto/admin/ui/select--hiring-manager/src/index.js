import React from 'react'
import styled from 'styled-components'
import {default as __Select} from 'react-select'
import {fromHiringManager} from '@tt/domains'

const A = {}

const options = [
  {id: `kevin`, name: `Kevin Mant`, email: `kevin.mant@ozminerals.com`},
  {id: `nadia`, name: `Nadia Niscioli`, email: `Nadia.Niscioli@ozminerals.com`},
  {id: `pablo`, name: `Pablo Castro`, email: `Pablo.Castro@ozminerals.com`},
]

const styles = {
  container: base => ({
    ...base,
    flex: 1,
  }),
}

export function Select({valueById, defaultValue, ...props}) {
  const value = valueById
    ? options.find(({id}) => id === valueById)
    : props.value

  React.useEffect(() => {
    fromHiringManager.run.query()
  })

  return (
    <A.Select
      options={options}
      isClearable={false}
      isSearchable={true}
      styles={styles}
      getOptionLabel={({name}) => name}
      getOptionValue={({id}) => id}
      {...props}
      value={value}
      placeholder={`Select`}
      theme={theme => ({
        ...theme,
        colors: {
          ...theme.colors,
          primary25: 'rgba(254,184,43,0.5)',
          primary: 'rgba(254,184,43,1)',
        },
      })}
    />
  )
}

A.Select = styled(__Select)`
  && {
    .css-1wa3eu0-placeholder {
      color: rgba(0, 0, 0, 0.38);
    }
  }
`
