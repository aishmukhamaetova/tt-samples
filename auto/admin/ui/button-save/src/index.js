import React from 'react'
import Button from '@material-ui/core/Button'
import styled from 'styled-components'

const A = {}

export function ButtonSave(props) {
  return !props.disabled ? (
    <A.Button size="small" variant="outlined" {...props}>
      Save
    </A.Button>
  ) : (
    <Button size="small" variant="outlined" {...props}>
      Save
    </Button>
  )
}

A.Button = styled(Button)`
  && {
    background: rgba(0, 0, 0, 0.9);
    border: 1px solid #feb82b !important;
    color: #fff !important;
  }
`
