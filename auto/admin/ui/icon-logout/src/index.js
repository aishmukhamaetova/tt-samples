import React from 'react'
import styled from 'styled-components'
import {FaSignOutAlt} from 'react-icons/fa'

const A = {}

export function IconLogout({className}) {
  return (
    <A.Icon className={className} />
  )
}

A.Icon = styled(FaSignOutAlt)`
`