import React from 'react'
import styled from 'styled-components'

const A = {}

const Item = ({doc, className}) => {
  const Component = A[doc.as]

  return (
    <Component>
      <A.Image alt={doc.id} className={className} src={doc.imageLink} />
      <A.Label>{doc.name}</A.Label>
    </Component>
  )
}

export function ImageBlock({
  className,
  isselected,
  onClick,
  id,
  block,
  isView,
}) {
  return (
    <A.Select isselected={isselected} isView={isView} onClick={onClick}>
      {block.map((doc, index) => (
        <Item key={index} doc={doc} isView={isView} />
      ))}
    </A.Select>
  )
}

A.Select = styled.div`
  width: 330px;
  padding: 10px;
  display: grid;
  grid-template-areas: 'first second third';
  cursor: ${({isView}) => (isView ? 'default' : 'pointer')};
  background-color: ${({isselected, isView}) =>
    isselected || isView ? 'none' : 'none'};
  border-radius: 4px;
  border: solid 1px
    ${({isselected, isView}) =>
      isselected || isView ? 'rgba(254,184,43,1)' : 'rgba(0, 0, 0, 0.23)'};
  box-shadow: ${({isselected, isView}) =>
    isselected || isView ? '0 4px 5px -4px rgba(0, 0, 0, 0.6)' : 'none'};
  text-align: center;
  grid-template-columns: 33.3% 33.3% 33.3%;
`

A.Image = styled.img`
  width: 90px;
  height: 60px;
`
A.Label = styled.div`
  font-size: 0.75em;
  white-space: nowrap;
`
A.AreaFirst = styled.div`
  grid-area: first;
`

A.AreaSecond = styled.div`
  grid-area: second;
`

A.AreaThird = styled.div`
  grid-area: third;
`
