import React from 'react'
import styled from 'styled-components'
import {default as __Select} from 'react-select'

const A = {}

const defaultOptions = [
  {id: `open`, name: `Open`},
  {id: `filled`, name: `Filled`},
  {id: `cancelled`, name: `Cancelled`},
]

const styles = {
  container: base => ({
    ...base,
    flex: 1,
  }),
}

export function Select({options = defaultOptions, valueById, ...props}) {
  const value = valueById
    ? options.find(({id}) => id === valueById)
    : props.value

  return (
    <A.Select
      defaultValue={options[0]}
      options={options}
      isClearable={false}
      isSearchable={true}
      styles={styles}
      getOptionLabel={({name}) => name}
      getOptionValue={({id}) => id}
      {...props}
      value={value}
    />
  )
}

A.Select = styled(__Select)``
