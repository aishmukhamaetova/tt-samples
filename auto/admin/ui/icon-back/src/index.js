import styled from 'styled-components'

export const IconArrowLeft = styled.i`
  border: solid #FEB82B;
  border-width: 0 3px 3px 0;
  display: inline-block;
  padding: 5px;
  transform: rotate(135deg);
`
