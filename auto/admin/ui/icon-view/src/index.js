import React from 'react'
import styled from 'styled-components'
import {FaEye} from 'react-icons/fa'

const A = {}

export function IconView({className}) {
  return <A.Icon className={className} />
}

A.Icon = styled(FaEye)``
