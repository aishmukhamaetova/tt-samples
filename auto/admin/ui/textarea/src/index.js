import React from 'react'
import {default as __TextField} from '@material-ui/core/TextField'
import styled from 'styled-components'

const A = {}

export function Textarea(props) {
  return (
    <A.TextField
      variant="outlined"
      size="small"
      multiline
      rows="2"
      rowsMax="2"
      fullWidth
      {...props}
    />
  )
}

A.TextField = styled(__TextField)`
  && {
    font-family: 'Frutiger', 'Roboto', sans-serif;
    .MuiInputBase-root {
      align-items: flex-start;
      height: ${props => props.height || 'auto'};
    }
    textarea {
      height: 100% !important;
    }
  }
`
