import React from 'react'
import Button from '@material-ui/core/Button'
import styled from 'styled-components'

const A = {}

export function ButtonShortlist(props) {
  return (
    <A.Button size="small" variant="outlined" {...props}>Shortlist</A.Button>
  )
}

A.Button = styled(Button)`
`