import React from 'react'
import styled from 'styled-components'
import {FaPlusCircle} from 'react-icons/fa'

const A = {}

export function IconAdd({className}) {
  return (
    <A.Icon className={className} />
  )
}

A.Icon = styled(FaPlusCircle)`
  color: #feb82b;
`