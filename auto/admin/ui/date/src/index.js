import React from 'react'
import styled from 'styled-components'
import DateFnsUtils from '@date-io/date-fns'
import {KeyboardDatePicker} from '@material-ui/pickers'
import {MuiPickersUtilsProvider} from '@material-ui/pickers'
import au from 'date-fns/locale/en-AU' //south australia locale

const A = {}
const localeMap = {
  au,
}

export function Datepicker(props) {
  const {minDate = new Date()} = props
  const onChange = payload => {
    return props.onChange && props.onChange(payload)
  }

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={localeMap['au']}>
      <A.DatePicker
        inputVariant="outlined"
        minDate={minDate}
        format="dd/MM/yyyy"
        placeholder="10/10/2018"
        onChange={payload => {
          props.onChange && props.onChange(payload)
        }}
        value={props.value ? props.value : minDate}
        {...props}
      />
    </MuiPickersUtilsProvider>
  )
}

A.DatePicker = styled(KeyboardDatePicker)`
  && {
    .MuiInputBase-root {
      font-family: 'Frutiger', 'Roboto', sans-serif;
      padding-right: 0px;
      input {
        padding: 10px;
      }
    }
    .MuiIconButton-root {
      background-color: transparent;
      padding: 5px;
      margin-right: 5px;
    }
  }
`
