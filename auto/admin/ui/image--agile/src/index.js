import React from 'react'
import styled from 'styled-components'

const A = {}

export function ImageAgile({className}) {
  return (
    <A.Image className={className} atl="Agile" src="/images/icon-agile.png" />
  )
}

A.Image = styled.img`
`