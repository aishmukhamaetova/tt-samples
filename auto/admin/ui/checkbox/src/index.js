import React from 'react'
import {Checkbox as CheckboxMUI} from '@material-ui/core'
import styled from 'styled-components'

export function Checkbox({className, checked, handleChange}) {
  return (
    <CheckboxMUI
      checked={checked ? true : false}
      onChange={e =>
        handleChange ? handleChange(checked ? false : true) : null
      }
      size="small"
      color="blue"
      className={className}
    />
  )
}
