import React from 'react'
import styled from 'styled-components'
import {IconLogout} from '@tt/component--admin--icon-logout'
import {useHistory} from 'react-router-dom'
import {fromOkta} from '@tt/domains'

const A = {}

export function BlockAdminHeaderLogout() {
  const history = useHistory()

  const onClick = () => {
    fromOkta.on.clear()
    window.location.href = `/saml/logout`
  }

  return (
    <A.Block onClick={onClick}>
      <A.AreaIcon>
        <IconLogout />
      </A.AreaIcon>
      <A.AreaName>Logout</A.AreaName>
    </A.Block>
  )
}

A.Block = styled.div`
  font-size: 0.85em;
  display: grid;
  grid-template-areas: 'icon name';
  grid-template-rows: 26px;
  padding-left: 15px;
  cursor: pointer;
`

A.AreaIcon = styled.div`
  grid-area: icon;
  display: flex;
  align-items: center;
  justify-content: center;
`
A.AreaName = styled.div`
  grid-area: name;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-left: 10px;
  padding-right: 15px;
`
