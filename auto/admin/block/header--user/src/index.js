import React from 'react'
import styled from 'styled-components'
import {IconAccount} from '@tt/component--admin--icon-account'

const A = {}

export function BlockAdminHeaderUser(props) {
  const {user = ''} = props
  return (
    <A.Block>
      <A.AreaIcon>{user ? <IconAccount /> : null}</A.AreaIcon>
      <A.AreaName>{user}</A.AreaName>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas: 'icon name';
  font-size: 0.85em;
  grid-template-rows: 26px;
  border-right: 1px solid black;
`

A.AreaIcon = styled.div`
  grid-area: icon;
  display: flex;
  align-items: center;
  justify-content: center;
`
A.AreaName = styled.div`
  grid-area: name;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-left: 10px;
  padding-right: 15px;
`
