import React from 'react'
import styled from 'styled-components'
import {IconArrowLeft} from '@tt/component--admin--icon-back'

const A = {}

export function BlockAdminBack(props) {
  return (
    <A.Block onClick={props.onClick}>
      <A.AreaIcon>
        <IconArrowLeft />
      </A.AreaIcon>
      <A.AreaBack>
        Back
      </A.AreaBack>
    </A.Block>
  )
}

A.Block = styled.div`
  display: grid;
  grid-template-areas:
    'icon back'
  ;
  grid-template-columns: 15px 100px;
  grid-template-rows: 70px;
  cursor: pointer;
  user-select: none;
`

A.AreaIcon = styled.div`
  grid-area: icon;
  display: flex;
  align-items: center;
`
A.AreaBack = styled.div`
  grid-area: back;
  display: flex;
  align-items: center;
`
