module.exports = {
  apps: [
    {
      name: 'tt/preconstruct',
      script: 'preconstruct',
      args: 'watch',
      instances: 1,
      autorestart: true,
      restart_delay: 5000
    },
    {
      name: 'tt/app',
      cwd: 'app',
      script: 'yarn',
      args: 'start',
      instances: 1,
      autorestart: true,
      exec_interpreter: 'npx',
      exec_mode: 'fork_mode',
      env: {
        PORT: 4000
      },
      env_production: {
        PORT: 4000,
        NODE_ENV: 'production'
      },
      env_staging: {
        PORT: 4000,
        NODE_ENV: 'staging'
      }
    },
    {
      name: 'tt/admin',
      cwd: 'admin',
      script: 'yarn',
      args: 'start',
      instances: 1,
      autorestart: true,
      exec_interpreter: 'npx',
      exec_mode: 'fork_mode',
      env: {
        NODE_ENV: 'development',
        PORT: 5000
      },
      env_production: {
        PORT: 5000,
        NODE_ENV: 'production'
      },
      env_staging: {
        PORT: 5000,
        NODE_ENV: 'staging'
      }
    },
    {
      name: 'tt/backend',
      cwd: 'backend',
      script: 'yarn',
      args: 'start',
      instances: 1,
      autorestart: true,
      exec_interpreter: 'npx',
      exec_mode: 'fork_mode',
      restart_delay: 5000,

      env: {
        NODE_ENV: 'development',
        DEBUG: '@tt:*',
        PORT: 3000
      },
      env_production: {
        DEBUG: '@tt:*',
        NODE_ENV: 'production',
        PORT: 3000
      },
      env_staging: {
        DEBUG: '@tt:*',
        NODE_ENV: 'staging',
        PORT: 3000
      }
    }
  ]
}
