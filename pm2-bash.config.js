module.exports = {
  apps: [
    {
      name: 'bash',
      script: '/bin/sh',
      autorestart: true,
    },
  ],
}
