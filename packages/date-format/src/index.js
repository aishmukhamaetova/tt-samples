// import auLocale from 'date-fns/locale/en-AU' //south australia locale
// https://date-fns.org/v2.0.0-alpha.27/docs/Time-Zones
import {parseFromTimeZone, formatToTimeZone} from 'date-fns-timezone'
import {listTimeZones} from 'timezone-support'

// console.log('listTimeZones', listTimeZones())
const timeZone = 'Australia/Adelaide'

export function formatDate(date = '', hhmm = '') {
  try {
    const format = `DD/MM/YYYY ${hhmm}`
    const output = formatToTimeZone(date, format, {
      timeZone,
    })
    return output
  } catch (err) {
    return ``
  }
}
