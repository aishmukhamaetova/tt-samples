import {forward} from 'effector'

import {fromAuth} from './domain-auth'
import {fromOkta} from './domain-okta'
import {fromPageLogin} from './domain-page-login'

forward({from: fromPageLogin.on.rememberMe, to: fromAuth.on.rememberMe}) //forward

export {fromAuth, fromPageLogin, fromOkta}
export {fromHiringManager} from './domain--hiring-manager'
export {fromActivity} from './domain-activity'
export {fromPageApplicationAudio} from './domain-page-application-audio'
export {fromPageApplicationVideo} from './domain-page-application-video'
export {fromPageApplicationWritten} from './domain-page-application-written'
export {fromPageProfileCreate} from './domain-page-profile-create'
export {fromPageProfileEdit} from './domain-page-profile-edit'
export {fromPageProfileChangePassword} from './domain-page-profile-change-password'
export {fromUser} from './domain-user'

export {config, configSet} from './config'

export {Application} from './domain-application'
