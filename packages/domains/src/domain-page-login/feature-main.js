import createDebugger from 'debug'
import {createDomain} from 'effector'

const who = [`@tt`, `domain-page-login`, `feature-main`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

const $ = {}
const on = {}
const run = {}

function getLSItem(name) {
  const o = localStorage.getItem(name)
  const value = JSON.parse(o)
  return value
}

function setLSItem(name, v) {
  localStorage.setItem(name, JSON.stringify(v))
}

on.clear = domain.event()

$.username = domain.store(getLSItem(`username`) || ``) //test@test.com
on.username = domain.event()
on.usernameInput = on.username.prepend((e) => e.target.value)
$.username.on(on.username, (state, v) => v.trim())
$.username.on(on.clear, () => getLSItem(`username`) || ``)
$.username.reset(on.clear)
$.username.watch((state) => {
  debug(`username`, state)
})

$.password = domain.store(``)
on.password = domain.event()
on.passwordInput = on.password.prepend((e) => e.target.value)
$.password.on(on.password, (state, v) => v.trim())
$.password.reset(on.clear)
$.password.watch((state) => debug(`password`, state))

$.rememberMe = domain.store(getLSItem(`username`) ? true : false)
on.rememberMe = domain.event()
// $.rememberMe.on(on.clear, () => false)
on.rememberMeInput = on.rememberMe.prepend((e) => e.target.checked)
$.rememberMe.on(on.rememberMe, (state, v) => v)
$.rememberMe.watch((v) => {
  if (v) {
    setLSItem(`rememberMe`, v)
  } else {
    localStorage.removeItem(`rememberMe`)
  }
  debug(`rememberMe`, typeof v, v)
})

$.isUsernameValid = $.username.map((v) => {
  const isValid = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    v?.toLowerCase(),
  )
  return isValid
})

export default {$, on, run}
