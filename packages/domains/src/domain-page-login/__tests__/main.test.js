import main from '../feature-main'

const fixtures = {
  username: [
    `f`,
    `fo`,
    `foo`,
    `foo@`,
    `foo@b`,
    `foo@ba`,
    `foo@bar`,
    `foo@bar.`,
    `foo@bar.c`,
    `foo@bar.co`,
    `foo@bar.com`,
  ],
  password: [
    `p`,
    `pa`,
    `pas`,
    `pass`,
    `passw`,
    `passwo`,
    `passwor`,
    `password`,
  ],
}

test('set sequence', () => {
  //main.on.clear()

  fixtures.username.forEach(main.on.username)
  fixtures.password.forEach(main.on.password)

  const username = main.$.username.getState()
  const password = main.$.password.getState()

  expect(username).toBe(fixtures.username[fixtures.username.length - 1])
  expect(password).toBe(fixtures.password[fixtures.password.length - 1])
})

test('clear', () => {
  fixtures.username.forEach(main.on.username)
  fixtures.password.forEach(main.on.password)

  main.on.clear()

  const username = main.$.username.getState()
  const password = main.$.password.getState()

  expect(username).toBe(``)
  expect(password).toBe(``)
})
