import main from './feature-main'
import {runUserForgot} from './run-user-forgot'

export const fromPageLogin = {
  ...main,
  runUserForgot,
}
