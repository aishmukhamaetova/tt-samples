import createDebugger from 'debug'
import {createEffect} from 'effector'

import {config} from '../config'

const who = [`@tt`, `domain-page-login`, `run-user-forgot`]
const debug = createDebugger(who.join(`:`))

export const runUserForgot = createEffect({
  async handler(payload) {
    const {params} = payload
    const {email} = params

    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email
      })
    }
    const res = await fetch(`${config.API}/api/v1/user/forgot`, options)
    const r = await res.json()

    const {errors} = r

    if (errors?.length === 0) {
      return r
    }

    return r
  }
})

runUserForgot.fail.watch(({params, error}) => {
  debug(params)
  debug(error)
})
