import createDebugger from 'debug'
import {createDomain} from 'effector'
import main from './feature-main'

import {config} from '../config'

const who = [`@tt`, `domain-auth`, `feature-remove`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

const $ = {}
const on = {}
const run = {}

run.remove = domain.effect({
  async handler(payload) {
    debug(payload)

    const url = `${config.API}/api/v1/user/remove`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    }
    const res = await fetch(url, options)
    const r = await res.json()
    if (r.errors.length === 0) {
      return r.result
    }

    return false
  },
})

export default {$, on, run}
