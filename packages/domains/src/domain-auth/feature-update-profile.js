import createDebugger from 'debug'
import {createDomain} from 'effector'
import main from './feature-main'

import {config} from '../config'

const who = [`@tt`, `domain-auth`, `feature-update-profile`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

const $ = {}
const on = {}
const run = {}

run.updateProfile = domain.effect({
  async handler(payload) {
    debug(payload)

    const url = `${config.API}/api/v1/user/update`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {user, errors} = r

    if (errors.length === 0) {
      main.on.currentUser(user)
    }

    return r
  },
})

export default {$, on, run}
