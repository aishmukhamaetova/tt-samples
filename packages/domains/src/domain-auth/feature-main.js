import createDebugger from 'debug'
import {createDomain} from 'effector'
import {createEvent} from 'effector'

const who = [`@tt`, `domain-auth`, `feature-main`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

const $ = {}
const on = {}
const run = {}

function getLSItem(name) {
  try {
    const o = localStorage.getItem(name)
    if (o) {
      const value = JSON.parse(o)
      return value
    }
    return
  } catch (err) {
    return
  }
}

function setLSItem(name, v) {
  localStorage.setItem(name, JSON.stringify(v))
}

on.clear = createEvent()

function makeLSSimple(name, defaultValue, isClear = true) {
  $[name] = domain.store(getLSItem(name) || defaultValue)
  on[name] = domain.event()
  $[name].on(on[name], (state, v) => v)
  if (isClear) {
    $[name].on(on.clear, () => defaultValue)
  }
  $[name].watch((v) => setLSItem(name, v))
  $[name].watch((v) => debug(name, typeof v, v))
}

// on.clear.watch((v) => console.log(`CLEAR`))

makeLSSimple(`rememberMe`, false, false)
makeLSSimple(`sessionId`, null)
makeLSSimple(`currentUser`, null)
makeLSSimple(`username`, null)

on.favouriteRemove = domain.event()
on.favouriteAdd = domain.event()

$.currentUser.on(on.favouriteRemove, (state, {activityId}) => {
  if (state && state.favouriteActivities) {
    return {
      ...state,
      favouriteActivities: state.favouriteActivities.filter(
        (v) => v !== activityId
      )
    }
  }

  return state
})

$.currentUser.on(on.favouriteAdd, (state, {activityId}) => {
  if (state && state.favouriteActivities) {
    return {
      ...state,
      favouriteActivities: [...state.favouriteActivities, activityId]
    }
  }
  return state
})

$.rememberMe.watch((v) => {
  if (!v) {
    localStorage.removeItem(`sessionId`)
    localStorage.removeItem(`currentUser`)
    localStorage.removeItem(`rememberMe`)
  } else {
    setLSItem(`sessionId`, $[`sessionId`].getState())
    setLSItem(`currentUser`, $[`currentUser`].getState())
    setLSItem(`rememberMe`, $[`rememberMe`].getState())
  }
})

export default {$, on, run}
