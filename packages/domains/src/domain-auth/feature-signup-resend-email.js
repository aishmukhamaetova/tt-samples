import createDebugger from 'debug'
import {createDomain} from 'effector'

import {config} from '../config'

const who = [`@tt`, `domain-auth`, `feature-signup-resend-email`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

const $ = {}
const on = {}
const run = {}

run.signupResendEmail = domain.effect({
  async handler(payload) {
    debug(payload)

    const url = `${config.API}/api/v1/user/signup/resend`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    }
    const res = await fetch(url, options)
    const r = await res.json()

    return r
  },
})

export default {$, on, run}
