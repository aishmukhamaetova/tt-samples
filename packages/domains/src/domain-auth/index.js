import login from './feature-login'
import logout from './feature-logout'
import remove from './feature-remove'
import main from './feature-main'
import signup from './feature-signup'
import signupResendEmail from './feature-signup-resend-email'
import updateProfile from './feature-update-profile'
import sendFeedback from './feature-send-feedback'
import session from './feature--session'

export const fromAuth = {
  ...main,

  run: {
    ...main.run,
    ...login.run,
    ...logout.run,
    ...remove.run,
    ...signup.run,
    ...signupResendEmail.run,
    ...updateProfile.run,
    ...sendFeedback.run,
    ...session.run,
  },
}
