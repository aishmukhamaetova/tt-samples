import createDebugger from 'debug'
import {createDomain} from 'effector'
import main from './feature-main'

import {config} from '../config'

const who = [`@tt`, `domain-auth`, `feature-login`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

function setLSItem(name, v) {
  localStorage.setItem(name, JSON.stringify(v))
}

function getLSItem(name) {
  try {
    const o = localStorage.getItem(name)
    if (o) {
      const value = JSON.parse(o)
      return value
    }
    return
  } catch (err) {
    return
  }
}

const $ = {}
const on = {}
const run = {}

run.login = domain.effect({
  async handler(payload) {
    debug(payload)

    const url = `${config.API}/api/v1/user/login`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {session, user, errors} = r

    if (errors.length === 0) {
      main.on.sessionId(session.id)
      main.on.currentUser(user)
      const {email} = user
      const rememberMe =
        main.$[`rememberMe`].getState() || getLSItem(`rememberMe`)
      if (rememberMe) {
        setLSItem(`username`, email)
        setLSItem(`rememberMe`, rememberMe)
      } else {
        localStorage.removeItem(`username`)
        localStorage.removeItem(`rememberMe`)
      }
    }

    return r
  }
})

export default {$, on, run}
