import createDebugger          from 'debug'
import {combine, createDomain} from 'effector'

import authMain from '../domain-auth/feature-main'
import {config} from '../config'


const who = [`@tt`, `domain-auth`, `feature-logout`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

const createCoeffect = ($store, handler) => {
  const fx = domain.effect()
  $store.watch((state) => fx.use((param) => handler(state, param)))
  return fx
}

const $ = {}
const on = {}
const run = {}

run.logout = createCoeffect(
  combine({
    sessionId: authMain.$.sessionId,
  }),
  async function(state) {
    debug(state)

    const url = `${config.API}/api/v1/user/logout`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(state),
    }
    const res = await fetch(url, options)
    const r = await res.json()

    return r
  }
)

export default {$, on, run}
