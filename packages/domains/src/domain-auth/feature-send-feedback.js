import createDebugger from 'debug'
import {createDomain, combine} from 'effector'
import authMain from '../domain-auth/feature-main'

import {config} from '../config'

const who = [`@tt`, `domain-auth`, `feature-send-feedback`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

const createCoeffect = ($store, handler) => {
  const fx = domain.effect()
  $store.watch((state) => fx.use((param) => handler(state, param)))
  return fx
}

const $ = {}
const on = {}
const run = {}

run.sendFeedback = createCoeffect(
  combine({
    sessionId: authMain.$.sessionId
  }),
  async function handler(state, payload) {
    debug(payload)

    payload.sessionId = state.sessionId

    const url = `${config.API}/api/v1/user/sendfeedback`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {errors} = r
    debug(errors)

    return r
  }
)

export default {$, on, run}
