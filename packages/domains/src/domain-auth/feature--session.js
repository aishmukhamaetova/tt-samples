import createDebugger from 'debug'
import {createDomain} from 'effector'
import {combine} from 'effector'
import main from './feature-main'

import {config} from '../config'

const who = [`@tt`, `domain-auth`, `feature-session`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

const $ = {}
const on = {}
const run = {}

const createCoeffect = ($store, handler) => {
  const fx = domain.effect()
  $store.watch(state => fx.use(param => handler(state, param)))
  return fx
}

run.session = createCoeffect(
  combine({
    sessionId: main.$.sessionId,
  }),
  async function(state, payload) {
    debug(payload)

    const url = `${config.API}/api/v1/user/session`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(state),
    }
    const res = await fetch(url, options)
    const r = await res.json()

    if (r.errors.length === 0) {
      return r.result
    }

    return false
  },
)

export default {$, on, run}
