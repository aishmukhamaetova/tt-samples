export const config = {
  API: process.env.NODE_ENV === `production` ? `` : `http://localhost:3000`,
  ACTIVITY_SIZE: process.env.REACT_APP_ACTIVITY_SIZE ?? 10,
  AUDIO_SIZE: process.env.REACT_APP_AUDIO_SIZE ?? 8,
  VIDEO_SIZE: process.env.REACT_APP_VIDEO_SIZE ?? 10,
  SAML_ENV: process.env.REACT_APP_SAML_ENV ?? `dev`,
}

export function configSet(name, value) {
  config[name] = value
}
