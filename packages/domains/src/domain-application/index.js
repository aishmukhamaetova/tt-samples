import createDebugger from 'debug'
import {combine} from 'effector'
import {createDomain} from 'effector'

import {config} from '../config'
import authMain from '../domain-auth/feature-main'

const who = [`@tt`, `domain-application`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

const $ = {}
const on = {}
const run = {}

function makeSimple(name, defaultValue) {
  $[name] = domain.store(defaultValue)
  on[name] = domain.event()
  $[name].on(on[name], (state, v) => v)
  $[name].watch((v) => debug(name, v))
}

const createCoeffect = ($store, handler) => {
  const fx = domain.effect()
  $store.watch((state) => fx.use((param) => handler(state, param)))
  return fx
}

makeSimple(`list`, [])

on.listUpdate = domain.event()
$.list.on(on.listUpdate, (state, payload) => {
  return state.map((v) => (v.id === payload.id ? {...v, ...payload} : v))
})

run.query = createCoeffect(
  combine({
    sessionId: authMain.$.sessionId
  }),
  async function (state, payload) {
    debug(`query`, `state`, state, `payload`, payload)

    const url = `${config.API}/api/v1/application/queryOwn`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({sessionId: state.sessionId})
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {applicationList, errors} = r

    if (errors.length === 0) {
      on.list(applicationList)
    }

    return r
  }
)

run.query.fail.watch((v) => debug(`run.query.fail`, v))

run.withdraw = createCoeffect(
  combine({
    sessionId: authMain.$.sessionId
  }),
  async function (state, payload) {
    debug(`run.withdraw`, `state`, state, `payload`, payload)

    payload.sessionId = state.sessionId
    const body = payload

    const url = `${config.API}/api/v1/application/withdraw` //withdraw logic
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {applicationList, errors} = r

    if (errors.length === 0 && applicationList.length) {
      on.listUpdate(applicationList[0])
    }

    return r
  }
)

export const Application = {
  statusMap: {
    RECEIVED: `received`,
    REVIEW: `review`,
    SHORTLIST: `shortlist`,
    SUCCESSFUL: `successful`,
    UNSUCCESSFUL: `unsuccessful`,
    WITHDRAWN: `withdrawn`
  },

  $,
  on,
  run
}
