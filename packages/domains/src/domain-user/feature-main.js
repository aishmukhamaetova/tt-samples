import createDebugger from 'debug'
import {combine} from 'effector'
import {createDomain} from 'effector'
import {createEvent} from 'effector'

import {config} from '../config'
import authMain from '../domain-auth/feature-main'

const who = [`@tt`, `domain-user`, `feature-main`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

const $ = {}
const on = {}
const run = {}

function makeSimple(name, defaultValue) {
  $[name] = domain.store(defaultValue)
  on[name] = domain.event()
  $[name].on(on[name], (state, v) => v)
  $[name].watch((v) => debug(name, v))
}

const createCoeffect = ($store, handler) => {
  const fx = domain.effect()
  $store.watch((state) => fx.use((param) => handler(state, param)))
  return fx
}

makeSimple(`sort`, null)
makeSimple(`list`, [])
$.count = $.list.map((v) => v.length)

on.switchSort = createEvent()

$.sort.on(on.switchSort, (state) => !state)

$.list.on($.sort, (list, v) => {
  if (v === null) return list

  return v
    ? [...list].sort((a, b) => a.name.localeCompare(b.name))
    : [...list].sort((a, b) => b.name.localeCompare(a.name))
})

on.listUpdateApplication = domain.event()

$.list.on(on.listUpdateApplication, (state, payload) => {
  const {userId} = payload

  const output = state.map((v) =>
    v.id === userId
      ? {
          ...v,
          applicationList: v.applicationList.map((doc) =>
            doc.id === payload.id ? {...doc, ...payload} : doc
          )
        }
      : v
  )

  return output
})

run.queryAdmin = createCoeffect(
  combine({
    sessionId: authMain.$.sessionId
  }),
  async function (state, payload) {
    debug(`state`, state, `payload`, payload)

    const url = `${config.API}/api/v1/user/query-admin`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin'
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {userList, errors} = r

    userList.sort((a, b) => new Date(b.updatedAt) - new Date(a.updatedAt))

    if (errors.length === 0) {
      on.list(userList)
    }

    return r
  }
)

run.updateApplication = createCoeffect(
  combine({
    sessionId: authMain.$.sessionId
  }),
  async function (state, payload) {
    debug(
      `domain user run.updateApplication`,
      `state`,
      state,
      `payload`,
      payload
    )

    const body = payload

    const url = `${config.API}/api/v1/application/update-admin`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {applicationList, errors} = r

    if (errors.length === 0 && applicationList.length) {
      on.listUpdateApplication(applicationList[0])
    }

    return r
  }
)

export default {$, on, run}
