import main from './feature-main'

export const fromUser = {
  ...main,

  run: {
    ...main.run,
  },
}
