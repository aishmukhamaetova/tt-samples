import createDebugger from 'debug'
import {createEffect} from 'effector'

import {config} from '../config'

const who = [`@tt`, `domain-page-login`, `run-user-update-by-code`]
const debug = createDebugger(who.join(`:`))

const runUserUpdateByCode = createEffect({
  async handler(payload) {
    const {params} = payload
    const {code, password} = params

    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        code,
        password
      })
    }
    const res = await fetch(`${config.API}/api/v1/user/updateByCode`, options)
    const r = await res.json()

    const {errors} = r

    if (errors.length === 0) {
      return r
    }

    throw new Error(errors[0])
  }
})

runUserUpdateByCode.fail.watch(({params, error}) => {
  debug(params)
  debug(error)
})

const runCheckChangePasswordCode = async (payload) => {
  const {code} = payload

  const options = {
    method: `POST`,
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      code
    })
  }
  const res = await fetch(
    `${config.API}/api/v1/user/checkPasswordCode`,
    options
  )
  const r = await res.json()

  const {errors} = r

  if (errors.length === 0) {
    return r
  }

  throw new Error(errors[0])
}

export const fromPageProfileChangePassword = {
  runUserUpdateByCode,
  runCheckChangePasswordCode
}
