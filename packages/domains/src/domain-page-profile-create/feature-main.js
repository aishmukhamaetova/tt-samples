import createDebugger from 'debug'
import {createDomain} from 'effector'
import {combine} from 'effector'

const who = [`@tt`, `domain-page-profile-create`, `feature-main`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

const $ = {}
const on = {}
const run = {}

on.clear = domain.event()

function fieldCreate(name, options = {}) {
  $[name] = domain.store(``)
  $[name].reset(on.clear)
  on[name] = domain.event()
  on[`${name}Input`] = on[name].prepend(e => e.target.value)
  $[name].on(on[name], (state, v) => {
    if (name === `skills`) {
      return v.replace(/[^a-zA-Z0-9\s]/g, ``)
    }
    return name === 'email' || name === 'phone' || name === 'password'
      ? v.trim()
      : v
  })
  $[name].watch(state => debug(name, state))
}

const list = [`name`, `email`, `phone`, `password`, `verify`, `skills`]

list.forEach(fieldCreate)

on.clearSkills = domain.event()
$.skills.reset(on.clearSkills)

// $.list = $.skills.map(state =>
//   state
//     .split(`,`)
//     .map(v => v.trim())
//     .filter(v => v),
// )

$.list = domain.store([])
on.list = domain.event()
$.list.on(on.list, (state, v) => v)

on.listRemove = domain.event()
on.listPush = domain.event()
$.list.on(on.listRemove, (state, v) =>
  state.filter(doc => doc.trim() !== v.trim()),
)
$.list.on(on.listPush, (state, v) => {
  const value = v.trim()
  if (state.includes(value)) return state
  return state.length < 5 ? [...state, value] : state
})

$.isSubmitActive = combine(
  {
    list: $.list,
    name: $.name,
    email: $.email,
    password: $.password,
  },
  ({list, name, email, password}) => {
    return (
      list.length > 0 &&
      name.length > 0 &&
      email.length > 0 &&
      password.length > 0
    )
  },
)

$.isSubmitActive.watch(v => debug(`$.isSubmitActive`, v))

export default {$, on, run}
