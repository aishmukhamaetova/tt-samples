import auth from './feature-auth'
import main from './feature-main'

export const fromOkta = {
  ...main,

  run: {
    ...main.run,
    ...auth.run
  }
}
