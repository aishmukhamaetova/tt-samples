import createDebugger from 'debug'
import {createDomain} from 'effector'
import {createEvent} from 'effector'

const who = [`@tt`, `domain-auth`, `feature-main`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

const $ = {}
const on = {}
const run = {}

function getLSItem(name) {
  try {
    const o = localStorage.getItem(name)
    if (o) {
      const value = JSON.parse(o)
      return value
    }
    return
  } catch (err) {
    return
  }
}

function setLSItem(name, v) {
  localStorage.setItem(name, JSON.stringify(v))
}

on.clear = createEvent()

function makeLSSimple(name, defaultValue, isClear = true) {
  $[name] = domain.store(getLSItem(name) || defaultValue)
  on[name] = domain.event()
  $[name].on(on[name], (state, v) => v)
  $[name].on(on.clear, () => defaultValue)
  $[name].watch((v) => setLSItem(name, v))
  $[name].watch((v) => debug(name, typeof v, v))
}

on.clear.watch((v) => console.log(`CLEAR`))

makeLSSimple(`passport`, {})

// $.passport.watch((v) => {
//   console.log('watcher passport', v)
//   if (!v) {
//     localStorage.removeItem(`passport`)
//   } else {
//     setLSItem(`passport`, v)
//   }
// })

export default {$, on, run}
