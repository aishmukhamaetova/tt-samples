import createDebugger from 'debug'
import {createDomain} from 'effector'
import main from './feature-main'

import {config} from '../config'

const who = [`@tt`, `domain-okta`, `feature-auth`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

function setLSItem(name, v) {
  localStorage.setItem(name, JSON.stringify(v))
}

function getLSItem(name) {
  try {
    const o = localStorage.getItem(name)
    if (o) {
      const value = JSON.parse(o)
      return value
    }
    return
  } catch (err) {
    return
  }
}

const $ = {}
const on = {}
const run = {}

run.auth = domain.effect({
  async handler(payload) {
    debug(payload)

    const url = `${config.API}/api/v1/user/okta`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({})
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {passport, errors} = r
    if (errors.length === 0) {
      if (passport) {
        main.on.passport(passport) //main.$.passport
      }
    } else {
      return (window.location.href = `/saml/login`)
    }

    return r
  }
})

export default {$, on, run}
