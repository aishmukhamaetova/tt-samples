import main from './feature-main'

export const fromActivity = {
  ...main,

  run: {
    ...main.run,
  },
}
