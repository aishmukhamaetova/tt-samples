import createDebugger from 'debug'
import {combine} from 'effector'
import {createDomain} from 'effector'
import {createEvent} from 'effector'

import {config} from '../config'
import authMain from '../domain-auth/feature-main'

const who = [`@tt`, `domain-activity`, `feature-main`]

const debug = createDebugger(who.join(`:`))

const domain = createDomain(who.join(`.`))

const $ = {}
const on = {}
const run = {}
const clear = createEvent()

function makeSimple(name, defaultValue) {
  $[name] = domain.store(defaultValue)
  on[name] = domain.event()
  $[name].on(on[name], (state, v) => v)
  $[name].watch((v) => debug(name, v))
}

const createCoeffect = ($store, handler) => {
  const fx = domain.effect()
  $store.watch((state) => fx.use((param) => handler(state, param)))
  return fx
}

makeSimple(`all`, {v: null, id: null})
makeSimple(`sort`, {v: null, type: null})
makeSimple(`list`, [])
makeSimple(`listCount`, 0)

on.switchSort = createEvent()
on.onSelectAll = createEvent()
on.onSelectOne = createEvent()

// fromActivity.on.onSelectOne({
//   actId: activityDoc?.id,
//   applId: doc?.id,
//   v,
// })

$.list.on(on.onSelectOne, (list, {actId, applId, v}) => {
  return [...list].map((doc) => {
    if (doc.id === actId && doc.applicationList) {
      const {applicationList = []} = doc
      doc.applicationList = applicationList.map((doc) => {
        if (doc.id === applId) {
          return {...doc, selected: v}
        }
        return {...doc}
      })
    }

    return doc
  })
})
//select all
$.all.on(on.onSelectAll, (state, {id, v}) => {
  return {id, v: !v}
})

$.list.on($.all, (list, {v, id}) => {
  return [...list].map((doc) => {
    if (doc.id === id && doc.applicationList) {
      const {applicationList = []} = doc
      doc.applicationList = applicationList.map((doc) => ({
        ...doc,
        selected: v
      }))
    }

    return doc
  })
})

//sort
$.sort.on(on.switchSort, (state, {type}) => {
  return {type, v: !state?.v}
})

$.list.on($.sort, (list, {v, type}) => {
  if (v === null) return list
  if (!type) return list

  if (type === `applicationsCount`) {
    return v
      ? [...list].sort((a, b) => b.applicationsCount - a.applicationsCount)
      : [...list].sort((a, b) => a.applicationsCount - b.applicationsCount)
  }

  if (type === `createdAt`) {
    return v
      ? [...list].sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt))
      : [...list].sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt))
  }

  if (type === `dateClose`) {
    return v
      ? [...list].sort((a, b) => new Date(b.dateClose) - new Date(a.dateClose))
      : [...list].sort((a, b) => new Date(a.dateClose) - new Date(b.dateClose))
  }

  return v
    ? [...list].sort((a, b) => a[type]?.localeCompare(b[type]))
    : [...list].sort((a, b) => b[type]?.localeCompare(a[type]))
})

$.favouriteList = combine($.list, authMain.$.currentUser, (list, user) => {
  if (list.length && user && user.favouriteActivities) {
    return list.filter(({id}) => user.favouriteActivities.includes(id))
  }

  return []
})

$.favouriteList.watch((v) => debug(`favouriteList`, v))

on.listUpdate = domain.event()
$.list.on(on.listUpdate, (state, payload) => {
  return state.map((v) => (v.id === payload.id ? {...v, ...payload} : v))
})

on.listUpdateApplication = domain.event()
$.list.on(on.listUpdateApplication, (state, payload) => {
  const {activityId} = payload

  return state.map((v) =>
    v.id === activityId
      ? {
          ...v,
          applicationList: v.applicationList.map((doc) =>
            doc.id === payload.id ? {...doc, ...payload} : doc
          )
        }
      : v
  )
})

run.query = createCoeffect(
  combine({
    sessionId: authMain.$.sessionId
  }),
  async function (state, payload) {
    debug(`state`, state, `payload`, payload)

    const url = `${config.API}/api/v1/activity/query`
    const options = {
      method: `GET`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin'
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {activityList, errors} = r

    //sort
    activityList.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt))

    if (errors.length === 0) {
      on.list(activityList)
      on.listCount(activityList.length)
    }

    return r
  }
)

run.queryFavourites = createCoeffect(
  combine({
    sessionId: authMain.$.sessionId
  }),
  async function (state, payload) {
    debug(`state`, state, `payload`, payload)

    payload.sessionId = state.sessionId

    const url = `${config.API}/api/v1/activity/query-favourites`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {activityList, errors} = r

    //sort
    activityList.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt))

    if (errors.length === 0) {
      on.list(activityList)
      on.listCount(activityList.length)
    }

    return r
  }
)

run.oneDetail = createCoeffect(
  combine({
    sessionId: authMain.$.sessionId
  }),
  async function (state, payload) {
    debug(`state`, state, `payload`, payload)

    payload.sessionId = state.sessionId

    const url = `${config.API}/api/v1/activity/one-detail`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {activity, errors} = r

    if (errors.length === 0 && activity) {
      on.list([activity])
      on.listCount(1)
    }

    return r
  }
)

run.queryAdmin = createCoeffect(
  combine({
    sessionId: authMain.$.sessionId
  }),
  async function (state, payload) {
    debug(`state`, state, `payload`, payload)

    const url = `${config.API}/api/v1/activity/query-admin`
    const options = {
      method: 'POST',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin'
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {activityList, errors} = r

    activityList.sort((a, b) => new Date(b.updatedAt) - new Date(a.updatedAt))

    if (errors.length === 0) {
      on.listCount(activityList.length)
      on.list(activityList)
    }

    return r
  }
)

run.update = createCoeffect(
  combine({
    sessionId: authMain.$.sessionId
  }),
  async function (state, payload) {
    debug(`run.update`, `state`, state, `payload`, payload)

    const body = payload

    const url = `${config.API}/api/v1/activity/update`
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {activityList, errors} = r

    if (errors.length === 0 && activityList.length) {
      on.listUpdate(activityList[0])
    }

    return r
  }
)

run.updateApplication = createCoeffect(
  combine({
    sessionId: authMain.$.sessionId
  }),
  async function (state, payload) {
    debug(
      `domain activity run.updateApplication`,
      `state`,
      state,
      `payload`,
      payload
    )

    const body = payload

    const url = `${config.API}/api/v1/application/update-admin` //onChangeStatus
    const options = {
      method: `POST`,
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    }
    const res = await fetch(url, options)
    const r = await res.json()
    const {applicationList, errors} = r

    if (errors.length === 0 && applicationList.length) {
      on.listUpdateApplication(applicationList[0])
    }

    return r
  }
)

export default {$, on, run}
