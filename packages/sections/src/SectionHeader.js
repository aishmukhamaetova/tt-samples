import React from 'react'
import {useHistory} from 'react-router-dom'

const A = {}

const areas = `title`

export function SectionHeader({onBackClick, isIndexPage}) {
  const history = useHistory()

  return (
    <A.SectionComposition areas={areas}>
      {B => (
        <>
          <B.Title>
            <A.Title alignItems={`center`} justifyContent={`center`} flex>
              {!isIndexPage && (
                <A.IconContainerBack
                  alignItems={`center`}
                  justifyContent={`center`}
                  flex
                  onClick={onBackClick}
                >
                  <IconLeftArrow />
                </A.IconContainerBack>
              )}
              {/* {!isIndexPage && (
                <A.IconContainerHome
                  alignItems={`center`}
                  justifyContent={`center`}
                  flex
                  onClick={() => history.push(`/`)}
                >
                  <IconHome />
                </A.IconContainerHome>
              )} */}
              Team Tasker
            </A.Title>
          </B.Title>
        </>
      )}
    </A.SectionComposition>
  )
}

import {Box} from 'atomic-layout'
import {Composition} from 'atomic-layout'
import styled from 'styled-components'
import {IconLeftArrow} from '@tt/ui'
import {IconHome} from '@tt/ui'

A.IconContainerBack = styled(Box)`
  user-select: none;
  cursor: pointer;

  position: absolute;
  left: 0;
  top: 0;

  width: 40px;
  height: 45px;
`
A.IconContainerHome = styled(Box)`
  user-select: none;
  cursor: pointer;

  position: absolute;
  left: 30px;
  top: 0;

  width: 40px;
  height: 45px;
`

A.Title = styled(Box)`
  position: relative;

  background-color: rgba(74, 74, 74, 1);
  color: #fff;

  font-size: 1.1em;
  font-weight: bold;

  height: 45px;
`

A.SectionComposition = styled(Composition)``
