import React from 'react'

const A = {}

const areas = `title`

export function SectionTitle({title}) {
  return (
    <A.SectionTitle areas={areas}>
      {B => (
        <>
          <B.Title as={A.Title} alignItems={`center`} paddingLeft={15} flex>
            {title}
          </B.Title>
        </>
      )}
    </A.SectionTitle>
  )
}

import {Composition} from 'atomic-layout'
import styled from 'styled-components'

A.Title = styled.div`
  height: 45px;

  background: rgba(241, 241, 241, 1);

  font-size: 1.3em;
  font-weight: bold;
  color: #000;

  padding-left: 15px;

  align-items: center;
`

A.SectionTitle = styled(Composition)``
