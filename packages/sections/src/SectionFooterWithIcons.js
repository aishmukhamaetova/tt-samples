import React from 'react'

const A = {}

const COLOR = `rgb(255,176,0)`

export function SectionFooterWithIcons({
  isActiveRoute,
  onProfileClick,
  onStatusClick,
  onActivitiesClick,
  onFavouritesClick,
}) {
  return (
    <A.Grid>
      <A.AreaOne>
        <A.ButtonFooter
          icon={<A.IconActivities color={COLOR} size={24} />}
          label={`Activities`}
          onClick={onActivitiesClick}
          isActive={true}
          isActiveRoute={isActiveRoute('/activities')}
        />
      </A.AreaOne>
      <A.AreaTwo>
        {' '}
        <A.ButtonFooter
          icon={<A.IconFavourites color={COLOR} size={24} />}
          label={`Favourites`}
          onClick={onFavouritesClick}
          isActiveRoute={isActiveRoute('/favourites')}
        />
      </A.AreaTwo>
      <A.AreaThree>
        <A.ButtonFooter
          icon={<A.IconStatus color={COLOR} size={24} />}
          label={`Status`}
          onClick={onStatusClick}
          isActiveRoute={isActiveRoute('/status')}
        />
      </A.AreaThree>
      <A.AreaFour>
        <A.ButtonFooter
          icon={<A.IconProfile color={COLOR} size={24} />}
          label={`Profile`}
          onClick={onProfileClick}
          isActiveRoute={isActiveRoute('/profile')}
        />
      </A.AreaFour>
    </A.Grid>
  )
}

import styled from 'styled-components'

import {ButtonFooter} from '@tt/ui'
import {IconActivities} from '@tt/ui'
import {IconFavourites} from '@tt/ui'
import {IconProfile} from '@tt/ui'
import {IconStatus} from '@tt/ui'

A.ButtonFooter = ButtonFooter
A.IconActivities = IconActivities
A.IconFavourites = IconFavourites
A.IconProfile = IconProfile
A.IconStatus = IconStatus

A.Grid = styled.div`
  display: grid;
  grid-template-areas: 'one two three four';
  grid-template-rows: 55px;

  background-color: rgb(244, 244, 244);
  font-size: 0.8em;

  align-items: center;
`

A.AreaOne = styled.div`
  grid-area: one;
`
A.AreaTwo = styled.div`
  grid-area: two;
`
A.AreaThree = styled.div`
  grid-area: three;
`
A.AreaFour = styled.div`
  grid-area: four;
`
