import React from 'react'

import {TextArea} from '@tt/ui'

const A = {}

import {Modal} from '@tt/ui'

export function SectionModal({isOpen, onClose, onSubmit}) {
  const [value, setValue] = React.useState(``)

  return isOpen ? (
    <Modal onClose={onClose} isDark>
      <A.Grid>
        <A.AreaTitle>Feedback to OZ Minerals</A.AreaTitle>
        <A.AreaInput>
          <TextArea
            value={value}
            onChange={({target: {value}}) =>
              value.length <= 250 && setValue(value)
            }
          />
        </A.AreaInput>
        <A.AreaButton>
          <A.ButtonOutline onClick={() => onSubmit({value})}>
            Submit
          </A.ButtonOutline>
        </A.AreaButton>
      </A.Grid>
    </Modal>
  ) : null
}

import styled from 'styled-components'
import {ButtonOutline} from '@tt/ui'

A.ButtonOutline = styled(ButtonOutline)`
  height: 30px;
`

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'title'
    'input'
    'submit';

  width: 311px;
`

A.AreaTitle = styled.div`
  grid-area: title;

  padding-left: 23px;
  padding-top: 17px;
`

A.AreaInput = styled.div`
  grid-area: input;
  padding: 17px;
`

A.AreaButton = styled.div`
  grid-area: submit;

  padding: 0 17px 17px 17px;
`
