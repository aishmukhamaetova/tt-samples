import React from 'react'
import A from './style'

import {useStore} from 'effector-react'
import {fromAuth} from '@tt/domains'
import {SectionTitle} from '@tt/sections'

const areas = `
  title
  content
  fill
  feedback
`

const templateRows = `auto auto 1fr auto`

export function SectionForm({onFeedbackClick, onEditClick, onLogout}) {
  const currentUser = useStore(fromAuth.$.currentUser)

  const {email = ``, name = ``, phone = ``, skills = ``} = currentUser || {}

  return (
    <A.SectionComposition
      alignContent={`flex-start`}
      templateRows={templateRows}
      areas={areas}
    >
      {B => (
        <>
          <SectionTitle title={`Profile`} />
          {currentUser ? (
            <B.Content
              as={A.Profile}
              justifyContent={`start`}
              paddingBottom={15}
              paddingTop={15}
            >
              <A.FieldProfile label={`Name`} fontWeight={`bold`} text={name} />
              <A.FieldProfile label={`Email Address`} text={email} />
              <A.FieldProfile label={`Phone Number`} text={phone} />
              <A.FieldProfile label={`Password`} text={`xxxxxx`} />
              <A.FieldProfile label={`Skills`} text={skills} />
              <A.Pencil onClick={onEditClick} />
            </B.Content>
          ) : null}
          <B.Feedback
            as={A.Feedback}
            align={`end`}
            alignItems={`end`}
            alignContent={`space-between`}
            justifyContent={`center`}
            flexDirection={`column`}
            flex
            paddingTop={15}
            paddingBottom={15}
          >
            <A.ButtonCreate onClick={onFeedbackClick}>
              Provide Feedback
            </A.ButtonCreate>
            <A.ButtonCreate onClick={onLogout} style={{marginTop: 10}}>
              Logout
            </A.ButtonCreate>
          </B.Feedback>
        </>
      )}
    </A.SectionComposition>
  )
}
