import {useStore} from 'effector-react'
import React from 'react'

import {fromAuth} from '@tt/domains'
import {fromPageLogin} from '@tt/domains'

import {useHistory} from 'react-router-dom'

const A = {}

const areas = `
  header
  form
  footer
`

const templateRows = `auto 1fr auto`

export function PageProfileView({
  onBackClick,
  onEditClick,
  onProfileClick,
  onStatusClick,
  onActivitiesClick,
  onFavouritesClick,
  onLogout,
  isActiveRoute,
}) {
  const history = useHistory()

  React.useEffect(() => {
    async function check() {
      const result = await fromAuth.run.session()
      if (!result) {
        history.push(`/signin`)
      }
    }

    check()
  })

  const [isOpen, setIsOpen] = React.useState(false)
  const sessionId = useStore(fromAuth.$.sessionId)
  const {email} = useStore(fromAuth.$.currentUser) || {}

  const onFeedbackClick = () => {
    setIsOpen(true)
  }

  const onModalClose = () => {
    setIsOpen(false)
  }

  const onModalSubmit = async ({value}) => {
    try {
      setIsOpen(true)

      const r = await fromAuth.run.updateProfile({
        note: value,
        sessionId,
      })

      const {messageId} = await fromAuth.run.sendFeedback({
        note: value,
        email,
      })

      if (r.errors.length === 0 && messageId) {
        setIsOpen(false)
      } else {
        setIsOpen(false)
        // console.warn(`Cannot send feedback`, r.errors)
      }
    } catch (err) {
      setIsOpen(false)
      // console.warn(`Cannot save feedback`)
    }
  }

  const onLogoutClick = async () => {
    await fromAuth.run.logout()
    fromAuth.on.clear()
    fromPageLogin.on.clear()
    onLogout?.()
  }

  return (
    <>
      <A.SectionModal
        isOpen={isOpen}
        onClose={onModalClose}
        onSubmit={onModalSubmit}
      />
      <A.PageComposition areas={areas} templateRows={templateRows}>
        {(B) => (
          <>
            <B.Header>
              <SectionHeader onBackClick={onBackClick} />
            </B.Header>
            <B.Form>
              <A.SectionForm
                onFeedbackClick={onFeedbackClick}
                onEditClick={onEditClick}
                onLogout={onLogoutClick}
              />
            </B.Form>
            <B.Footer>
              <SectionFooterWithIcons
                isActiveRoute={isActiveRoute}
                onProfileClick={onProfileClick}
                onStatusClick={onStatusClick}
                onActivitiesClick={onActivitiesClick}
                onFavouritesClick={onFavouritesClick}
              />
            </B.Footer>
          </>
        )}
      </A.PageComposition>
    </>
  )
}

import {Composition} from 'atomic-layout'
import styled from 'styled-components'
import {SectionForm} from './section-form'
import {SectionHeader} from '@tt/sections'
import {SectionFooterWithIcons} from '@tt/sections'
import {SectionModal} from './SectionModal'

A.SectionForm = SectionForm
A.SectionModal = SectionModal

A.PageComposition = styled(Composition)`
  min-height: 100vh;
`
