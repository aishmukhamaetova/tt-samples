import React from 'react'
import {fromAuth} from '@tt/domains'

const A = {}

const areas = `
  header
  form
`

export function PageProfileCreateExpired({onBackClick, code, isNotVerified}) {
  const [status, setStatus] = React.useState({
    status: isNotVerified ? 'notverified' : null
  })
  const resendEmail = async (e) => {
    setStatus({status: `sending`, msg: `Sending email...`})
    const {errors} = await fromAuth.run.signupResendEmail({code})
    if (errors.length > 0) {
      const [error] = errors
      setStatus({status: `error`, msg: error?.message})
    } else {
      setStatus({status: `success`, msg: `Email sent.`})
    }
  }

  return (
    <A.PageComposition areas={areas}>
      {(B) => (
        <>
          <B.Header>
            <SectionHeader onBackClick={onBackClick} />
          </B.Header>
          <B.Form justifyContent={`center`} flex paddingTop={15}>
            <Choose>
              <When condition={status?.status === null}>
                You verified your account or verification link expired, try{' '}
                <A.Link onClick={resendEmail}> resend </A.Link> email.
              </When>
              <When condition={status?.status === 'notverified'}>
                Please click <A.Link onClick={resendEmail}> resend </A.Link>{' '}
                email.
              </When>
              <When condition={status?.status === 'sending'}>
                {status?.msg}
              </When>
              <When condition={status?.status === 'success'}>
                {' '}
                {status?.msg}
              </When>
              <When condition={status?.status === 'error'}>
                <A.Error>{status?.msg}</A.Error>
              </When>

              <Otherwise></Otherwise>
            </Choose>
          </B.Form>
        </>
      )}
    </A.PageComposition>
  )
}

import {Composition} from 'atomic-layout'
import styled from 'styled-components'
import {SectionForm} from './SectionForm'
import {SectionHeader} from '@tt/sections'

A.SectionForm = SectionForm
A.Link = styled.span`
  text-decoration: underline;
  cursor: pointer;
  margin: auto 5px;
  &:hover {
    text-decoration: none;
  }
`
A.PageComposition = Composition
A.Error = styled.div`
  color: ${(props) => (props.color ? props.color : '#e60b0b')};
`
