import React from 'react'

const A = {}

const areas = `
  header
  form
`

export function PageProfileCreate({onBackClick, onSuccess}) {
  return (
    <A.PageComposition areas={areas}>
      {B => (
        <>
          <B.Header>
            <SectionHeader onBackClick={onBackClick} />
          </B.Header>
          <B.Form>
            <A.SectionForm onSuccess={onSuccess} />
          </B.Form>
        </>
      )}
    </A.PageComposition>
  )
}

import {Composition} from 'atomic-layout'
// import styled from 'styled-components'
import {SectionForm} from './SectionForm'
import {SectionHeader} from '@tt/sections'

A.SectionForm = SectionForm

A.PageComposition = Composition
