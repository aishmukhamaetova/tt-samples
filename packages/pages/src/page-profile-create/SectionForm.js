import React from 'react'

import {IconEmail} from '@tt/ui'
import {IconLock} from '@tt/ui'
import {IconPhone} from '@tt/ui'
import {IconUser} from '@tt/ui'

import {useStore} from 'effector-react'
import {useList} from 'effector-react'
import {fromAuth} from '@tt/domains'
import {fromPageProfileCreate} from '@tt/domains'
import {SectionTitle} from '@tt/sections'

const A = {}
const COUNT_SKILLS = 5
const areas = `
  title
  error
  backenderror
  name
  email
  phone
  password
  verify
  skill
  tag
  tags
  terms
  text
  readtext
  readmore
  create
`

const COLOR_EMAIL = `rgb(140,105,154)`
const COLOR_PHONE = `rgb(27,111,82)`
const COLOR_LOCK = `rgb(180,16,58)`
const COLOR_USER = `rgb(232,109,50)`

function Field({name, valueToCompare, icon, label, type = 'text'}) {
  const value = useStore(fromPageProfileCreate.$[name])
  const onChange = fromPageProfileCreate.on[`${name}Input`]
  const isInputInvalid =
    (name === 'password' || name === 'verify') &&
    value &&
    valueToCompare &&
    value !== valueToCompare

  return (
    <>
      <A.Input
        isInputInvalid={isInputInvalid}
        icon={icon}
        label={
          valueToCompare !== undefined
            ? `${label}${value !== valueToCompare ? '' : ''}`
            : label
        }
        type={type}
        value={value}
        onChange={onChange}
      />
      {name === `verify` && isInputInvalid ? (
        <A.Error className={name} color={'#e60b0b'}>
          Password does not match
        </A.Error>
      ) : null}
    </>
  )
}

function FieldTag({name, placeholder}) {
  const value = useStore(fromPageProfileCreate.$[name])
  const onChange = fromPageProfileCreate.on[`${name}Input`]
  const onKeyDown = ({keyCode}) => {
    if (keyCode === 13 && value.trim().length > 0) {
      fromPageProfileCreate.on.listPush(value)
      fromPageProfileCreate.on.clearSkills()
    }
  }

  return (
    <A.InputTag
      value={value}
      maxLength="20"
      onChange={onChange}
      onKeyDown={onKeyDown}
      placeholder={placeholder}
    />
  )
}

export function SectionForm({onSuccess}) {
  const [check, setCheck] = React.useState(false)
  const [isMore, setIsMore] = React.useState(false)

  const [isErrorOnSingUp, showErrorOnSingUp] = React.useState({
    state: false,
    msg: ``
  })

  const email = useStore(fromPageProfileCreate.$.email)
  const password = useStore(fromPageProfileCreate.$.password)
  const verify = useStore(fromPageProfileCreate.$.verify)
  const name = useStore(fromPageProfileCreate.$.name)
  const phone = useStore(fromPageProfileCreate.$.phone)
  const list = useStore(fromPageProfileCreate.$.list)
  const isSubmitActive = useStore(fromPageProfileCreate.$.isSubmitActive)

  const onMore = () => setIsMore((v) => !v)

  const tags = useList(fromPageProfileCreate.$.list, (name) => (
    <A.Tag
      value={name}
      onClick={() => fromPageProfileCreate.on.listRemove(name)}
    />
  ))

  const onSubmit = async (isTermsChecked) => {
    showErrorOnSingUp({state: false, msg: ``})
    if (!isSubmitActive) {
      showErrorOnSingUp({state: true, msg: `Please fill all fields`})
    }
    if (!isTermsChecked || !isSubmitActive) {
      return
    }

    try {
      const {errors} = await fromAuth.run.signup({
        email,
        password,
        skills: list.join(`,`),
        name,
        phone
      })

      if (errors.length) {
        const [error] = errors
        showErrorOnSingUp({state: true, msg: error?.message})
      }

      if (errors.length === 0 && onSuccess) {
        onSuccess()
      }
    } catch (err) {
      showErrorOnSingUp({state: true, msg: `Something went wrong`})

      console.warn(`SectionForm`, `Error`, err)
    }
  }
  const [isTermsChecked, setChange] = React.useState(false)

  return (
    <A.SectionComposition areas={areas}>
      {(B) => (
        <>
          <SectionTitle title={`Create Profile`} />
          <B.Error
            justifyContent={`start`}
            flex
            paddingTop={14}
            paddingLeft={16}
          >
            <A.Error
              color={
                !isSubmitActive && isErrorOnSingUp?.state ? '#e60b0b' : '#000'
              }
            >
              <b>We won't misuse your details!</b>
              <div>Please complete all fields</div>
            </A.Error>
          </B.Error>

          <B.Backenderror
            justifyContent={`start`}
            flex
            paddingTop={14}
            paddingLeft={16}
          >
            {isErrorOnSingUp?.state ? (
              <A.Error>
                {isErrorOnSingUp?.msg || `Email Address already exists`}
              </A.Error>
            ) : null}
          </B.Backenderror>

          <B.Name justifyContent={`center`} flex paddingTop={14}>
            <Field
              name={`name`}
              icon={<IconUser color={COLOR_USER} />}
              label={`Name`}
            />
          </B.Name>
          <B.Email justifyContent={`center`} flex paddingTop={14}>
            <Field
              name={`email`}
              icon={<IconEmail color={COLOR_EMAIL} />}
              label={`Email Address`}
            />
          </B.Email>
          <B.Phone justifyContent={`center`} flex paddingTop={14}>
            <Field
              type={`number`}
              name={`phone`}
              icon={<IconPhone color={COLOR_PHONE} />}
              label={`Phone Number`}
            />
          </B.Phone>
          <B.Password justifyContent={`center`} flex paddingTop={15}>
            <Field
              name={`password`}
              icon={<IconLock color={COLOR_LOCK} />}
              label={`Password`}
              type={`password`}
            />
          </B.Password>
          <B.Verify
            as={A.Verify}
            justifyContent={`center`}
            flex
            paddingTop={15}
          >
            <Field
              name={`verify`}
              icon={<IconLock color={COLOR_LOCK} />}
              label={`Verify Password`}
              type={`password`}
              valueToCompare={password}
            />
          </B.Verify>
          <B.Skill
            alignItems={`center`}
            flex
            paddingTop={20}
            flexDirection={`column`}
            paddingLeft={20}
          >
            <A.SkillTitle>Skills</A.SkillTitle>
            <A.SkillText>No need to submit your CV!</A.SkillText>
            <A.SkillText>
              <div>
                Tell us in {COUNT_SKILLS} word tags how you can add value to our
                team
              </div>
              <div>
                (To create a word tag, enter a word, then tap/enter to create
                new word)
              </div>
            </A.SkillText>
          </B.Skill>
          <B.Tag justifyContent={`center`} flex paddingTop={15}>
            <FieldTag name={`skills`} placeholder={`Describe youself`} />
          </B.Tag>
          <B.Tags justifyContent={`center`} flex paddingTop={15}>
            <A.Tags>{tags}</A.Tags>
          </B.Tags>
          <B.Terms justifyContent={`center`} flex paddingTop={10}>
            <A.Terms flex justifyContent={`flex-start`}>
              <A.FieldCheckbox
                size={`small`}
                label={`Terms & Conditions`}
                checked={isTermsChecked}
                onChange={() => setChange(!isTermsChecked)}
              />
            </A.Terms>
          </B.Terms>
          <B.Text
            justifyContent={`center`}
            flex
            paddingLeft={5}
            marginTop={-10}
          >
            <A.TermsText>
              <p>
                I have read the Terms and Conditions, Disclaimer and Privacy
                information and I understand I am providing personal information
                for the purposes of considering and connecting opportunities to
                work with, or provide services to OZ Minerals under a variety of
                potential arrangements.
              </p>
            </A.TermsText>
          </B.Text>

          <B.Readtext>
            {isMore ? (
              <A.TermsText>
                <p>
                  You agree that information contained on this Site is for
                  personal use only and may not be sold, redistributed or used
                  for any commercial purpose (this includes but is not limited
                  to the use of Advertiser contact details for unsolicited
                  commercial correspondence and the information available via
                  our insights and resources subdomain{' '}
                  <a href="https://insightsresources.seek.com.au/">
                    https://insightsresources.seek.com.au/
                  </a>
                  ). You may download material from this Site for your personal,{' '}
                  <nobr>non-commercial</nobr> use only, provided you keep intact
                  all copyright and other proprietary notices. You may not
                  modify, copy, reproduce, republish, upload, post, transmit or
                  distribute in any way any material from this Site including
                  code and software. You must not use this Site for any purpose
                  that is unlawful or prohibited by these terms of use. You may
                  not use data mining, robots, screen scraping, or similar
                  automated data gathering, extraction or publication tools on
                  this Site (including without limitation for the purposes of
                  establishing, maintaining, advancing or reproducing
                  information contained on our Site on your own website or in
                  any other publication), except with Our prior written consent.
                </p>
                <p>
                  OZ Minerals gives no guarantee to you of the continued
                  availability of any particular job advertised on the Site and
                  will not be liable to you should OZ Minerals have filled the
                  vacancy at any time prior to removal of the advertisement from
                  the Site. Whilst OZ Minerals takes efforts to ensure that jobs
                  advertised are for actual job vacancies, it gives no guarantee
                  to you that every job advertisement represents an actual job
                  vacancy.
                </p>
                <p>
                  To become a registered user, you must provide a password and a
                  login name. You are entirely responsible if you do not
                  maintain the confidentiality of your password and login name.
                  Furthermore, you are entirely responsible for any and all
                  activities that occur under your login name. You may change
                  your password at any time by following instructions. You may
                  also delete services attached to your registration at your
                  convenience. You agree to immediately notify OZ Minerals of
                  any unauthorized use of your login name or any other breach of
                  security known to you.
                </p>
                <p>
                  You agree that it is a condition on your use of the Team
                  Tasker Site and of any other services provided by OZ Minerals
                  or through the Site that you will not through any act or
                  omission (including but not limited to creating a profile on
                  the Team Tasker Site) mislead or deceive others.
                </p>
              </A.TermsText>
            ) : null}
          </B.Readtext>

          <B.Readmore>
            <A.AreaMore onClick={onMore}>
              {isMore ? `Read Less` : `Read More`}
            </A.AreaMore>
          </B.Readmore>

          <B.Create
            justifyContent={`center`}
            flex
            paddingTop={15}
            paddingBottom={15}
          >
            <A.ButtonCreate
              isDisabled={!isTermsChecked || verify !== password}
              onClick={() => onSubmit(isTermsChecked)}
            >
              Create Profile
            </A.ButtonCreate>
          </B.Create>
        </>
      )}
    </A.SectionComposition>
  )
}

import styled from 'styled-components'
import {Box} from 'atomic-layout'
import {Composition} from 'atomic-layout'

import {Button} from '@tt/ui'
import {ButtonOutline} from '@tt/ui'
import {FieldCheckbox} from '@tt/ui'
import {Input} from '@tt/ui'
import {InputTag} from '@tt/ui'
import {Tag} from '@tt/ui'

A.Button = Button
A.FieldCheckbox = FieldCheckbox
A.Tag = Tag

A.ButtonCreate = styled(ButtonOutline)`
  width: 320px;
  height: 50px;

  @media (min-width: 340px) {
    width: 340px;
  }
`

A.Verify = styled.div`
  display: grid;
`

A.Error = styled.div`
  color: ${(props) => (props.color ? props.color : '#e60b0b')};
  font-size: 0.8em;
  &.verify {
    bottom: -12px;
    position: relative;
  }
`
A.Normal = styled.div`
  color: #000;
  font-size: 0.8em;
`

A.InputTag = styled(InputTag)`
  width: 340px;
`

A.Terms = styled(Box)`
  font-size: 0.7em;
  width: 340px;
`

A.TermsText = styled(Box)`
  && {
    font-size: 13px;
    font-weight: 500;
    width: 290px;
    user-select: none;
    margin: 0 auto;
    p {
      font-size: 13px;
      font-weight: 500;
      a {
        text-decoration: underline;
        color: #000;
        &: hover {
          text-decoration: none;
        }
      }
    }
  }
`

A.SkillTitle = styled.div`
  font-size: 1.3em;
  font-weight: 600;

  width: 340px;

  margin-top: 15px;
`

A.SkillText = styled.div`
  font-size: 0.7em;
  font-weight: 600;
  width: 340px;
`

A.Tags = styled.div`
  width: 340px;
  margin-left: -5px;
  margin-right: -5px;
  position: relative;
`

A.Input = styled(Input)`
  width: 340px;
  grid-template-areas:
    'icon label'
    'input input';
`

A.SectionComposition = styled(Composition)``

A.AreaMore = styled(Box)`
  cursor: pointer;

  grid-area: more;

  display: flex;
  justify-content: center;

  text-decoration: underline;

  font-size: 0.7em;
  font-weight: 500;

  font-weight: bold;
  letter-spacing: -0.21px;
  &:hover {
    text-decoration: none;
  }
`
