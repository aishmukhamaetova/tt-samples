import React from 'react'

import {HistoryCardNode} from './HistoryCardNode'
import {Application} from '@tt/domains'
import {formatDate} from '@tt/date-format'

const imagesMap = {
  application: {
    yes: `/images/history/application-1.svg`,
  },
  review: {
    no: `/images/history/review-0.svg`,
    yes: `/images/history/review-1.svg`,
  },
  shortlist: {
    no: `/images/history/shortlist-0.svg`,
    yes: `/images/history/shortlist-1.svg`,
  },
  success: {
    no: `/images/history/success-0.svg`,
    yes: `/images/history/success-1.svg`,
    cancel: `/images/history/success-cancel.svg`,
    unsuccess: `/images/history/success-unsuccess.svg`,
    withdrawn: `/images/history/success-withdrawn.svg`,
  },
}

const successText = {
  no: `Successful / Unsuccessful`,
  yes: `Successful`,
  cancel: `Cancelled`,
  unsuccess: `Unsuccessful`,
  withdrawn: `Withdrawn`,
}

// const imageApplication = `/images/history-application-active-88-88.png`
// const imageReview = `/images/history-review-88-88.png`
// const imageShort = `/images/history-short-88-88.png`
// const imageResult = `/images/history-result-88-88.png`

const A = {}

export function HistoryCardContent(props) {
  const {
    className,
    doc,
    review = `no`,
    shortlist = `no`,
    success = `no`,
  } = props

  const title = doc?.activity?.tags?.join?.(`, `)
  const location = doc?.activity?.location?.join?.(`, `)
  const duration = `${doc?.activity?.duration || ''} ${doc?.activity
    ?.durationType || ''}`

  const showWithdraw = doc?.status !== Application.statusMap.WITHDRAWN

  return (
    <A.Grid className={className}>
      <A.AreaTitle>{title}</A.AreaTitle>
      <A.AreaDescription>
        {location}, {duration}
      </A.AreaDescription>
      <A.AreaApplication>
        <HistoryCardNode
          icon={<img src={imagesMap.application.yes} />}
          title={`Application Received`}
          date={``}
        />
      </A.AreaApplication>
      <A.AreaReview>
        <HistoryCardNode
          icon={<img src={imagesMap.review[review]} />}
          title={`Under Review`}
          date={``}
        />
      </A.AreaReview>
      <A.AreaShort>
        <HistoryCardNode
          icon={<img src={imagesMap.shortlist[shortlist]} />}
          title={`Shortlist`}
          date={``}
        />
      </A.AreaShort>
      <A.AreaResult>
        <HistoryCardNode
          icon={<img src={imagesMap.success[success]} />}
          title={successText[success]}
          date={``}
          isArrow={false}
        />
      </A.AreaResult>
      <A.AreaStatus>
        <div>Status -</div>
        <div>
          Activity {doc?.activity?.status}. Application {doc?.status}
        </div>
      </A.AreaStatus>
      <A.AreaDate>{formatDate(doc.updatedAt)}</A.AreaDate>
      {showWithdraw && (
        <A.AreaWithdraw onClick={props.onWithDraw}>Withdraw</A.AreaWithdraw>
      )}
    </A.Grid>
  )
}

import styled from 'styled-components'

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'title'
    'description'
    'application'
    'review'
    'short'
    'result'
    'status'
    'date'
    'withdraw';
`

A.AreaTitle = styled.div`
  grid-area: title;

  font-size: 14px;
  font-weight: bold;
  color: #000;
  letter-spacing: -0.22px;
  line-height: 20px;
`
A.AreaDescription = styled.div`
  grid-area: description;

  font-size: 15px;
  color: #000;
  letter-spacing: -0.24px;
  line-height: 20px;
`
A.AreaApplication = styled.div`
  grid-area: application;

  padding-top: 23px;
`
A.AreaReview = styled.div`
  grid-area: review;
`
A.AreaShort = styled.div`
  grid-area: short;
`
A.AreaResult = styled.div`
  grid-area: result;
`
A.AreaStatus = styled.div`
  grid-area: status;

  padding-top: 20px;

  font-size: 18px;
  font-weight: bold;
  color: #000000;
  letter-spacing: -0.43px;
  line-height: 22px;
`
A.AreaDate = styled.div`
  grid-area: date;

  padding-top: 14px;

  font-size: 15px;
  color: #000000;
  letter-spacing: -0.24px;
  line-height: 18px;
`
A.AreaWithdraw = styled.div`
  grid-area: withdraw;

  padding-top: 14px;

  font-size: 12px;
  color: #e60b0b;
  letter-spacing: -0.19px;

  cursor: pointer;
  user-select: none;
`
