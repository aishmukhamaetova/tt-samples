import React from 'react'
import {HistoryCardPath} from './HistoryCardPath'

const A = {}

const imageArrow = `/images/history-arrow.png`

export function HistoryCardNode({className, icon, title, date, isArrow}) {
  return (
    <A.Grid>
      <A.AreaPath>
        <HistoryCardPath icon={icon} isArrow={isArrow} />
      </A.AreaPath>
      <A.AreaTitle>{title}</A.AreaTitle>
      <A.AreaDate>{date}</A.AreaDate>
    </A.Grid>
  )
}

import styled from 'styled-components'

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'path title'
    'path date'
    'path other'
    'path other';
  grid-template-rows: 44px 44px;

  justify-content: flex-start;
`

A.AreaPath = styled.div`
  grid-area: path;

  padding-left: 19px;
  padding-right: 18px;
`
A.AreaTitle = styled.div`
  grid-area: title;

  display: flex;
  align-items: flex-end;

  font-size: 20px;
  font-weight: bold;
  color: #000000;
  letter-spacing: -0.48px;
  line-height: 22px;
`
A.AreaDate = styled.div`
  grid-area: date;

  font-size: 15px;
  color: #000000;
  letter-spacing: -0.24px;
  line-height: 18px;
`
