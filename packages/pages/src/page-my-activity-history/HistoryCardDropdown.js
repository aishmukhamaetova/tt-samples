import React from 'react'
import {IconDownArrow} from '@tt/ui'
const A = {}

export function HistoryCardDropdown({className, icon, title, onClick}) {
  return (
    <A.Grid className={className} onClick={onClick}>
      <A.AreaImage>{icon}</A.AreaImage>
      <A.AreaTitle>{title}</A.AreaTitle>
      <A.AreaActions>
        <IconDownArrow color="#000" size={2} />
      </A.AreaActions>
    </A.Grid>
  )
}

import styled from 'styled-components'

A.Grid = styled.div`
  user-select: none;

  display: grid;
  grid-template-areas: 'image title actions';
  grid-template-rows: 47px;
  grid-template-columns: auto 1fr auto;

  align-items: center;

  padding-left: 9px;
  padding-right: 17px;

  background: #fff;
  border: solid 1px rgba(253, 183, 62, 1);
  border-radius: 8px;

  box-shadow: 0 4px 5px -4px black;
`

A.AreaImage = styled.div`
  grid-area: image;
`

A.AreaTitle = styled.div`
  grid-area: title;

  padding-left: 6px;
  font-size: 16px;
  font-weight: bold;
  color: #000000;
  letter-spacing: -0.32px;
  line-height: 21px;
`

A.AreaActions = styled.div`
  grid-area: actions;

  padding-bottom: 4px;
`
