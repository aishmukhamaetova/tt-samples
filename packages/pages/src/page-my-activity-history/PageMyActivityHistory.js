import React from 'react'
import styled from 'styled-components'

import {Area} from '@tt/ui'

import {SectionHeader} from '@tt/sections'
import {SectionHistory} from './SectionHistory'
import {SectionFooterWithIcons} from '@tt/sections'
import {SectionModalWithdrawn} from './SectionModalWithdrawn'

import {Application} from '@tt/domains'

import {fromAuth} from '@tt/domains'
import {useHistory} from 'react-router-dom'

const A = {}

export function PageMyActivityHistory({
  onBackClick,
  onProfileClick,
  onStatusClick,
  onActivitiesClick,
  onFavouritesClick,
  isActiveRoute
}) {
  const history = useHistory()

  React.useEffect(() => {
    async function check() {
      const result = await fromAuth.run.session()
      if (!result) {
        history.push(`/signin`)
      }
    }

    check()
  })

  const [doc, setDoc] = React.useState({})
  const [isOpen, setIsOpen] = React.useState(false)
  const [height, setHeight] = React.useState(window.innerHeight)

  React.useEffect(() => {
    Application.run.query() //my activity history
  }, [])

  React.useEffect(() => {
    const onResize = () => setHeight(window.innerHeight)
    window.addEventListener('resize', onResize)
    return () => {
      window.removeEventListener('resize', onResize)
    }
  }, [])

  function onAccept() {
    setIsOpen(false)

    Application.run.withdraw({
      id: doc.id,
      status: Application.statusMap.WITHDRAWN
    })
  }

  function onClose() {
    setIsOpen(false)
  }

  function onWithdraw({doc}) {
    setDoc(doc)
    setIsOpen(true)
  }

  return (
    <>
      <SectionModalWithdrawn
        doc={doc}
        isOpen={isOpen}
        onAccept={onAccept}
        onClose={onClose}
      />
      <A.PageGrid style={{'--min-height': `${height}px`}}>
        <A.AreaHeader>
          <SectionHeader onBackClick={onBackClick} />
        </A.AreaHeader>
        <A.AreaContent>
          <SectionHistory onWithdraw={onWithdraw} />
        </A.AreaContent>
        <A.AreaFooter>
          <SectionFooterWithIcons
            isActiveRoute={isActiveRoute}
            onProfileClick={onProfileClick}
            onStatusClick={onStatusClick}
            onActivitiesClick={onActivitiesClick}
            onFavouritesClick={onFavouritesClick}
          />
        </A.AreaFooter>
      </A.PageGrid>
    </>
  )
}

A.PageGrid = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'content'
    'footer';
  grid-template-rows: auto 1fr auto;

  min-height: 100vh;
  min-height: var(--min-height);

  width: 100vw;

  overflow-x: hidden;
`

A.AreaHeader = styled.div`
  grid-area: header;
`

A.AreaContent = styled(Area)`
  grid-area: content;

  background: rgb(246, 246, 246);

  padding-left: 21px;
  padding-top: 12px;
  padding-right: 33px;
  padding-bottom: 21px;
`

A.AreaFooter = styled.div`
  grid-area: footer;
`
