import React from 'react'

const A = {}

const imageArrow = `/images/history-arrow.png`

export function HistoryCardPath({icon, isArrow = true}) {
  return (
    <A.Grid>
      <A.AreaIcon>{icon}</A.AreaIcon>
      <A.AreaArrow>{isArrow ? <img src={imageArrow} /> : null}</A.AreaArrow>
    </A.Grid>
  )
}

import styled from 'styled-components'

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'icon'
    'arrow';
`

A.AreaIcon = styled.div`
  grid-area: icon;

  display: flex;
  justify-content: center;
`

A.AreaArrow = styled.div`
  grid-area: arrow;

  display: flex;
  justify-content: center;
`
