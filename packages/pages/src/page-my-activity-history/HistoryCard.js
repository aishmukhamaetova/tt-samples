import React from 'react'

import {HistoryCardDropdown} from './HistoryCardDropdown'
import {HistoryCardContent} from './HistoryCardContent'

const A = {}

export function HistoryCard({
  className,
  doc,
  icon,
  title,
  review,
  shortlist,
  success,
  onWithDraw,
}) {
  const [isOpen, setIsOpen] = React.useState(false)

  return (
    <A.Grid className={className}>
      <A.AreaDropdown>
        <HistoryCardDropdown
          icon={icon}
          title={title}
          onClick={() => setIsOpen(v => !v)}
        />
      </A.AreaDropdown>
      {isOpen ? (
        <A.AreaContent>
          <HistoryCardContent
            doc={doc}
            review={review}
            shortlist={shortlist}
            success={success}
            onWithDraw={onWithDraw}
          />
        </A.AreaContent>
      ) : null}
    </A.Grid>
  )
}

import styled from 'styled-components'

A.Grid = styled.div`
  user-select: none;

  display: grid;
  grid-template-areas:
    'dropdown'
    'content';
`

A.AreaDropdown = styled.div`
  grid-area: dropdown;

  z-index: 1;
`

A.AreaContent = styled.div`
  grid-area: content;

  margin-top: -13px;
  padding-top: 27px;

  padding-left: 12px;
  padding-right: 12px;
  padding-bottom: 9px;

  border: solid 1px rgba(253, 183, 62, 1);
  border-radius: 12px;

  z-index: 0;
`
