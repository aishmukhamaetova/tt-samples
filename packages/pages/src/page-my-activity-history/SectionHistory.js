import {useList} from 'effector-react'

import React from 'react'
import styled from 'styled-components'
import {HistoryCard} from './HistoryCard'
import {IconProfile} from '@tt/ui'

import {Application} from '@tt/domains'

const A = {}

const statusPropsMap = {
  [Application.statusMap.RECEIVED]: {},
  [Application.statusMap.REVIEW]: {review: `yes`},
  [Application.statusMap.SHORTLIST]: {review: `yes`, shortlist: `yes`},
  [Application.statusMap.SUCCESSFUL]: {
    review: `yes`,
    shortlist: `yes`,
    success: `yes`,
  },
  [Application.statusMap.UNSUCCESSFUL]: {
    review: `yes`,
    shortlist: `yes`,
    success: `unsuccess`,
  },
  [Application.statusMap.WITHDRAWN]: {
    review: `no`,
    shortlist: `no`,
    success: `withdrawn`,
  },
  cancelled: {
    review: `no`,
    shortlist: `no`,
    success: `cancel`,
  },
}

function getStatusProps(status, activityStatus) {
  try {
    if (status === `withdrawn`) {
      return {...statusPropsMap[status]}
    }
    if (activityStatus === `filled`) {
      return statusPropsMap[Application.statusMap.UNSUCCESSFUL]
    }
    return activityStatus === `cancelled`
      ? {...statusPropsMap.cancelled}
      : {...statusPropsMap[status]}
  } catch (err) {
    return {}
  }
}

export function SectionHistory({onWithdraw}) {
  const list = useList(Application.$.list, doc => (
    <A.HistoryCard
      icon={<IconProfile />}
      title={doc?.activity?.title}
      doc={doc}
      {...getStatusProps(doc?.status, doc?.activity?.status)}
      onWithDraw={() => onWithdraw?.({doc})}
    />
  ))

  return <A.Grid>{list}</A.Grid>
}

A.Grid = styled.div`
  height: 100%;

  display: grid;
  grid-auto-rows: auto;
  grid-row-gap: 10px;

  align-content: flex-start;

  padding-bottom: 2px;
`

A.HistoryCard = styled(HistoryCard)``
