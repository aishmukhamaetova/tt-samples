import React from 'react'

import {Area} from '@tt/ui'

// import {SectionActivityTags} from './SectionActivityTags'
import {SectionFooterWithIcons} from '@tt/sections'
import {SectionHeader} from '@tt/sections'
// import {SectionSearch} from './SectionSearch'
import {SectionActivityCards} from './SectionActivityCards'
import {SectionTitle} from '@tt/sections'

import {fromActivity} from '@tt/domains'

import {fromAuth} from '@tt/domains'
import {useHistory} from 'react-router-dom'

const A = {}

const areas = `
  'header'
  'title'
  'search'
  'tags'
  'cards'
  'footer'
`

const templateRows = `auto auto auto auto 1fr auto`

export function PageActivities({
  onBackClick,
  onJobClick,
  redirectToSignIn,
  onProfileClick,
  onStatusClick,
  onActivitiesClick,
  onFavouritesClick,
  isActiveRoute
}) {
  // const history = useHistory()

  // React.useEffect(() => {
  //   async function check() {
  //     const result = await fromAuth.run.session()
  //     if (!result) {
  //       history.push(`/signin`)
  //     }
  //   }

  //   check()
  // })

  React.useEffect(() => {
    fromActivity.run.query()
  })

  return (
    <A.PageGrid areas={areas} templateRows={templateRows}>
      <Area name="header">
        <SectionHeader onBackClick={onBackClick} />
      </Area>
      <SectionTitle title={`Activities`} />
      {/* <Area name="search">
        <SectionSearch />
      </Area>
      <Area name="tags">
        <SectionActivityTags />
      </Area> */}
      <Area name="cards">
        <SectionActivityCards
          onJobClick={onJobClick}
          redirectToSignIn={redirectToSignIn}
        />
      </Area>
      <Area name="footer">
        <SectionFooterWithIcons
          isActiveRoute={isActiveRoute}
          onProfileClick={onProfileClick}
          onStatusClick={onStatusClick}
          onActivitiesClick={onActivitiesClick}
          onFavouritesClick={onFavouritesClick}
        />
      </Area>
    </A.PageGrid>
  )
}

import styled from 'styled-components'
import {Grid} from '@tt/ui'

A.PageGrid = styled(Grid)`
  min-height: 100vh;
`
