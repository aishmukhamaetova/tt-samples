import {useStore} from 'effector-react'
import React from 'react'
import {Area} from '@tt/ui'
import {ActivityCard} from '@tt/ui'
import {IconCheckCircle} from '@tt/ui'
import moment from 'moment'
import {fromActivity} from '@tt/domains'
import {fromAuth} from '@tt/domains'

const A = {}

const areas = `'card'`

const templateRows = `auto`
const templateCols = `97.5%`

const statusFilter = ({status}) => ![`cancelled`, `filled`].includes(status)

const reformatDate = (v) => {
  return new Date(v).toISOString().split('T')[0]
}
const sortByDate = (a, b) => {
  return b?.timestamp - a?.timestamp
}

const momentFromServer = (createdAt) => moment(createdAt).fromNow()

export function SectionActivityCards({
  className,
  onJobClick,
  redirectToSignIn
}) {
  const list = useStore(fromActivity.$.list)
  const sessionId = useStore(fromAuth.$.sessionId)

  const isFunction = (functionToCheck) => {
    return (
      functionToCheck &&
      {}.toString.call(functionToCheck) === '[object Function]'
    )
  }
  const listFiltered = list
    .filter(statusFilter)
    .map((i) => {
      return {
        ...i,
        ...{
          timestamp: new Date(reformatDate(i?.createdAt)).getTime()
        }
      }
    })
    .sort(sortByDate)

  const currentUser = useStore(fromAuth.$.currentUser)

  const {favouriteActivities = []} = currentUser || {}

  const onClickStar = async ({id, isFavourite}) => {
    if (!sessionId && isFunction(redirectToSignIn)) {
      return redirectToSignIn()
    }
    try {
      if (isFavourite) {
        const {errors} = await fromAuth.run.updateProfile({
          sessionId,
          favouriteActivities: favouriteActivities.filter((v) => v !== id)
        })
        if (errors.length === 0) {
          fromAuth.favouriteAdd({activityId: id})
        } else {
          //
        }
      } else {
        const {errors} = await fromAuth.run.updateProfile({
          sessionId,
          favouriteActivities: [...favouriteActivities, id]
        })
        if (errors.length === 0) {
          fromAuth.favouriteRemove({activityId: id})
        } else {
          //
        }
      }
    } catch (err) {
      console.warn(`SectionForm`, `Error`, err)
    }
  }

  // '--cols': templateCols,
  // '--rows': templateRows,
  // '--areas': areas,

  return (
    <A.Grid areas={areas} templateCols={templateCols} className={className}>
      {listFiltered.map((item, index) => {
        const {
          id,
          title,
          description,
          createdAt,
          summary,
          tags = [],
          location = [],
          iconId
        } = item
        const count = !/\s/g.test(title) ? 20 : 30
        const short =
          `${(title.length > count && title.substr(0, count)) || title}${
            title.length > count ? `...` : ``
          }` || title

        const shortTags = () => {
          const str = tags?.join(`, `)
          return (
            `${(str.length > 40 && str.substr(0, 40)) || str}${
              str.length > 40 ? `...` : ``
            }` || str
          )
        }

        const isFavourite = favouriteActivities.includes(id)
        return (
          <A.AreaCard key={id} name="card">
            <A.ActivityCard
              icon={
                <A.Img
                  className={iconId}
                  src={`/images/icons/${iconId?.toLowerCase()}.png`}
                />
              }
              title={short}
              date={momentFromServer(createdAt)}
              tags={shortTags()}
              location={location.join(`, `)}
              duration={`${item.duration} ${item.durationType}`}
              onClick={() => onJobClick({id})}
              onClickStar={() => onClickStar({id, isFavourite})}
              isFavourite={isFavourite}
            >
              {summary}
            </A.ActivityCard>
          </A.AreaCard>
        )
      })}
    </A.Grid>
  )
}

import styled from 'styled-components'
import {Grid} from '@tt/ui'

A.ActivityCard = styled(ActivityCard)``
A.Grid = styled(Grid)`
  background: rgb(211, 211, 211);

  height: 100%;

  grid-row-gap: 10px;

  justify-content: center;

  padding: 10px;
`

A.AreaCard = styled(Area)`
  grid-row: auto;
  grid-col: 90%;
`

A.Img = styled.img`
  width: 20px;
  height: auto;
  &.tt {
    width: 12px;
    height: auto;
    border: 3px solid #f8b802;
    border-radius: 5px;
  }
`
