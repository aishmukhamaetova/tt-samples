import React from 'react'
import A from './style'
import {SectionTitle} from '@tt/sections'

const areas = `
  title
  content
  fill
  feedback
`

const templateRows = `auto auto 1fr auto`

export function SectionForm({onFeedbackClick}) {
  return (
    <A.SectionComposition
      alignContent={`flex-start`}
      templateRows={templateRows}
      areas={areas}
    >
      {B => (
        <>
          <SectionTitle title={`Profile`} />
          <B.Content
            as={A.Profile}
            justifyContent={`start`}
            paddingBottom={15}
            paddingTop={15}
          >
            <A.FieldProfile
              label={`Name`}
              fontWeight={`bold`}
              text={`Priya Shcharma`}
            />
            <A.FieldProfile
              label={`Email Address`}
              text={`rpiya.sharma@gmail.com`}
            />
            <A.FieldProfile label={`Phone Number`} text={`0414095960`} />
            <A.FieldProfile label={`Password`} text={`xxxxxx`} />
            <A.FieldProfile
              label={`Skills`}
              text={`HR, Change Management, SAP Specialist`}
            />
            <A.Pencil />
          </B.Content>
          <B.Feedback
            as={A.Feedback}
            align={`end`}
            alignItems={`end`}
            alignContent={`space-between`}
            justifyContent={`center`}
            flex
            paddingTop={15}
            paddingBottom={15}
          >
            <A.ButtonCreate onClick={onFeedbackClick}>
              Provide Feedback
            </A.ButtonCreate>
          </B.Feedback>
        </>
      )}
    </A.SectionComposition>
  )
}
