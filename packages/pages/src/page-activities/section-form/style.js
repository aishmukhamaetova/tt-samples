import styled from 'styled-components'
import {Composition} from 'atomic-layout'

import {ButtonOutline} from '@tt/ui'
import {FieldProfile} from '@tt/ui'
import {IconPencil} from '@tt/ui'

const A = {
  FieldProfile,
}

A.ButtonCreate = styled(ButtonOutline)`
  height: 50px;

  width: 80vw;
  min-width: 80vw;

  margin: auto;
`

A.Pencil = styled(IconPencil)`
  position: absolute;

  right: 15px;
  top: 0px;
  margin-top: 15px;

  cursor: pointer;
`

A.Profile = styled.div`
  font-size: 0.9em;

  width: 100vw;

  background: rgba(247, 247, 247, 1);

  position: relative;
`

A.Feedback = styled.div``

A.Popup = styled.div`
  background: #d8d8d8;
`

A.SectionComposition = styled(Composition)`
  background: rgba(211, 211, 211);
  height: 100%;
`

export default A
