import React from 'react'

const A = {}

export function SectionSearch() {
  return (
    <A.Grid>
      <A.AreaSearch>
        <A.InputSearch />
      </A.AreaSearch>
    </A.Grid>
  )
}

import styled from 'styled-components'
import {InputSearch} from '@tt/ui'

A.InputSearch = InputSearch

A.Grid = styled.div`
  display: grid;
  grid-template-areas: 'search';
  grid-template-rows: 52px;
  align-items: center;

  background: rgba(242, 242, 242, 1);

  border-top: 1px solid rgb(227, 226, 226);
  border-bottom: 1px solid rgb(227, 226, 226);
`

A.AreaSearch = styled.div`
  grid-area: 'search';

  padding: 0 16px;
`
