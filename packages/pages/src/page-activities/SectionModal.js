import React from 'react'

import {TextArea} from '@tt/ui'

const A = {}

const areas = `
  title
  input
`

import {Modal} from '@tt/ui'

export function SectionModal({isOpen, onClose}) {
  return isOpen ? (
    <Modal onClose={onClose} isDark>
      <A.SectionComposition areas={areas} width={311}>
        {B => (
          <>
            <B.Title paddingLeft={23} paddingTop={17}>
              Feedback to OZ Minerals
            </B.Title>
            <B.Input
              paddingLeft={23}
              paddingTop={17}
              paddingRight={23}
              paddingBottom={17}
            >
              <TextArea />
            </B.Input>
          </>
        )}
      </A.SectionComposition>
    </Modal>
  ) : null
}

import styled from 'styled-components'
import {Composition} from 'atomic-layout'

A.SectionComposition = styled(Composition)`
  font-weight: 300;
`
