import React from 'react'
import {Area} from '@tt/ui'
import {Tag} from '@tt/ui'
import {TagsContainer} from '@tt/ui'

const A = {}

const areas = `
  'title'
  'tags'
`

const templateRows = `30px auto`

export function SectionActivityTags() {
  return (
    <A.Grid areas={areas} templateRows={templateRows}>
      <A.AreaTitle name="title">
        <A.Title>Activity Tags</A.Title>
      </A.AreaTitle>
      <A.AreaTags name="tags">
        <TagsContainer gutter={5}>
          <Tag value="Analyst" height={30} />
          <Tag value="Engineering" height={30} />
          <Tag value="BA" height={30} />
          <Tag value="Reporting" height={30} />
          <Tag value="SAP" height={30} />
          <Tag value="Analyst" height={30} />
          <Tag value="Engineering" height={30} />
          <Tag value="BA" height={30} />
          <Tag value="Reporting" height={30} />
          <Tag value="SAP" height={30} />
        </TagsContainer>
      </A.AreaTags>
    </A.Grid>
  )
}

import styled from 'styled-components'
import {Grid} from '@tt/ui'

A.Grid = styled(Grid)`
  background: rgb(211, 211, 211);
`

A.AreaTitle = styled(Area)`
  display: flex;
  align-items: center;
`

A.AreaTags = styled(Area)`
  padding: 0 10px;
`

A.Title = styled.div`
  padding-left: 16px;

  font-size: 16px;
  font-weight: 500;
  color: #000;
  letter-spacing: -0.38px;
  line-height: 22px;
`
