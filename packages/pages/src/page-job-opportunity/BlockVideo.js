import React from 'react'

const A = {}

export function BlockVideo({doc}) {
  return (
    <A.Grid>
      <A.AreaVideo>
        <A.ReactPlayer
          playIcon={<A.PlayIcon />}
          url={doc?.videoLink}
          width="100%"
          height="100%"
          wrapper={A.Wrapper}
          playsinline
          controls
        />
      </A.AreaVideo>
    </A.Grid>
  )
}

import styled from 'styled-components'
import ReactPlayer from 'react-player'

A.Grid = styled.div`
  display: grid;
  grid-template-areas: 'video';

  width: 100%;
  height: 100%;
  position: relative;
`

A.AreaVideo = styled.div`
  grid-area: video;
`

A.Icon = styled.svg`
  width: 48px;
  height: 48px;
  fill: #000;
`

A.PlayIcon = ({}) => (
  <A.Icon>
    <path d="M10 15h8c1 0 2-1 2-2V3c0-1-1-2-2-2H2C1 1 0 2 0 3v10c0 1 1 2 2 2h4v4l4-4zM5 7h2v2H5V7zm4 0h2v2H9V7zm4 0h2v2h-2V7z" />
  </A.Icon>
)

A.ReactPlayer = styled(ReactPlayer)`
  width: 100%;
  height: 100%;
  user-select: none;
  cursor: pointer;
  position: absolute;
  top: 0;
  left: 0;
`
