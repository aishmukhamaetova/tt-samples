import React from 'react'

const A = {}

export function WhatsCard({imageLink, title, type}) {
  return (
    <A.Grid className={type}>
      <A.AreaImage>
        <A.Image bgImg={imageLink} alt={title}></A.Image>
      </A.AreaImage>
      <A.AreaTitle>{title}</A.AreaTitle>
    </A.Grid>
  )
}

import styled from 'styled-components'

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'image'
    'title';
  grid-template-rows: 1fr 20px;
  grid-template-columns: 100%;
  grid-row-gap: 0px;
  overflow: hidden;
  align-items: center;
  justify-content: center;
  &.site {
    margin: 0 3px 5px 7px;
  }
  &.food {
    margin: 0px 0px 5px 0px;
  }
  &.travel {
    margin: 0px 0px 5px 0px;
  }
  &.wfh {
    margin: 0px 0px 5px 8px;
  }
  &.flex {
    margin: 0px 0px 5px 7px;
  }
  &.nocommuting {
    margin: 0 3px 5px 7px;
  }
`

A.AreaImage = styled.div`
  grid-area: image;
  display: flex;
  align-items: center;
  justify-content: center;
  background: white;
`

A.Image = styled.div`
  width: 100px;
  height: 70px;
  margin: auto auto;
  background-repeat: no-repeat;
  background-position: center center;
  background-color: #fff;
  background-size: auto 100%;
  background-image: ${props => (props.bgImg ? `url(${props.bgImg})` : `none`)};
`

A.AreaTitle = styled.div`
  grid-area: title;

  display: flex;
  align-items: center;
  justify-content: center;

  font-size: 12px;
  font-weight: normal;
  color: #000000;
  text-align: center;
  align-items: center;
  justify-content: center;
  line-height: 12px;
`
