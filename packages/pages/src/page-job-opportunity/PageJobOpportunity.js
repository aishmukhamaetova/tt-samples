import React from 'react'

import {Area} from '@tt/ui'

import {SectionActivityDetails} from './SectionActivityDetails'
import {SectionHeader} from '@tt/sections'
import {SectionFooterWithIcons} from '@tt/sections'

import {fromActivity} from '@tt/domains'

import {fromAuth} from '@tt/domains'
import {useHistory} from 'react-router-dom'

const A = {}

export function PageJobOpportunity({
  id,
  onApply,
  onBackClick,
  onProfileClick,
  onStatusClick,
  onFavouritesClick,
  onActivitiesClick,
  isActiveRoute,
}) {
  const history = useHistory()

  React.useEffect(() => {
    async function check() {
      const result = await fromAuth.run.session()
      if (!result) {
        history.push(`/signin`)
      }
    }

    check()
  })

  const [height, setHeight] = React.useState(window.innerHeight)

  React.useEffect(() => {
    fromActivity.run.oneDetail({activityId: id})
  })

  React.useEffect(() => {
    const onResize = () => setHeight(window.innerHeight)
    window.addEventListener('resize', onResize)
    return () => {
      window.removeEventListener('resize', onResize)
    }
  })

  return (
    <A.PageGrid style={{'--min-height': `${height}px`}}>
      <A.AreaHeader>
        <SectionHeader onBackClick={onBackClick} />
      </A.AreaHeader>
      <A.AreaContent>
        <SectionActivityDetails id={id} onApply={onApply} />
      </A.AreaContent>
      <A.AreaFooter>
        <SectionFooterWithIcons
          isActiveRoute={isActiveRoute}
          onProfileClick={onProfileClick}
          onStatusClick={onStatusClick}
          onActivitiesClick={onActivitiesClick}
          onFavouritesClick={onFavouritesClick}
        />
      </A.AreaFooter>
    </A.PageGrid>
  )
}

import styled from 'styled-components'

A.PageGrid = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'content'
    'footer';
  grid-template-rows: auto 1fr auto;

  background: rgb(250, 250, 250);

  min-height: 100vh;
  min-height: var(--min-height);

  width: 100vw;

  overflow-x: hidden;
`

A.AreaHeader = styled.div`
  grid-area: header;
`

A.AreaContent = styled(Area)`
  grid-area: content;
`

A.AreaFooter = styled.div`
  grid-area: footer;
`
