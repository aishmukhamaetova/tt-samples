import React from 'react'
import {BlockActivityCard} from './BlockActivityCard'
import {ButtonOutline} from '@tt/ui'
import {useStore} from 'effector-react'
import {fromAuth} from '@tt/domains'
import {fromActivity} from '@tt/domains'
import {BlockVideo} from './BlockVideo'

const A = {}

export function SectionActivityDetails({id, onApply}) {
  const list = useStore(fromActivity.$.list)
  const doc = list.find(o => o.id === id)
  const sessionId = useStore(fromAuth.$.sessionId)
  return (
    <A.Grid>
      <A.AreaVideo>
        <BlockVideo doc={doc} />
      </A.AreaVideo>
      <A.AreaGrey />
      <A.AreaCard>
        <BlockActivityCard id={id} sessionId={sessionId} doc={doc} />
      </A.AreaCard>
      <A.AreaSubmit>
        <A.ButtonOutline onClick={onApply}>Quick Apply</A.ButtonOutline>
      </A.AreaSubmit>
    </A.Grid>
  )
}

import styled from 'styled-components'

A.ButtonOutline = styled(ButtonOutline)`
  height: 41px;
`

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'video'
    'grey'
    'card'
    'submit';
  grid-template-rows: 209px 123px auto auto;
`

A.AreaVideo = styled.div`
  grid-area: video;

  background: #595e60;

  display: flex;
  justify-content: center;
`
A.AreaGrey = styled.div`
  grid-area: grey;

  background: rgba(0, 0, 0, 0.4);
`
A.AreaCard = styled.div`
  grid-area: card;

  margin-top: -120px;

  padding-left: 8px;
  padding-right: 8px;
  padding-top: 4px;
`

A.AreaSubmit = styled.div`
  grid-area: submit;

  padding-left: 17px;
  padding-right: 17px;
  padding-bottom: 13px;
`
