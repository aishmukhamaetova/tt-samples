import {useStore} from 'effector-react'

import React from 'react'
import {fromActivity} from '@tt/domains'
import {fromAuth} from '@tt/domains'
import {IconStar} from '@tt/ui'
import {formatDate} from '@tt/date-format'

import {WhatsCard} from './WhatsCard'

const A = {}

const imageFlex = `/images/opportunity-flex.png`
const imageFood = `/images/opportunity-food.png`
const imageNoCommuting = `/images/opportunity-no-commuting.png`
const imageSite = `/images/opportunity-site.png`
const imageTravel = `/images/opportunity-travel.jpg`
const imageWfh = `/images/opportunity-wfh.png`

const COLOR = `rgb(255, 176, 0)`

export function BlockActivityCard({id, sessionId, doc}) {
  const currentUser = useStore(fromAuth.$.currentUser)

  const toUppercase = username =>
    username.charAt(0).toUpperCase() + username.substring(1)

  const options = [
    {id: `kevin`, name: `Kevin Mant`, email: `kevin.mant@ozminerals.com`},
    {
      id: `nadia`,
      name: `Nadia Niscioli`,
      email: `Nadia.Niscioli@ozminerals.com`,
    },
    {id: `pablo`, name: `Pablo Castro`, email: `Pablo.Castro@ozminerals.com`},
  ]

  const findEmail = valueById => {
    const {email = ''} = options.find(({id}) => id === valueById) || {}
    return email
  }

  const {favouriteActivities = []} = currentUser || {}
  const isFavourite = favouriteActivities.includes(id)

  const [isMore, setIsMore] = React.useState(false)

  const onMore = () => setIsMore(v => !v)

  const onClickStar = async () => {
    try {
      if (isFavourite) {
        const {errors} = await fromAuth.run.updateProfile({
          sessionId,
          favouriteActivities: favouriteActivities.filter(v => v !== id),
        })
        if (errors.length === 0) {
          fromAuth.favouriteAdd({activityId: id})
        }
      } else {
        const {errors} = await fromAuth.run.updateProfile({
          sessionId,
          favouriteActivities: [...favouriteActivities, id],
        })
        if (errors.length === 0) {
          fromAuth.favouriteRemove({activityId: id})
        }
      }
    } catch (err) {
      console.warn(`SectionForm`, `Error`, err)
    }
  }
  return doc ? (
    <A.Grid>
      <A.AreaTitle>{doc?.title}</A.AreaTitle>
      <A.AreaTags>
        {doc?.tags.map((i, index) => (
          <span key={index}>{`${i}${
            doc?.tags.length - 1 === index ? '' : ','
          } `}</span>
        ))}
      </A.AreaTags>
      <A.AreaRight>
        <IconStar
          variant={!isFavourite ? `regular` : null}
          color={COLOR}
          size={27}
          onClick={() => onClickStar()}
        />
      </A.AreaRight>
      <A.AreaLocation>
        {doc?.location.join(`, `)}, {doc?.duration}&nbsp;{doc?.durationType}
      </A.AreaLocation>
      <A.AreaStartDate>
        Start Date&nbsp;&#8722;&nbsp;{formatDate(doc?.dateStart)}
      </A.AreaStartDate>
      <A.AreaDescription>Activity Description</A.AreaDescription>
      <A.AreaWhere>
        <b>Where&nbsp;&#8722;&nbsp;</b>
        <span>{doc?.location.join(`, `)}</span>
      </A.AreaWhere>
      <A.AreaWhen>
        <b>When can I start&nbsp;&#8722;&nbsp;</b>
        <span>{formatDate(doc?.dateStart)}</span>
      </A.AreaWhen>
      <A.AreaDuration>
        <b>Duration&nbsp;&#8722;&nbsp;</b>
        {doc?.duration}&nbsp;{doc?.durationType}
      </A.AreaDuration>
      <A.AreaSkills>
        <b>Skills I need to bring&nbsp;&#8722;&nbsp;</b>
        {doc?.tags.map((i, index) => (
          <span key={index}>{`${i}${
            doc?.tags.length - 1 === index ? '' : ','
          } `}</span>
        ))}
      </A.AreaSkills>
      <A.AreaSummary>
        <b>Summary&nbsp;&#8722;&nbsp;</b>
        <A.Paragraph>{doc?.summary}</A.Paragraph>
      </A.AreaSummary>
      <A.AreaNeed>
        <b>What do I need to achieve&nbsp;&#8722;&nbsp;</b>
        <A.Paragraph>{doc?.description}</A.Paragraph>
      </A.AreaNeed>
      <A.AreaWhat>
        <b>What's in it for me&nbsp;&#8722;&nbsp;</b>
      </A.AreaWhat>

      {doc?.included?.map((group, index) =>
        group === `group0` ? (
          <A.AreaWhatCardsFirst key={index}>
            <WhatsCard
              type={`travel`}
              imageLink={imageTravel}
              title={`Travel and Accommodation`}
            />
            <WhatsCard type={`food`} imageLink={imageFood} title={`Food`} />
            <WhatsCard
              type={`site`}
              imageLink={imageSite}
              title={`Site Experience`}
            />
          </A.AreaWhatCardsFirst>
        ) : group === `group1` ? (
          <A.AreaWhatCardsSecond key={index}>
            <WhatsCard
              type={`wfh`}
              imageLink={imageWfh}
              title={`Work from Home`}
            />
            <WhatsCard
              type={`flex`}
              imageLink={imageFlex}
              title={`Flex Working Hours`}
            />
            <WhatsCard
              type={`nocommuting`}
              imageLink={imageNoCommuting}
              title={`No Commuting`}
            />
          </A.AreaWhatCardsSecond>
        ) : null,
      )}

      <A.AreaWho>
        <b>Who's my team&nbsp;&#8722;&nbsp;</b>
        <A.Paragraph>{doc?.aboutTeam}</A.Paragraph>
      </A.AreaWho>
      <A.AreaApply>
        <b>When do I need to apply by&nbsp;&#8722;&nbsp;</b>
        <span>{formatDate(doc?.dateClose)}</span>
      </A.AreaApply>
      {isMore ? (
        <A.AreaHiringManager>
          <b>What's the Hiring Manager detais&nbsp;&#8722;&nbsp;</b>
          <A.Paragraph>
            <div>{toUppercase(doc?.hiringManagerId)}</div>
            <div>{findEmail(doc?.hiringManagerId)}</div>
          </A.Paragraph>
        </A.AreaHiringManager>
      ) : null}
      <A.AreaMore onClick={onMore}>
        {isMore ? `Learn Less` : `Learn More`}
      </A.AreaMore>
    </A.Grid>
  ) : null
}

import styled from 'styled-components'

A.Grid = styled.div`
  --bg-from: rgb(197, 197, 197);
  --bg-to: rgb(250, 250, 250);

  display: grid;
  grid-template-areas:
    'title title right'
    'hr hr right'
    'location date date'
    'description description description'
    'where where where'
    'when when when'
    'duration duration duration'
    'skills skills skills'
    'summary summary summary'
    'need need need'
    'what what what'
    'whatCardsFirst whatCardsFirst whatCardsFirst'
    'whatCardsSecond whatCardsSecond whatCardsSecond'
    'who who who'
    'apply apply apply'
    'details details details'
    'more more more'
    'submit submit submit';
  grid-row-gap: 8px;
  grid-template-columns: 1fr 1fr 35px;

  border-radius: 12px;
  background: linear-gradient(var(--bg-from), var(--bg-to) 20%);

  padding: 20px 8px;

  font-size: 0.93em;
  color: #000000;
  line-height: 18px;
`

A.AreaTitle = styled.div`
  grid-area: title;
  font-weight: 600;
  font-size: 1.3em;
`
A.AreaTags = styled.div`
  grid-area: hr;
  font-weight: 600;
  font-size: 1em;
  text-transform: capitalize;
`

A.AreaRight = styled.div`
  grid-area: right;

  display: flex;
  justify-content: flex-end;
  align-items: center;
`
A.AreaLocation = styled.div`
  grid-area: location;

  font-size: 0.93em;
  font-weight: normal;
`
A.AreaStartDate = styled.div`
  grid-area: date;

  font-size: 0.93em;
  font-weight: normal;
  text-align: right;
`
A.AreaDescription = styled.div`
  grid-area: description;
  border-top: 1px solid rgba(0, 0, 0, 0.1);
  font-size: 1.25em;
  padding: 15px 0 5px;
  color: #000;
  font-weight: 600;
`
A.AreaWhere = styled.div`
  grid-area: where;
  font-size: 0.93em;
`
A.AreaWhen = styled.div`
  grid-area: when;
  font-size: 0.93em;
`
A.AreaDuration = styled.div`
  grid-area: duration;
  font-size: 0.93em;
`
A.AreaSkills = styled.div`
  grid-area: skills;
  font-size: 0.93em;
`

A.Paragraph = styled.div`
  font-weight: normal;
  padding: 5px 0;
  &:nth-child(1) {
    padding-top: 4px;
  }
`
A.AreaNeed = styled.div`
  grid-area: need;
  font-size: 0.93em;
`

A.AreaSummary = styled.div`
  grid-area: summary;
  font-size: 0.93em;
`
A.AreaWhat = styled.div`
  grid-area: what;
  font-size: 0.93em;
`
A.AreaWhatCardsFirst = styled.div`
  grid-area: whatCardsFirst;

  display: flex;
  flex-wrap: wrap;

  justify-content: center;
`

A.AreaWhatCardsSecond = styled.div`
  grid-area: whatCardsSecond;

  display: flex;
  flex-wrap: wrap;

  justify-content: center;
`

A.AreaWho = styled.div`
  grid-area: who;
  font-size: 0.93em;
`
A.AreaApply = styled.div`
  grid-area: apply;
  font-size: 0.93em;
`
A.AreaHiringManager = styled.div`
  grid-area: details;
  font-size: 0.93em;
  margin-bottom: 20px;
`
A.AreaMore = styled.div`
  cursor: pointer;

  grid-area: more;

  display: flex;
  justify-content: center;

  text-decoration: underline;

  font-size: 0.93em;
  font-weight: bold;
  color: #feb82b;
`

A.AreaSubmit = styled.div`
  grid-area: submit;
`
