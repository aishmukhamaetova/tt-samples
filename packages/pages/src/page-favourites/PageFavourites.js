import React from 'react'

import {Area} from '@tt/ui'

import {SectionFooterWithIcons} from '@tt/sections'
import {SectionHeader} from '@tt/sections'
import {SectionActivityCards} from './SectionActivityCards'
import {SectionTitle} from '@tt/sections'

import {fromActivity} from '@tt/domains'

import {fromAuth} from '@tt/domains'
import {useHistory} from 'react-router-dom'

const A = {}

const areas = `
  'header'
  'title'
  'cards'
  'footer'
`

const templateRows = `auto auto 1fr auto`

export function PageFavourites({
  onBackClick,
  onJobClick,
  onProfileClick,
  onStatusClick,
  onActivitiesClick,
  onFavouritesClick,
  isActiveRoute
}) {
  const history = useHistory()

  React.useEffect(() => {
    async function check() {
      const result = await fromAuth.run.session()
      if (!result) {
        history.push(`/signin`)
      }
    }

    check()
  })

  React.useEffect(() => {
    fromActivity.run.queryFavourites({})
  })

  return (
    <A.PageGrid areas={areas} templateRows={templateRows}>
      <Area name="header">
        <SectionHeader onBackClick={onBackClick} />
      </Area>
      <SectionTitle title={`Favourites`} />
      <Area name="cards">
        <SectionActivityCards onJobClick={onJobClick} />
      </Area>
      <Area name="footer">
        <SectionFooterWithIcons
          isActiveRoute={isActiveRoute}
          onProfileClick={onProfileClick}
          onStatusClick={onStatusClick}
          onActivitiesClick={onActivitiesClick}
          onFavouritesClick={onFavouritesClick}
        />
      </Area>
    </A.PageGrid>
  )
}

import styled from 'styled-components'
import {Grid} from '@tt/ui'

A.PageGrid = styled(Grid)`
  min-height: 100vh;
`
