import React from 'react'

const A = {}

const areas = `
  header
  form
  back
  foot
`

const templateRows = `1fr 1fr 1fr 80vh `

export function PageVerificationEmail({
  onBackClick,
  msg,
  onSingInClick,
  verified
}) {
  return (
    <A.PageComposition areas={areas} templateRows={templateRows}>
      {(B) => (
        <>
          <B.Header>
            <SectionHeader onBackClick={onBackClick} />
          </B.Header>
          <B.Form justifyContent={`center`} flex paddingTop={15}>
            {msg}
          </B.Form>
          <B.Back justifyContent={`center`} flex paddingTop={15}>
            {verified ? (
              <A.ButtonSign onClick={onSingInClick}>Sign In</A.ButtonSign>
            ) : (
              <A.ButtonSign onClick={onBackClick}>
                Back to Home Page
              </A.ButtonSign>
            )}
          </B.Back>
          <B.Foot justifyContent={`center`} flex paddingTop={15}>
            &nbsp;
          </B.Foot>
        </>
      )}
    </A.PageComposition>
  )
}

import {Composition} from 'atomic-layout'
import {SectionHeader} from '@tt/sections'
import {ButtonOutline} from '@tt/ui'
import styled from 'styled-components'

A.PageComposition = Composition

A.ButtonSign = styled(ButtonOutline)`
  width: 320px;
  height: 50px;

  @media (min-width: 340px) {
    width: 340px;
  }
`
