import React from 'react'

const A = {}

const areas = `
  header
  form
`

const templateRows = `1fr 80vh`

export function PageNotFound(props) {
  return (
    <A.PageComposition areas={areas} templateRows={templateRows}>
      {B => (
        <>
          <B.Header>
            <SectionHeader onBackClick={props.onBackClick} />
          </B.Header>
          <B.Form>
            Page not found
          </B.Form>
        </>
      )}
    </A.PageComposition>
  )
}

import {Composition} from 'atomic-layout'
import {SectionHeader} from '@tt/sections'

A.PageComposition = Composition
