import {useStore} from 'effector-react'
import React from 'react'
import styled from 'styled-components'

import {fromPageApplicationVideo} from '@tt/domains'
import {ButtonOutline} from '@tt/ui'

const A = {}

export function BlockFormVideoSubmit({id, onSubmitSuccess}) {
  const lastSuccess = useStore(
    fromPageApplicationVideo.videoSubmit.$.lastSuccess,
  )

  React.useEffect(() => {
    if (lastSuccess && onSubmitSuccess) {
      onSubmitSuccess()
    }
  }, [lastSuccess])

  React.useEffect(() => fromPageApplicationVideo.on.clear, [])

  function onSubmit() {
    fromPageApplicationVideo.run.videoSubmit({
      activityId: id,
      type: `video/quicktime`,
    })
  }

  return (
    <A.Grid>
      <A.AreaSubmit>
        <A.ButtonOutline onClick={onSubmit}>Submit</A.ButtonOutline>
      </A.AreaSubmit>
    </A.Grid>
  )
}

A.Grid = styled.div`
  display: grid;
  grid-template-areas: 'submit';

  grid-template-columns: 80vw;
`

A.AreaSubmit = styled.div`
  grid-area: submit;
`

A.ButtonOutline = styled(ButtonOutline)`
  height: 40px;
`
