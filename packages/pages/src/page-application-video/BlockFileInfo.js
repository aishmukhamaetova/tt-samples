import {useStore} from 'effector-react'
import React from 'react'
import styled from 'styled-components'

import {fromPageApplicationVideo} from '@tt/domains'
import {IconClose} from '@tt/ui'

const A = {}

export function BlockFileInfo() {
  const file = useStore(fromPageApplicationVideo.$.file)

  const short =
    file?.name?.length < 10
      ? file?.name
      : `${file?.name?.substr(0, 3)}...${file?.name?.substr(
          file?.name?.length - 5,
          5,
        )}`

  function onClose() {
    fromPageApplicationVideo.on.file(null)
  }

  return (
    <A.Grid>
      {file ? (
        <>
          <A.AreaName>{short}</A.AreaName>
          <A.AreaActions>
            <IconClose onClick={onClose} />
          </A.AreaActions>
        </>
      ) : null}
    </A.Grid>
  )
}

A.Grid = styled.div`
  display: grid;
  grid-template-areas: 'name actions';

  grid-template-rows: 45px;
  grid-template-columns: auto 5px;
`

A.AreaName = styled.div`
  grid-area: name;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 25px;
  white-space: nowrap;
`

A.AreaActions = styled.div`
  grid-area: actions;
`
