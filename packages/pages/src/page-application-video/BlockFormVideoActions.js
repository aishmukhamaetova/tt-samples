import {useStore} from 'effector-react'
import React from 'react'
import styled from 'styled-components'

import {fromPageApplicationVideo} from '@tt/domains'
import {ButtonOutline} from '@tt/ui'

const A = {}

export function BlockFormVideoActions() {
  const file = useStore(fromPageApplicationVideo.$.file)
  // const source = useStore(fromPageApplicationAudio.$.source)
  // const audio = useAudioStream(stream)

  const ref1 = React.useRef(null)
  const ref2 = React.useRef(null)

  React.useEffect(() => {
    if (file === null) {
      if (ref1.current) {
        ref1.current.value = null
      }
      if (ref2.current) {
        ref2.current.value = null
      }
    }
  }, [ref1, ref2, file])

  // React.useEffect(() => {
  //   fromPageApplicationAudio.on.clear()
  // }, [])

  // function onRecord() {
  //   fromPageApplicationAudio.run.audioRecord()
  // }

  // const listen =
  //   stream && !source ? (
  //     <audio key={`stream`} ref={audio} autoPlay controls muted />
  //   ) : (
  //     <audio key={`source`} src={source} autoPlay controls />
  //   )

  // function onChange(e) {
  //   console.log(`onChange`, e.target.files)
  // }

  return (
    <A.Grid>
      <A.AreaRecord>
        <label>
          <A.ButtonOutline>Create Video</A.ButtonOutline>
          <A.Input
            ref={ref1}
            id="videoFile1"
            type="file"
            accept="video/*"
            capture=""
            onChange={fromPageApplicationVideo.on.fileOnChange}
          />
        </label>
      </A.AreaRecord>
      <A.AreaAccess>
        <label>
          <A.ButtonOutline>Access Camera Roll</A.ButtonOutline>
          <A.Input
            ref={ref2}
            id="videoFile2"
            type="file"
            accept="video/*"
            onChange={fromPageApplicationVideo.on.fileOnChange}
          />
        </label>
      </A.AreaAccess>
      {/* <A.AreaDescription>
        {file ? `${file.name} - ${file.type} - ${file.size}` : null}
      </A.AreaDescription> */}
    </A.Grid>
  )
}

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'record'
    'access'
    'description';

  grid-row-gap: 20px;

  grid-template-columns: 80vw;
  grid-template-rows: 50px;
`

A.AreaDescription = styled.div`
  grid-area: description;

  display: flex;

  justify-content: center;
  align-items: center;
`

A.AreaRecord = styled.div`
  grid-area: record;
`
A.AreaAccess = styled.div`
  grid-area: access;
`

A.ButtonOutline = styled(ButtonOutline)`
  height: 50px;
`

A.Input = styled.input`
  display: none;
`
