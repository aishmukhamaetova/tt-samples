import {useStore} from 'effector-react'
import React from 'react'

import {Area} from '@tt/ui'

import {SectionHeader} from '@tt/sections'
import {SectionContent} from './SectionContent'
import {SectionFooterWithIcons} from '@tt/sections'

import {fromAuth} from '@tt/domains'
import {Application} from '@tt/domains'
import {useHistory} from 'react-router-dom'
import {useParams} from 'react-router-dom'

const A = {}

export function PageHowToApply({
  onBackClick,
  onProfileClick,
  onStatusClick,
  onActivitiesClick,
  onFavouritesClick,
  isActiveRoute,
  onChoose,
}) {
  const history = useHistory()
  const {id} = useParams()
  const list = useStore(Application.$.list)
  const isAlreadyExist = list.find(doc => doc.activityId === id) !== undefined

  React.useEffect(() => {
    async function check() {
      const result = await fromAuth.run.session()
      if (!result) {
        history.push(`/signin`)
      }
    }

    check()
  })

  React.useEffect(() => {
    Application.run.query()
  }, [])

  const [height, setHeight] = React.useState(window.innerHeight)

  React.useEffect(() => {
    const onResize = () => setHeight(window.innerHeight)
    window.addEventListener('resize', onResize)
    return () => {
      window.removeEventListener('resize', onResize)
    }
  })

  return (
    <A.PageGrid style={{'--min-height': `${height}px`}}>
      <A.AreaHeader>
        <SectionHeader onBackClick={onBackClick} />
      </A.AreaHeader>
      <A.AreaContent>
        <SectionContent onChoose={onChoose} isAlreadyExist={isAlreadyExist} />
      </A.AreaContent>
      <A.AreaFooter>
        <SectionFooterWithIcons
          isActiveRoute={isActiveRoute}
          onProfileClick={onProfileClick}
          onStatusClick={onStatusClick}
          onActivitiesClick={onActivitiesClick}
          onFavouritesClick={onFavouritesClick}
        />
      </A.AreaFooter>
    </A.PageGrid>
  )
}

import styled from 'styled-components'

A.PageGrid = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'content'
    'footer';
  grid-template-rows: auto 1fr auto;

  min-height: 100vh;
  min-height: var(--min-height);
`

A.AreaHeader = styled.div`
  grid-area: header;
`

A.AreaContent = styled(Area)`
  grid-area: content;

  background: rgb(246, 246, 246);

  padding-left: 21px;
  padding-top: 12px;
  padding-right: 33px;
  padding-bottom: 21px;
`

A.AreaFooter = styled.div`
  grid-area: footer;
`
