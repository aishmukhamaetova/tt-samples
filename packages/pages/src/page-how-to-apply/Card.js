import React from 'react'

const A = {}

export function Card({description, imageLink, title, onChoose}) {
  return (
    <A.Grid onClick={onChoose}>
      <A.AreaImage onClick={onChoose}>
        <img src={imageLink} alt="Card image" />
      </A.AreaImage>
      <A.AreaTitle onClick={onChoose}>{title}</A.AreaTitle>
      <A.AreaDescription onClick={onChoose}>{description}</A.AreaDescription>
    </A.Grid>
  )
}

import styled from 'styled-components'

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'image title'
    'image description';
  justify-content: flex-start;
`

A.AreaImage = styled.div`
  grid-area: image;

  padding-right: 20px;
`

A.AreaTitle = styled.div`
  grid-area: title;

  display: flex;
  align-items: flex-end;

  font-size: 21px;
  font-weight: bold;
  color: #000;
  letter-spacing: -0.5px;
  line-height: 22px;
`

A.AreaDescription = styled.div`
  grid-area: description;

  font-size: 15px;
  color: #000000;
  letter-spacing: -0.24px;
  line-height: 18px;
`
