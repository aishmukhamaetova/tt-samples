import React from 'react'
import {Card} from './Card'

const A = {}

export function SectionContent({isAlreadyExist, onChoose}) {
  return (
    <A.Grid>
      <A.AreaTitle>How to Apply</A.AreaTitle>
      <A.AreaDescription>
        {isAlreadyExist ? <A.Error>Already applied</A.Error> : null}
        Describe why you are suitable using <b>one</b> of the following -
      </A.AreaDescription>
      <A.AreaVideo>
        <Card
          onChoose={() => !isAlreadyExist && onChoose('video')}
          imageLink={`/images/video-application-88-88.png`}
          title={`Video Application`}
          description={`Record a 1 min video clip`}
        />
      </A.AreaVideo>
      <A.AreaAudio>
        <Card
          onChoose={() => !isAlreadyExist && onChoose('audio')}
          imageLink={`/images/audio-application-88-88.png`}
          title={`Audio Application`}
          description={`Record a 1 min audio message`}
        />
      </A.AreaAudio>
      <A.AreaWritten>
        <Card
          onChoose={() => !isAlreadyExist && onChoose('writing')}
          imageLink={`/images/written-application-88-88.png`}
          title={`Written Application`}
          description={`Write maximum 900 characters`}
        />
      </A.AreaWritten>
      <A.AreaText>
        <A.AreaInfo>Information</A.AreaInfo>

        <p>
          This is not a regular job application, in fact it’s not a regular job!
        </p>
        <p>
          Once you’ve had a chance to review the information provided by the
          leader of the team, have a think about your pitch to be involved with
          the work posted on Team Tasker.
        </p>
        <p>
          <b>What are your super powers?</b> What skills or experience do you
          bring to this activity or project? What caught your interest?
        </p>
        <p>
          Our hope is that people will be bold enough to create a{' '}
          <b>short 1 minute video or audio recording</b> to pitch their
          potential contribution to the listed activity.
        </p>
        <p>
          We realise not everyone will be comfortable with this, so we also have
          an option to create a short written pitch as an alternative.
        </p>
        <p>
          Each Team Tasker will only be able to{' '}
          <b>
            apply once for each activity, using one of the three options above.
          </b>
        </p>
        <p>
          Often our leaders will contact you to set up another chat to progress
          your application, and we encourage you to <b>review the status</b>{' '}
          within Team Tasker for updates.
        </p>
        <p>
          We thank you for your interest in this new way of working for us at OZ
          Minerals. We appreciate your feedback.
        </p>
      </A.AreaText>
      {/* <A.AreaButton>
        <A.ButtonSubmit>Submit Application</A.ButtonSubmit>
      </A.AreaButton> */}
    </A.Grid>
  )
}

import styled from 'styled-components'
import {ButtonOutline} from '@tt/ui'

A.ButtonSubmit = styled(ButtonOutline)`
  height: 40px;
`

A.Grid = styled.div`
  height: 100%;

  display: grid;
  grid-template-areas:
    'title'
    'description'
    'video'
    'audio'
    'written'
    'text'
    'button';
  grid-template-rows: auto auto auto auto 1fr;
`

A.AreaTitle = styled.div`
  grid-area: title;

  font-size: 21px;
  font-weight: bold;
  color: #000;
  letter-spacing: -0.5px;
  line-height: 22px;
`

A.AreaDescription = styled.div`
  grid-area: description;
  padding-top: 9px;

  font-size: 15px;
  color: #000;
  letter-spacing: -0.24px;
  line-height: 20px;
`

A.AreaVideo = styled.div`
  grid-area: video;
  padding-top: 52px;
  a {
    color: black;
    &:hover {
      text-decoration: none;
    }
  }
`

A.AreaAudio = styled.div`
  grid-area: audio;
  padding-top: 37px;
  a {
    color: black;
    &:hover {
      text-decoration: none;
    }
  }
`

A.AreaWritten = styled.div`
  grid-area: written;
  padding-top: 37px;
  padding-bottom: 50px;
`

A.AreaInfo = styled.div`
  font-size: 14px;
  font-weight: 600;
  color: #000;
  text-align: left;
  margin: 5px auto;
`

A.AreaText = styled.div`
  && {
    grid-area: text;
    padding: 10px auto;
    color: #000;
    text-align: left;
    font-size: 12px;
    line-height: 1.2;

    p {
      line-height: 1.2;
      font-size: 12px;
    }
  }
`

A.AreaButton = styled.div`
  grid-area: button;

  padding-top: 21px;
`

A.Error = styled.div`
  color: #e60b0b;
  font-size: 0.8em;
`
