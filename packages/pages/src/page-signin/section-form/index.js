import React from 'react'
import A from './style'

import {IconEmail} from '@tt/ui'
import {IconLock} from '@tt/ui'

import {useStore} from 'effector-react'
import {fromAuth} from '@tt/domains'
import {fromPageLogin} from '@tt/domains'
import {SectionTitle} from '@tt/sections'
import {SectionModaSuccess} from './SectionModaSuccess'

const areas = `
  title
  error
  email
  password
  forgot
  remember
  signin
`

const COLOR_EMAIL = `rgb(140,105,154)`
const COLOR_LOCK = `rgb(180,16,58)`

export function SectionForm(props) {
  const username = useStore(fromPageLogin.$.username)
  const password = useStore(fromPageLogin.$.password)
  const rememberMe = useStore(fromPageLogin.$.rememberMe)
  const [code, setCode] = React.useState(``)

  const isUsernameValid = useStore(fromPageLogin.$.isUsernameValid)

  const [isErrorOnSingIn, showErrorOnSingIn] = React.useState({
    status: false,
    msg: ``
  })
  const [isErrorOnForgotPassword, showErroronForgotPassword] = React.useState({
    status: false,
    msg: ``
  })
  const [isSuccessReset, onSuccessReset] = React.useState(false)

  const delay = (ms) => new Promise((rs) => setTimeout(rs, ms))

  async function timeout(ms) {
    await delay(ms)
    return {
      errors: [`Timeout`]
    }
  }

  const onSubmit = async () => {
    showErrorOnSingIn({status: false, msg: ``})
    showErroronForgotPassword({status: false, msg: ``})

    try {
      const {errors} = await fromAuth.run.login({email: username, password})
      if (errors.length) {
        const [error = {}] = errors
        if (error?.code) {
          setCode(error?.code)
        }

        showErrorOnSingIn({status: true, msg: error?.message})
      }
      if (errors.length === 0 && props.onSuccess) {
        props.onSuccess()
      }
    } catch (err) {
      showErrorOnSingIn({status: true, msg: null})
      console.warn(`SectionForm`, `ERROR`, err)
    }
  }

  const onForgotPassword = async () => {
    showErrorOnSingIn({status: false, msg: ``})
    showErroronForgotPassword({status: false, msg: ``})

    if (!username) {
      return
    }
    try {
      const {errors} = await Promise.race([
        fromPageLogin.runUserForgot({
          params: {
            email: username
          }
        }),
        timeout(2000)
      ])

      if (errors.length) {
        const [error] = errors
        if (error?.code) {
          setCode(error?.code)
        }
        showErroronForgotPassword({
          status: true,
          msg: error?.message || `Something went wrong`
        })
      }
      if (errors.length === 0 && props.onSuccess) {
        onSuccessReset(true)
      }
    } catch (err) {
      showErroronForgotPassword({status: true, msg: `Something went wrong`})
    }
  }

  const onSuccessClose = () => {
    onSuccessReset(false)
  }
  return (
    <A.SectionComposition areas={areas}>
      {(B) => (
        <>
          <SectionTitle title={`Sign In`} />
          <B.Error
            justifyContent={`start`}
            flex
            paddingTop={14}
            paddingLeft={16}
          >
            <SectionModaSuccess
              isOpen={isSuccessReset}
              onClose={onSuccessClose}
              text={`Please check your email and follow the link.`}
            />
            {isErrorOnSingIn?.status ? (
              <A.Error>
                {isErrorOnSingIn?.msg === 'unverified' ? (
                  <span>
                    Cannot sign in, {username} is not verified. Select{' '}
                    <A.ErrorLink href={`/verifyemail/${code}`}>
                      Resend
                    </A.ErrorLink>{' '}
                    verification email to verify email.
                  </span>
                ) : (
                  isErrorOnSingIn?.msg || `Email Address or Password is invalid`
                )}{' '}
              </A.Error>
            ) : null}

            {isErrorOnForgotPassword?.status ? (
              <A.Error>
                {isErrorOnForgotPassword?.msg === 'unverified' ? (
                  <span>
                    Cannot sign in, {username} is not verified. Select{' '}
                    <A.ErrorLink href={`/verifyemail/${code}`}>
                      Resend
                    </A.ErrorLink>{' '}
                    verification email to verify email.
                  </span>
                ) : (
                  isErrorOnForgotPassword?.msg || `Something went wrong`
                )}
              </A.Error>
            ) : null}
          </B.Error>
          <B.Email justifyContent={`center`} flex paddingTop={14}>
            <A.Input
              type={`email`}
              isInputInvalid={username && !isUsernameValid}
              icon={<IconEmail color={COLOR_EMAIL} />}
              label={`Email Address`}
              value={username}
              onChange={fromPageLogin.on.usernameInput}
            />
          </B.Email>
          <B.Password justifyContent={`center`} flex paddingTop={15}>
            <A.Input
              icon={<IconLock color={COLOR_LOCK} />}
              label={`Password`}
              type={`password`}
              value={password}
              onChange={fromPageLogin.on.passwordInput}
            />
          </B.Password>
          <B.Forgot
            justifyContent={`center`}
            flex
            paddingTop={10}
            paddingRight={5}
          >
            <A.Forgot flex justifyContent={`flex-end`}>
              <A.Button
                isDisabled={!isUsernameValid}
                onClick={onForgotPassword}
              >
                Forgot Password?
              </A.Button>
            </A.Forgot>
          </B.Forgot>
          <B.Remember justifyContent={`center`} flex paddingTop={10}>
            <A.Remember flex justifyContent={`flex-start`}>
              <A.FieldCheckbox
                label={`Remember Me`}
                checked={rememberMe}
                onChange={fromPageLogin.on.rememberMeInput}
              />
            </A.Remember>
          </B.Remember>
          <B.Signin justifyContent={`center`} flex>
            <A.ButtonSignIn onClick={onSubmit} isDisabled={!isUsernameValid}>
              Sign In
            </A.ButtonSignIn>
          </B.Signin>
        </>
      )}
    </A.SectionComposition>
  )
}
