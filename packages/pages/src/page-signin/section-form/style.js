import styled from 'styled-components'
import {Box} from 'atomic-layout'
import {Composition} from 'atomic-layout'

import {Button} from '@tt/ui'
import {ButtonOutline} from '@tt/ui'
import {FieldCheckbox} from '@tt/ui'
import {Input} from '@tt/ui'

const A = {
  Button,
  FieldCheckbox
}

A.ButtonSignIn = styled(ButtonOutline)`
  width: 320px;
  height: 50px;

  @media (min-width: 340px) {
    width: 340px;
  }
`

A.Forgot = styled(Box)`
  font-size: 0.9em;

  width: 340px;
`

A.Remember = styled(Box)`
  width: 340px;
`

A.Input = styled(Input)`
  width: 340px;
`
A.Error = styled.div`
  color: #e60b0b;
  font-size: 0.8em;
`

A.ErrorLink = styled.a`
  color: #e60b0b;
  &:hover {
    text-decoration: none;
  }
`

A.SectionComposition = styled(Composition)``

export default A
