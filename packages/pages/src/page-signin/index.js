import React from 'react'
import A from './style'
import {SectionHeader} from '@tt/sections'
import {fromPageLogin} from '@tt/domains'

const areas = `
  header
  form
`

export function PageSignIn({onBackClick, onSuccess, onForgotPassword}) {
  React.useEffect(() => fromPageLogin.on.clear, [])

  return (
    <A.PageComposition areas={areas}>
      {B => (
        <>
          <B.Header>
            <SectionHeader onBackClick={onBackClick} />
          </B.Header>
          <B.Form>
            <A.SectionForm
              onSuccess={onSuccess}
              onForgotPassword={onForgotPassword}
            />
          </B.Form>
        </>
      )}
    </A.PageComposition>
  )
}
