import {Composition} from 'atomic-layout'
import styled from 'styled-components'
import {SectionForm} from './section-form'

const A = {
  SectionForm,
}

A.PageComposition = styled(Composition)``

export default A
