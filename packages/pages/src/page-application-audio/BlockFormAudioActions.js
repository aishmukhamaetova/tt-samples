import {useStore} from 'effector-react'
import React from 'react'
import styled from 'styled-components'

import {fromPageApplicationAudio} from '@tt/domains'
import {ButtonOutline} from '@tt/ui'

const A = {}

// function useAudioStream(stream) {
//   const ref = React.useRef(null)

//   React.useEffect(() => {
//     if (ref.current) {
//       ref.current.srcObject = stream
//     }
//   }, [ref, stream])

//   return ref
// }

export function BlockFormAudioActions(props) {
  const isSupported = useStore(
    fromPageApplicationAudio.audioRecord.$.isSupported,
  )
  const isStarted = useStore(fromPageApplicationAudio.audioRecord.$.isStarted)

  const stream = useStore(fromPageApplicationAudio.$.stream)
  const blob = useStore(fromPageApplicationAudio.$.blob)
  const file = useStore(fromPageApplicationAudio.$.file)
  // const audio = useAudioStream(stream)

  const audio = React.useRef(null)
  const ref2 = React.useRef(null)

  React.useEffect(() => {
    if (file === null) {
      if (ref2.current) {
        ref2.current.value = null
      }
    }
  }, [ref2, file])

  React.useEffect(() => {
    fromPageApplicationAudio.on.clear()
  }, [])

  // React.useEffect(() => {
  //   if (audio.current && blob) {
  //     audio.current.src = URL.createObjectURL(blob)
  //   }
  // }, [audio, blob])

  function onRecord() {
    // fromPageApplicationAudio.on.file(null)
    if (isStarted) {
      fromPageApplicationAudio.run.audioStop()
    } else {
      fromPageApplicationAudio.run.audioRecord({audio})
    }
  }

  return (
    <A.Grid>
      <A.AreaListen>
        {!isSupported && `Not supported`}
        <audio ref={audio} controls />
      </A.AreaListen>
      <A.AreaRecord>
        <A.ButtonOutline onClick={onRecord}>
          {isStarted ? `Stop Voice Recording` : `Create Voice Recording`}
        </A.ButtonOutline>
      </A.AreaRecord>
      <A.AreaAccess>
        <label>
          <A.ButtonOutline>Access Saved Audio</A.ButtonOutline>
          <A.Input
            ref={ref2}
            id="audioFile2"
            type="file"
            accept="audio/*"
            onChange={(e) => {
              fromPageApplicationAudio.on.fileOnChange(e)
            }}
            onClick={(e) => {
              fromPageApplicationAudio.on.clear()
            }}
          />
        </label>
      </A.AreaAccess>
    </A.Grid>
  )
}

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'listen'
    'record'
    'access';

  grid-row-gap: 20px;

  grid-template-columns: 80vw;
  grid-template-rows: 50px;
`

A.AreaListen = styled.div`
  grid-area: listen;

  display: flex;

  justify-content: center;
  align-items: center;
`

A.AreaRecord = styled.div`
  grid-area: record;
`
A.AreaAccess = styled.div`
  grid-area: access;
`

A.ButtonOutline = styled(ButtonOutline)`
  height: 50px;
`
A.Input = styled.input`
  display: none;
`
