import {useStore} from 'effector-react'
import React from 'react'
import styled from 'styled-components'

import {fromPageApplicationAudio} from '@tt/domains'
import {ButtonOutline} from '@tt/ui'

const A = {}

export function BlockFormAudioSubmit({id, onSubmitSuccess}) {
  const file = useStore(fromPageApplicationAudio.$.file)
  const blob = useStore(fromPageApplicationAudio.$.blob)

  const lastSuccess = useStore(
    fromPageApplicationAudio.audioSubmit.$.lastSuccess,
  )

  const lastSuccess2 = useStore(
    fromPageApplicationAudio.audioSubmitFile.$.lastSuccess,
  )

  React.useEffect(() => {
    if (lastSuccess && onSubmitSuccess) {
      onSubmitSuccess()
    }
  }, [lastSuccess])

  React.useEffect(() => {
    if (lastSuccess2 && onSubmitSuccess) {
      onSubmitSuccess()
    }
  }, [lastSuccess2])

  React.useEffect(() => fromPageApplicationAudio.on.clear, [])

  function onSubmit() {
    if (file) {
      fromPageApplicationAudio.run.audioSubmitFile({
        activityId: id,
        type: `audio/webm`,
      })
    } else {
      fromPageApplicationAudio.run.audioSubmit({
        activityId: id,
        type: `audio/webm`,
      })
    }
  }

  return (
    <A.Grid>
      <A.AreaSubmit>
        <A.ButtonOutline disabled={!(file || blob)} onClick={onSubmit}>
          Submit
        </A.ButtonOutline>
      </A.AreaSubmit>
    </A.Grid>
  )
}

A.Grid = styled.div`
  display: grid;
  grid-template-areas: 'submit';

  grid-template-columns: 80vw;
`

A.AreaSubmit = styled.div`
  grid-area: submit;
`

A.ButtonOutline = styled(ButtonOutline)`
  opacity: ${props => (props.disabled ? '0.5' : '1.0')};
  height: 45px;
`
