import {createGate, useGate} from 'effector-react'
import {useStore} from 'effector-react'
import React from 'react'
import styled from 'styled-components'

import {SectionFormAudio} from './SectionFormAudio'
import {SectionHeader} from '@tt/sections'
import {SectionModal} from './SectionModal'

import {fromPageApplicationAudio} from '@tt/domains'
import {fromAuth} from '@tt/domains'
import {useHistory} from 'react-router-dom'

const A = {}

const Gate = createGate()

fromPageApplicationAudio.audioSubmitFile.$.lastSuccess.reset(Gate.close)
fromPageApplicationAudio.audioSubmit.$.lastSuccess.reset(Gate.close)

export function PageApplicationAudio({id, onBackClick, onSubmitSuccess}) {
  useGate(Gate)

  const history = useHistory()

  React.useEffect(() => {
    async function check() {
      const result = await fromAuth.run.session()
      if (!result) {
        history.push(`/signin`)
      }
    }

    check()
  })

  const [height, setHeight] = React.useState(window.innerHeight)

  const isPending = useStore(fromPageApplicationAudio.run.audioSubmit.pending)

  React.useEffect(() => {
    const onResize = () => setHeight(window.innerHeight)
    window.addEventListener('resize', onResize)
    return () => {
      window.removeEventListener('resize', onResize)
    }
  })

  return (
    <A.Grid style={{'--min-height': `${height}px`}}>
      <SectionModal isOpen={isPending} />
      <A.AreaHeader>
        <SectionHeader onBackClick={onBackClick} />
      </A.AreaHeader>
      <A.AreaForm>
        <SectionFormAudio id={id} onSubmitSuccess={onSubmitSuccess} />
      </A.AreaForm>
    </A.Grid>
  )
}

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'form';
  grid-template-rows: auto 1fr;

  min-height: 100vh;
  min-height: var(--min-height);

  width: 100vw;

  overflow-x: hidden;
`

A.AreaHeader = styled.div`
  grid-area: header;
`
A.AreaForm = styled.div`
  grid-area: form;

  padding: 38px;
`
