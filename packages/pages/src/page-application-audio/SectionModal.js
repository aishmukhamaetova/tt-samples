import React from 'react'

const A = {}

const areas = `
  title
  loader
`

import {Modal} from '@tt/ui'

export function SectionModal({isOpen, onClose}) {
  return isOpen ? (
    <Modal onClose={onClose}>
      <A.SectionComposition areas={areas} width={277}>
        {B => (
          <>
            <B.Title
              padding={10}
              style={{borderBottom: `1px solid #eee`, fontWeight: `bold`}}
              justifyContent={`center`}
              alignItems={`center`}
              flex
            >
              File uploading
            </B.Title>
            <B.Loader
              padding={10}
              justifyContent={`center`}
              alignItems={`center`}
              flex
            >
              <img src="/images/spinner.svg" />
            </B.Loader>
          </>
        )}
      </A.SectionComposition>
    </Modal>
  ) : null
}

import styled from 'styled-components'
import {Composition} from 'atomic-layout'

A.SectionComposition = styled(Composition)`
  font-weight: 300;
`
