import {useStore} from 'effector-react'
import React from 'react'
import styled from 'styled-components'

import {fromPageApplicationAudio} from '@tt/domains'
import {IconClose} from '@tt/ui'

const A = {}

export function BlockFileInfo() {
  const file = useStore(fromPageApplicationAudio.$.file)
  const blob = useStore(fromPageApplicationAudio.$.blob)

  function onClose() {
    fromPageApplicationAudio.on.file(null)
  }

  function onBlobClose() {
    fromPageApplicationAudio.on.blob(null)
  }

  function MakeShort(name) {
    return name?.length < 10
      ? name
      : `${name?.substr(0, 3)}...${name?.substr(name?.length - 5, 5)}`
  }

  if (blob) {
    return (
      <A.Grid>
        <A.AreaName>{`${blob?.size}.${blob?.type}`}</A.AreaName>
        <A.AreaActions>
          <IconClose onClick={onBlobClose} />
        </A.AreaActions>
      </A.Grid>
    )
  }

  if (file) {
    return (
      <A.Grid>
        <A.AreaName>{MakeShort(file?.name)}</A.AreaName>
        <A.AreaActions>
          <IconClose onClick={onClose} />
        </A.AreaActions>
      </A.Grid>
    )
  }

  return null
}

A.Grid = styled.div`
  display: grid;
  grid-template-areas: 'name actions';

  grid-template-rows: 45px;
  grid-template-columns: 40vw 5vw;
`

A.AreaName = styled.div`
  grid-area: name;

  display: flex;
  align-items: center;
  justify-content: center;
`

A.AreaActions = styled.div`
  grid-area: actions;
`
