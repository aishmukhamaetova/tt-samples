import {useStore} from 'effector-react'
import React from 'react'
import {BlockFormAudioActions} from './BlockFormAudioActions'
import {BlockFormAudioSubmit} from './BlockFormAudioSubmit'
import {BlockFileInfo} from './BlockFileInfo'
import {fromPageApplicationAudio} from '@tt/domains'
import {fromActivity} from '@tt/domains'

const A = {}

export function SectionFormAudio({id, onSubmitSuccess}) {
  const errors1 = useStore(fromPageApplicationAudio.audioSubmit.$.errors)
  const errors2 = useStore(fromPageApplicationAudio.audioSubmitFile.$.errors)
  const error = useStore(fromPageApplicationAudio.$.error)

  const errors = [...errors1, ...errors2]

  const list = useStore(fromActivity.$.list)
  const doc = list?.find(o => o.id === id)
  const short =
    doc?.title && !/\s/g.test(doc?.title)
      ? `${doc?.title.length > 20 && doc?.title.substr(0, 20)}${
          doc?.title.length > 20 ? `...` : ``
        }` || doc?.title
      : doc?.title

  return (
    <A.Container>
      <A.Grid>
        <A.AreaTitle>
          {short || `Empty`}
          {errors.length > 0 && (
            <A.Error color={`red`}>Already applied</A.Error>
          )}
          {error && <A.Error color={`red`}>{error}</A.Error>}
        </A.AreaTitle>
        <A.AreaDescription>
          Add a 1 min audio message describing how you can add value to our
          team.
        </A.AreaDescription>
        <A.AreaActions>
          <BlockFormAudioActions />
        </A.AreaActions>
        <A.AreaInfo>
          <BlockFileInfo />
        </A.AreaInfo>
        <A.AreaSubmit>
          <BlockFormAudioSubmit id={id} onSubmitSuccess={onSubmitSuccess} />
        </A.AreaSubmit>
      </A.Grid>
    </A.Container>
  )
}

import styled from 'styled-components'

A.Container = styled.div`
  height: 100%;
`

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'title'
    'description'
    'actions'
    'info'
    'submit';
  grid-row-gap: 10px;

  grid-template-rows: auto auto 1fr auto auto;

  height: 100%;
`

A.AreaTitle = styled.div`
  grid-area: title;

  font-size: 21px;
  font-weight: bold;
  color: #000000;
  letter-spacing: -0.5px;
  line-height: 22px;
`
A.AreaDescription = styled.div`
  grid-area: description;

  font-size: 20px;
  color: #000000;
  letter-spacing: -0.32px;
  line-height: 18px;
`
A.AreaActions = styled.div`
  grid-area: actions;

  display: flex;

  align-items: center;
  justify-content: center;
`
A.AreaInfo = styled.div`
  grid-area: info;

  display: flex;

  justify-content: center;
`
A.AreaSubmit = styled.div`
  grid-area: submit;

  display: flex;

  justify-content: center;
`

A.Error = styled.div`
  color: ${props => (props.color ? props.color : '#e60b0b')};
  margin: 10px auto;
`
