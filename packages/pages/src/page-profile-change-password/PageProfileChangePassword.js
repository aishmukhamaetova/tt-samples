import React from 'react'
import {fromPageProfileChangePassword} from '@tt/domains'
import {SectionTitle} from '@tt/sections'

const A = {}

const areas = `
  header
  form
`

const templateRows = `1fr 80vh`

export function PageProfileChangePassword(props) {
  const [code, setCode] = React.useState(null)

  React.useEffect(() => {
    async function checkCode() {
      try {
        await fromPageProfileChangePassword.runCheckChangePasswordCode({
          code: props.code,
        })
        setCode(props.code)
      } catch (e) {
        setCode('invalid')
      }
    }

    if (code === null) {
      checkCode()
    }
  })

  return (
    <A.PageComposition areas={areas} templateRows={templateRows}>
      {(B) => (
        <>
          <B.Header>
            <SectionHeader onBackClick={props.onBackClick} />
          </B.Header>
          <B.Form>
            <A.SectionForm code={code} onSuccess={props.onSuccess} />
          </B.Form>
          )}
        </>
      )}
    </A.PageComposition>
  )
}

import {Composition} from 'atomic-layout'
import {SectionForm} from './SectionForm'
import {SectionHeader} from '@tt/sections'

A.SectionForm = SectionForm

A.PageComposition = Composition
