import React from 'react'

import {IconLock} from '@tt/ui'
import {SectionTitle} from '@tt/sections'

import {fromPageProfileChangePassword} from '@tt/domains'

const A = {}

const areas = `
  title
  error
  password
  verifypassword
  save
`
const templateRows = `1fr 20px auto 48vh 1fr`

const COLOR_LOCK = `rgb(180,16,58)`

export function SectionForm(props) {
  const {code} = props
  const [password1, setPassword1] = React.useState(``)
  const [password2, setPassword2] = React.useState(``)

  const isCodeInvalid = code === null || code === 'invalid'

  const onSubmit = async () => {
    if (isCodeInvalid) {
      return
    }

    try {
      if (password1 === password2) {
        await fromPageProfileChangePassword.runUserUpdateByCode({
          params: {
            code: props?.code,
            password: password1,
          },
        })

        if (props.onSuccess) {
          props.onSuccess()
        }
      }
    } catch (err) {}
  }

  return (
    <A.SectionComposition areas={areas} templateRows={templateRows}>
      {(B) => (
        <>
          <SectionTitle title={`Change Password`} />

          <B.Error
            justifyContent={`start`}
            flex
            paddingTop={15}
            paddingLeft={15}
          >
            {password1 && password2 && password1 !== password2 ? (
              <A.Error color={'#e60b0b'}>Password does not match</A.Error>
            ) : null}

            {isCodeInvalid ? (
              <A.Error color={'#e60b0b'}>
                Code not found, expired or has been used
              </A.Error>
            ) : null}
          </B.Error>

          <B.Password justifyContent={`center`} flex paddingTop={15}>
            <A.Input
              disabled={isCodeInvalid}
              icon={<IconLock color={COLOR_LOCK} />}
              label={`New Password`}
              type={`password`}
              value={password1}
              onChange={({target: {value}}) =>
                !isCodeInvalid && setPassword1(value)
              }
            />
          </B.Password>

          <B.Verifypassword justifyContent={`center`} flex paddingTop={15}>
            <A.Input
              disabled={isCodeInvalid}
              isInputInvalid={password1 && password2 && password1 !== password2}
              icon={<IconLock color={COLOR_LOCK} />}
              label={`Verify Password`}
              type={`password`}
              value={password2}
              onChange={({target: {value}}) =>
                !isCodeInvalid && setPassword2(value)
              }
            />
          </B.Verifypassword>

          <B.Save justifyContent={`center`} flex paddingTop={15}>
            <A.ButtonCreate
              disabled={
                (password1 && password2 && password1 !== password2) ||
                code === null ||
                code === 'invalid'
              }
              onClick={onSubmit}
            >
              {`Change Password`}
            </A.ButtonCreate>
          </B.Save>
        </>
      )}
    </A.SectionComposition>
  )
}

import styled from 'styled-components'
import {Box} from 'atomic-layout'
import {Composition} from 'atomic-layout'

import {Button} from '@tt/ui'
import {ButtonOutline} from '@tt/ui'
import {FieldCheckbox} from '@tt/ui'
import {Input} from '@tt/ui'
import {InputTag} from '@tt/ui'
import {Tag} from '@tt/ui'

A.Button = Button
A.FieldCheckbox = FieldCheckbox
A.Tag = Tag

A.ButtonCreate = styled(ButtonOutline)`
  width: 320px;
  height: 50px;
  opacity: ${(props) => (props.disabled ? '0.5' : '1.0')};
  @media (min-width: 340px) {
    width: 340px;
  }
`

A.InputTag = styled(InputTag)`
  width: 340px;
`

A.Terms = styled(Box)`
  font-size: 0.7em;
  width: 340px;
`

A.TermsText = styled(Box)`
  font-size: 0.7em;
  font-weight: 500;
  width: 290px;
`

A.SkillTitle = styled.div`
  font-size: 1.3em;
  font-weight: 500;

  width: 340px;
`

A.SkillText = styled.div`
  font-size: 0.7em;
  font-weight: 500;
  width: 340px;
`

A.Tags = styled.div`
  width: 340px;
  margin-left: -5px;
  margin-irhgt: -5px;
  position: relative;
`

A.Input = styled(Input)`
  width: 340px;
  opacity: ${(props) => (props.disabled ? '0.5' : '1.0')};
`
A.Error = styled.div`
  color: ${(props) => (props.color ? props.color : '#e60b0b')};
  font-size: 0.8em;
`

A.SectionComposition = styled(Composition)``
