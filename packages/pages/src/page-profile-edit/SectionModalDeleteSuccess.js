import React from 'react'

import {IconCheckCircle} from '@tt/ui'

const A = {}

const areas = `
  title
  content
`

import {Modal} from '@tt/ui'

const COLOR = `rgb(254,184,43)`

export function SectionModalDeleteSuccess({name, isOpen, onClose}) {
  return isOpen ? (
    <Modal onClose={onClose}>
      <A.SectionComposition areas={areas} width={277}>
        {B => (
          <>
            <B.Title
              as={A.Title}
              alignItems={`center`}
              justifyContent={`center`}
              height={35}
              flex
            >
              <IconCheckCircle color={COLOR} size={22} />
              <A.TitleLabel>Confirmation</A.TitleLabel>
            </B.Title>
            <B.Content paddingTop={15} paddingBottom={15}>
              <A.Content>{name} profile deleted.</A.Content>
            </B.Content>
          </>
        )}
      </A.SectionComposition>
    </Modal>
  ) : null
}

import styled from 'styled-components'
import {Composition} from 'atomic-layout'

A.Content = styled.div`
  text-align: center;

  padding: 10px 35px;
`

A.SectionComposition = styled(Composition)`
  font-weight: 300;
`

A.Title = styled.div`
  border-bottom: 1px solid #eee;
`

A.TitleLabel = styled.div`
  font-size: 0.9em;
  font-weight: 500;

  padding-left: 5px;
`
