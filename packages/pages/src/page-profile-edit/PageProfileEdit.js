import React from 'react'
import {useStore} from 'effector-react'

import {fromAuth} from '@tt/domains'
import {useHistory} from 'react-router-dom'

const A = {}

export function PageProfileEdit({onBackClick, onSuccessEdit}) {
  const history = useHistory()

  React.useEffect(() => {
    async function check() {
      const result = await fromAuth.run.session()
      if (!result) {
        setTimeout(() => {
          history.push(`/`)
        }, 3000)
      }
    }

    check()
  })

  const [isOpen, setIsOpen] = React.useState(false)
  const [isSuccessOpen, setIsSuccessOpen] = React.useState(false)
  const [isErrorOnRemove, showErrorOnRemove] = React.useState(false)
  const sessionId = useStore(fromAuth.$.sessionId)
  const {email, name} = useStore(fromAuth.$.currentUser)

  const onDeleteAccept = async () => {
    showErrorOnRemove(false)
    setIsOpen(false)

    try {
      const {errors = []} = await fromAuth.run.remove({email, sessionId})

      if (errors.length > 0) {
        showErrorOnRemove(true)
        setIsSuccessOpen(false)
        return
      }

      if (errors.length === 0) {
        //add endpoint to server
        setIsSuccessOpen(true)
        setTimeout(() => {
          setIsSuccessOpen(false)
          history.push(`/`)
        }, 3000)
      }
    } catch (err) {
      showErrorOnRemove(true)
    }
  }

  const onDeleteClick = () => {
    setIsOpen(true)
  }

  const onDeleteClose = () => {
    setIsOpen(false)
  }

  const onSuccessClose = () => {
    setIsSuccessOpen(false)
  }

  return (
    <>
      <A.SectionModalDelete
        isOpen={isOpen}
        onAccept={onDeleteAccept}
        onClose={onDeleteClose}
      />
      <A.SectionModalDeleteSuccess
        name={name}
        isOpen={isSuccessOpen}
        onClose={onSuccessClose}
      />
      <A.Grid>
        <A.AreaHeader>
          <SectionHeader onBackClick={onBackClick} />
        </A.AreaHeader>
        <A.AreaForm>
          <SectionForm
            onDeleteClick={onDeleteClick}
            onSuccessEdit={onSuccessEdit}
          />
        </A.AreaForm>
      </A.Grid>
    </>
  )
}

import styled from 'styled-components'
import {SectionForm} from './SectionForm'
import {SectionHeader} from '@tt/sections'
import {SectionModalDelete} from './SectionModalDelete'
import {SectionModalDeleteSuccess} from './SectionModalDeleteSuccess'

A.SectionModalDelete = SectionModalDelete
A.SectionModalDeleteSuccess = SectionModalDeleteSuccess

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'form';
`

A.AreaHeader = styled.div`
  grid-area: header;
`
A.AreaForm = styled.div`
  grid-area: form;
`
