import React from 'react'

import {IconTimesCircle} from '@tt/ui'

const A = {}

const areas = `
  title
  content
  actions
`

import {Modal} from '@tt/ui'

export function SectionModalDelete({isOpen, onAccept, onClose}) {
  return isOpen ? (
    <Modal onClose={onClose}>
      <A.SectionComposition areas={areas} width={277}>
        {B => (
          <>
            <B.Title
              as={A.Title}
              alignItems={`center`}
              justifyContent={`center`}
              height={35}
              flex
            >
              <IconTimesCircle color={`red`} size={22} />
              <A.TitleLabel>Delete</A.TitleLabel>
            </B.Title>
            <B.Content paddingTop={15}>
              <A.Content>
                Confirm you wish to delete your Team Tasker profile.
              </A.Content>
            </B.Content>
            <B.Actions
              flex
              justifyContent={`center`}
              paddingTop={15}
              paddingBottom={25}
            >
              <A.ButtonCancel onClick={onClose}>Cancel</A.ButtonCancel>
              <A.ButtonDelete onClick={onAccept}>Delete</A.ButtonDelete>
            </B.Actions>
          </>
        )}
      </A.SectionComposition>
    </Modal>
  ) : null
}

import styled from 'styled-components'
import {Composition} from 'atomic-layout'

A.ButtonCancel = styled.div`
  user-select: none;
  cursor: pointer;

  border-radius: 8px;

  padding: 5px 10px;
  margin-right: 5px;

  border: 1px solid #979797;
`

A.ButtonDelete = styled.div`
  user-select: none;
  cursor: pointer;

  background: #000;
  color: #fff;

  font-weight: 400;

  border: 1px solid rgba(253, 183, 62, 1);
  border-radius: 6px;

  padding: 5px 10px;

  margin-left: 5px;
`

A.Content = styled.div`
  text-align: center;

  padding: 10px 45px;
`

A.SectionComposition = styled(Composition)`
  font-weight: 300;
`

A.Title = styled.div`
  border-bottom: 1px solid #eee;
`

A.TitleLabel = styled.div`
  font-size: 0.9em;
  font-weight: 500;

  padding-left: 5px;
`
