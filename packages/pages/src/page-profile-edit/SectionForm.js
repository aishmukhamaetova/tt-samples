import React from 'react'

import {IconEmail} from '@tt/ui'
import {IconLock} from '@tt/ui'
import {IconPhone} from '@tt/ui'
import {IconUser} from '@tt/ui'

import {useStore} from 'effector-react'
import {useList} from 'effector-react'
import {fromAuth} from '@tt/domains'
import {fromPageProfileEdit} from '@tt/domains'
import {SectionTitle} from '@tt/sections'

const A = {}
const COUNT_SKILLS = 5

const areas = `
  title
  name
  email
  phone
  password
  skill
  tag
  tags
  terms
  text
  readtext
  readmore
  delete
  submit
`

const COLOR_EMAIL = `rgb(140,105,154)`
const COLOR_PHONE = `rgb(27,111,82)`
const COLOR_LOCK = `rgb(180,16,58)`
const COLOR_USER = `rgb(232,109,50)`

function Field({name, icon, label, type = 'text'}) {
  const value = useStore(fromPageProfileEdit.$[name])
  const onChange = fromPageProfileEdit.on[`${name}Input`]

  return (
    <A.Input
      icon={icon}
      label={label}
      type={type}
      value={value}
      onChange={onChange}
    />
  )
}

function FieldTag({name}) {
  const value = useStore(fromPageProfileEdit.$[name])
  const onChange = fromPageProfileEdit.on[`${name}Input`]
  const onKeyDown = ({keyCode}) => {
    if (keyCode === 13 && value.trim().length > 0) {
      fromPageProfileEdit.on.listPush(value)
      fromPageProfileEdit.on.clearSkills()
    }
  }

  return (
    <A.InputTag
      value={value}
      maxLength="20"
      onChange={onChange}
      onKeyDown={onKeyDown}
    />
  )
}

export function SectionForm({onDeleteClick, onSuccessEdit}) {
  const sessionId = useStore(fromAuth.$.sessionId)
  const currentUser = useStore(fromAuth.$.currentUser)
  const password = useStore(fromPageProfileEdit.$.password)
  const [isMore, setIsMore] = React.useState(false)

  // const skills = useStore(fromPageProfileEdit.$.skills)
  const name = useStore(fromPageProfileEdit.$.name)
  const phone = useStore(fromPageProfileEdit.$.phone)
  const list = useStore(fromPageProfileEdit.$.list)

  const {email = ``} = currentUser || {}

  React.useEffect(() => {
    const {name = ``, phone = ``, skills = ``} = currentUser || {}

    fromPageProfileEdit.on.name(name)
    fromPageProfileEdit.on.phone(phone)
    fromPageProfileEdit.on.password(``)
    fromPageProfileEdit.on.list(skills.split(`,`))
  }, [])

  const onMore = () => setIsMore(v => !v)

  const tags = useList(fromPageProfileEdit.$.list, name => (
    <A.Tag
      value={name}
      onClick={() => fromPageProfileEdit.on.listRemove(name)}
    />
  ))

  const onSubmit = async () => {
    try {
      const {errors} = await fromAuth.run.updateProfile({
        password,
        skills: list.join(`,`),
        name,
        phone,
        sessionId,
      })
      if (errors.length === 0) {
        onSuccessEdit()
      }
    } catch (err) {
      console.warn(`SectionForm`, `Error`, err)
    }
  }

  return (
    <A.SectionComposition areas={areas}>
      {B => (
        <>
          <SectionTitle title={`Edit Profile`} />
          <B.Name justifyContent={`center`} flex paddingTop={14}>
            <Field
              name={`name`}
              icon={<IconUser color={COLOR_USER} />}
              label={`Name`}
            />
          </B.Name>
          <B.Email justifyContent={`center`} flex paddingTop={14}>
            <A.Input
              icon={<IconEmail color={COLOR_EMAIL} />}
              label={`Email Address`}
              value={email}
              isDisabled
            />
          </B.Email>
          <B.Phone justifyContent={`center`} flex>
            <Field
              type={`number`}
              name={`phone`}
              icon={<IconPhone color={COLOR_PHONE} />}
              label={`Phone Number`}
            />
          </B.Phone>
          <B.Password justifyContent={`center`} flex paddingTop={15}>
            <Field
              name={`password`}
              icon={<IconLock color={COLOR_LOCK} />}
              label={`Password`}
              type={`password`}
            />
          </B.Password>
          <B.Skill
            alignItems={`center`}
            flex
            paddingTop={20}
            flexDirection={`column`}
            paddingLeft={20}
          >
            <A.SkillTitle>Skills</A.SkillTitle>
            <A.SkillText>No need to submit CV!</A.SkillText>
            <A.SkillText>
              <div>
                Tell us in {COUNT_SKILLS} word tags how you can add value to our
                team
              </div>
              <div>
                (To create a word tag, enter a word, then tap/enter to create
                new word)
              </div>
            </A.SkillText>
          </B.Skill>
          <B.Tag justifyContent={`center`} flex paddingTop={15}>
            <FieldTag name={`skills`} />
          </B.Tag>
          <B.Tags justifyContent={`center`} flex paddingTop={15}>
            <A.Tags>{tags}</A.Tags>
          </B.Tags>
          <B.Terms justifyContent={`center`} flex paddingTop={10}>
            <A.Terms flex justifyContent={`flex-start`}>
              <A.FieldCheckbox
                size={`small`}
                checked={true}
                label={`Terms & Conditions`}
                readOnly={true}
              />
            </A.Terms>
          </B.Terms>
          <B.Text
            justifyContent={`center`}
            flex
            paddingLeft={5}
            marginTop={-10}
          >
            <A.TermsText>
              <p>
                I&nbsp;have read the Terms and Conditions, Disclaimer and
                Privacy information and I&nbsp;understand
                I&nbsp;am&nbsp;providing personal information for the purposes
                of&nbsp;considering and connecting opportunities to&nbsp;work
                with, or&nbsp;provide services to&nbsp;OZ&nbsp;Minerals under
                a&nbsp;variety of&nbsp;potential arrangements.
              </p>
            </A.TermsText>
          </B.Text>

          <B.Readtext>
            {isMore ? (
              <A.TermsText>
                <p>
                  You agree that information contained on&nbsp;this Site
                  is&nbsp;for personal use only and may not be&nbsp;sold,
                  redistributed or&nbsp;used for any commercial purpose (this
                  includes but is&nbsp;not limited to&nbsp;the use
                  of&nbsp;Advertiser contact details for unsolicited commercial
                  correspondence and the information available via our insights
                  and resources subdomain{' '}
                  <a href="https://insightsresources.seek.com.au/">
                    https://insightsresources.seek.com.au/
                  </a>
                  ). You may download material from this Site for your personal,{' '}
                  <nobr>non-commercial</nobr> use only, provided you keep intact
                  all copyright and other proprietary notices. You may not
                  modify, copy, reproduce, republish, upload, post, transmit
                  or&nbsp;distribute in&nbsp;any way any material from this Site
                  including code and software. You must not use this Site for
                  any purpose that is&nbsp;unlawful or&nbsp;prohibited
                  by&nbsp;these terms of&nbsp;use. You may not use data mining,
                  robots, screen scraping, or&nbsp;similar automated data
                  gathering, extraction or&nbsp;publication tools on&nbsp;this
                  Site (including without limitation for the purposes
                  of&nbsp;establishing, maintaining, advancing
                  or&nbsp;reproducing information contained on&nbsp;our Site
                  on&nbsp;your own website or&nbsp;in&nbsp;any other
                  publication), except with Our prior written consent.
                </p>
                <p>
                  OZ&nbsp;Minerals gives no&nbsp;guarantee to&nbsp;you
                  of&nbsp;the continued availability of&nbsp;any particular job
                  advertised on&nbsp;the Site and will not be&nbsp;liable
                  to&nbsp;you should OZ&nbsp;Minerals have filled the vacancy
                  at&nbsp;any time prior to&nbsp;removal of&nbsp;the
                  advertisement from the Site. Whilst OZ&nbsp;Minerals takes
                  efforts to&nbsp;ensure that jobs advertised are for actual job
                  vacancies, it&nbsp;gives no&nbsp;guarantee to&nbsp;you that
                  every job advertisement represents an&nbsp;actual job vacancy.
                </p>
                <p>
                  To&nbsp;become a&nbsp;registered user, you must provide
                  a&nbsp;password and a&nbsp;login name. You are entirely
                  responsible if&nbsp;you do&nbsp;not maintain the
                  confidentiality of&nbsp;your password and login name.
                  Furthermore, you are entirely responsible for any and all
                  activities that occur under your login name. You may change
                  your password at&nbsp;any time by&nbsp;following instructions.
                  You may also delete services attached to&nbsp;your
                  registration at&nbsp;your convenience. You agree
                  to&nbsp;immediately notify OZ&nbsp;Minerals of&nbsp;any
                  unauthorized use of&nbsp;your login name or&nbsp;any other
                  breach of&nbsp;security known to&nbsp;you.
                </p>
                <p>
                  You agree that it&nbsp;is&nbsp;a&nbsp;condition on&nbsp;your
                  use of&nbsp;the Team Tasker Site and of&nbsp;any other
                  services provided by&nbsp;OZ&nbsp;Minerals or&nbsp;through the
                  Site that you will not through any act or&nbsp;omission
                  (including but not limited to&nbsp;creating a&nbsp;profile
                  on&nbsp;the Team Tasker Site) mislead or&nbsp;deceive others.
                </p>
              </A.TermsText>
            ) : null}
          </B.Readtext>

          <B.Readmore>
            <A.AreaMore onClick={onMore}>
              {isMore ? `Read Less` : `Read More`}
            </A.AreaMore>
          </B.Readmore>
          <B.Delete justifyContent={`center`} flex paddingTop={40}>
            <A.Delete onClick={onDeleteClick}>Delete Profile</A.Delete>
          </B.Delete>
          <B.Submit
            justifyContent={`center`}
            flex
            paddingTop={25}
            paddingBottom={15}
          >
            <A.ButtonCreate onClick={onSubmit}>Save Profile</A.ButtonCreate>
          </B.Submit>
        </>
      )}
    </A.SectionComposition>
  )
}

import styled from 'styled-components'
import {Box} from 'atomic-layout'
import {Composition} from 'atomic-layout'

import {Button} from '@tt/ui'
import {ButtonOutline} from '@tt/ui'
import {FieldCheckbox} from '@tt/ui'
import {Input} from '@tt/ui'
import {InputTag} from '@tt/ui'
import {Tag} from '@tt/ui'

A.Button = Button
A.FieldCheckbox = FieldCheckbox
A.Tag = Tag

A.ButtonCreate = styled(ButtonOutline)`
  width: 320px;
  height: 50px;

  @media (min-width: 340px) {
    width: 340px;
  }
`

A.Delete = styled.span`
  user-select: none;
  cursor: pointer;

  font-size: 0.7em;
  color: #e60b0b;

  width: 340px;
`

A.InputTag = styled(InputTag)`
  width: 340px;
`

A.Terms = styled(Box)`
  font-size: 0.7em;
  width: 340px;
`

A.TermsText = styled(Box)`
  && {
    font-size: 13px;
    font-weight: 500;
    width: 290px;
    user-select: none;
    margin: 0 auto;
    p {
      font-size: 13px;
      font-weight: 500;

      a {
        text-decoration: underline;
        color: #000;
        &: hover {
          text-decoration: none;
        }
      }
    }
  }
`

A.AreaMore = styled(Box)`
  cursor: pointer;

  grid-area: more;

  display: flex;
  justify-content: center;

  text-decoration: underline;

  font-size: 0.7em;
  font-weight: 500;

  font-weight: bold;
  letter-spacing: -0.21px;
  &:hover {
    text-decoration: none;
  }
`

A.SkillTitle = styled.div`
  font-size: 1.3em;
  font-weight: 600;
  width: 340px;
`

A.SkillText = styled.div`
  font-size: 0.7em;
  font-weight: 600;
  width: 340px;
`

A.Tags = styled.div`
  width: 340px;
  margin-left: -5px;
  margin-right: -5px;
  position: relative;
`

A.Input = styled(Input)`
  width: 340px;
`

A.SectionComposition = styled(Composition)``
