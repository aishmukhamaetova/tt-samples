import styled from 'styled-components'
import {SectionNav} from './section-nav'
import {SectionVideo} from './section-video'
import {Composition} from 'atomic-layout'
import {SectionBar} from './section-bar'
import {SectionFooter} from './section-footer'
const A = {
  SectionBar,
  SectionNav,
  SectionFooter,
}

A.PageComposition = styled(Composition)`
  min-height: 86vh;
  a {
    text-decoration: none;
  }
`

A.Header = styled.div`
  height: 45px;
`

export default A
