import React from 'react'
import A from './style'
const areas = `
  video
`
export function SectionVideo() {
  return (
    <A.SectionComposition areas={areas} alignContent={`start`}>
      {B => (
        <>
          <B.Video as={A.Video} justifyContent={`center`} paddingTop={60} flex>
            {/* <A.Video>
              <img alt="video" url="/images/home-video-356-183.png" />
            </A.Video> */}

            <A.ReactPlayer
              playIcon={<A.PlayIcon />}
              // light={'/images/poster.png'}
              url={[{src: `/videos/promo.mp4`, type: 'video/mp4'}]}
              width="100%"
              height="100%"
              wrapper={A.Wrapper}
              controls
              config={{
                file: {
                  forceVideo: true,
                  attributes: {
                    onContextMenu: e => {
                      e.preventDefault()
                    },
                    controls: true,
                    poster: '/images/poster.png',
                  },
                },
              }}
            />

            {/* config={{
   file: {
     attributes: {
       onContextMenu: e => {
         e.preventDefault()
       },
       controls: true,
       controlsList: 'nofullscreen',
       poster: '/images/poster.png',
       preload: 'auto',
       playsInline: true,
       height: '250px',
       autoPlay: true,
     },
     forceVideo: true,
   },
 }} */}

            {/* fileConfig={{attributes: {poster: '/images/poster.png'}}} */}

            {/* <A.ReactPlayer width="100%" height="100%" /> */}
          </B.Video>
        </>
      )}
    </A.SectionComposition>
  )
}
