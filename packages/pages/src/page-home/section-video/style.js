import styled from 'styled-components'
import {Composition} from 'atomic-layout'
import ReactPlayer from 'react-player'
import React from 'react'

const A = {}

A.Icon = styled.svg`
  width: 48px;
  height: 48px;
  fill: #000;
`

A.PlayIcon = ({}) => (
  <A.Icon>
    <path d="M10 15h8c1 0 2-1 2-2V3c0-1-1-2-2-2H2C1 1 0 2 0 3v10c0 1 1 2 2 2h4v4l4-4zM5 7h2v2H5V7zm4 0h2v2H9V7zm4 0h2v2h-2V7z" />
  </A.Icon>
)

A.SectionComposition = styled(Composition)`
  width: 100%;
  height: 250px;
  position: relative;
`
A.ReactPlayer = styled(ReactPlayer)`
  width: 100%;
  height: 100%;
  user-select: none;
  cursor: pointer;
  position: absolute;
  top: 0;
  left: 0;
`
A.Wrapper = styled.div`
  background-color: black;
  // background-image: url('/images/poster.png');
  background-repeat: no-repeat;
  background-size: 60% auto;
  background-position: center center;
`

export default A
