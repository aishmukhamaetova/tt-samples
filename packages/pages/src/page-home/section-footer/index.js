import React from 'react'
import styled from 'styled-components'
import {Composition} from 'atomic-layout'
import {Box} from 'atomic-layout'

const A = {}

const areas = `
  one
  two
`
const templateRows = `20px 1fr`

export function SectionFooter() {
  return (
    <A.SectionComposition templateRows={templateRows} areas={areas}>
      {B => (
        <>
          <B.One justifyContent={`center`} paddingBottom={32} flex>
            <A.LinkContainer>
              <A.Link href={`//www.ozminerals.com/`} target="blank">
                Find out more about OZ Minerals
              </A.Link>
            </A.LinkContainer>
          </B.One>
          <B.Two
            justifyContent={`start`}
            paddingTop={35}
            paddingLeft={15}
            paddingRight={15}
            paddingBottom={15}
            flex
          >
            <A.TermsText>
              <b>Terms & Conditions</b>
              <br />
              <p>
                You agree that information contained on this Site is for
                personal use only and may not be sold, redistributed or used for
                any commercial purpose (this includes but is not limited to the
                use of Advertiser contact details for unsolicited commercial
                correspondence and the information available via our insights
                and resources subdomain{' '}
                <a href="https://insightsresources.seek.com.au/">
                  https://insightsresources.seek.com.au/
                </a>
                ). You may download material from this Site for your personal,{' '}
                <nobr>non-commercial</nobr> use only, provided you keep intact
                all copyright and other proprietary notices. You may not modify,
                copy, reproduce, republish, upload, post, transmit or distribute
                in any way any material from this Site including code and
                software. You must not use this Site for any purpose that is
                unlawful or prohibited by these terms of use. You may not use
                data mining, robots, screen scraping, or similar automated data
                gathering, extraction or publication tools on this Site
                (including without limitation for the purposes of establishing,
                maintaining, advancing or reproducing information contained on
                our Site on your own website or in any other publication),
                except with Our prior written consent.
              </p>
              <p>
                OZ Minerals gives no guarantee to you of the continued
                availability of any particular job advertised on the Site and
                will not be liable to you should OZ Minerals have filled the
                vacancy at any time prior to removal of the advertisement from
                the Site. Whilst OZ Minerals takes efforts to ensure that jobs
                advertised are for actual job vacancies, it gives no guarantee
                to you that every job advertisement represents an actual job
                vacancy.
              </p>
              <p>
                To become a registered user, you must provide a password and a
                login name. You are entirely responsible if you do not maintain
                the confidentiality of your password and login name.
                Furthermore, you are entirely responsible for any and all
                activities that occur under your login name. You may change your
                password at any time by following instructions. You may also
                delete services attached to your registration at your
                convenience. You agree to immediately notify OZ Minerals of any
                unauthorized use of your login name or any other breach of
                security known to you.
              </p>
              <p>
                You agree that it is a condition on your use of the Team Tasker
                Site and of any other services provided by OZ Minerals or
                through the Site that you will not through any act or omission
                (including but not limited to creating a profile on the Team
                Tasker Site) mislead or deceive others.
              </p>
              <br />
              <b>Disclaimer</b>
              <br />
              <p>
                OZ Minerals Limited (‘OZ Minerals’) has endeavoured to ensure
                that all information provided on the OZ Minerals website
                (www.ozminerals.com) is accurate and <nobr>up-to-date</nobr>.
              </p>
              <p>
                None of OZ Minerals, its related bodies corporate, officers,
                employees and contractors makes or gives any representation,
                warranty or guarantee in relation to the information
                (‘Information’) provided on the website to the reader
                (‘Recipient’) of the website or any of its related bodies
                corporate, officers, employees and or contractors.
              </p>
              <p>
                The Information is provided expressly on the basis that the
                Recipient will carry out its own independent inquiries regarding
                the Information and make its own independent decisions about the
                affairs, financial position or prospects of OZ Minerals. OZ
                Minerals reserves the right to update, amend or supplement the
                Information at any time in its absolute discretion (without
                incurring any obligation to do so).
              </p>
              <p>
                OZ Minerals, it related bodies corporate, officers, employees
                and contractors will not be liable for any inaccuracies or
                omissions or any direct, special, indirect or consequential
                damages or losses, or any other damages or losses of whatsoever
                kind resulting from whatever cause arising through the use of
                any Information obtained either directly or indirectly from or
                through this website and any decisions based on such Information
                are the sole responsibility of the Recipient.
              </p>
              <p>
                Links to other websites are provided for your convenience and OZ
                Minerals is not responsible for the information contained on
                those websites. The provision of a link to another website does
                not constitute an endorsement or approval of that website or any
                services or products offered on that website.
              </p>
              <p>
                Nothing in this website constitutes or is intended to constitute
                an offer of, or an invitation to purchase or subscribe for,
                securities.
              </p>
              <p>
                OZ Minerals regards your privacy as important and will take all
                reasonable steps to ensure that if you have subscribed to
                receive OZ Minerals’ updates that your email address will remain
                confidential.
              </p>
              <p>
                'OZ Minerals' and all of its associated trademarks are
                trademarks of OZ Minerals Limited and its related bodies
                corporate.
              </p>
              <br />
              <b>OZ Minerals Privacy Policy</b>
              <br />
              <p>
                OZ Minerals Limited (OZ Minerals) views the personal privacy of
                visitors of its website as an important issue, and is committed
                to maintaining the security of personal information provided to
                OZ Minerals by users accessing teamtasker.ozminerals.com. This
                Privacy Policy outlines how OZ Minerals collects, uses and
                manages information obtained from visitors to the OZ Minerals
                website.
              </p>
              <br />
              <b>Hyperlinks to other websites</b>
              <br />
              <p>
                OZ Minerals provides links to other websites from hyperlinks
                within ozminerals.com. OZ Minerals' Privacy Policy does not
                cover these external websites.
              </p>
              <br />
              <b>What Information do we collect?</b>
              <br />
              <p>OZ Minerals may collect:</p>
              <p>
                domain names and details of what pages have been accessed for
                data and statistical analysis;
              </p>
              <p>
                personal details such as names, email addresses, phone numbers,
                past working history information:
              </p>
              <p>responding to email requests made via our website;</p>
              <p>
                Personal information is only used for the purpose which it was
                provided and is not shared with any third parties.
              </p>
              <br />
              <b>Privacy Policy Questions</b>
              <br />
              <p>
                If you have any questions or you believe that OZ Minerals has
                not adhered to this Privacy Policy, please contact:
              </p>
              <p>
                Michelle Pole
                <br />
                Senior Legal Counsel and Company Secretary
              </p>
              <p>
                Telephone
                <br />
                (61 8) 8229 6678
              </p>
              <p>
                Email
                <br />
                <a href="mailto:michelle.pole@ozminerals.com">
                  michelle.pole@ozminerals.com
                </a>
              </p>
              <br />
              <b>Privacy Policy Changes</b>
              <br />
              <p>
                Any changes to&nbsp;this Privacy Policy that may be&nbsp;made
                from time to&nbsp;time will be&nbsp;available on&nbsp;Team
                Tasker or&nbsp;the OZ&nbsp;Minerals website.
              </p>
              <p>Privacy Statement last updated: 11/02/2020</p>
            </A.TermsText>
          </B.Two>
        </>
      )}
    </A.SectionComposition>
  )
}

A.Link = styled.a`
  && {
    font-size: 0.9em;
    color: rgba(84, 82, 82, 1);
    text-decoration: underline;
    &:hover {
      text-decoration: none;
    }
  }
`

A.LinkContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  width: 230px;
  height: 30px;
`

A.SectionComposition = styled(Composition)``

A.TermsText = styled(Box)`
  && {
    font-size: 13px;
    font-weight: 500;
    margin: 0 auto;
    p {
      font-size: 13px;
      font-weight: 500;
      a {
        text-decoration: underline;
        color: #000;
        &: hover {
          text-decoration: none;
        }
      }
    }
  }
`
