import styled from 'styled-components'
import {Composition} from 'atomic-layout'
import {Button} from '@tt/ui'
import {ButtonOutline} from '@tt/ui'

const A = {}

A.ButtonSignIn = styled(ButtonOutline)`
  width: 320px;
  height: 50px;

  @media (min-width: 340px) {
    width: 340px;
  }
  text-deocration: none;
  a {
    text-deocration: none;
  }
`

A.ButtonCreate = styled(Button)`
  width: 320px;
  height: 30px;

  color: rgba(253, 183, 62, 1);

  @media (min-width: 340px) {
    width: 340px;
  }
`

A.ButtonNew = styled(Button)`
  width: 320px;
  height: 30px;

  color: rgba(65, 63, 63, 1);

  @media (min-width: 340px) {
    width: 340px;
  }
`

A.SectionComposition = styled(Composition)``

export default A
