import React from 'react'
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom'
import A from './style'
const areas = `
  signin
  new
  create
`

export function SectionNav({onSignInClick, onProfileCreate}) {
  return (
    <A.SectionComposition areas={areas}>
      {B => (
        <>
          <B.Signin justifyContent={`center`} paddingTop={20} flex>
            <A.ButtonSignIn onClick={onSignInClick}>Sign In</A.ButtonSignIn>
          </B.Signin>
          <B.New justifyContent={`center`} paddingTop={5} flex>
            <A.ButtonNew>New to Team Tasker?</A.ButtonNew>
          </B.New>
          <B.Create justifyContent={`center`} paddingTop={3} flex>
            <A.ButtonCreate onClick={onProfileCreate}>
              Create account
            </A.ButtonCreate>
          </B.Create>
        </>
      )}
    </A.SectionComposition>
  )
}
