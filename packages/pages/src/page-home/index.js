import React from 'react'
import A from './style'
import {SectionFooterWithIcons} from '@tt/sections'
import {SectionHeader} from '@tt/sections'
import {SectionVideo} from './section-video'

const areas = `
  header
  bar
  video
  nav
  fill
  footer  
`

const templateRows = `auto auto auto auto 1fr auto`

export function PageHome({
  onSignInClick,
  onProfileCreate,
  onViewActivities,
  isActiveRoute,
}) {
  return (
    <A.PageComposition
      areas={areas}
      alignContent={`flex-start`}
      templateRows={templateRows}
    >
      {B => (
        <>
          <B.Header>
            <SectionHeader isIndexPage={true} />
          </B.Header>
          <B.Bar>
            <A.SectionBar onViewActivities={onViewActivities} />
          </B.Bar>
          <B.Video>
            <SectionVideo />
          </B.Video>
          <B.Nav>
            <A.SectionNav
              onSignInClick={onSignInClick}
              onProfileCreate={onProfileCreate}
            />
          </B.Nav>
          <B.Footer>
            <A.SectionFooter />
          </B.Footer>
        </>
      )}
    </A.PageComposition>
  )
}
