import styled from 'styled-components'
import {Composition} from 'atomic-layout'

const A = {}

A.Button = styled.div`
  padding-top: 14px;
  padding-bottom: 14px;
  padding-left: 20px;
  padding-right: 20px;

  font-size: 1.1em;
  font-weight: 300;

  user-select: none;
  cursor: pointer;
`

A.Label = styled.div`
  font-size: 1.25em;
  font-weight: 500;
`

A.SectionComposition = styled(Composition)`
  border: 1px solid rgba(74, 74, 74, 1);

  height: 178px;
`

export default A
