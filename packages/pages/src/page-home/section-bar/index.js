import React from 'react'
import A from './style'

const areas = `
  button
  image
  label
`

export function SectionBar({onViewActivities}) {
  return (
    <A.SectionComposition areas={areas} alignContent={`start`}>
      {B => (
        <>
          <B.Button justifyContent={`flex-end`} flex>
            <A.Button onClick={onViewActivities}>View Activities</A.Button>
          </B.Button>
          <B.Image justifyContent={`center`} flex paddingTop={14}>
            <img alt="Home logo" src="/images/logo.png" />
          </B.Image>
          <B.Label justifyContent={`center`} flex>
            <A.Label>Working together to create value</A.Label>
          </B.Label>
        </>
      )}
    </A.SectionComposition>
  )
}
