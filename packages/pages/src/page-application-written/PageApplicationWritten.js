import React from 'react'
import styled from 'styled-components'

import {SectionFormWrite} from './SectionFormWrite'
import {SectionHeader} from '@tt/sections'

import {fromAuth} from '@tt/domains'
import {useHistory} from 'react-router-dom'

const A = {}

export function PageApplicationWritten({id, onBackClick, onSubmitSuccess}) {
  const history = useHistory()

  React.useEffect(() => {
    async function check() {
      const result = await fromAuth.run.session()
      if (!result) {
        history.push(`/signin`)
      }
    }

    check()
  })

  const [height, setHeight] = React.useState(window.innerHeight)

  React.useEffect(() => {
    const onResize = () => setHeight(window.innerHeight)
    window.addEventListener('resize', onResize)
    return () => {
      window.removeEventListener('resize', onResize)
    }
  })

  return (
    <A.Grid style={{'--min-height': `${height}px`}}>
      <A.AreaHeader>
        <SectionHeader onBackClick={onBackClick} />
      </A.AreaHeader>
      <A.AreaForm>
        <SectionFormWrite id={id} onSubmitSuccess={onSubmitSuccess} />
      </A.AreaForm>
    </A.Grid>
  )
}

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'form';
  grid-template-rows: auto 1fr;

  min-height: 100vh;
  min-height: var(--min-height);

  width: 100vw;

  overflow-x: hidden;
`

A.AreaHeader = styled.div`
  grid-area: header;
`
A.AreaForm = styled.div`
  grid-area: form;

  padding: 38px;
`
