import {useStore} from 'effector-react'
import React from 'react'
import styled from 'styled-components'
import {BlockFormSubmit} from './BlockFormSubmit'
import {TextArea} from '@tt/ui'
import {fromPageApplicationWritten} from '@tt/domains'
import {fromActivity} from '@tt/domains'

const A = {}

export function SectionFormWrite({id, onSubmitSuccess}) {
  const text = useStore(fromPageApplicationWritten.$.text)
  const errors = useStore(fromPageApplicationWritten.submit.$.errors)

  const list = useStore(fromActivity.$.list)
  const doc = list?.find(o => o.id === id)
  const short =
    doc?.title && !/\s/g.test(doc?.title)
      ? `${doc?.title.length > 20 && doc?.title.substr(0, 20)}${
          doc?.title.length > 20 ? `...` : ``
        }` || doc?.title
      : doc?.title
  return (
    <A.Container>
      <A.Grid>
        <A.AreaTitle>
          {short || `Empty`}
          {errors.length > 0 && (
            <span style={{color: `red`}}>&nbsp;Already applied</span>
          )}
        </A.AreaTitle>
        <A.AreaDescription>
          Describe how you can add value to our team (maximum 900 characters)
        </A.AreaDescription>
        <A.AreaText>
          <A.Textarea
            onChange={fromPageApplicationWritten.on.textInput}
            value={text}
            maxLength={900}
          />
        </A.AreaText>
        <A.AreaSubmit>
          <BlockFormSubmit id={id} onSubmitSuccess={onSubmitSuccess} />
        </A.AreaSubmit>
      </A.Grid>
    </A.Container>
  )
}

A.Container = styled.div`
  height: 100%;
`

A.Grid = styled.div`
  display: grid;
  grid-template-areas:
    'title'
    'description'
    'text'
    'submit';
  grid-row-gap: 10px;

  grid-template-rows: auto auto 1fr auto;

  height: 100%;
`

A.AreaTitle = styled.div`
  grid-area: title;

  font-size: 21px;
  font-weight: bold;
  color: #000000;
  letter-spacing: -0.5px;
  line-height: 22px;
`
A.AreaDescription = styled.div`
  grid-area: description;

  font-size: 20px;
  color: #000000;
  letter-spacing: -0.32px;
  line-height: 18px;
`
A.AreaText = styled.div`
  grid-area: text;

  display: flex;
  justify-content: center;
`
A.AreaSubmit = styled.div`
  grid-area: submit;

  display: flex;

  justify-content: center;
`
A.Textarea = styled(TextArea)`
  width: 80vw;
`
