import {useStore, createGate, useGate} from 'effector-react'
import React from 'react'
import styled from 'styled-components'

import {fromPageApplicationWritten} from '@tt/domains'
import {ButtonOutline} from '@tt/ui'

const A = {}

const Gate = createGate()

fromPageApplicationWritten.submit.$.lastSuccess.reset(Gate.close)

export function BlockFormSubmit({id, onSubmitSuccess}) {
  useGate(Gate)

  const lastSuccess = useStore(fromPageApplicationWritten.submit.$.lastSuccess)

  React.useEffect(() => {
    if (lastSuccess && onSubmitSuccess) {
      onSubmitSuccess()
    }
  }, [lastSuccess])

  React.useEffect(() => fromPageApplicationWritten.on.clear, [])

  function onSubmit() {
    fromPageApplicationWritten.run.submit({activityId: id})
  }

  return (
    <A.Grid>
      <A.AreaSubmit>
        <A.ButtonOutline onClick={onSubmit}>Submit</A.ButtonOutline>
      </A.AreaSubmit>
    </A.Grid>
  )
}

A.Grid = styled.div`
  display: grid;
  grid-template-areas: 'submit';

  grid-template-columns: 80vw;
`

A.AreaSubmit = styled.div`
  grid-area: submit;
`

A.ButtonOutline = styled(ButtonOutline)`
  height: 40px;
`
