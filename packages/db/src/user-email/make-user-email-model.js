import dynamoose from 'dynamoose'

export function makeUserEmailModel() {
  const UserEmailSchema = new dynamoose.Schema({
    email: {
      type: String,
      hashKey: true,
      required: true,
      lowercase: true
    },
    userId: {
      type: String,
      index: {
        global: true,
        project: true
      },
      required: true
    }
  })

  const UserEmail = dynamoose.model(`UserEmail`, UserEmailSchema)

  return {UserEmail, UserEmailSchema}
}
