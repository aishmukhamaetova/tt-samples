import {db} from '../db'

export async function activityCreate(payload) {
  try {
    const doc = new db.Activity(payload)
    await doc.save({overwrite: false})

    return doc
  } catch (err) {
    throw new Error(`Cannot create activity`)
  }
}
