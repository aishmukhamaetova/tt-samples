import dynamoose from 'dynamoose'
import uuid from 'uuid/v4'

const NAME = `Activity`

export function makeActivityModel() {
  const Schema = new dynamoose.Schema(
    {
      id: {
        type: String,
        index: {
          global: true,
          project: true,
        },
        required: true,
        default: uuid,
      },
      title: {
        type: String,
      },
      status: {
        type: String,
      },
      hiringManagerId: {
        type: String,
      },
      tags: {
        type: [String],
      },
      iconId: {
        type: String,
      },
      location: {
        type: [String],
      },
      duration: {
        type: String,
      },
      durationType: {
        type: String,
      },
      summary: {
        type: String,
      },
      dateStart: {
        type: Date,
      },
      dateEnd: {
        type: Date,
      },
      description: {
        type: String,
      },
      included: {
        type: [String],
      },
      aboutTeam: {
        type: String,
      },
      dateClose: {
        type: Date,
      },
      videoLink: {
        type: String,
      },
      comment: {
        type: String,
      },
    },
    {
      timestamps: true,
    },
  )

  const Model = dynamoose.model(NAME, Schema)

  return {[NAME]: Model, [`${NAME}Schema`]: Schema}
}
