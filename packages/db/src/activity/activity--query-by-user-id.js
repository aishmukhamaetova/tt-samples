import createDebugger from 'debug'

import {db} from '../db'

const who = [`@tt`, `db`, `activity`, `query-by-user-id`]

const log = createDebugger(who.join(`:`))

export async function activityQueryByUserId(payload, options = {}) {
  try {
    log(payload)

    const {userId} = payload

    if (options.counts) {
      const r = await db.Activity.scan('userId')
        .eq(userId)
        .counts()
        .exec()

      log(`r`, r)

      return r
    }

    const docList = await db.Activity.scan(`userId`)
      .eq(userId)
      .exec()

    return docList
  } catch (err) {
    throw err
  }
}
