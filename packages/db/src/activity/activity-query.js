import createDebugger from 'debug'

import {db} from '../db'

const who = [`@tt`, `db`, `activity`, `query`]

const log = createDebugger(who.join(`:`))

export async function activityQuery(payload) {
  try {
    log(payload)

    const docList = await db.Activity.scan()
      .all()
      .exec()

    return docList
  } catch (err) {
    throw err
  }
}
