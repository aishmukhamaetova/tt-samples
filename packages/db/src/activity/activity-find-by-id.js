import createDebugger from 'debug'

import {db} from '../db'

const who = [`@tt`, `db`, `activity`, `find-by-id`]

const log = createDebugger(who.join(`:`))

export async function activityById(id) {
  try {
    log(id)

    const activity = await db.Activity.get(id)
    return activity
  } catch (err) {
    throw err
  }
}
