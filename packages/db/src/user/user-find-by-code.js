import createDebugger from 'debug'

import {db} from '../db'

const who = [`@tt`, `db`, `user`, `userFindByCode`]

const log = createDebugger(who.join(`:`))

export async function userFindByCode(payload) {
  try {
    log(payload)

    const {code} = payload
    const docs = await db.User.scan({code: {eq: code}}).exec()

    if (docs.length > 0) {
      const user = docs[0]
      const docsUserEmail = await db.UserEmail.scan({userId: {eq: user.id}}).exec()
      user.email = docsUserEmail[0].email
      return user
    }

    return null
  } catch (err) {
    throw err
  }
}
