import {genSaltSync} from 'bcryptjs'
import {hashSync} from 'bcryptjs'

import {db} from '../db'

function generatePassword(password) {
  const salt = genSaltSync(10)
  return hashSync(password, salt)
}

export async function userUpdate(payload) {
  try {
    const {id, password} = payload

    const values = password
      ? {
          ...payload,
          password: generatePassword(password)
        }
      : payload

    if (!password) {
      delete values.password
    }

    const doc = await db.User.update(id, values)

    const {email} =
      (await db.UserEmail.queryOne({
        userId: {eq: doc.id}
      }).exec()) || {}

    return {...doc, email}
  } catch (err) {
    throw err
  }
}
