import {genSaltSync} from 'bcryptjs'
import {hashSync} from 'bcryptjs'

import {db} from '../db'

function generatePassword(password) {
  const salt = genSaltSync(10)
  return hashSync(password, salt)
}

export async function userUpdateNote(payload) {
  try {
    const {id, adminNote} = payload
    const {createdAt, updatedAt, ...user} = await db.User.get(id)
    if (user) {
      const values = {
        ...user,
        adminNote,
      }
      const doc = await db.User.update(id, values)
      console.log('db doc', doc)
      return {...doc}
    }
  } catch (err) {
    throw err
  }
}
