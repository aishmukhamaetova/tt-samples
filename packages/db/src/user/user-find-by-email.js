import createDebugger from 'debug'

import {db} from '../db'

const who = [`@tt`, `db`, `user`, `userFindByEmail`]

const log = createDebugger(who.join(`:`))

export async function userFindByEmail(payload) {
  try {
    log(payload)

    const {email} = payload
    const docUserEmail = await db.UserEmail.get(email)

    if (docUserEmail) {
      const doc = await db.User.get(docUserEmail.userId)

      return {...doc, email}
    }

    return null
  } catch (err) {
    throw err
  }
}
