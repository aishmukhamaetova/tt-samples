import createDebugger from 'debug'
import {compareSync} from 'bcryptjs'

import {db} from '../db'

const who = [`@tt`, `db`, `user`, `userFindForRemove`]

const log = createDebugger(who.join(`:`))

export async function userFindForRemove(payload) {
  try {
    log(payload)

    const {email} = payload
    const {userId} = await db.UserEmail.get(email)
    if (userId) {
      const docUserEmailDeleted = await db.UserEmail.delete({email: email})
      const docUserDeleted = await db.User.delete({id: userId})

      return true
    }

    return new Error(`Cannot remove user: something went wrong`)
  } catch (err) {
    throw new Error(`Cannot remove user`)
  }
}
