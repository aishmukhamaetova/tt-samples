import {genSaltSync} from 'bcryptjs'
import {hashSync} from 'bcryptjs'
import uuid from 'uuid/v4'

import {db} from '../db'

export async function userCreate({email, password, skills, name, phone}) {
  try {
    const salt = genSaltSync(10)
    const nextPassword = hashSync(password, salt)
    const code = uuid()

    const docUserEmail = await db.UserEmail.get(email)
    if (docUserEmail) {
      throw new Error(`Cannot create user because email already exists`)
    }

    const doc = new db.User({
      password: nextPassword,
      status: 'unverified',
      skills,
      name,
      phone,
      code,
      codeDate: new Date()
    })

    await doc.save({overwrite: false})

    const nextDocUserEmail = new db.UserEmail({email, userId: doc.id})

    await nextDocUserEmail.save({overwrite: false})

    return {...doc, email}
  } catch (err) {
    throw err
  }
}
