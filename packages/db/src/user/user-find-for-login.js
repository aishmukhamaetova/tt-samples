import createDebugger from 'debug'
import {compareSync} from 'bcryptjs'

import {db} from '../db'

const who = [`@tt`, `db`, `user`, `userFindForLogin`]

const log = createDebugger(who.join(`:`))

export async function userFindForLogin(payload) {
  try {
    log(payload)

    const {email, password} = payload
    const docUserEmail = await db.UserEmail.get(email)

    if (docUserEmail) {
      const doc = await db.User.get({id: docUserEmail.userId})

      const {code, codeDate, ...user} = doc

      if (doc.status === 'unverified') {
        return {...user, status: doc.status, code}
      }

      if (compareSync(password, doc.password)) {
        return {...user, email}
      }
    }

    return null
  } catch (err) {
    throw err
  }
}
