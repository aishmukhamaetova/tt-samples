import createDebugger from 'debug'
import {applicationQueryByUserId} from '../application/application--query-by-user-id'

import {db} from '../db'

const who = [`@tt`, `db`, `user`, `query`]

const log = createDebugger(who.join(`:`))

export async function userQuery(payload, options) {
  try {
    const {isAdminRequest = false} = options

    log(payload)

    const userEmail = await db.UserEmail.scan()
      .all()
      .exec()

    const userMap = userEmail.reduce((r, v) => ((r[v.userId] = v.email), r), {})

    const docList = await db.User.scan()
      .all()
      .exec()

    for (let doc of docList) {
      delete doc['password']
      doc.email = userMap[doc.id]

      if (isAdminRequest) {
        const list = await applicationQueryByUserId({
          userId: doc.id,
        })

        const successList = list.filter(({status}) => status === `success`)

        doc[`applicationList`] = list
        doc[`applicationsSuccess`] = successList.length

        for (let docApplication of list) {
          try {
            const activity = await db.Activity.get(docApplication.activityId)
            docApplication.activity = activity
          } catch (err) {
            log(`ERROR`, err)
            docApplication.activity = {}
          }
        }
      }
    }

    return docList
  } catch (err) {
    throw err
  }
}
