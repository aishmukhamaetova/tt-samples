import dynamoose from 'dynamoose'
import uuid from 'uuid/v4'

export function makeUserModel() {
  const UserSchema = new dynamoose.Schema(
    {
      id: {
        type: String,
        hashKey: true,
        required: true,
        default: uuid,
      },
      code: {
        type: String,
        required: false,
      },
      codeDate: {
        type: Date,
        required: false,
      },
      status: {
        type: String // verified, unverified
      },
      password: {
        type: String,
        required: true,
      },
      skills: {
        type: String,
      },
      name: {
        type: String,
      },
      phone: {
        type: String,
      },
      note: {
        type: String,
      },
      favouriteActivities: {
        type: [String],
      },
      adminNote: {
        type: String,
      },
    },
    {
      timestamps: true,
    },
  )

  const User = dynamoose.model(`User`, UserSchema)

  return {User, UserSchema}
}
