import dynamoose from 'dynamoose'
import dynalite from 'dynalite'

import {makeUserModel} from './user/make-user-model'
// import {userCreate} from './user/user-create'

import {makeActivityModel} from './activity/make-activity-model'
import {makeApplicationModel} from './application/make-application-model'
import {makeHiringManagerModel} from './hiring-manager/make-hiring-manager-model'
import {makeSessionModel} from './session/make-session-model'
// import {sessionCreate} from './session/session-create'

import {makeUserEmailModel} from './user-email/make-user-email-model'

export const db = {}

const startUpAndReturnDynamo = async () => {
  const dynaliteServer = dynalite({path: `./data`, createTableMs: 50})
  await dynaliteServer.listen(8000)
  return dynaliteServer
}

const createDynamooseInstance = async () => {
  // console.log(
  //   'createDynamooseInstance for real',
  //   process.env,
  //   dynamoose.AWS.config,
  // )
  console.log('process.env.NODE_ENV db', process.env.NODE_ENV)
  if (
    process.env.NODE_ENV === `production` ||
    process.env.NODE_ENV === `staging`
  ) {
    dynamoose.AWS.config.update({
      accessKeyId: process.env.AWS_ACCESS_KEY,
      secretAccessKey: process.env.AWS_SECRET_KEY,
      region: process.env.AWS_REGION || 'ap-southeast-2'
    })
  } else {
    console.log('process.env.NODE_ENV db', process.env.NODE_ENV, 'LOCAL DB UP')
    dynamoose.AWS.config.update({
      accessKeyId: 'AKID',
      secretAccessKey: 'SECRET',
      region: 'us-east-1'
    })

    dynamoose.local('http://localhost:8000')
  }
}

function initModels() {
  const {Activity} = makeActivityModel()
  const {Application} = makeApplicationModel()
  const {HiringManager} = makeHiringManagerModel()
  const {User} = makeUserModel()
  const {Session} = makeSessionModel()
  const {UserEmail} = makeUserEmailModel()

  db.Activity = Activity
  db.Application = Application
  db.HiringManager = HiringManager
  db.User = User
  db.UserEmail = UserEmail
  db.Session = Session
}

export async function start() {
  if (
    process.env.NODE_ENV !== `production` &&
    process.env.NODE_ENV !== `staging`
  ) {
    await startUpAndReturnDynamo()
  }
  createDynamooseInstance()
  initModels()
}
