import React from 'react'
import {FaSearch} from 'react-icons/fa'

export function IconActivities(props) {
  return <FaSearch {...props} />
}
