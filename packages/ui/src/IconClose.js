import React from 'react'
import {FaTimes} from 'react-icons/fa'

export function IconClose(props) {
  return <FaTimes {...props} />
}
