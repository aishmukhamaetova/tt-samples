import {Box} from 'atomic-layout'
import {Composition} from 'atomic-layout'
import React from 'react'
import styled from 'styled-components'

const A = {}

const areas = `
  icon label
  input input
`

const templateRows = `45px 45px`
const templateCols = `28px 1fr`

A.Composition = styled(Composition)`
  --padding-left: 10px;
`

A.Icon = styled.div``

A.Label = styled.div`
  font-size: 1em;
  font-weight: 600;
`

A.Text = styled.div`
  padding-left: var(--padding-left);

  font-family: 'Frutiger', 'Roboto', sans-serif;
  font-weight: 300;
  font-size: 1.2em;
`

A.Input = styled(Box)`
  background: rgba(241,241,242,1);
  border-style: solid;
  border-width:  1px;
  border-color: ${props =>
    props.isInputInvalid ? '#ff0000' : 'rgba(253, 183, 62, 1)'};

  border-radius: 8px;

  padding: 2px 2px 2px var(--padding-left);

  box-shadow: 0 4px 5px -4px rgba(0,0,0,0.6);

  height: 100%;

  & input {
    -webkit-box-shadow: none; 
    box-shadow: none;
    background: transparent;

    margin: 2px;
    border: none;

    width: 100%;
    height: 100%
    
    font-family: 'Frutiger', 'Roboto', sans-serif;
    font-weight: 300;
    font-size: 1.2em;

    color: #000;

    outline: none;

    &:-webkit-autofill,
    &:-webkit-autofill:hover,
    &:-webkit-autofill:focus textarea:-webkit-autofill {
      -webkit-box-shadow: 0 0 0px 1000px rgba(241,241,242,1) inset !important; 
      border-radius: 8px; 
      background: transparent !important;

    }    

    }
`

export function Input({
  className,
  icon,
  label,
  type = `text`,
  value,
  isDisabled = false,
  onChange,
  isInputInvalid,
}) {
  return (
    <A.Composition
      className={className}
      areas={areas}
      justifyContent={`flex-start`}
      templateCols={templateCols}
      templateRows={templateRows}
    >
      {B => (
        <>
          <B.Icon as={A.Icon} flex alignItems={`center`}>
            {icon}
          </B.Icon>
          <B.Label as={A.Label} flex alignItems={`center`}>
            {label}
          </B.Label>
          <B.Input>
            {isDisabled ? (
              <A.Text>{value}</A.Text>
            ) : (
              <A.Input
                isInputInvalid={isInputInvalid}
                flex
                alignItems={`center`}
              >
                <input type={type} value={value} onChange={onChange} />
              </A.Input>
            )}
          </B.Input>
        </>
      )}
    </A.Composition>
  )
}
