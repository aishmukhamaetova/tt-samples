import React from 'react'

const A = {}

export function TagsContainer({children, className, gutter = 5}) {
  const style = {
    '--gutter': `${gutter}px`,
  }

  return (
    <A.TagsContainer className={className} style={style}>
      {children}
    </A.TagsContainer>
  )
}

import styled from 'styled-components'

A.TagsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;

  margin: -var(--gutter);
`
