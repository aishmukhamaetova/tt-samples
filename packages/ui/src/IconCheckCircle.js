import React from 'react'
import {FaCheckCircle} from 'react-icons/fa'

export function IconCheckCircle(props) {
  return <FaCheckCircle {...props} />
}
