import React from 'react'

const A = {}

export function GridLow({
  areas,
  children,
  className,
  templateCols,
  templateRows,
}) {
  const style = {
    '--cols': templateCols,
    '--rows': templateRows,
    '--areas': areas,
  }

  return (
    <A.Grid className={className} style={style}>
      {children}
    </A.Grid>
  )
}

import styled from 'styled-components'

A.Grid = styled.div`
  display: grid;
  grid-template-areas: var(--areas, none);
  grid-template-columns: var(--cols, none);
  grid-template-rows: var(--rows, none);
`

export const Grid = styled(GridLow)``
