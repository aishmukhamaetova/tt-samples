import React from 'react'
import {IconSearch} from './IconSearch'

const A = {}

const COLOR = `rgb(138, 138, 139)`

const areas = `'icon input'`

const templateCols = `30px 1fr`
const templateRows = `36px`

export function InputSearch({className, value = `Search`, onChange}) {
  return (
    <A.Grid
      areas={areas}
      className={className}
      templateCols={templateCols}
      templateRows={templateRows}
    >
      <A.AreaIcon>
        <IconSearch color={COLOR} />
      </A.AreaIcon>
      <A.AreaInput>
        <A.Input defaultValue={value} onChange={onChange} />
      </A.AreaInput>
    </A.Grid>
  )
}

import styled from 'styled-components'
import {Grid} from './Grid'

A.Grid = styled(Grid)`
  justify-content: start;

  background: rgba(241, 241, 242, 1);

  border: solid 1px rgba(253, 183, 62, 1);
  border-radius: 11px;
`

A.AreaIcon = styled.div`
  grid-area: 'icon';

  display: flex;

  justify-content: center;
  align-items: center;
`

A.AreaInput = styled.div`
  grid-area: 'input';

  padding-bottom: 4px;
`

A.Input = styled.input`
  background: transparent;

  border: none;

  width: 100%;
  height: 100%

  font-family: 'Frutiger', 'Roboto', sans-serif;
  font-weight: 300;
  font-size: 1em;

  color: rgb(129, 129, 130);

  outline: none;
`
