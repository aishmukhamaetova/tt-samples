import styled from 'styled-components'

const size = ({size = 3}) => `${size}px ${size}px`
const padding = ({size = 3}) => `${size * 1.5}px`

export const IconDownArrow = styled.i`
  border: solid ${props => props.color || `#fff`};
  border-width: 0 ${size} 0;
  display: inline-block;
  padding: ${padding};
  transform: rotate(45deg);
`
