import React from 'react'
import {FaTimesCircle} from 'react-icons/fa'

export function IconTimesCircle(props) {
  return <FaTimesCircle {...props} />
}
