import React from 'react'
import {FaTasks} from 'react-icons/fa'

export function IconStatus(props) {
  return <FaTasks {...props} />
}
