import React from 'react'
import {FaPencilAlt} from 'react-icons/fa'

export function IconPencil(props) {
  return <FaPencilAlt {...props} />
}
