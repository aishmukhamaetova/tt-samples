import React from 'react'
import {FaUser} from 'react-icons/fa'

export function IconUser(props) {
  return <FaUser {...props} />
}
