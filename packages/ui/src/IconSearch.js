import React from 'react'
import {FaSearch} from 'react-icons/fa'

export function IconSearch(props) {
  return <FaSearch {...props} />
}
