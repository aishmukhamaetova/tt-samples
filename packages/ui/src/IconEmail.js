import React from 'react'
import {FaEnvelope} from 'react-icons/fa'

export function IconEmail(props) {
  return <FaEnvelope {...props} />
}
