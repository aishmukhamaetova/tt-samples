import React from 'react'

const A = {}

function AreaLow({className, children, name}) {
  const style = {
    '--grid-area': name,
  }

  return (
    <A.GridArea className={className} style={style}>
      {children}
    </A.GridArea>
  )
}

import styled from 'styled-components'

A.GridArea = styled.div`
  grid-area: var(--grid-area);
`

export const Area = styled(AreaLow)``
