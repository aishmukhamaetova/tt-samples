import React from 'react'
import {FaUser} from 'react-icons/fa'

export function IconProfile(props) {
  return <FaUser {...props} />
}
