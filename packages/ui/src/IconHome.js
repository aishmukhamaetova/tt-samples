import React from 'react'
import {FaHome} from 'react-icons/fa'

export function IconHome(props) {
  return <FaHome {...props} />
}
