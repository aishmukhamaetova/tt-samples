import React from 'react'
import styled from 'styled-components'
import {Checkbox} from './Checkbox'

const A = {}

export function FieldCheckbox({className, label, checked, ...props}) {
  return (
    <A.Grid className={className}>
      <A.AreaInput>
        <A.Checkbox type="checkbox" checked={checked} {...props} />
      </A.AreaInput>
      <A.AreaLabel>{label}</A.AreaLabel>
    </A.Grid>
  )
}

A.Grid = styled.div`
  display: grid;
  grid-template-areas: 'input label';
  grid-template-rows: 43px;
  grid-template-columns: 28px 1fr;

  justify-content: flex-start;
`

A.AreaInput = styled.div`
  grid-area: input;

  display: flex;
  align-items: center;
`

A.AreaLabel = styled.div`
  grid-area: label;

  display: flex;
  align-items: center;

  font-size: 1.1em;
  font-weight: bold;

  user-select: none;
`

A.Checkbox = styled(Checkbox)``
