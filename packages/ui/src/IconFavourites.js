import React from 'react'
import {FaStar} from 'react-icons/fa'

export function IconFavourites(props) {
  return <FaStar {...props} />
}
