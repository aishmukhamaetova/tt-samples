import React from 'react'
import {Area} from './Area'
import {IconClose} from './IconClose'

const A = {}

const areas = `'label icon'`

const templateCols = 'auto 20px'

export function Tag({className, value, height = 45, onClick}) {
  const templateRows = `${height}px`
  return (
    <A.Container className={className}>
      <A.Grid
        areas={areas}
        templateCols={templateCols}
        templateRows={templateRows}
      >
        <A.AreaLabel name="label">{value}</A.AreaLabel>
        <A.AreaIcon name="icon">
          <IconClose onClick={onClick} />
        </A.AreaIcon>
      </A.Grid>
    </A.Container>
  )
}

import styled from 'styled-components'
import {Grid} from './Grid'

A.Container = styled.div`
  display: inline-block;

  margin: var(--gutter, 5px);
`

A.Grid = styled(Grid)`
  background: #fff;
  color: #000;

  border: solid 1px rgba(253, 183, 62, 1);
  border-radius: 8px;
`

A.AreaLabel = styled(Area)`
  display: flex;

  align-items: center;
  justify-content: start;

  padding: 0 15px;
`

A.AreaIcon = styled(Area)`
  user-select: none;
  cursor: pointer;

  display: flex;

  align-items: center;
  justify-content: center;

  padding-right: 10px;
`
