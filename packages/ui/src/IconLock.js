import React from 'react'
import {FaLock} from 'react-icons/fa'

export function IconLock(props) {
  return <FaLock {...props} />
}
