import {Composition} from 'atomic-layout'
import React from 'react'
import styled from 'styled-components'

import {useOutsideClickRef} from 'rooks'

const A = {}

const windowColor = ({isDark}) => `--window-color: ${isDark ? `#fff` : `#000`};`
const windowBg = ({isDark}) => `--window-bg: ${isDark ? `#000` : `#fff`};`

A.Composition = styled(Composition)`
  ${windowColor}
  ${windowBg}

  background-color: rgba(0, 0, 0, 0.3);

  position: fixed;

  left: 0;
  top: 0;

  width: 100vw;
  height: 100vh;

  z-index: 99999;

  color: var(--window-color);
`

A.Window = styled.div`
  background: var(--window-bg);

  border: 2px solid rgba(253, 183, 62, 1);
  border-radius: 6px;
`

export function Modal({className, children, isDark, onClose}) {
  const onOutsideClick = () => {
    if (onClose) {
      onClose()
    }
  }

  const [ref] = useOutsideClickRef(onOutsideClick)

  return (
    <A.Composition
      alignItems={`center`}
      className={className}
      justifyContent={`center`}
      isDark={isDark}
    >
      <A.Window ref={ref}>{children}</A.Window>
    </A.Composition>
  )
}
