import React from 'react'
import styled from 'styled-components'

const A = {}

A.Title = styled.div`
  height: 45px;

  background: rgba(241, 241, 241, 1);

  font-size: 1.3em;
  font-weight: bold;
  color: #000;
`

export function TitleContainer({children}) {
  return <A.Title>{children}</A.Title>
}
