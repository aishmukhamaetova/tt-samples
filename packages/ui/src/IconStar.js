import React from 'react'
import {FaStar} from 'react-icons/fa'
import {FaRegStar} from 'react-icons/fa'

const Icon = {
  regular: FaRegStar,
}

export function IconStar(props) {
  const Component = React.useMemo(() => Icon[props.variant] || FaStar, [
    props.variant,
  ])

  return <Component {...props} />
}
