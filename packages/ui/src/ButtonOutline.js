import React from 'react'
import styled from 'styled-components'

import {Button} from './Button'

const A = {}

A.ButtonOutline = styled(Button)`
  background: ${props => (props.isDisabled ? 'rgba(0, 0, 0, 0.5)' : '#000')};

  border: solid 1px rgba(253, 183, 62, 1);
  border-radius: 10px;

  color: #fff;
`

export function ButtonOutline(props) {
  return <A.ButtonOutline {...props} />
}
