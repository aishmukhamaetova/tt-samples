import {Composition} from 'atomic-layout'
import React from 'react'
import styled from 'styled-components'

const A = {}

const areas = `
  input
`

const templateRows = `35px`
const templateCols = `1fr`

A.Badge = styled.div`
  position: absolute;
  right: 5px;
  bottom: 5px;

  font-weight: 300;
  font-size: 0.6em;

  color: rgb(171, 171, 172);
`

A.Input = styled.div`
  position: relative;

  background: rgba(241, 241, 242, 1);

  border: solid 1px rgba(253, 183, 62, 1);
  border-radius: 11px;


  & input {
    padding: 10px;
    background: transparent;
    border: none;

    width: 100%;
    height: 100%
    
    font-family: 'Frutiger', 'Roboto', sans-serif;
    font-weight: 300;
    font-size: 1em;

    color: rgb(171, 171, 172);

    outline: none;
  }
`

export function InputTag({
  className,
  type = `text`,
  value = `Describe yourself`,
  placeholder = ``,
  ...props
}) {
  return (
    <Composition
      className={className}
      areas={areas}
      justifyContent={`flex-start`}
      templateCols={templateCols}
      templateRows={templateRows}
    >
      {B => (
        <>
          <B.Input as={A.Input} flex alignItems={`center`}>
            <input
              type={type}
              value={value}
              placeholder={placeholder}
              {...props}
            />
            <A.Badge>{`${value}`.length}</A.Badge>
          </B.Input>
        </>
      )}
    </Composition>
  )
}
