import {Composition} from 'atomic-layout'
import React from 'react'
import styled from 'styled-components'

const A = {}

A.Composition = styled(Composition)`
  user-select: none;
  cursor: pointer;
`

A.Label = styled.div`
  font-weight: ${props => (props.isActiveRoute ? `bold` : `nowmal`)};
`
const areas = `
  icon
  label
`

export function ButtonFooter({className, icon, label, onClick, isActiveRoute}) {
  return (
    <A.Composition className={className} areas={areas} onClick={onClick}>
      {B => (
        <>
          <B.Icon justifyContent={`center`} flex>
            {icon}
          </B.Icon>
          <B.Label
            as={A.Label}
            justifyContent={`center`}
            isActiveRoute={isActiveRoute}
            flex
            paddingTop={5}
          >
            {label}
          </B.Label>
        </>
      )}
    </A.Composition>
  )
}
