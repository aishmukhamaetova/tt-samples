import {Composition} from 'atomic-layout'
import React from 'react'
import styled from 'styled-components'

const A = {}

const areas = `
  input
`

const templateCols = `1fr`

A.Badge = styled.div`
  position: absolute;
  right: 15px;
  bottom: 5px;

  font-weight: 300;
  font-size: 0.9em;

  color: #363636;
`

A.Input = styled.div`
  position: relative;

  background: rgba(211, 211, 211, 1);

  border: solid 1px rgba(253, 183, 62, 1);
  border-radius: 2px;

  padding-left: 8px;
  min-height: 129px;

  & textarea {
    background: transparent;

    border: none;

    width: 100%;
    height: 100%
    
    font-family: 'Frutiger', 'Roboto', sans-serif;
    font-weight: 300;
    font-size: 0.8em;

    color: #212121;

    outline: none;

    padding-top: 15px;
    min-height: 129px;
  }
`

export function TextArea({
  className,
  icon,
  label,
  type = `text`,
  value,
  onChange,
  maxLength = 900,
}) {
  return (
    <Composition
      className={className}
      areas={areas}
      justifyContent={`flex-start`}
      templateCols={templateCols}
    >
      {B => (
        <>
          <B.Input as={A.Input} flex alignItems={`center`}>
            <textarea type={type} value={value} onChange={onChange} />
            {maxLength > value?.length ? (
              <A.Badge>{`${value}`.length}</A.Badge>
            ) : (
              <A.Badge>{maxLength}</A.Badge>
            )}
          </B.Input>
        </>
      )}
    </Composition>
  )
}
