import styled from 'styled-components'

export const IconLeftArrow = styled.i`
  border: solid #fff;
  border-width: 0 3px 3px 0;
  display: inline-block;
  padding: 5px;
  transform: rotate(135deg);
`
