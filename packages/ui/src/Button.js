import {Box} from 'atomic-layout'
import React from 'react'
import styled from 'styled-components'

const A = {}

A.Button = styled(Box)`
  user-select: none;
  cursor: pointer;

  display: flex;
  align-items: center;
  justify-content: center;

  font-size: 1.1em;
  font-weight: bold;

  color: ${props => (props.isDisabled ? 'rgba(0, 0, 0, 0.5)' : '#000')};
`

export function Button(props) {
  return <A.Button {...props} />
}
