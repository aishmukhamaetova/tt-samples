import React from 'react'
import {FaPhone} from 'react-icons/fa'

export function IconPhone(props) {
  return <FaPhone {...props} />
}
