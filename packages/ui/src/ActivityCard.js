import React from 'react'
import {Area} from './Area'
import {IconStar} from './IconStar'
// import {Link} from 'react-router-dom'

const A = {}

const areas = `
  'icon title date'
  'tags tags favourite'
  'location location favourite'
  'text text favourite'
`

const templateRows = `20px 20px 20px 62px`
const templateCols = `20px 1fr 50px`

const COLOR = `rgb(255, 176, 0)`

export function ActivityCard({
  children,
  icon,
  title,
  tags,
  date,
  location,
  duration,
  onClick,
  onClickStar,
  isFavourite,
}) {
  return (
    <A.Container onClick={onClick}>
      <A.Grid
        areas={areas}
        templateCols={templateCols}
        templateRows={templateRows}
      >
        <Area name="icon">{icon}</Area>
        <Area name="title" as={A.AreaTitle}>
          {title}
        </Area>
        <Area name="date" as={A.AreaDate}>
          {date}
        </Area>
        <Area name="tags" as={A.AreaTags}>
          {tags}
        </Area>
        <Area name="location" as={A.AreaLocation}>
          {location}, {duration}
        </Area>
        <Area name="favourite" as={A.Favourite}>
          <IconStar
            variant={!isFavourite ? `regular` : null}
            color={COLOR}
            size={27}
            onClick={e => {
              if (onClickStar) {
                e.stopPropagation()
                onClickStar()
              }
            }}
          />
          {/* onClick={() => setFavourite(!isFavourite)} */}
        </Area>
        <Area name="text" as={A.Text}>
          <A.TextContainer>{children}</A.TextContainer>
        </Area>
      </A.Grid>
    </A.Container>
  )
}

import styled from 'styled-components'
import {Grid} from './Grid'

A.Container = styled.div`
  width: 100%;
  height: 150px;

  background: rgba(232, 232, 232, 1);

  border: solid 1px rgba(253, 183, 62, 1);
  border-radius: 12px;

  cursor: pointer;
`

A.Grid = styled(Grid)`
  padding: 10px;

  grid-column-gap: 6px;
  grid-row-gap: 4px;

  justify-content: flex-start;
`

A.AreaTitle = styled(Area)`
  font-size: 16px;
  font-weight: bold;
  letter-spacing: -0.32px;
  line-height: 20px;
  white-space: normal;
  overflow: hidden;
`

A.AreaDate = styled(Area)`
  font-size: 13px;
  font-weight: 300;
  letter-spacing: -0.31px;
  text-align: right;

  color: rgb(114, 114, 114);
`

A.AreaTags = styled(Area)`
  font-size: 14px;
  font-weight: 600;
  letter-spacing: -0.22px;
  line-height: 20px;
  white-space: nowrap;
`

A.AreaLocation = styled(Area)`
  font-size: 15px;
  font-weight: 300;
  letter-spacing: -0.24px;
  line-height: 20px;
`

A.Text = styled(Area)`
  font-size: 15px;
  font-weight: 300;
  letter-spacing: -0.24px;
  line-height: 20px;
`

A.TextContainer = styled.div`
  overflow: hidden;
  position: relative;
  line-height: 1.2em;
  max-height: 3.6em;
  text-align: left;
`

A.Favourite = styled(Area)`
  display: flex;
  justify-content: flex-end;
  align-items: center;

  padding-bottom: 25px;
`
