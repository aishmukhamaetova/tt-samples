import {Composition} from 'atomic-layout'
import React from 'react'
import styled from 'styled-components'

const A = {}

const areas = `label content`

const templateRows = `auto`
const templateCols = `110px auto`

A.Label = styled.div`
  font-weight: bold;
  white-space: nowrap;
`


A.Content = styled.div`
  font-weight: ${({fontWeight}) => (fontWeight || `normal`)};

`


export function FieldProfile({className, label, text, fontWeight}) {
  return (
    <Composition
      className={className}
      areas={areas}
      justify={`start`}
      justifyContent={`start`}
      justifyItems={`start`}
      templateCols={templateCols}
      templateRows={templateRows}
      paddingLeft={15}
      paddingTop={10}
    >
      {B => (
        <>
          <B.Label as={A.Label} flex alignItems={`center` } >
            {label}
          </B.Label>
          <B.Content fontWeight={fontWeight} paddingLeft={15} as={A.Content} flex alignItems={`center`}>
            {text}
          </B.Content>
        </>
      )}
    </Composition>
  )
}
