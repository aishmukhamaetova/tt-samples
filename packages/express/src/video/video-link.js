import createDebugger from 'debug'
import AWS from 'aws-sdk'
import uuid from 'uuid/v4'

import '../configure-aws'

const who = [`@tt`, `express`, `video`, `url`]

const log = createDebugger(who.join(`:`))

const BUCKET = process.env.AWS_VIDEO || 'team-tasker-video'

const get = ({filename, type}) =>
  new Promise((resolve, reject) => {
    const s3 = new AWS.S3({region: process.env.AWS_REGION || 'ap-southeast-2'})

    s3.getSignedUrl(
      'putObject',
      {
        Bucket: BUCKET,
        Key: filename,
        Expires: 600,
        ContentType: type,
        // ACL: `bucket-owner-full-control`,
        ACL: 'public-read'
      },
      (err, data) => {
        if (!err) {
          return resolve(data)
        }

        reject(err)
      }
    )
  })

export async function videoLink(req, res) {
  try {
    log(req.body)

    const {sessionId, activityId, type} = req.body

    const ext = `webm`
    const filename = `${uuid()}.${ext}`

    const link = await get({filename, type})

    res.json({
      link,
      errors: [],
      version: `20191208.2203`
    })
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot generate link`
        }
      ]
    })
  }
}
