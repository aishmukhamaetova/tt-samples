import createDebugger from 'debug'

import {userUpdate as dbUserUpdate} from '@tt/db'
import {sessionQuery} from '@tt/db'

const who = [`@tt`, `express`, `user`, `update`]

const log = createDebugger(who.join(`:`))

export async function userUpdate(req, res) {
  try {
    log(req.body)

    const payload = req.body
    const {sessionId} = payload

    const session = await sessionQuery({id: sessionId})

    if (session) {
      const {userId} = session

      const doc = await dbUserUpdate({
        ...payload,
        id: userId,
      })

      const {password: _remove, ...user} = doc

      res.json({
        user,
        errors: [],
      })
    }
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot update user`,
        },
      ],
    })
  }
}
