import createDebugger from 'debug'

const who = [`@tt`, `express`, `user`, `query-okta`]

const log = createDebugger(who.join(`:`))

export async function userQueryOkta(req, res) {
  try {
    log(req.body)
    const passport = req.session && req.session.passport
    console.log('PASSPORT', req.session)

    if (process.env.SAML_ENV === 'dev') {
      res.json({
        passport: {user: 'local'},
        errors: []
      })
    } else if (passport) {
      res.json({
        passport,
        errors: []
      })
    } else {
      res.json({
        errors: [
          {
            message: `Okta: not authorized to access this resource/api`
          }
        ]
      })
    }
  } catch (err) {
    console.log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot execute user-query-okta`
        }
      ]
    })
  }
}
