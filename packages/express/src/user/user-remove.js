import createDebugger from 'debug'

import {userFindForRemove} from '@tt/db'
import {sessionRemove} from '@tt/db'

const who = [`@tt`, `express`, `user`, `remove`]

const log = createDebugger(who.join(`:`))

export async function userRemove(req, res) {
  try {
    log(req.body)

    const {email, sessionId} = req.body

    const userRmoved = await userFindForRemove({email})
    const sessionRemoved = await sessionRemove({sessionId})
    if (sessionRemoved && userRmoved) {
      res.json({
        result: [
          {
            id: sessionId,
          },
        ],
        errors: [],
      })
    } else {
      throw new Error(`Cannot find user and remove ${email}`)
    }
  } catch (err) {
    log(`ERROR!`, err)

    res.json({
      errors: [
        {
          message: `Cannot remove user`,
        },
      ],
    })
  }
}
