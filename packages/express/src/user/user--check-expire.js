import createDebugger from 'debug'

import {sessionQuery} from '@tt/db'
import {sessionRemove} from '@tt/db'

const who = [`@tt`, `express`, `user`, `check-expire`]

const log = createDebugger(who.join(`:`))

const SESSION_EXP_TIME = process.env.SESSION_EXP_TIME || 1000 * 60 * 15

export async function userCheckExpire(req, res, next) {
  try {
    log(req.body)

    const payload = req.body
    const {sessionId} = payload

    const session = await sessionQuery({id: sessionId})

    if (!session) {
      throw new Error(`Cannot find session`)
    }

    const createdTime = new Date(session.createdAt).getTime()
    const currentTime = new Date().getTime()

    if (currentTime - createdTime > SESSION_EXP_TIME) {
      sessionRemove({sessionId: session.id})

      return res.json({
        result: false,
        errors: [
          {
            message: `Session is expired and removed.`
          }
        ]
      })
    }

    return next()
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot find session`
        }
      ]
    })
  }
}
