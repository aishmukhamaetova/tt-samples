import createDebugger from 'debug'

import {checkCodeEmail} from './user-check-code'
import {userUpdate} from '@tt/db'

const who = [`@tt`, `express`, `user`, `userSignupVerify`]

const log = createDebugger(who.join(`:`))

const HOST = process.env.HOST

export async function userSignupVerify(req, res) {
  const params = req.params
  const {code} = params
  try {
    const user = await checkCodeEmail(code)
    if (user?.expired) {
      return res.redirect(`${HOST}/profile/create/expired/${code}`)
    }
    if (user?.verified) {
      return res.redirect(`${HOST}/profile/create/expired/${code}`)
    }

    if (user && !user.expired) {
      await userUpdate({
        code: null,
        codeDate: null,
        status: 'verified',
        id: user && user.id
      })
      return res.redirect(`${HOST}/profile/user-verified`) //throw new Error(`Code not found, likely user verified`)
    }
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: err?.message
        }
      ]
    })
  }
}
