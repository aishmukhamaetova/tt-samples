import createDebugger from 'debug'

import {userUpdateNote as dbUserUpdateNote} from '@tt/db'
import {sessionQuery} from '@tt/db'

const who = [`@tt`, `express`, `user`, `updateNote`]

const log = createDebugger(who.join(`:`))

export async function userUpdateNote(req, res) {
  try {
    log('req.body', req.body)

    const payload = req.body
    const {id} = payload
    if (id) {
      const doc = await dbUserUpdateNote({
        ...payload,
        id,
      })

      res.json({
        ...doc,
        errors: [],
      })
    }
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot update note Admin`,
        },
      ],
    })
  }
}
