import createDebugger from 'debug'
import uuid from 'uuid/v4'

import {userFindByEmail} from '@tt/db'
import {userUpdate} from '@tt/db'

const AWS = require('aws-sdk')

const who = [`@tt`, `express`, `user`, `send-feedback`]

const log = createDebugger(who.join(`:`))

const ses = new AWS.SES({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
  region: process.env.AWS_REGION,
  apiVersion: '2010-12-01',
})

const EMAILTO = process.env.EMAILTO
const EMAILFROM = process.env.EMAILFROM

const sendEmail = ({email, note, name, phone}) => {
  const params = {
    Destination: {
      ToAddresses: [EMAILTO], // teamtasker@ozminerals.com
    },
    ConfigurationSetName: `team-tasker-set`,
    Message: {
      Body: {
        Text: {
          Charset: 'UTF-8',
          Data: `From User: ${email}, ${name}, ${phone}. ${note}`,
        },
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Team tasker feedback email',
      },
    },
    Source: `${EMAILFROM}`,
  }

  const output = ses.sendEmail(params).promise()
  return output
}

export async function userSendFeedback(req, res) {
  try {
    log(req.body)

    const {note, email} = req.body

    const doc = await userFindByEmail({email})
    const {name, phone} = doc

    if (doc) {
      const response = (await sendEmail({note, email, name, phone})) || {}
      const {MessageId} = response

      res.json({
        messageId: MessageId,
        errors: [],
      })
    } else {
      throw new Error(`Cannot send feedback from the user ${email}.`)
    }
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot send feedback from the user ${email}.`,
        },
      ],
    })
  }
}
