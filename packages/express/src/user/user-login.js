import createDebugger from 'debug'

import {userFindForLogin} from '@tt/db'
import {sessionCreate} from '@tt/db'

const who = [`@tt`, `express`, `user`, `login`]

const log = createDebugger(who.join(`:`))

export async function userLogin(req, res) {
  try {
    log(req.body)

    const {email, password} = req.body

    const doc = await userFindForLogin({email, password})

    if (doc && doc.status === 'unverified') {
      return res.json({
        errors: [
          {
            code: doc.code,
            message: `unverified`
          }
        ]
      })
    } else if (doc && doc.email) {
      const {password, ...user} = doc
      const session = await sessionCreate({userId: doc.id})

      return res.json({
        session,
        user,
        errors: []
      })
    } else {
      return res.json({
        errors: [
          {
            message: `Cannot find user or password mismatch for ${email}`
          }
        ]
      })
    }
  } catch (err) {
    log(`ERROR`, err)

    return res.json({
      errors: [
        {
          message: err?.message || `Cannot login`
        }
      ]
    })
  }
}
