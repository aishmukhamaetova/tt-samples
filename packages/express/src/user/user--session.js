import createDebugger from 'debug'

import {sessionQuery} from '@tt/db'
import {sessionRemove} from '@tt/db'

const who = [`@tt`, `express`, `user`, `session`]

const log = createDebugger(who.join(`:`))

export async function userSession(req, res) {
  try {
    log(req.body)

    const payload = req.body
    const {sessionId} = payload

    const session = await sessionQuery({id: sessionId})

    if (session) {
      res.json({
        result: true,
        errors: [],
      })
    } else {
      res.json({
        result: false,
        errors: [],
      })
    }
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot find session`,
        },
      ],
    })
  }
}
