import createDebugger from 'debug'
const AWS = require('aws-sdk')

import {userCreate} from '@tt/db'

const who = [`@tt`, `express`, `user`, `signup`]

const log = createDebugger(who.join(`:`))

const ses = new AWS.SES({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
  region: process.env.AWS_REGION,
  apiVersion: '2010-12-01'
})

const BACKEND_HOST = process.env.BACKEND_HOST

const sendEmail = ({to, code}) => {
  const toLowercase = to?.toLowerCase()
  const params = {
    Destination: {
      ToAddresses: [toLowercase] // Email address/addresses that you want to send your email
    },
    ConfigurationSetName: `team-tasker-set`,
    Message: {
      Body: {
        // Html: {
        //   Charset: 'UTF-8',
        //   Data:
        //     "<html><body><h1>Hello  Charith</h1><p style='color:red'>Sample description</p> <p>Time 1517831318946</p></body></html>",
        // },
        Text: {
          Charset: 'UTF-8',
          Data: `Please click <a href="${BACKEND_HOST}/api/v1/user/signup/verify/${code}" target="_blank">verify account</a>`
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Team tasker sign up, verify email process'
      }
    },
    Source: process.env.EMAILFROM || `no-reply.teamtasker@ozminerals.com`
  }

  return ses.sendEmail(params).promise()
}

export async function userSignup(req, res) {
  try {
    log(req.body)

    const {email, password, skills, name, phone} = req.body

    const doc = await userCreate({email, password, skills, name, phone})
    const {password: _remove, ...user} = doc

    await sendEmail({to: email, code: user && user.code})

    res.json({
      errors: []
    })
  } catch (err) {
    res.json({
      errors: [
        {
          message: err?.message || `Cannot signup`
        }
      ]
    })
  }
}
