import createDebugger from 'debug'

import {userUpdate as dbUserUpdate} from '@tt/db'
import {userFindByCode} from '@tt/db'

const who = [`@tt`, `express`, `user`, `updateByCode`]

const log = createDebugger(who.join(`:`))

export async function userUpdateByCode(req, res) {
  try {
    log(req.body)

    const payload = req.body
    const {code} = payload

    const user = await userFindByCode({code})

    log(`user`, user, user?.id)

    // const session = await sessionQuery({id: sessionId})

    if (user) {
      const doc = await dbUserUpdate({
        ...payload,
        code: null,
        id: user.id,
      })

      const {password: _remove, code: _remove2, ...rest} = doc

      res.json({
        user: rest,
        errors: [],
      })
    }
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot update user`,
        },
      ],
    })
  }
}
