import createDebugger from 'debug'
import uuid from 'uuid/v4'

import {userFindByEmail} from '@tt/db'
import {userUpdate} from '@tt/db'

const AWS = require('aws-sdk')

const who = [`@tt`, `express`, `user`, `forget`]

const log = createDebugger(who.join(`:`))

const ses = new AWS.SES({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
  region: process.env.AWS_REGION,
  apiVersion: '2010-12-01'
})

const HOST = process.env.HOST

const sendEmail = ({to, code}) => {
  const toLowercase = to?.toLowerCase()
  const params = {
    Destination: {
      ToAddresses: [toLowercase] // Email address/addresses that you want to send your email
    },
    ConfigurationSetName: `team-tasker-set`,
    Message: {
      Body: {
        // Html: {
        //   Charset: 'UTF-8',
        //   Data:
        //     "<html><body><h1>Hello  Charith</h1><p style='color:red'>Sample description</p> <p>Time 1517831318946</p></body></html>",
        // },
        Text: {
          Charset: 'UTF-8',
          Data: `Code: ${HOST}/profile/changepassword/${code}`
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Team tasker restore email'
      }
    },
    Source: process.env.EMAILFROM || `no-reply.teamtasker@ozminerals.com`
  }

  return ses.sendEmail(params).promise()
}

export async function userForgot(req, res) {
  try {
    log(req.body)

    const {email} = req.body

    const doc = await userFindByEmail({email})

    if (doc && doc.status === 'verified') {
      const code = uuid()
      const userUpdated = await userUpdate({
        id: doc.id,
        code,
        codeDate: new Date()
      })
      const {password: _remove, code: _remove2, ...user} = userUpdated

      await sendEmail({to: email, code})

      res.json({
        user,
        errors: []
      })
    } else if (doc.status === 'unverified') {
      res.json({
        errors: [
          {
            code: doc.code,
            email: doc.email,
            message: `unverified`
          }
        ]
      })
    } else {
      res.json({
        errors: [
          {
            message: `User not found. Please Create Account.`
          }
        ]
      })
    }
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot create code to restore password. Cannot find user for ${email}`
        }
      ]
    })
  }
}
