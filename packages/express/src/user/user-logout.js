import createDebugger from 'debug'

import {sessionRemove} from '@tt/db'

const who = [`@tt`, `express`, `user`, `logout`]

const log = createDebugger(who.join(`:`))

export async function userLogout(req, res) {
  try {
    log(req.body)

    const {sessionId} = req.body

    const r = await sessionRemove({sessionId})

    if (r) {
      res.json({
        result: [
          {
            id: sessionId,
          },
        ],
        errors: [],
      })
    } else {
      throw new Error(`Cannot find session with sessionId ${sessionId}`)
    }
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot logout`,
        },
      ],
    })
  }
}
