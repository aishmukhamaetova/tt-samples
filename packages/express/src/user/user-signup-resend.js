import createDebugger from 'debug'
import uuid from 'uuid/v4'
const AWS = require('aws-sdk')

import {userFindByCode, userUpdate} from '@tt/db'

const who = [`@tt`, `express`, `user`, `signup-resend`]

const log = createDebugger(who.join(`:`))

const ses = new AWS.SES({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
  region: process.env.AWS_REGION,
  apiVersion: '2010-12-01'
})

const BACKEND_HOST = process.env.BACKEND_HOST
const VERIFY_EXP_TIME = process.env.VERIFY_EXP_TIME

const sendEmail = ({to, code}) => {
  const toLowercase = to?.toLowerCase()
  const params = {
    Destination: {
      ToAddresses: [toLowercase] // Email address/addresses that you want to send your email
    },
    ConfigurationSetName: `team-tasker-set`,
    Message: {
      Body: {
        // Html: {
        //   Charset: 'UTF-8',
        //   Data:
        //     "<html><body><h1>Hello  Charith</h1><p style='color:red'>Sample description</p> <p>Time 1517831318946</p></body></html>",
        // },
        Text: {
          Charset: 'UTF-8',
          Data: `Please click <a href="${BACKEND_HOST}/api/v1/user/signup/verify/${code}" target="_blank">verify account</a>`
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Team tasker sign up email'
      }
    },
    Source: process.env.EMAILFROM || `no-reply.teamtasker@ozminerals.com`
  }

  return ses.sendEmail(params).promise()
}

export async function userSignupResend(req, res) {
  try {
    log(req.body)

    const {code} = req.body

    const user = await userFindByCode({code})
    if (!user) {
      return res.json({
        errors: [
          {
            message: `Error occurred. User is not found by UIID code.`
          }
        ]
      })
    }

    if (user.status === 'verified') {
      return res.json({
        errors: [
          {
            message: `Error occurred. User is already verified.`
          }
        ]
      })
    }
    if (!user.code) {
      return res.json({
        errors: [
          {
            message: `Error occurred. Code is not found.`
          }
        ]
      })
    }

    if (!user.codeDate) {
      return res.json({
        errors: [
          {
            message: `Error occurred. CodeDate is not found.`
          }
        ]
      })
    }

    //IF code expired resend new code. Otherwsie old code
    const expired = Date.now() - new Date(user.codeDate) > VERIFY_EXP_TIME
    const newCode = expired ? uuid() : user.code

    await sendEmail({to: user?.email, code: newCode})
    await userUpdate({id: user?.id, code: newCode, codeDate: new Date()})

    res.json({
      errors: []
    })
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: err?.message || `Cannot resend email verification`
        }
      ]
    })
  }
}
