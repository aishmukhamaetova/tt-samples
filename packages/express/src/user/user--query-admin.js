import createDebugger from 'debug'
import {userQuery} from '@tt/db'

const who = [`@tt`, `express`, `user`, `query-admin`]

const log = createDebugger(who.join(`:`))

export async function userQueryAdmin(req, res) {
  try {
    log(req.body)

    const userList = await userQuery({}, {isAdminRequest: true})

    res.json({
      userList,
      errors: []
    })
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot execute user-query-admin`
        }
      ]
    })
  }
}
