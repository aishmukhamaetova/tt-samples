import createDebugger from 'debug'

import {userFindByCode} from '@tt/db'

const who = [`@tt`, `express`, `user`, `checkCode`]

const log = createDebugger(who.join(`:`))

//we have 2 code expired variabled / for forgot password and verify email
const VERIFY_EXP_TIME = process.env.VERIFY_EXP_TIME
const CODE_EXP_TIME = process.env.CODE_EXP_TIME

export async function checkCodeEmail(code) {
  let user

  try {
    user = await userFindByCode({code})
    if (!user) {
      return {verified: true}
    } else {
      const expired = Date.now() - new Date(user.codeDate) > VERIFY_EXP_TIME

      const {password: _remove, code: _remove2, ...rest} = user

      return {...rest, expired}
    }

    //end
  } catch (err) {
    throw new Error('Code not found')
  }
}

export async function checkCodePassword(code) {
  let user

  try {
    user = await userFindByCode({code})

    if (!user) {
      throw new Error('Code not found')
    }

    const expired = Date.now() - new Date(user.codeDate) > CODE_EXP_TIME

    if (expired) {
      throw new Error(`Code expired`)
    }

    if (user) {
      const {password: _remove, code: _remove2, ...rest} = user

      return rest
    }
    //end
  } catch (err) {
    throw new Error('Code not found')
  }
}

export async function userCheckPasswordCode(req, res) {
  try {
    log(req.body)

    const payload = req.body
    const {code} = payload

    const user = await checkCodePassword(code)

    log(`user`, user, user?.id)

    // const session = await sessionQuery({id: sessionId})
    res.json({
      user,
      errors: []
    })
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: err?.message
        }
      ]
    })
  }
}
