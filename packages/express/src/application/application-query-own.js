import createDebugger from 'debug'
import {applicationQueryOwn as serviceApplicationQueryOwn} from '@tt/db'
import {sessionQuery} from '@tt/db'

const who = [`@tt`, `express`, `application`, `queryOwn`]

const log = createDebugger(who.join(`:`))

export async function applicationQueryOwn(req, res) {
  try {
    log(req.body)

    const payload = req.body

    const {sessionId} = payload

    const session = await sessionQuery({id: sessionId})

    if (session && session.userId) {
      const {userId} = session

      const applicationList = await serviceApplicationQueryOwn({userId})

      return res.json({
        applicationList,
        errors: [],
      })
    }

    throw new Error(`Session not found`)
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot query application`,
        },
      ],
    })
  }
}
