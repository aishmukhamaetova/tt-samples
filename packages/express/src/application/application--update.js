import createDebugger from 'debug'
import {Application} from '@tt/db--application'

const who = [`@tt`, `express`, `application`, `update`]

const log = createDebugger(who.join(`:`))

export async function applicationUpdate(req, res) {
  try {
    log(req.body)

    const payload = req.body

    const {result, errors} = await Application.update(payload)

    if (errors.length === 0) {
      return res.json({
        applicationList: result,
        errors: [],
      })
    }

    throw new Error(`Cannot update application with`, JSON.stringify(payload))
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot update application`,
        },
      ],
    })
  }
}
