import {applicationCreate} from './application-create'
import {applicationQueryOwn} from './application-query-own'
import {applicationUpdate} from './application--update'

export const application = {
  create: applicationCreate,
  queryOwn: applicationQueryOwn,
  update: applicationUpdate,
}
