import createDebugger from 'debug'
import {applicationCreate as serviceApplicationCreate} from '@tt/db'
import {applicationQueryByUserId as serviceApplicationQueryByUserId} from '@tt/db'
import {sessionQuery} from '@tt/db'

const who = [`@tt`, `express`, `application`, `create`]

const log = createDebugger(who.join(`:`))

export async function applicationCreate(req, res) {
  try {
    log(req.body)

    const payload = req.body

    const {sessionId, activityId} = payload

    const session = await sessionQuery({id: sessionId})

    if (session && session.userId) {
      const {userId} = session

      const applicationList = await serviceApplicationQueryByUserId({
        userId,
      })

      const previous = applicationList.filter(v => v.activityId === activityId)

      if (previous && previous.length) {
        return res.json({
          applicationList: previous,
          errors: [
            {
              message: `Applied already`,
            },
          ],
        })
      }

      const application = await serviceApplicationCreate({
        ...payload,
        userId,
      })

      if (application) {
        return res.json({
          application,
          errors: [],
        })
      }
    }

    throw new Error(`Cannot create application with`, JSON.stringify(payload))
  } catch (err) {
    log(`ERROR`, err)

    res.json({
      errors: [
        {
          message: `Cannot create application`,
        },
      ],
    })
  }
}
