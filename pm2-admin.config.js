module.exports = {
  apps: [
    {
      name: 'tt/admin',
      cwd: 'admin',
      script: 'server.js',
      instances: 1,
      autorestart: true,
      env: {
        PORT: 5000
      },

      env_production: {
        REACT_APP_API: 'https://admin-teamtasker.ozminerals.com',
        NODE_ENV: 'production',
        PORT: 5000
      }
    },
    {
      name: 'tt/backend',
      cwd: 'backend',
      script: 'yarn',
      args: 'prod',
      instances: 1,
      autorestart: true,
      exec_interpreter: 'npx',
      exec_mode: 'fork_mode',
      restart_delay: 4000,

      env: {
        PORT: 3000
      },

      env_production: {
        NODE_ENV: 'production',
        PORT: 3000,
        AWS_REGION: 'ap-southeast-2',

        HOST: 'https://teamtasker.ozminerals.com',

        AWS_ACTIVITY: 'team-tasker--activity-videos3',
        AWS_AUDIO: 'team-tasker-audio3',
        AWS_VIDEO: 'team-tasker-video3',
        SAML_ENV: 'production'
      }
    }
  ]
}
