import React from 'react'
import {useParams} from 'react-router-dom'
import {useHistory} from 'react-router-dom'

import {PageActivities} from '@tt/pages'
import {PageApplicationAudio} from '@tt/pages'
import {PageApplicationVideo} from '@tt/pages'
import {PageApplicationWritten} from '@tt/pages'
import {PageFavourites} from '@tt/pages'
import {PageHome} from '@tt/pages'
import {PageHowToApply} from '@tt/pages'
import {PageJobOpportunity} from '@tt/pages'
import {PageSignIn} from '@tt/pages'
import {PageMyActivityHistory} from '@tt/pages'
import {PageProfileCreate} from '@tt/pages'
import {PageProfileCreateExpired} from '@tt/pages'
import {PageProfileView} from '@tt/pages'
import {PageProfileEdit} from '@tt/pages'
import {PageProfileChangePassword} from '@tt/pages'
import {PageNotFound} from '@tt/pages'
import {PageVerificationEmail} from '@tt/pages'

const Page = {}

function Home() {
  const history = useHistory()

  const onSignInClick = () => history.push(`/signin`)
  const onProfileCreate = () => history.push(`/profile/create`)
  const onViewActivities = () => history.push(`/activities`)

  return (
    <PageHome
      onSignInClick={onSignInClick}
      onProfileCreate={onProfileCreate}
      onViewActivities={onViewActivities}
    />
  )
}

function SignIn({currentUser}) {
  const history = useHistory()

  const onBackClick = () => {
    if (!currentUser) {
      history.push(`/`)
    } else {
      history.goBack()
    }
  }
  const onSuccess = () => history.push(`/activities`)
  const onForgotPassword = () => history.push(`/profile/changepassword`)

  return (
    <PageSignIn
      onBackClick={onBackClick}
      onSuccess={onSuccess}
      onForgotPassword={onForgotPassword}
    />
  )
}

function Activities() {
  const history = useHistory()
  const isActiveRoute = (path) => {
    return history.location.pathname === path
  }

  const onBackClick = () => history.goBack()
  const onJobClick = ({id}) => {
    history.push(`/jobopportunity/${id}`)
  }
  const redirectToSignIn = () => {
    history.push(`/signin`)
  }
  const onProfileClick = () => history.push(`/profile`)
  const onStatusClick = () => history.push(`/status`)
  const onActivitiesClick = () => history.push(`/activities`)
  const onFavouritesClick = () => history.push(`/favourites`)

  return (
    <PageActivities
      onBackClick={onBackClick}
      onJobClick={onJobClick}
      redirectToSignIn={redirectToSignIn}
      onProfileClick={onProfileClick}
      onStatusClick={onStatusClick}
      onActivitiesClick={onActivitiesClick}
      onFavouritesClick={onFavouritesClick}
      isActiveRoute={isActiveRoute}
    />
  )
}

function ApplicationAudio() {
  const history = useHistory()
  const {id} = useParams()

  const onBackClick = () => history.goBack()
  const onSubmitSuccess = () => history.push(`/status`)

  return (
    <PageApplicationAudio
      id={id}
      onBackClick={onBackClick}
      onSubmitSuccess={onSubmitSuccess}
    />
  )
}

function ApplicationVideo() {
  const history = useHistory()
  const {id} = useParams()

  const onBackClick = () => history.goBack()
  const onSubmitSuccess = () => history.push(`/status`)

  return (
    <PageApplicationVideo
      id={id}
      onBackClick={onBackClick}
      onSubmitSuccess={onSubmitSuccess}
    />
  )
}

function ApplicationWritten() {
  const history = useHistory()
  const {id} = useParams()

  const onBackClick = () => history.goBack()
  const onSubmitSuccess = () => history.push(`/status`)

  return (
    <PageApplicationWritten
      id={id}
      onBackClick={onBackClick}
      onSubmitSuccess={onSubmitSuccess}
    />
  )
}

function Favourites() {
  const history = useHistory()
  const onBackClick = () => history.goBack()
  const onJobClick = ({id}) => {
    history.push(`/jobopportunity/${id}`)
  }

  const onProfileClick = () => history.push(`/profile`)
  const onStatusClick = () => history.push(`/status`)
  const onActivitiesClick = () => history.push(`/activities`)
  const onFavouritesClick = () => history.push(`/favourites`)
  const isActiveRoute = (path) => {
    return history.location.pathname === path
  }

  return (
    <PageFavourites
      isActiveRoute={isActiveRoute}
      onBackClick={onBackClick}
      onJobClick={onJobClick}
      onProfileClick={onProfileClick}
      onStatusClick={onStatusClick}
      onActivitiesClick={onActivitiesClick}
      onFavouritesClick={onFavouritesClick}
    />
  )
}

function HowToApply() {
  const history = useHistory()
  const {id} = useParams()

  const onBackClick = () => history.goBack()
  const isActiveRoute = (path) => {
    return history.location.pathname === path
  }

  const onProfileClick = () => history.push(`/profile`)
  const onStatusClick = () => history.push(`/status`)
  const onActivitiesClick = () => history.push(`/activities`)
  const onFavouritesClick = () => history.push(`/favourites`)
  const onChoose = (tag) => history.push(`/apply/${tag}/${id}`)
  return (
    <PageHowToApply
      isActiveRoute={isActiveRoute}
      onProfileClick={onProfileClick}
      onStatusClick={onStatusClick}
      onActivitiesClick={onActivitiesClick}
      onFavouritesClick={onFavouritesClick}
      onBackClick={onBackClick}
      onChoose={onChoose}
    />
  )
}

function JobOpportunity() {
  const history = useHistory()
  const {id} = useParams()
  const onApply = () => history.push(`/how/to/apply/${id}`)
  const isActiveRoute = (path) => {
    return history.location.pathname === path
  }

  const onProfileClick = () => history.push(`/profile`)
  const onStatusClick = () => history.push(`/status`)
  const onActivitiesClick = () => history.push(`/activities`)
  const onFavouritesClick = () => history.push(`/favourites`)

  const onBackClick = () => history.goBack()

  return (
    <PageJobOpportunity
      id={id}
      isActiveRoute={isActiveRoute}
      onBackClick={onBackClick}
      onProfileClick={onProfileClick}
      onStatusClick={onStatusClick}
      onActivitiesClick={onActivitiesClick}
      onFavouritesClick={onFavouritesClick}
      onApply={onApply}
    />
  )
}

function MyActivityHistory() {
  const history = useHistory()
  const isActiveRoute = (path) => {
    return history.location.pathname === path
  }

  const onProfileClick = () => history.push(`/profile`)
  const onStatusClick = () => history.push(`/status`)
  const onActivitiesClick = () => history.push(`/activities`)
  const onFavouritesClick = () => history.push(`/favourites`)

  const onBackClick = () => history.goBack()

  return (
    <PageMyActivityHistory
      isActiveRoute={isActiveRoute}
      onProfileClick={onProfileClick}
      onStatusClick={onStatusClick}
      onActivitiesClick={onActivitiesClick}
      onFavouritesClick={onFavouritesClick}
      onBackClick={onBackClick}
    />
  )
}

function ProfileView() {
  const history = useHistory()
  const isActiveRoute = (path) => {
    return history.location.pathname === path
  }

  const onProfileClick = () => history.push(`/profile`)
  const onStatusClick = () => history.push(`/status`)
  const onActivitiesClick = () => history.push(`/activities`)
  const onFavouritesClick = () => history.push(`/favourites`)

  const onBackClick = () => history.goBack()
  const onEditClick = () => history.push(`/profile/edit`)
  const onLogout = () => (window.location.href = `/`)

  return (
    <PageProfileView
      isActiveRoute={isActiveRoute}
      onProfileClick={onProfileClick}
      onStatusClick={onStatusClick}
      onActivitiesClick={onActivitiesClick}
      onFavouritesClick={onFavouritesClick}
      onBackClick={onBackClick}
      onEditClick={onEditClick}
      onLogout={onLogout}
    />
  )
}

function ProfileCreate() {
  const history = useHistory()

  const onBackClick = () => history.goBack()
  const onSuccess = () => history.push(`/profile/create/success`)

  return <PageProfileCreate onBackClick={onBackClick} onSuccess={onSuccess} />
}

function ProfileCreateExpired() {
  const history = useHistory()
  const {code} = useParams()

  const onBackClick = () => history.goBack()

  return <PageProfileCreateExpired onBackClick={onBackClick} code={code} />
}

function ProfileVerify() {
  const history = useHistory()
  const {code} = useParams()

  const onBackClick = () => history.goBack()

  return (
    <PageProfileCreateExpired
      onBackClick={onBackClick}
      code={code}
      isNotVerified={true}
    />
  )
}

function ProfileCreateSuccess({verified}) {
  const history = useHistory()

  const onBackClick = () => history.push(`/`)
  const onSingInClick = () => history.push(`/signin`)

  return verified ? (
    <PageVerificationEmail
      verified={verified}
      onSingInClick={onSingInClick}
      msg={`Email is verified, please Sign In`}
    />
  ) : (
    <PageVerificationEmail
      onBackClick={onBackClick}
      msg={`We have  sent you an email to verify account creation`}
    />
  )
}

function ProfileEdit() {
  const history = useHistory()

  const onBackClick = () => history.goBack()
  const onSuccessEdit = () => history.push(`/profile`)

  return (
    <PageProfileEdit onBackClick={onBackClick} onSuccessEdit={onSuccessEdit} />
  )
}

function ProfileChangePassword() {
  const {code} = useParams()
  const history = useHistory()

  const onBackClick = () => history.goBack()
  const onSuccess = () => history.push(`/signin`)

  return (
    <PageProfileChangePassword
      code={code}
      onBackClick={onBackClick}
      onSuccess={onSuccess}
    />
  )
}

function NotFound() {
  const history = useHistory()

  const onBackClick = () => history.goBack()

  return <PageNotFound onBackClick={onBackClick} />
}

Page.Activities = Activities
Page.ApplicationAudio = ApplicationAudio
Page.ApplicationVideo = ApplicationVideo
Page.ApplicationWritten = ApplicationWritten
Page.Favourites = Favourites
Page.Home = Home
Page.HowToApply = HowToApply
Page.JobOpportunity = JobOpportunity
Page.SignIn = SignIn
Page.MyActivityHistory = MyActivityHistory
Page.Profile = ProfileView
Page.ProfileCreate = ProfileCreate
Page.ProfileCreateExpired = ProfileCreateExpired
Page.ProfileVerify = ProfileVerify
Page.ProfileCreateSuccess = ProfileCreateSuccess
Page.ProfileEdit = ProfileEdit
Page.ProfileChangePassword = ProfileChangePassword
Page.NotFound = NotFound

export default Page
