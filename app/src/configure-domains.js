import {configSet} from '@tt/domains'

configSet(`API`, process.env.REACT_APP_API)
configSet(`AUDIO_SIZE`, process.env.REACT_APP_AUDIO_SIZE ?? 8)
configSet(`VIDEO_SIZE`, process.env.REACT_APP_VIDEO_SIZE ?? 10)
