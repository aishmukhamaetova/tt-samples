import React from 'react'
import {BrowserRouter} from 'react-router-dom'
import {Switch} from 'react-router-dom'
import {Route} from 'react-router-dom'
import {createGlobalStyle} from 'styled-components'
import Page from './Page'
import {fromAuth} from '@tt/domains'
import {useStore} from 'effector-react'

const GlobalStyle = createGlobalStyle`
  html, body {
    font-family: 'Open Sans', sans-serif;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-decoration: none;
    font-style: normal;
    color: rgba(0,0,0,0.95);
  }

`

function App() {
  const currentUser = useStore(fromAuth.$.currentUser)

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <Page.Home />
        </Route>
        <Route path="/apply/audio/:id">
          <Page.ApplicationAudio />
        </Route>
        <Route path="/apply/video/:id">
          <Page.ApplicationVideo />
        </Route>
        <Route path="/apply/writing/:id">
          <Page.ApplicationWritten />
        </Route>
        <Route path="/favourites">
          <Page.Favourites />
        </Route>
        <Route path="/activities">
          <Page.Activities />
        </Route>
        <Route path="/signin">
          <Page.SignIn currentUser={currentUser} />
        </Route>
        <Route path="/jobopportunity/:id">
          <Page.JobOpportunity />
        </Route>
        <Route path="/how/to/apply/:id">
          <Page.HowToApply />
        </Route>
        <Route path="/profile/changepassword/:code">
          <Page.ProfileChangePassword />
        </Route>
        <Route path="/profile/create/expired/:code">
          <Page.ProfileCreateExpired />
        </Route>
        <Route path="/verifyemail/:code">
          <Page.ProfileVerify />
        </Route>

        <Route path="/profile/user-verified">
          <Page.ProfileCreateSuccess verified={true} />
        </Route>
        <Route path="/profile/create/success">
          <Page.ProfileCreateSuccess />
        </Route>
        <Route path="/profile/create">
          <Page.ProfileCreate />
        </Route>
        <Route path="/profile/edit">
          <Page.ProfileEdit />
        </Route>
        <Route path="/profile">
          <Page.Profile />
        </Route>
        <Route path="/status">
          <Page.MyActivityHistory />
        </Route>
        <Route path="*">
          <Page.NotFound />
        </Route>
      </Switch>
    </BrowserRouter>
  )
}

export default App
