import React from 'react'

const isNotSupported = !navigator.mediaDevices || MediaRecorder.notSupported

export default function BlockFormAudioActions() {
  const audio = React.useRef(null)
  const recorder = React.useRef(null)

  function onStart() {
    navigator.mediaDevices.getUserMedia({audio: true}).then(stream => {
      recorder.current = new MediaRecorder(stream)

      recorder.current.addEventListener('dataavailable', e => {
        audio.current.src = URL.createObjectURL(e.data)
      })

      recorder.current.start()
    })
  }

  function onStop() {
    recorder.current.stop()
    recorder.current.stream.getTracks().forEach(i => i.stop())
  }

  return (
    <div>
      {isNotSupported && `Not supported`}
      <button onClick={onStart}>start</button>
      <button onClick={onStop}>stop</button>
      <audio ref={audio} controls />
    </div>
  )
}
